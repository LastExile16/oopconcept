Language.instance({
  'cancel': 'キャンセル',
  'canvas-empty': 'キャンバスが空です。',
  'center-link-pos': '接続されたノード間の中心位置',
  'concept-label': 'コンセプトラベル',
  'concept-should-less': 'コンセプトラベルは6語未満で,空にすることはできません。',
  'confirm-clear-canvas-impact': 'キャンバスをクリアしますか？<br>保存されていないデータはすべて失われます。<br>コンセプトマッピング情報もリセットされます。<br>続行しますか？',
  'delete': '消去',
  'delete-all-selected': '選択したすべてを消去',
  'delete-selected-node:': '選択したノードを削除: "',
  'delete-selected-nodes:': '選択したノードを削除: "',
  'do-you-want-to-continue': '続けたいですか？',
  'duplicate': '副本',
  'edit': '編集',
  'edit-concept': 'コンセプトの編集',
  'edit-link': 'リンクの編集',
  'link-label': 'リンクラベル',
  'link-should-less': 'リンクラベルは6語未満で,空にすることはできません。',
  'no': 'いいえ',
  'new-concept': '新しいコンセプト',
  'new-link': '新しいリンク',
  'ok': '了解',
  'switch-arrow-direction': '矢印の方向を切り替える',
  'unable-to-layout': 'マップをレイアウトできません',
  'unable-to-save-empty-map': '空のマップを保存できません。',
  'yes': 'はい',
});
