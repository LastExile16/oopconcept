class Notification {

  /* Requires bootstrap-notify */

  constructor() {
    this._defaults = {
      template: '<div data-notify="container" class="col-xs-11 col-sm-4 alert alert-{0}" role="alert" style="text-align:center">' +
        '<button type="button" aria-hidden="true" class="close" data-notify="dismiss" style="margin:8px 3px"><i class="far fa-times-circle text-{0}"></i></button>' +
        '<span data-notify="message" style="margin-right: 1.5em; display: inline-block;">{2}</span>' +
        '</div>',
      type: 'info',
      animate: {
        enter: 'animated fadeInRight faster',
        exit: 'animated fadeOutRight faster'
      },
      placement: {
        from: 'bottom',
        align: 'right'
      },
      z_index: 99999,
      title: '',
      delay: 2000
    }
  }

  notify(text, options) {
    if (text == undefined) text = "undefined";
    let settings = Object.assign({}, this._defaults, options);
    let notification = $.notify(text, settings);
    return notification;
  }

}