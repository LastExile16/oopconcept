Language.instance({
  'cancel': 'キャンセル',
  'close': '終了',
  'delete': '消去',
  'edit': '編集',
  'end': '終了',
  'finish': '終了',
  'no': 'いいえ',
  'ok': '了解',
  'yes': 'はい',
  'loading': '読み込み中...'
});