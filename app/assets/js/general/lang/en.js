Language.instance({
  'cancel': 'Cancel',
  'close': 'Close',
  'delete': 'Delete',
  'edit': 'Edit',
  'end': 'End',
  'finish': 'Finish',
  'no': 'No',
  'ok': 'OK',
  'yes': 'Yes',
  'loading': 'Loading...'
})