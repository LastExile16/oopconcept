class Ajax {
  constructor(gui, options = {}) {
    let def = {
      method: 'get',
      delay: 2000,
      useDefaultRejection: true,
      holdError: false,
      timeout: 30000
    }
    this.gui = gui;
    this.settings = Object.assign(def, options);
  }

  static ins(gui = null, options = {}) {
    return new Ajax(gui, options);
  }

  onReject(error) {
    let ajax = this;
    if(ajax.gui) ajax.gui.notify(error, {
      type: 'danger',
      delay: ajax.settings.delay
    });
  }

  holdError() {
    this.settings.delay = 0;
    return this;
  }

  hold() {
    this.holdError()
  }

  post(url, data, options) {
    this.settings.method = 'post'
    return this.send(url, data, options)
  }

  get(url, data, options) {
    this.settings.method = 'get'
    return this.send(url, data, options)
  }

  send(url, data, options) {

    this.settings = Object.assign(this.settings, options);

    if(!arguments.length) return;
    let u = undefined;
    let d = undefined;
    if(typeof arguments[0] == 'object') {
      u = arguments[0].url;
      d = arguments[0].data ? arguments[0].data : undefined
    } else u = url;
    d = data ? data : d;

    let ajax = this;
    this.promise = new Promise(function (resolve, reject) {
      $.ajax({
        url: u,
        method: ajax.settings.method,
        data: d,
        timeout: ajax.settings.timeout
      }).done(function (response) { 
        if (!response.status) reject(response.error ? response.error : response)
        else resolve(response.result);
        return;
      }).fail(function (response) { console.log(response); console.log(response.statusText);
        if (!response.status) reject(response.error ? response.error : response.responseText);
        else reject(response.responseText);
      })
    });
    return ajax.settings.holdError ? this.holdError() : this;
  }
  then(onResolve, onReject) {
    if(this.settings.useDefaultRejection)
      this.promise.then(onResolve, onReject).catch(this.onReject.bind(this));
    else this.promise.then(onResolve, onReject)
    return this;
  }
  catch(onReject) {
    this.promise.catch(onReject);
    return this;
  }
}