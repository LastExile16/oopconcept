if (!Date.now) {
  Date.now = function () {
    return new Date().getTime();
  }
}

class Logger {

  constructor(uid, seq, phpsessid) {
    if (!navigator.sendBeacon) {
      console.error('Logging feature unavailable. Beacon function not supported.');
      this.enabled = false;
      return;
    }
    this.enabled = true;
    
    this.uid = uid;
    this.seq = seq ? seq : 0;
    this.phpsessid = phpsessid;

    let logger = this;
    window.addEventListener("unload", function() {
      logger.log('unload-browser', {uid: uid, phpsessid: phpsessid});
    })
  }

  setSeq(seq) {
    this.seq = seq;
  }

  setSessId(sessId) {
    this.phpsessid = sessId;
  }

  setUid(uid) {
    this.uid = uid;
  }

  setEnabled(enabled = true) {
    if (!navigator.sendBeacon) {
      console.error('Logging feature unavailable. Beacon function not supported.');
      this.enabled = false;
      return;
    }
    this.enabled = enabled;
  }

  setMid(mid) {
    this.mid = mid;
  }

  setGmid(gmid) {
    this.gmid = gmid;
  }

  setRid(rid) {
    this.rid = rid;
  }

  setKid(kid) {
    this.kid = kid;
  }

  log(action, data) {

    if (!this.enabled) return;

    this.seq++;

    let fData = new FormData();
    fData.append('uid', this.uid);
    fData.append('seq', this.seq);
    fData.append('action', action);
    fData.append('time_client', Date.now() / 1000 | 0); // the number of milliseconds elapsed since January 1, 1970 00:00:00 UTC.
    fData.append('data', JSON.stringify(data));
    fData.append('phpsessid', this.phpsessid);    
    
    if(this.mid) fData.append('mid', this.mid);
    if(this.gmid) fData.append('gmid', this.gmid);
    if(this.rid) fData.append('rid', this.rid);
    if(this.kid) fData.append('kid', this.kid);
    
    for(var pair of fData.entries()) {
      console.log(pair[0]+ ', '+ pair[1]); 
    }

    let url = baseUrl + "logApi/log";
    let status = navigator.sendBeacon(url, fData);

    console.log(action, this.uid, this.seq, status);

    return status;
  }

}