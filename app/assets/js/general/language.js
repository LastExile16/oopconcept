class Language {

  constructor() {
    this.text = null; // for formatting purposes
  }

  static instance(lang) { 
    if (typeof Language.inst == 'undefined') Language.inst = new Language();
    if (typeof lang != 'undefined') return Language.inst.load(lang);
    return Language.inst;
  }

  load(lang) { 
    if (typeof Language.lang == 'undefined') Language.lang = lang;
    else Language.lang = Object.assign(Language.lang, lang);
    return this;
  }

  get(key) {
    if (!Language.lang || typeof Language.lang[key] == 'undefined') {
      return key;
    }
    this.text = Language.lang[key];
    let args = (Array.from(arguments));
    args.shift();
    return this.f(...args);
  }

  f() {
    var i = arguments.length;
    while (i--)
      this.text = this.text.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    return this.text;
  }
}

Language.inst;
Language.lang; // is object key-translations container.