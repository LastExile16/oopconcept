class EventListener {

  constructor(logger, canvas) {
    this.logger = logger;
    this.canvas = canvas;
  }

  getLogger() {
    return this.logger;
  }

  static addMapStateData(cy, data) {
    if (!cy) return data;
    let conceptNodes = cy.nodes('[type="concept"]').jsons();
    let concepts = [];
    conceptNodes.forEach(c => {
      concepts.push({
        id: c.data.id,
        name: c.data.name,
        position: c.position
      })
    });
    let links = [];
    let edges = [];
    let edgeEs = cy.edges().jsons();
    edgeEs.forEach(e => {
      edges.push({
        s: e.data.source,
        t: e.data.target,
        ty: e.data.type
      })
    })
    let propositions = [];
    let noPropositions = [];
    let partPropositions = [];
    let linkNodes = cy.nodes('[type="link"]');
    linkNodes.toArray().forEach(l => {
      let edges = l.connectedEdges();
      let link = l.json();
      links.push({
        id: link.data.id,
        name: link.data.name,
        position: link.position
      });
      let proposition = {
        source: null,
        link: {
          id: link.data.id,
          name: link.data.name
        },
        target: null,
      }
      edges.toArray().forEach(e => {
        let tn = cy.nodes('[id="' + e.data('target') + '"]').json();
        switch (e.data('type')) {
          case 'left':
            proposition.source = {
              id: tn.data.id,
              name: tn.data.name
            }
            break;
          case 'right':
            proposition.target = {
              id: tn.data.id,
              name: tn.data.name
            }
            break;
        }
      });
      if (proposition.source != null && proposition.target != null) {
        propositions.push(proposition);
      } else if (proposition.source == null && proposition.target == null) {
        noPropositions.push(proposition);
      } else if (proposition.source == null || proposition.target == null) {
        partPropositions.push(proposition);
      }
    });
    data.cmapState = {
      concepts: concepts,
      links: links,
      edges: edges,
      propositions: propositions,
      partPropositions: partPropositions,
      noPropositions: noPropositions,
      nc: concepts.length,
      nl: links.length,
      ne: edges.length,
      np: propositions.length,
      npp: partPropositions.length,
      nnp: noPropositions.length,
    };

    if (data.lmid) {
      data.feedbackState = {
        lmid: data.lmid
      }
      delete data.lmid;
    }
    return data;
  }

  onCanvasEvent(action, data) { // console.log(action, data);
    switch (action) {
      case 'drag-concept':
      case 'drag-link':
      case 'drag-node-group':
        return;
      case 'connect-left':
      case 'connect-right':
      case 'change-connect-left':
      case 'change-connect-right':
      case 'disconnect-left':
      case 'disconnect-right':
      case 'rename-link':
      case 'rename-concept':
      case 'delete-link':
      case 'delete-concept':
      case 'duplicate-link':
      case 'duplicate-concept':
      case 'create-link':
      case 'create-concept':
      case 'switch-direction':
        if (!this.canvas) break;
        data = EventListener.addMapStateData(this.canvas.getCy(), data);
        break;
    }
    if (this.logger != null) this.logger.log(action, data);
  }

  onToolbarEvent(action, data) {
    if (this.logger != null) this.logger.log(action, data);
  }

  onActionEvent(action, data) {
    if (this.logger != null) this.logger.log(action, data);
  }

  onAppEvent(action, data) {
    switch(action) {
      case 'get-feedback':
        if (!this.canvas) break;
        data = EventListener.addMapStateData(this.canvas.getCy(), data);
        break;
    }
    if (this.logger != null) this.logger.log(action, data);
  }

  onCollabEvent(action, data) {
    if (this.logger != null) this.logger.log(action, data);
  }

}