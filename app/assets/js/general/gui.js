class GUI {
  constructor() {
    this.notification = new Notification();
    this.l = Language.instance()
  }

  setModalWidth(settings) {
    let w = '500px';
    if (settings['width']) {
      w = settings['width'];
      switch (w) {
        case 'wider':
          w = '65%';
          break;
        case 'wide':
          w = '80%';
          break;
        case 'xwide':
          w = '95%';
          break;
        case 'narrow':
          w = '300px';
          break;
        case 'normal':
          w = '500px';
          break;
      }
    }
    if (settings['selector']) {
      $(settings['selector'] + " .modal-dialog").css("max-width", w);
    } else $(".modal-dialog").css("max-width", w);
  }

  dialog(text, options) {
    let gui = this;
    let dialog = $('#modal-dialog');
    let defaults = {
      'text-positive': gui.l ? gui.l.get('yes') : 'Yes',
      'text-negative': gui.l ? gui.l.get('no') : 'No',
      'text': 'Do you want to continue?',
      'positive-callback': undefined,
      'negative-callback': undefined,
      'shown-callback': undefined,
      'hidden-callback': undefined,
      'width': 'normal',
      'selector': '#modal-dialog',
      'text-positive': gui.l ? gui.l.get('ok') : 'OK',
      'type': 'dialog'
    }

    let message = text || defaults['text'];
    let settings = Object.assign({}, defaults, options);
    this.setModalWidth(settings);

    if (settings.type == 'dialog') {
      dialog.find('.btn-negative').hide();
    } else dialog.find('.btn-negative').show();

    dialog.find('.btn-positive').html(settings['text-positive']);
    dialog.find('.btn-negative').html(settings['text-negative']);
    dialog.find('#modal-dialog-content').html(message);

    if (settings['negative-callback'] == undefined) {
      dialog.find('.btn-negative')
        .off('click')
        .on('click', function () {
          dialog.modal('hide');
        }); //.hide();
    } else dialog.find('.btn-negative')
      .off('click')
      .on('click', settings['negative-callback'])
      .show();
    if (settings['positive-callback'] != undefined) {
      dialog.find('.btn-positive')
        .off('click')
        .on('click', settings['positive-callback']);
    } else dialog.find('.btn-positive')
      .off('click')
      .on('click', function () {
        dialog.modal('hide');
      });

    if (settings['shown-callback'] != undefined) {
      dialog.on('shown.bs.modal', settings['shown-callback']);
    }
    if (settings['hidden-callback'] != undefined) {
      dialog.on('hidden.bs.modal', settings['hidden-callback']);
    }

    return dialog.modal(settings);
  }

  confirm(text, options) {
    let gui = this;
    let settings = Object.assign({}, options) // console.log(settings)
    let dialog = this.dialog(text, Object.assign({
      type: 'confirm',
      'text-positive': gui.l ? gui.l.get('yes') : 'Yes'
    }, options))
    if (typeof settings.callback == 'function') settings.callback();
    return dialog;
  }

  modal(selector, options) {
    let defaults = {
      width: 'normal'
    }
    let settings = Object.assign({}, defaults, options) // console.log(settings)
    this.setModalWidth({
      selector: selector,
      width: settings.width
    })
    $(selector).data('bs.modal', null);
    $(selector).modal(settings);
    if (typeof settings.callback == 'function') settings.callback();
  }

  notify(message, options) {
    let settings = Object.assign({}, options) // console.log(settings)
    this.notification.notify(message, options);
    if (typeof settings.callback == 'function') settings.callback();
  }

  showLoading(show = true, options) {
    let gui = this;
    if (!show) return $('#loading-modal').modal('hide');

    let defaults = {
      text: '<em>' + gui.l.get('loading') + '</em>',
      width: 'narrow'
    }
    let settings = Object.assign({}, defaults, options);
    this.setModalWidth({
      width: settings['width'],
      selector: '.modal-dialog-loading'
    });
    $('#modal-loading-text').html(settings['text']);
    return $('#loading-modal').modal({
      backdrop: "static", //remove ability to close modal with click
      keyboard: false, //remove option to close with keyboard
      show: true //Display loader!
    });
  }

  loading(selector, options) {
    let settings = Object.assign({
      text: '',
      class: '',
      style: 'text-align: center;'
    }, options);
    $(selector).html(`<div class="loading${settings.class ? ' ' + settings.class : ''}" style="${settings.style}"><img src="${assetsUrl}images/ellipsis.svg">${settings.text ? '<br><small>' + settings.text + '</small></div>' : ''}`);
    return $(selector + ' .loading');
  }

  /* Helper */

  nl2br(str) {
    if (typeof str === 'undefined' || str === null) return '';
    var breakTag = '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
  }

  em2px(em = 1) {
    return em * parseFloat(getComputedStyle($('body').get(0)).fontSize);
  }


}