class AdminApp {

  // This class act as a BRIDGE between clients and socket.io server
  // Responsible in communication among clients
  // This class also features a simple single-room chatting app 

  constructor(options) {

    // Default settings
    let defaultSettings = {}

    // Override default settings by options
    this.settings = Object.assign({}, defaultSettings, options);
    this.gui = new GUI();
    this.ajax = Ajax.ins(this.gui);

    // Holder for socket event listeners
    this.eventListeners = [];

    this.init();
    this.handleEvent();

    $('#bt-refresh-material-cmm').click();
    $('#bt-refresh-material').click();
    $('#bt-refresh-group').click();
    $('#bt-refresh-role').click();
    $('#bt-refresh-user').click();

  }

  init() {
    $('#app-menu').removeClass('d-none').addClass('d-inline-block');
  }

  handleEvent() {
    let app = this;
    this.handleCMMMaterialEvent(app);
    this.handleMaterialEvent(app);
    this.handleGroupEvent(app);
    this.handleRoleEvent(app);
    this.handleUserEvent(app);
  }

  handleCMMMaterialEvent(app) {
    $('#bt-refresh-material-cmm').on('click', function () {
      // console.log('click')
      app.ajax.get(
        baseUrl + 'adminApi/getCMMMaterials'
      ).then(function (materials) {
        // console.log(materials)
        let materialList = '';
        materials.forEach((material) => {
          // console.log(material);
          let fid = material.fid ? '<br><span class="badge badge-warning">' + material.fid + '</span>' : '';
          materialList += '<div class="item" data-mid="' + material.mid + '" data-fid="' + material.fid + '" data-name="' + material.name + '">';
          materialList += '<div class="align-items-center list d-flex justify-content-between">';
          materialList += '<div class="item-row">';
          materialList += '<span class="badge badge-info">' + material.mid + '</span> '
          materialList += material.name + fid;
          materialList += '</div>';
          materialList += '<div class="text-right">';
          materialList += '<div class="btn-group btn-sm">';
          if (material.state == 'enabled')
            materialList += '<button class="bt-enable btn btn-sm btn-outline-success"><i class="fas fa-check-circle"></i></button>';
          else if (material.state == 'disabled')
            materialList += '<button class="bt-enable btn btn-sm btn-outline-danger"><i class="fas fa-times-circle"></i></button>';
          materialList += '<button class="bt-edit btn btn-sm btn-outline-warning"><i class="fas fa-pencil-alt"></i></button>';
          materialList += '<button class="bt-delete btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>';
          materialList += '</div></div>';
          materialList += '</div>';
          materialList += '</div>';
        });
        $('#material-cmm-list').html(materialList);
        $('.input-keyword-material-cmm').trigger('keyup');
      });
    });
    $('#bt-filter-material-cmm').on('click', function () {
      let row = $(this).closest('.row').siblings('.filter').slideToggle({
        duration: 100,
        complete: function () {
          $('.input-keyword-material-cmm').focus();
        }
      });
    });
    $('.input-keyword-material-cmm').on('keyup', function (e) {
      let materials = $('#material-cmm-list').children();
      let keyword = $(this).val().trim();
      materials.each((index, material) => {

        if ($(material).data('name').toString().match(new RegExp(keyword, 'gi')))
          $(material).show();
        else $(material).hide();
      })
    })
    $('#material-cmm-list').on('click', '.bt-delete', function () {
      let row = $(this).closest('.item');
      let name = row.data('name');
      let mid = row.data('mid');
      let confirmDialog = app.gui.confirm('Delete this material: ' + name + ' ?', {
        'positive-callback': function () {
          app.ajax.post(
            baseUrl + 'adminApi/deleteMaterial', {
              mid: mid
            }
          ).then(function (data) {
            if (data) {
              row.slideUp();
            }
            confirmDialog.modal('hide');
          })
        }
      });
    });
    $('#material-cmm-list').on('click', '.bt-enable', function () {
      let command = $(this).hasClass('btn-outline-success') ? 'disableCMMMaterial' : 'enableCMMMaterial';
      let row = $(this).closest('.item');
      let mid = row.data('mid');
      let button = $(this);
      let icon = $(this).find('> i');
      // let confirmDialog = app.gui.confirm('Delete this material: ' + name + ' ?', {
      //   'positive-callback': function () {
      app.ajax.post(
        baseUrl + 'adminApi/' + command, {
          mid: mid
        }
      ).then(function (data) {
        button.toggleClass('btn-outline-success').toggleClass('btn-outline-danger');
        icon.toggleClass('fa-check-circle').toggleClass('fa-times-circle');
      });
    });
    $('#material-cmm-list').on('click', '.bt-edit', function () {
      let row = $(this).closest('.item');
      let mid = row.data('mid');
      let name = row.data('name');
      let fid = row.data('fid');
      app.gui.modal('#modal-edit-material-cmm', {
        width: '600px',
        backdrop: 'static',
        keyboard: false
      })

      let modal = $('#modal-edit-material-cmm');
      modal.find('.bt-save-content').attr('data-mid', null)
      modal.find('.bt-save-nlp').attr('data-mid', null)

      modal.find('.input-material-cmm-content').val('');
      modal.find('.input-material-cmm-nlp').val('');

      app.ajax.get(baseUrl + 'adminApi/getCMMMaterial/' + mid).then(function (material) {
        // console.log(material);
        modal.find('.material-mid').text(material.mid);
        modal.find('.material-name').text(material.name);
        modal.find('.input-material-cmm-content').val(material.content);
        modal.find('.input-material-cmm-nlp').val(material.nlp);
        modal.find('.bt-save-content').attr('data-mid', mid)
        modal.find('.bt-save-nlp').attr('data-mid', mid)
      })

    });

    $('#modal-edit-material-cmm').on('click', '.bt-save-content', function () {
      let modal = $('#modal-edit-material-cmm');
      let content = modal.find('.input-material-cmm-content').val().trim();
      let mid = $(this).attr('data-mid');
      // console.log(mid, content);

      var updateContent = function (callback) {
        app.ajax.post(baseUrl + 'adminApi/insertUpdateCMMMaterialContent', {
          mid: mid,
          content: content
        }).then(function (result) {
          // console.log(result);
          app.gui.notify('Contents updated.', {
            type: 'success'
          });
          if (typeof callback == 'function') callback();
        })
      }

      if (content.length == 0) {
        let clear = app.gui.confirm('Clear contents?', {
          'positive-callback': function () {
            updateContent(function () {
              clear.modal('hide');
            });
          }
        })
      } else updateContent();
    });

    $('#modal-edit-material-cmm').on('click', '.bt-save-nlp', function () {
      let modal = $('#modal-edit-material-cmm');
      let nlp = modal.find('.input-material-cmm-nlp').val().trim();
      let mid = $(this).attr('data-mid');
      // console.log(mid, content);

      var updateNLP = function (callback) {
        app.ajax.post(baseUrl + 'adminApi/insertUpdateCMMMaterialNLP', {
          mid: mid,
          nlp: nlp
        }).then(function (result) {
          // console.log(result);
          app.gui.notify('NLP data updated.', {
            type: 'success'
          });
          if (typeof callback == 'function') callback();
        })
      }

      if (nlp.length == 0) {
        let clear = app.gui.confirm('Clear NLP data?', {
          'positive-callback': function () {
            updateNLP(function () {
              clear.modal('hide');
            });
          }
        })
      } else updateNLP();
    });

    $('#modal-edit-material-cmm').on('click', '.bt-process-nlp', function () {
      let modal = $('#modal-edit-material-cmm');
      let content = modal.find('.input-material-cmm-content').val().trim();

      let annotators = {
        "annotators": "tokenize,ssplit,pos,lemma,ner,parse,coref,relation,openie",
        "coref.algorithm": "neural",
        "outputFormat": "json",
        "timeout": 600000
      };

      app.ajax.post(baseUrl + 'adminApi/nlpProxy', {
        annotators: JSON.stringify(annotators),
        text: content
      }, {
        timeout: 600000
      }).then(function (nlp) {
        let modal = $('#modal-edit-material-cmm');
        modal.find('.input-material-cmm-nlp').val(nlp);
      })
    })

    // let updateMaterial = function (mid, fid, name, content) {
    //   app.ajax.post(
    //     baseUrl + 'adminApi/updateMaterial', {
    //       material: name,
    //       fid: fid,
    //       content: content,
    //       mid: mid
    //     }
    //   ).then(function (data) {
    //     app.gui.notify('Material updated.', {
    //       type: 'success'
    //     });
    //     $('#modal-edit-material-cmm').modal('hide');
    //     $('#bt-refresh-material-cmm').click();
    //   });
    // };
    // $('#modal-edit-material-cmm').on('click', '.bt-ok', function () {
    //   let name = $('#modal-edit-material-cmm .input-material-cmm-name').val();
    //   let content = '';
    //   let fid = $('#modal-edit-material-cmm .input-material-cmm-fid').val();
    //   let mid = $('#modal-edit-material-cmm .input-material-cmm-name').data('mid');
    //   if (name.trim().length > 0) {
    //     updateMaterial(mid, fid.trim().toUpperCase(), name.trim(), content.trim());
    //   }
    // });

  }

  handleMaterialEvent(app) {
    $('#bt-refresh-material').on('click', function () {
      app.ajax.get(
        baseUrl + 'adminApi/getMaterials'
      ).then(function (materials) {
        let materialList = '';
        materials.forEach((material) => {
          // console.log(material);
          let fid = material.fid ? '<br><span class="badge badge-warning">' + material.fid + '</span>' : '';
          materialList += '<div class="item" data-mid="' + material.mid + '" data-fid="' + material.fid + '" data-name="' + material.name + '">';
          materialList += '<div class="align-items-center list d-flex justify-content-between">';
          materialList += '<div class="item-row">';
          materialList += '<span class="badge badge-info">' + material.mid + '</span> '
          materialList += '<span class="material-name">' + material.name + '</span>' + fid;
          materialList += '</div>';
          materialList += '<div class="text-right">';
          materialList += '<div class="btn-group btn-sm">';
          materialList += '<button class="bt-goalmap btn btn-sm btn-outline-secondary"><i class="fab fa-hubspot"></i></button>';
          materialList += '</div>';
          materialList += '<div class="btn-group btn-sm">';
          if (material.enabled == '1')
            materialList += '<button class="bt-enable btn btn-sm btn-outline-success"><i class="fas fa-check-circle"></i></button>';
          else if (material.enabled == '0')
            materialList += '<button class="bt-enable btn btn-sm btn-outline-danger"><i class="fas fa-times-circle"></i></button>';
          materialList += '<button class="bt-edit btn btn-sm btn-outline-warning"><i class="fas fa-pencil-alt"></i></button>';
          materialList += '<button class="bt-delete btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>';
          materialList += '</div></div>';
          materialList += '</div>';
          materialList += '</div>';
        });
        $('#material-list').html(materialList);
        $('.input-keyword-material').trigger('keyup');
      });
    });
    $('#bt-filter-material').on('click', function () {
      let row = $(this).closest('.row').siblings('.filter').slideToggle({
        duration: 100,
        complete: function () {
          $('.input-keyword-material').focus();
        }
      });
    });
    $('.input-keyword-material').on('keyup', function (e) {
      let materials = $('#material-list').children();
      let keyword = $(this).val().trim();
      materials.each((index, material) => {

        if ($(material).data('name').toString().match(new RegExp(keyword, 'gi')))
          $(material).show();
        else $(material).hide();
      })
    })
    $('#bt-create-material').on('click', function () {
      app.gui.modal('#modal-create-material', {
        width: '600px',
        backdrop: 'static',
        keyboard: false
      })
      $('#modal-create-material .input-material-name').val('').focus();
      $('#modal-create-material .input-material-fid').val('');
      $('#modal-create-material textarea').tinymce({
        height: 500,
        menubar: false,
        plugins: [
          // 'advlist autolink lists link image charmap print preview anchor',
          // 'searchreplace visualblocks code fullscreen',
          // 'insertdatetime media table paste code help wordcount'
        ],
        toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
      });
    });
    let createMaterial = function (name, fid, content) {
      app.ajax.post(
        baseUrl + 'adminApi/createMaterial', {
          name: name,
          fid: fid,
          content: content
        }
      ).then(function (data) {
        $('#modal-create-material').modal('hide');
        $('#bt-refresh-material').click();
      });
    };
    $('#modal-create-material').on('click', '.bt-ok', function () {
      let name = $('#modal-create-material .input-material-name').val();
      let fid = $('#modal-create-material .input-material-fid').val();
      let content = '';
      if (name.trim().length > 0) {
        createMaterial(name.trim(), fid.trim().toUpperCase(), content.trim());
      }
    });
    $('#material-list').on('click', '.bt-delete', function () {
      let row = $(this).closest('.item');
      let name = row.data('name');
      let mid = row.data('mid');
      let confirmDialog = app.gui.confirm('Delete this material: ' + name + ' ?', {
        'positive-callback': function () {
          app.ajax.post(
            baseUrl + 'adminApi/deleteMaterial', {
              mid: mid
            }
          ).then(function (data) {
            if (data) {
              row.slideUp();
            }
            confirmDialog.modal('hide');
          })
        }
      });
    });
    $('#material-list').on('click', '.bt-enable', function () {
      let command = $(this).hasClass('btn-outline-success') ? 'disableMaterial' : 'enableMaterial';
      let row = $(this).closest('.item');
      let mid = row.data('mid');
      let button = $(this);
      let icon = $(this).find('> i');
      // let confirmDialog = app.gui.confirm('Delete this material: ' + name + ' ?', {
      //   'positive-callback': function () {
      app.ajax.post(
        baseUrl + 'adminApi/' + command, {
          mid: mid
        }
      ).then(function (data) {
        button.toggleClass('btn-outline-success').toggleClass('btn-outline-danger');
        icon.toggleClass('fa-check-circle').toggleClass('fa-times-circle');
      });
    });
    $('#material-list').on('click', '.bt-edit', function () {
      let row = $(this).closest('.item');
      let mid = row.data('mid');
      let name = row.data('name');
      let fid = row.data('fid');

      app.gui.modal('#modal-edit-material', {
        width: '600px',
        backdrop: 'static',
        keyboard: false
      })
      // $('#modal-edit-material').modal('show');
      $('#modal-edit-material .input-material-name').val(name).focus();
      $('#modal-edit-material .input-material-name').data('mid', mid);
      $('#modal-edit-material .input-material-fid').val(fid);
    });
    let updateMaterial = function (mid, fid, name, content) {
      app.ajax.post(
        baseUrl + 'adminApi/updateMaterial', {
          material: name,
          fid: fid,
          content: content,
          mid: mid
        }
      ).then(function (data) {
        app.gui.notify('Material updated.', {
          type: 'success'
        });
        $('#modal-edit-material').modal('hide');
        $('#bt-refresh-material').click();
      });
    };
    $('#modal-edit-material').on('click', '.bt-ok', function () {
      let name = $('#modal-edit-material .input-material-name').val();
      let content = '';
      let fid = $('#modal-edit-material .input-material-fid').val();
      let mid = $('#modal-edit-material .input-material-name').data('mid');
      if (name.trim().length > 0) {
        updateMaterial(mid, fid.trim().toUpperCase(), name.trim(), content.trim());
      }
    });


    $('#material-list').on('click', '.bt-goalmap', function () {
      // let command = $(this).hasClass('btn-outline-success') ? 'disableMaterial' : 'enableMaterial';
      let row = $(this).closest('.item');
      let mid = row.data('mid');
      let button = $(this);
      let icon = $(this).find('> i');
      $('#modal-material-goalmap').attr('data-mid', mid);
      $('#modal-material-goalmap').attr('data-name', row.find('.material-name').text());
      $('#modal-material-goalmap .material-name').html(row.find('.material-name').text())
      $('#modal-material-goalmap .goalmap-list').html('');
      $('#modal-material-goalmap .kit-list').html('');
      app.gui.modal('#modal-material-goalmap', {
        width: '800px'
      });
      $('#modal-material-goalmap .bt-refresh').trigger('click');
    });

    $('#modal-material-goalmap').on('click', '.bt-refresh', function () {
      let mid = $('#modal-material-goalmap').attr('data-mid');
      $('#modal-material-goalmap .goalmap-list').html('<div class="font-italic text-center pt-2">Loading...</div>');
      app.ajax.get(
        baseUrl + 'adminApi/getGoalmaps/' + mid
      ).then(function (goalmaps) {
        // console.log(goalmaps)
        if (goalmaps.length) {
          let list = "";
          goalmaps.forEach(gm => {
            list += '<div class="goalmap-item-row list-pointer list-hover align-items-center list d-flex justify-content-between" data-gmid="' + gm.gmid + '" data-mid="' + gm.mid + '">';
            list += '<div class="pl-2">';
            list += '<span class="badge badge-info">' + gm.gmid + '</span> ' + '<span class="goalmap-name">' + gm.name + "</span>";
            list += '</div>';
            list += '<div class="text-right">';
            list += '<div class="btn-group btn-sm">';
            list += '<button class="bt-move btn btn-sm btn-warning"><i class="fas fa-random"></i></button>';
            list += '<button class="bt-edit btn btn-sm btn-outline-warning"><i class="fas fa-pencil-alt"></i></button>';
            list += '<button class="bt-delete btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>';
            list += '</div>';
            list += '</div>';
            list += '</div>';
          })
          $('#modal-material-goalmap .goalmap-list').html(list);
          $('#modal-material-goalmap .kit-list').html('');
        } else {
          $('#modal-material-goalmap .goalmap-list').html('<div class="font-italic text-center pt-2">No goalmaps available.</div>');
          $('#modal-material-goalmap .kit-list').html('');
        }
      });
    })

    $('#modal-material-goalmap').on('click', '.goalmap-item-row', function () {
      let gmid = $(this).data('gmid');
      app.ajax.get(baseUrl + 'adminApi/getKits/' + gmid).then(function (kits) {
        // console.log(kits)
        if (kits.length) {
          let list = '';
          kits.forEach(kit => {
            list += '<div class="kit-item-row item align-items-center list d-flex justify-content-between" data-gmid="' + kit.gmid + '" data-kid="' + kit.kid + '">';
            list += '<div class="pl-2">';
            list += '<span class="badge badge-info">' + kit.kid + '</span> '
            list += '<span class="kit-name">' + kit.name + "</span>";
            // list += '<span class="kit-lm badge badge-warning ml-2">' + kit.learnermaps_count + " LMs</span>";
            list += '</div>';
            list += '<div class="text-right">';
            list += '<div class="btn-group btn-sm">';
            if (kit.enabled == '1')
              list += '<button class="bt-enable btn btn-sm btn-outline-success"><i class="fas fa-check-circle"></i></button>';
            else if (kit.enabled == '0')
              list += '<button class="bt-enable btn btn-sm btn-outline-danger"><i class="fas fa-times-circle"></i></button>';
            list += '<button class="bt-edit btn btn-sm btn-outline-warning"><i class="fas fa-pencil-alt"></i></button>';
            list += '<button class="bt-delete btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>';
            list += '</div>';
            if (kit.learnermaps_count > 0)
              list += '<br><button class="bt-learnermap btn btn-sm btn-outline-primary"><i class="fab fa-hubspot mr-2"></i>' + kit.learnermaps_count + ' Learnermaps</button>';
            list += '</div>';
            list += '</div>';
          })
          list += '<div class="mt-2"><span class="font-italic">Kit with learnermaps cannot be deleted.<br>Delete learnermaps attached to the kit first.</span></div>';
          $('#modal-material-goalmap .kit-list').html(list);
        } else {
          $('#modal-material-goalmap .kit-list').html('<div class="font-italic text-center pt-2">No kits available.</div>');
        }
      })
    });

    $('#modal-material-goalmap .goalmap-list').on('click', '.bt-move', function (e) {
      e.stopImmediatePropagation();
      let gmid = $(this).parents('.goalmap-item-row').data('gmid');
      let mid = $(this).parents('.goalmap-item-row').data('mid');
      app.ajax.get(
        baseUrl + 'adminApi/getMaterials'
      ).then(function (materials) {
        // console.warn(materials)
        // console.error(mid)
        let materialList = '<select class="material-target form-control" data-mid="' + mid + '">';
        materials.forEach((material) => {
          let selected = (mid == material.mid ? 'selected="selected"' : '');
          materialList += '<option value="' + material.mid + '" ' + selected + '>' + material.name + '</option>'
        });
        materialList += '</select>';
        $('#modal-goalmap-move-material .material-list').html(materialList);
        $('#modal-goalmap-move-material').attr('data-mid', mid);
        $('#modal-goalmap-move-material').attr('data-gmid', gmid);
        app.gui.modal('#modal-goalmap-move-material', {
          width: '400px'
        });
      });
    });

    $('#modal-goalmap-move-material').on('click', '.bt-ok', function () {
      let omid = $('#modal-goalmap-move-material').attr('data-mid');
      let mid = $('#modal-goalmap-move-material .material-target').val();
      let gmid = $('#modal-goalmap-move-material').attr('data-gmid');
      console.warn(omid, mid, gmid);
      if (omid == mid) {
        app.gui.dialog('You cannot move goalmap to its own topic/material.')
        return;
      }
      let confirm = app.gui.confirm('Move goalmap to topic: ' + $('#modal-goalmap-move-material .material-target option:selected').text() + '?', {
        'positive-callback': function () {
          app.ajax.post(
            baseUrl + 'adminApi/moveGoalmapToMaterial', {
              gmid: gmid,
              mid: mid,
              omid: omid
            }
          ).then(function () {
            app.gui.notify('Goalmap moved.', {
              type: 'success'
            })
            confirm.modal('hide');
            $('#modal-goalmap-move-material').modal('hide');
            $('#modal-material-goalmap').modal('hide');
            $('#material-list .item[data-mid="' + omid + '"] .bt-goalmap').trigger('click');
          });
        }
      });
    });

    $('#modal-material-goalmap .goalmap-list').on('click', '.bt-edit', function (e) {
      e.stopImmediatePropagation();
      let gmid = $(this).parents('.goalmap-item-row').data('gmid');
      let mid = $(this).parents('.goalmap-item-row').data('mid');
      let name = $(this).parents('.goalmap-item-row').find('.goalmap-name').text();
      $('#modal-goalmap-rename').attr('data-gmid', gmid);
      $('#modal-goalmap-rename').attr('data-mid', mid);
      $('#modal-goalmap-rename .goalmap-name').val(name);
      app.gui.modal('#modal-goalmap-rename', {
        width: '400px',
        callback: function () {
          $('#modal-goalmap-rename .goalmap-name').focus();
        }
      });
    });

    $('#modal-goalmap-rename').on('click', '.bt-ok', function () {
      let gmid = $('#modal-goalmap-rename').attr('data-gmid');
      let mid = $('#modal-goalmap-rename').attr('data-mid');
      let name = $('#modal-goalmap-rename .goalmap-name').val().trim();
      console.warn(gmid, name);
      if (name.length == 0) {
        app.gui.dialog('Cannot save empty name.');
        return;
      }
      let confirm = app.gui.confirm('Rename goalmap to: ' + name + '?', {
        'positive-callback': function () {
          app.ajax.post(
            baseUrl + 'adminApi/renameGoalmap', {
              gmid: gmid,
              name: name
            }
          ).then(function () {
            app.gui.notify('Goalmap renamed.', {
              type: 'success'
            });
            confirm.modal('hide');
            $('#modal-goalmap-rename').modal('hide');
            $('#modal-material-goalmap').modal('hide');
            $('#material-list .item[data-mid="' + mid + '"] .bt-goalmap').trigger('click');
          });
        }
      });
    });

    $('#modal-material-goalmap .goalmap-list').on('click', '.bt-delete', function (e) {
      e.stopImmediatePropagation();
      let gmid = $(this).parents('.goalmap-item-row').data('gmid');
      let mid = $(this).parents('.goalmap-item-row').data('mid');
      let name = $(this).parents('.goalmap-item-row').find('.goalmap-name').text();

      let confirm = app.gui.confirm('Delete this goalmap: ' + name + '?', {
        'positive-callback': function () {
          app.ajax.post(
            baseUrl + 'adminApi/deleteGoalmap', {
              gmid: gmid
            }
          ).then(function () {
            app.gui.notify('Goalmap deleted.', {
              type: 'success'
            });
            confirm.modal('hide');
            $('#material-list .item[data-mid="' + mid + '"] .bt-goalmap').trigger('click');
          });
        }
      })
    });

    $('#modal-material-goalmap .kit-list').on('click', '.bt-enable', function () {
      let command = $(this).hasClass('btn-outline-success') ? 'disableKit' : 'enableKit';
      let row = $(this).closest('.item');
      let kid = row.data('kid');
      let button = $(this);
      let icon = $(this).find('> i');
      // let confirmDialog = app.gui.confirm('Delete this material: ' + name + ' ?', {
      //   'positive-callback': function () {
      app.ajax.post(
        baseUrl + 'adminApi/' + command, {
          kid: kid
        }
      ).then(function (data) {
        button.toggleClass('btn-outline-success').toggleClass('btn-outline-danger');
        icon.toggleClass('fa-check-circle').toggleClass('fa-times-circle');
      });
    });


    $('#modal-material-goalmap .kit-list').on('click', '.bt-edit', function (e) {
      e.stopImmediatePropagation();
      let kid = $(this).parents('.kit-item-row').data('kid');
      app.ajax.get(baseUrl + 'adminApi/getKit/' + kid).then(function (kitset) {
        let kit = kitset.kitmap; // console.log(kit);
        $('#modal-kit-edit').attr('data-gmid', kit.gmid);
        $('#modal-kit-edit').attr('data-kid', kit.kid);
        $('#modal-kit-edit .kit-name-input').val(kit.name);
        if (kit.layout == "preset") $('#kit-option-position-preset').prop('checked', true);
        else $('#kit-option-position-preset').prop('checked', false);       
        if (kit.layout == "random") $('#kit-option-position-random').prop('checked', true);
        else $('#kit-option-position-random').prop('checked', false);
        if (kit.directed == "1") $('#kit-option-directed').prop('checked', true);
        else $('#kit-option-directed').prop('checked', false);
        if (kit.directed == "0") $('#kit-option-undirected').prop('checked', true);
        else $('#kit-option-undirected').prop('checked', false);
        if (kit.enabled == "1") $('#kit-status-enabled').prop('checked', true);
        else $('#kit-status-enabled').prop('checked', false);
        app.gui.modal('#modal-kit-edit', {
          width: '400px',
          callback: function () {
            $('#modal-kit-edit .kit-name-input').focus();
          }
        });
      });
    });

    $('#modal-kit-edit').on('click', '.bt-ok', function () {
      let confirm = app.gui.confirm('Update Kit data?', {
        'positive-callback': function () {

          let kid = $('#modal-kit-edit').attr('data-kid');
          let name = $('#modal-kit-edit .kit-name-input').val().trim();
          let layout = $('#modal-kit-edit input[name="layout"]:checked').val();
          let directed = $('#modal-kit-edit input[name="directed"]:checked').val();
          let enabled = $('#modal-kit-edit #kit-status-enabled').is(':checked') ? 1 : 0;
          // console.log(kid, name, layout, directed, enabled)

          if (name.length == 0) {
            app.gui.dialog('Kit name cannot empty.', {
              type: 'warning'
            });
            return
          }

          app.ajax.post(baseUrl + 'adminApi/updateKit', {
            name: name,
            layout: layout,
            directed: directed,
            enabled: enabled,
            kid: kid
          }).then(function () {
            let gmid = $('#modal-kit-edit').attr('data-gmid');
            confirm.modal('hide');
            app.gui.notify('Kit updated.', {
              type: 'success'
            });
            $('#modal-kit-edit').modal('hide');
            $('#modal-material-goalmap .goalmap-list .goalmap-item-row[data-gmid="' + gmid + '"]').trigger('click')
          });
        }
      })
    })

    $('#modal-material-goalmap .kit-list').on('click', '.bt-delete', function (e) {
      e.stopImmediatePropagation();
      let row = $(this).parents('.kit-item-row');
      let name = $(this).parents('.kit-item-row').find('.kit-name').text();
      let kid = $(this).parents('.kit-item-row').data('kid');
      let confirm = app.gui.confirm('Delete Kit: "' + name + '" permanently?', {
        'positive-callback': function () {
          app.ajax.post(baseUrl + 'adminApi/deleteKit', {
            kid: kid
          }).then(function () {
            let gmid = $('#modal-kit-edit').attr('data-gmid');
            confirm.modal('hide');
            app.gui.notify('Kit deleted.', {
              type: 'success'
            });
            row.slideToggle('fast', function() {
              row.remove();
            })
            // $('#modal-material-goalmap .goalmap-list .goalmap-item-row[data-gmid="' + gmid + '"]').trigger('click');
          }, function (err) {
            confirm.modal('hide');
            app.gui.notify('Unable to delete kit.<br>It has learnermaps attached to it.', {
              type: 'danger',
              delay: 0
            });
            console.error(err);
          });
        }
      });
    });

    $('#modal-material-goalmap .kit-list').on('click', '.bt-learnermap', function () {
      // let command = $(this).hasClass('btn-outline-success') ? 'disableKit' : 'enableKit';
      let row = $(this).closest('.item');
      let kid = row.data('kid');
      let name = row.find('.kit-name').text();
      // let button = $(this);
      // let icon = $(this).find('> i');
      // let confirmDialog = app.gui.confirm('Delete this material: ' + name + ' ?', {
      //   'positive-callback': function () {
      app.ajax.get(
        baseUrl + 'adminApi/getLearnermaps/' + kid
      ).then(function (learnermaps) {
        // console.log(learnermaps);
        let list = "";
        learnermaps.forEach(lm => {
          list += '<div class="d-flex list learnermap-item learnermap-item-row align-items-center justify-content-between" data-lmid="' + lm.lmid + '">';
          list += '<div>';
          list += '<span class="badge badge-primary mr-1">' + lm.lmid + '</span>';
          list += '<span class="badge badge-info mr-2">' + lm.create_time + '</span>';
          list += '<span class="lm-name">' + lm.name + '</span>';
          list += '</div>';
          list += '<div class="text-right">';
          list += '<div class="btn-group btn-sm">';
          // if (lm.enabled == '1')
          //   list += '<button class="bt-enable btn btn-sm btn-outline-success"><i class="fas fa-check-circle"></i></button>';
          // else if (kit.enabled == '0')
          //   list += '<button class="bt-enable btn btn-sm btn-outline-danger"><i class="fas fa-times-circle"></i></button>';
          list += '<button class="bt-preview btn btn-sm btn-outline-info"><i class="fas fa-search"></i></button>';
          list += '<button class="bt-delete btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>';
          list += '</div>';
          // if (kit.learnermaps_count > 0)
          //   list += '<br><button class="bt-learnermap btn btn-sm btn-outline-primary"><i class="fab fa-hubspot mr-2"></i>' + kit.learnermaps_count + ' Learnermaps</button>';
          list += '</div>';
          list += '</div>';
        });

        $('#modal-learnermap .learnermap-list').html(list);
        $('#modal-learnermap .kit-name').html(name);
        app.gui.modal('#modal-learnermap', {width: '550px'});
      });
    });

    $('#modal-learnermap .learnermap-list').on('click', '.bt-delete', function(){
      let row = $(this).closest('.learnermap-item');
      let lmid = $(this).closest('.learnermap-item').data('lmid'); 
      let name = $(this).closest('.learnermap-item').find('.lm-name').text();
      
      let confirm = app.gui.confirm('Delete this learnermap by ' + name + '?', {
        'positive-callback': function() {
          app.ajax.post(baseUrl + 'adminApi/deleteLearnermap', {
            lmid: lmid
          }).then(function(){
            confirm.modal('hide');
            app.gui.notify('Learnermap deleted', {type: 'success'});
            row.slideUp('fast', function() {
              row.remove();
            });
          })
        }
      })
    })

    $('#modal-learnermap .learnermap-list').on('click', '.bt-preview', function(){
      let row = $(this).closest('.learnermap-item');
      let lmid = $(this).closest('.learnermap-item').data('lmid'); 
      let name = $(this).closest('.learnermap-item').find('.lm-name').text();
      
      app.gui.notify('Yet to be implemented', {type: 'warning'});
    })

    // Material-Qset Handler

    $('#material-list').on('click', '.bt-material-qset', function () {
      let mid = $(this).closest('.item').data('mid');
      let name = $(this).closest('.item').data('name');
      $('#modal-material-qset .material-name').html('<strong>' + name + '</strong>')
      app.ajax.get(
        baseUrl + 'adminApi/getQsetOfMaterial/' + mid
      ).then(function (qsets) {
        let inList = '';
        qsets.in.forEach((qset) => {
          inList += '<div class="row align-items-center list" data-mid="' + mid + '" data-qsid="' + qset.qsid + '" data-name="' + qset.name + '">';
          inList += '<div class="col-sm-6"><span class="badge badge-info">' + qset.type + '</span> ' + qset.name + '</div>';
          inList += '<div class="col-sm-6 text-right"><div class="btn-material btn-sm">';
          inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
          inList += '</div></div>';
          inList += '</div>';
        });
        $('#modal-material-qset .in-list').html(inList);
        let notInList = '';
        qsets.notin.forEach((qset) => {
          notInList += '<div class="row align-items-center list" data-mid="' + mid + '" data-qsid="' + qset.qsid + '" data-name="' + qset.name + '">';
          notInList += '<div class="col-sm-6"><span class="badge badge-info">' + qset.type + '</span> ' + qset.name + '</div>';
          notInList += '<div class="col-sm-6 text-right"><div class="btn-material btn-sm">';
          notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
          notInList += '</div></div>';
          notInList += '</div>';
        });
        $('#modal-material-qset .not-in-list').html(notInList);
        app.gui.modal('#modal-material-qset', {
          width: 'wider'
        });
      });
    })

    $('#modal-material-qset').on('click', '.bt-add', function () {
      let row = $(this).closest('.row');
      let mid = row.data('mid')
      let qsid = row.data('qsid')
      let name = row.data('name')
      app.ajax.get(
        baseUrl + '/adminApi/addQsetToMaterial/' + qsid + '/' + mid
      ).then(function (response) {
        let inList = '';
        inList += '<div class="row align-items-center list" data-mid="' + mid + '" data-qsid="' + qsid + '" data-name="' + name + '">';
        inList += '<div class="col-sm-6">' + name + '</div>';
        inList += '<div class="col-sm-6 text-right"><div class="btn-material btn-sm">';
        inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
        inList += '</div></div>';
        inList += '</div>';
        $('#modal-material-qset .in-list').prepend(inList);
        row.slideUp();
      })
    })

    $('#modal-material-qset').on('click', '.bt-remove', function () {
      let row = $(this).closest('.row');
      let mid = row.data('mid')
      let qsid = row.data('qsid')
      let name = row.data('name')
      app.ajax.get(
        baseUrl + '/adminApi/removeQsetFromMaterial/' + qsid + '/' + mid
      ).then(function (response) {
        let notInList = '';
        notInList += '<div class="row align-items-center list" data-mid="' + mid + '" data-qsid="' + qsid + '" data-name="' + name + '">';
        notInList += '<div class="col-sm-6">' + name + '</div>';
        notInList += '<div class="col-sm-6 text-right"><div class="btn-material btn-sm">';
        notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
        notInList += '</div></div>';
        notInList += '</div>';
        $('#modal-material-qset .not-in-list').prepend(notInList);
        row.slideUp();
      })
    })
  }

  handleGroupEvent(app) {
    $('#bt-refresh-group').on('click', function () {
      app.ajax.get(
        baseUrl + 'adminApi/getGroupsWithCount'
      ).then(function (groups) {
        let groupList = '';
        groups.forEach((group) => {
          let fid = group.fid ? ' <span class="badge badge-warning">' + group.fid + '</span>' : '';
          groupList += '<div class="item" title="' + group.name + '" data-gid="' + group.gid + '" data-name="' + group.name + '">'
          groupList += '<div class="align-items-center list d-flex justify-content-between">';
          groupList += '<div class="item-row" style="text-overflow: ellipsis; overflow:auto"><span class="badge badge-info">' + group.gid + '</span> ' + group.name + fid + '</div>';
          groupList += '<div class="text-right"><div class="btn-group btn-group-sm mt-1 mb-1 mr-1">';
          groupList += '<button class="bt-group-user btn btn-outline-secondary"><i class="fas fa-user-plus"></i> ' + group.cusers + '</button>';
          groupList += '<button class="bt-group-material btn btn-outline-secondary"><i class="far fa-file-alt"></i> ' + group.cmaterials + '</button>';
          groupList += '</div><div class="btn-group btn-group-sm mt-1 mb-1 mr-1">';
          groupList += '<button class="bt-edit btn btn-outline-warning"><i class="fas fa-pencil-alt"></i></button>';
          groupList += '<button class="bt-delete btn btn-outline-danger"><i class="fas fa-trash"></i></button>';
          groupList += '</div></div>';
          groupList += '</div>';
          groupList += '</div>';
        });
        $('#group-list').html(groupList);
        $('.input-keyword-group').trigger('keyup');
      });
    });
    $('#bt-filter-group').on('click', function () {
      let row = $(this).closest('.row').siblings('.filter').slideToggle({
        duration: 100,
        complete: function () {
          $('.input-keyword-group').focus();
        }
      });
    });
    $('.input-keyword-group').on('keyup', function (e) {
      let groups = $('#group-list').children();
      let keyword = $(this).val().trim();
      groups.each((index, group) => {
        if ($(group).data('name').toString().match(new RegExp(keyword, 'gi')))
          $(group).show();
        else $(group).hide();
      })
    })
    $('#bt-create-group').on('click', function () {
      $('#modal-create-group').modal('show');
      $('#modal-create-group .input-group-name').val('').focus();
      $('#modal-create-group .input-group-fid').val('');
      $('#modal-create-group .input-group-type[value="regular"]').prop('checked', true);
      $('#modal-create-group .input-group-grade').val('');
      $('#modal-create-group .input-group-class').val('');
      $('#modal-create-group .input-group-note').val('');
    });
    let createGroup = function (name, fid, type, grade, clas, note) {

      app.ajax.post(
        baseUrl + 'adminApi/createGroup', {
          name: name,
          fid: fid,
          type: type,
          grade: grade,
          class: clas,
          note: note
        }
      ).then(function (data) {
        $('#modal-create-group').modal('hide');
        $('#bt-refresh-group').click();
      });
    };
    $('#modal-create-group').on('click', '.bt-ok', function () {
      let name = $('#modal-create-group .input-group-name').val();
      let fid = $('#modal-create-group .input-group-fid').val();
      let type = $('#modal-create-group .input-group-type:checked').val();
      let grade = $('#modal-create-group .input-group-grade').val();
      let clas = $('#modal-create-group .input-group-class').val();
      let note = $('#modal-create-group .input-group-note').val();
      if (type == undefined) {
        app.gui.dialog('Please select group type.', {
          width: '450px'
        })
        return;
      }
      if (name.trim().length > 0) {
        createGroup(name.trim(), fid.trim().toUpperCase(), type, grade.trim(), clas.trim(), note.trim());
      }
    });
    $('#group-list').on('click', '.bt-delete', function () {
      let row = $(this).closest('.item');
      let name = row.data('name');
      let gid = row.data('gid');

      let confirmDialog = app.gui.confirm('Delete this group: ' + name + ' ?', {
        'positive-callback': function () {
          app.ajax.post(
            baseUrl + 'adminApi/deleteGroup', {
              gid: gid
            }
          ).then(function (data) {
            confirmDialog.modal('hide');
            row.slideUp();
          }).finally(function () {
            confirmDialog.modal('hide');
          });
        }
      });
    });
    $('#group-list').on('click', '.bt-edit', function () {
      let row = $(this).closest('.item');
      let gid = row.data('gid');
      app.ajax.get(
        baseUrl + 'adminApi/getGroup/' + gid
      ).then(function (group) {
        if (group) {
          let name = group != null ? group.name : '';
          let fid = group != null ? group.fid : '';
          let type = group != null ? group.type : '';
          let grade = group != null ? group.grade : '';
          let clas = group != null ? group.class : '';
          let note = group != null ? group.note : '';

          $('#modal-edit-group .input-group-name').data('gid', gid);
          $('#modal-edit-group .input-group-name').val(name).focus();
          $('#modal-edit-group .input-group-fid').val(fid);
          $('#modal-edit-group .input-group-type[value="' + type + '"]').prop('checked', true);
          $('#modal-edit-group .input-group-grade').val(grade);
          $('#modal-edit-group .input-group-class').val(clas);
          $('#modal-edit-group .input-group-note').val(note);
          $('#modal-edit-group').modal('show');
        }
      });
    });
    let updateGroup = function (gid, name, fid, type, grade, clas, note) {
      app.ajax.post(
        baseUrl + 'adminApi/updateGroup', {
          name: name,
          fid: fid,
          type: type,
          grade: grade,
          class: clas,
          note: note,
          gid: gid
        }
      ).then(function (data) {
        app.gui.notify('Group updated.', {
          type: 'success'
        });
        $('#modal-edit-group').modal('hide');
        $('#bt-refresh-group').click();
      });
    };
    $('#modal-edit-group').on('click', '.bt-ok', function () {
      let gid = $('#modal-edit-group .input-group-name').data('gid');
      let name = $('#modal-edit-group .input-group-name').val();
      let fid = $('#modal-edit-group .input-group-fid').val();
      let type = $('#modal-edit-group .input-group-type:checked').val();
      let grade = $('#modal-edit-group .input-group-grade').val();
      let clas = $('#modal-edit-group .input-group-class').val();
      let note = $('#modal-edit-group .input-group-note').val();
      if (name.trim().length > 0) {
        updateGroup(gid, name.trim(), fid.trim().toUpperCase(), type, grade.trim(), clas.trim(), note.trim());
      }
    });

    // Group-Qset Handler

    $('#group-list').on('click', '.bt-group-qset', function () {
      let gid = $(this).closest('.item').data('gid');
      let name = $(this).closest('.item').data('name');
      $('#modal-group-qset .group-name').html('<strong>' + name + '</strong>')
      app.ajax.get(
        baseUrl + 'adminApi/getQsetGroup/' + gid
      ).then(function (qsets) {
        let inList = '';
        qsets.in.forEach((qset) => {
          inList += '<div class="row align-items-center list" data-gid="' + gid + '" data-qsid="' + qset.qsid + '" data-name="' + qset.name + '">';
          inList += '<div class="col-sm-6"><span class="badge badge-info">' + qset.type + '</span> ' + qset.name + '</div>';
          inList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
          inList += '</div></div>';
          inList += '</div>';
        });
        $('#modal-group-qset .in-list').html(inList);
        let notInList = '';
        qsets.notin.forEach((qset) => {
          notInList += '<div class="row align-items-center list" data-gid="' + gid + '" data-qsid="' + qset.qsid + '" data-name="' + qset.name + '">';
          notInList += '<div class="col-sm-6"><span class="badge badge-info">' + qset.type + '</span> ' + qset.name + '</div>';
          notInList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
          notInList += '</div></div>';
          notInList += '</div>';
        });
        $('#modal-group-qset .not-in-list').html(notInList);
        app.gui.modal('#modal-group-qset', {
          width: 'wider'
        });
      });
    })

    $('#modal-group-qset').on('click', '.bt-add', function () {
      let row = $(this).closest('.row');
      let gid = row.data('gid')
      let qsid = row.data('qsid')
      let name = row.data('name')
      app.ajax.get(
        baseUrl + '/adminApi/addQsetToGroup/' + qsid + '/' + gid
      ).then(function (response) {
        let inList = '';
        inList += '<div class="row align-items-center list" data-gid="' + gid + '" data-qsid="' + qsid + '" data-name="' + name + '">';
        inList += '<div class="col-sm-6">' + name + '</div>';
        inList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
        inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
        inList += '</div></div>';
        inList += '</div>';
        $('#modal-group-qset .in-list').prepend(inList);
        row.slideUp();
      })
    })

    $('#modal-group-qset').on('click', '.bt-remove', function () {
      let row = $(this).closest('.row');
      let gid = row.data('gid')
      let qsid = row.data('qsid')
      let name = row.data('name')
      app.ajax.get(
        baseUrl + '/adminApi/removeQsetFromGroup/' + qsid + '/' + gid
      ).then(function (response) {
        let notInList = '';
        notInList += '<div class="row align-items-center list" data-gid="' + gid + '" data-qsid="' + qsid + '" data-name="' + name + '">';
        notInList += '<div class="col-sm-6">' + name + '</div>';
        notInList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
        notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
        notInList += '</div></div>';
        notInList += '</div>';
        $('#modal-group-qset .not-in-list').prepend(notInList);
        row.slideUp();
      })
    })

    // Group-User Handler

    $('#group-list').on('click', '.bt-group-user', function () {
      let gid = $(this).closest('.item').data('gid');
      let name = $(this).closest('.item').data('name');
      $('#modal-group-user .group-name').html('<strong>' + name + '</strong>')
      app.ajax.get(
        baseUrl + 'adminApi/getUserGroup/' + gid
      ).then(function (users) {
        let inList = '';
        users.in.forEach((user) => {
          inList += '<div class="row align-items-center list" data-gid="' + gid + '" data-uid="' + user.uid + '" data-username="' + user.username + '">';
          inList += '<div class="col-sm-6">' + user.username + '</div>';
          inList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
          inList += '</div></div>';
          inList += '</div>';
        });
        $('#modal-group-user .in-list').html(inList);
        let notInList = '';
        users.notin.forEach((user) => {
          notInList += '<div class="row align-items-center list" data-gid="' + gid + '" data-uid="' + user.uid + '" data-username="' + user.username + '">';
          notInList += '<div class="col-sm-6">' + user.username + '</div>';
          notInList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
          notInList += '</div></div>';
          notInList += '</div>';
        });
        $('#modal-group-user .not-in-list').html(notInList);
        app.gui.modal('#modal-group-user', {
          width: 'wider'
        });
      });
    })

    $('#modal-group-user').on('click', '.bt-add', function () {
      let row = $(this).closest('.row');
      let gid = row.data('gid')
      let uid = row.data('uid')
      let username = row.data('username')
      app.ajax.get(
        baseUrl + '/adminApi/addUserToGroup/' + uid + '/' + gid
      ).then(function (response) {
        let inList = '';
        inList += '<div class="row align-items-center list" data-gid="' + gid + '" data-uid="' + uid + '" data-username="' + username + '">';
        inList += '<div class="col-sm-6">' + username + '</div>';
        inList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
        inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
        inList += '</div></div>';
        inList += '</div>';
        $('#modal-group-user .in-list').prepend(inList);
        row.slideUp();
      })
    })

    $('#modal-group-user').on('click', '.bt-remove', function () {
      let row = $(this).closest('.row');
      let gid = row.data('gid')
      let uid = row.data('uid')
      let username = row.data('username')
      app.ajax.get(
        baseUrl + '/adminApi/removeUserFromGroup/' + uid + '/' + gid
      ).then(function (response) {
        let notInList = '';
        notInList += '<div class="row align-items-center list" data-gid="' + gid + '" data-uid="' + uid + '" data-username="' + username + '">';
        notInList += '<div class="col-sm-6">' + username + '</div>';
        notInList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
        notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
        notInList += '</div></div>';
        notInList += '</div>';
        $('#modal-group-user .not-in-list').prepend(notInList);
        row.slideUp();
      })
    })

    // Group-Material Handlers

    $('#group-list').on('click', '.bt-group-material', function () {
      let gid = $(this).closest('.item').data('gid');
      let name = $(this).closest('.item').data('name');
      $('#modal-group-material .group-name').html('<strong>' + name + '</strong>')
      app.ajax.get(
        baseUrl + 'adminApi/getMaterialGroup/' + gid
      ).then(function (materials) {
        let inList = '';
        materials.in.forEach((material) => {
          inList += '<div class="row align-items-center list" data-gid="' + gid + '" data-mid="' + material.mid + '" data-name="' + material.name + '">';
          inList += '<div class="col-sm-6">' + material.name + '</div>';
          inList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
          inList += '</div></div>';
          inList += '</div>';
        });
        $('#modal-group-material .in-list').html(inList);
        let notInList = '';
        materials.notin.forEach((material) => {
          notInList += '<div class="row align-items-center list" data-gid="' + gid + '" data-mid="' + material.mid + '" data-name="' + material.name + '">';
          notInList += '<div class="col-sm-6">' + material.name + '</div>';
          notInList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
          notInList += '</div></div>';
          notInList += '</div>';
        });
        $('#modal-group-material .not-in-list').html(notInList);
        app.gui.modal('#modal-group-material', {
          width: 'wider'
        });
      })
    })

    $('#modal-group-material').on('click', '.bt-add', function () {
      let row = $(this).closest('.row');
      let gid = row.data('gid')
      let mid = row.data('mid')
      let name = row.data('name')
      app.ajax.get(
        baseUrl + '/adminApi/addMaterialToGroup/' + mid + '/' + gid
      ).then(function (response) {
        let inList = '';
        inList += '<div class="row align-items-center list" data-gid="' + gid + '" data-mid="' + mid + '" data-name="' + name + '">';
        inList += '<div class="col-sm-6">' + name + '</div>';
        inList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
        inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
        inList += '</div></div>';
        inList += '</div>';
        $('#modal-group-material .in-list').prepend(inList);
        row.slideUp();
      })
    })

    $('#modal-group-material').on('click', '.bt-remove', function () {
      let row = $(this).closest('.row');
      let gid = row.data('gid')
      let mid = row.data('mid')
      let name = row.data('name')
      app.ajax.get(
        baseUrl + '/adminApi/removeMaterialFromGroup/' + mid + '/' + gid
      ).then(function (response) {
        let notInList = '';
        notInList += '<div class="row align-items-center list" data-gid="' + gid + '" data-mid="' + mid + '" data-name="' + name + '">';
        notInList += '<div class="col-sm-6">' + name + '</div>';
        notInList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
        notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
        notInList += '</div></div>';
        notInList += '</div>';
        $('#modal-group-material .not-in-list').prepend(notInList);
        row.slideUp();
      })
    })

    // Group-Room Handlers

    $('#group-list').on('click', '.bt-group-room', function () {
      let gid = $(this).closest('.item').data('gid');
      let name = $(this).closest('.item').data('name');
      $('#modal-group-room .group-name').html('<strong>' + name + '</strong>')
      app.ajax.get(
        baseUrl + 'adminApi/getRoomGroup/' + gid
      ).then(function (rooms) {
        let inList = '';
        rooms.in.forEach((room) => {
          inList += '<div class="row align-items-center list" data-gid="' + gid + '" data-rid="' + room.rid + '" data-name="' + room.name + '">';
          inList += '<div class="col-sm-6">' + room.name + '</div>';
          inList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
          inList += '</div></div>';
          inList += '</div>';
        });
        $('#modal-group-room .in-list').html(inList);
        let notInList = '';
        rooms.notin.forEach((room) => {
          notInList += '<div class="row align-items-center list" data-gid="' + gid + '" data-rid="' + room.rid + '" data-name="' + room.name + '">';
          notInList += '<div class="col-sm-6">' + room.name + '</div>';
          notInList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
          notInList += '</div></div>';
          notInList += '</div>';
        });
        $('#modal-group-room .not-in-list').html(notInList);
        app.gui.modal('#modal-group-room', {
          width: 'wider'
        });
      })
    })

    $('#modal-group-room').on('click', '.bt-add', function () {
      let row = $(this).closest('.row');
      let gid = row.data('gid')
      let rid = row.data('rid')
      let name = row.data('name')
      app.ajax.get(
        baseUrl + '/adminApi/addRoomToGroup/' + rid + '/' + gid
      ).then(function (response) {
        let inList = '';
        inList += '<div class="row align-items-center list" data-gid="' + gid + '" data-rid="' + rid + '" data-name="' + name + '">';
        inList += '<div class="col-sm-6">' + name + '</div>';
        inList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
        inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
        inList += '</div></div>';
        inList += '</div>';
        $('#modal-group-room .in-list').prepend(inList);
        row.slideUp();
      })
    })

    $('#modal-group-room').on('click', '.bt-remove', function () {
      let row = $(this).closest('.row');
      let gid = row.data('gid')
      let rid = row.data('rid')
      let name = row.data('name')
      app.ajax.get(
        baseUrl + '/adminApi/removeRoomFromGroup/' + rid + '/' + gid
      ).then(function (response) {
        let notInList = '';
        notInList += '<div class="row align-items-center list" data-gid="' + gid + '" data-rid="' + rid + '" data-name="' + name + '">';
        notInList += '<div class="col-sm-6">' + name + '</div>';
        notInList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
        notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
        notInList += '</div></div>';
        notInList += '</div>';
        $('#modal-group-room .not-in-list').prepend(notInList);
        row.slideUp();
      })
    })

  }

  handleRoleEvent(app) {
    $('#bt-refresh-role').on('click', function () {
      app.ajax.get(
        baseUrl + 'adminApi/getRolesWithCount'
      ).then(function (roles) {
        let roleList = '';
        roles.forEach((role) => {
          roleList += '<div class="item" data-rid="' + role.rid + '" data-name="' + role.name + '">';
          roleList += '<div class="align-items-center list d-flex justify-content-between">';
          roleList += '<div class="item-row"><span class="badge badge-info">' + role.rid + '</span> ' + role.name + '</div>';
          roleList += '<div class="text-right"><div class="btn-group btn-sm">';
          roleList += '<button class="bt-user btn btn-sm btn-outline-secondary"><i class="fas fa-user-plus"></i> ' + role.cusers + '</button>';
          roleList += '</div><div class="btn-group btn-sm">'
          roleList += '<button class="bt-edit btn btn-sm btn-outline-warning"><i class="fas fa-pencil-alt"></i></button>';
          roleList += '<button class="bt-delete btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>';
          roleList += '</div></div>';
          roleList += '</div>';
          roleList += '</div>';
        });
        $('#role-list').html(roleList);
        $('.input-keyword-role').trigger('keyup');
      });
    });
    $('#bt-filter-role').on('click', function () {
      let filter = $(this).closest('.row').siblings('.filter').slideToggle({
        duration: 100,
        complete: function () {
          $('.input-keyword-role').focus();
        }
      });
    });
    $('.input-keyword-role').on('keyup', function (e) {
      let keyword = $(this).val().trim();
      let roles = $('#role-list').children();
      roles.each((index, role) => {
        if ($(role).data('name').toString().match(new RegExp(keyword, 'gi'))) $(role).show();
        else $(role).hide();
      })
    })
    $('#bt-create-role').on('click', function () {
      $('#modal-create-role').modal('show');
      $('#modal-create-role .input-role-rid').val('').focus();
      $('#modal-create-role .input-role-name').val('');
    });
    let createRole = function (rid, name) {
      app.ajax.post(
        baseUrl + 'adminApi/createRole', {
          rid: rid,
          name: name
        }
      ).then(function (data) {
        $('#modal-create-role').modal('hide');
        $('#bt-refresh-role').click();
      });
    };
    $('#modal-create-role').on('click', '.bt-ok', function () {
      let rid = $('#modal-create-role .input-role-rid').val();
      let name = $('#modal-create-role .input-role-name').val();
      if (rid.trim() == '') rid = name.substr(0, 3);
      if (name.trim().length > 0) {
        createRole(rid.trim().toUpperCase(), name.trim());
      }
    });
    $('#role-list').on('click', '.bt-delete', function () {
      let row = $(this).closest('.item');
      let name = row.data('name');
      let rid = row.data('rid');
      let confirmDialog = app.gui.confirm('Delete this role: ' + name + ' ?', {
        'positive-callback': function () {
          app.ajax.post(
            baseUrl + 'adminApi/deleteRole', {
              rid: rid
            }
          ).then(function (data) {
            confirmDialog.modal('hide');
            row.slideUp();
          }).finally(function () {
            confirmDialog.modal('hide');
          });
        }
      });
    });
    $('#role-list').on('click', '.bt-edit', function () {
      let row = $(this).closest('.item');
      let rid = row.data('rid');
      let name = row.data('name');
      $('#modal-edit-role').modal('show');
      $('#modal-edit-role .input-role-name').val(name).focus();
      $('#modal-edit-role .input-role-name').data('rid', rid);
      $('#modal-edit-role .input-role-rid').val(rid);
    });
    let updateRole = function (rid, nrid, name) {
      app.ajax.post(
        baseUrl + 'adminApi/updateRole', {
          nrid: nrid,
          name: name,
          rid: rid,
        }
      ).then(function (data) {
        app.gui.notify('Role updated.', {
          type: 'success'
        });
        $('#modal-edit-role').modal('hide');
        $('#bt-refresh-role').click();
      });
    };
    $('#modal-edit-role').on('click', '.bt-ok', function () {
      let name = $('#modal-edit-role .input-role-name').val();
      let rid = $('#modal-edit-role .input-role-name').data('rid');
      let nrid = $('#modal-edit-role .input-role-rid').val();

      if (name.trim().length > 0) {
        updateRole(rid, nrid, name.trim());
      }
    });
    $('#modal-edit-role .input-role-name').on('keyup', function (e) {
      if (e.keyCode === 13) {
        let name = $(this).val();
        let rid = $(this).data('rid');
        let nrid = $('#modal-edit-role .input-role-rid').val();
        if (name.trim().length > 0) {
          updateRole(rid, nrid, name.trim());
        }
      }
    });
    $('#role-list').on('click', '.bt-user', function () {
      let rid = $(this).closest('.item').data('rid');
      let name = $(this).closest('.item').data('name');
      $('#modal-role-user .role-name').html('<strong>' + name + '</strong>')
      app.ajax.get(
        baseUrl + 'adminApi/getUserWithRole/' + rid
      ).then(function (users) {
        let inList = '';
        users.in.forEach((user) => {
          inList += '<div class="row align-items-center list" data-rid="' + rid + '" data-uid="' + user.uid + '" data-username="' + user.username + '">';
          inList += '<div class="col-sm-6">' + user.username + '</div>';
          inList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Deassign</button>';
          inList += '</div></div>';
          inList += '</div>';
        });
        $('#modal-role-user .in-list').html(inList);
        let notInList = '';
        users.notin.forEach((user) => {
          notInList += '<div class="row align-items-center list" data-rid="' + rid + '" data-uid="' + user.uid + '" data-username="' + user.username + '">';
          notInList += '<div class="col-sm-6">' + user.username + '</div>';
          notInList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Assign</button>';
          notInList += '</div></div>';
          notInList += '</div>';
        });
        $('#modal-role-user .not-in-list').html(notInList);
        app.gui.modal('#modal-role-user', {
          width: 'wider'
        });
      })
    })
    $('#modal-role-user').on('click', '.bt-add', function () {
      let row = $(this).closest('.row');
      let rid = row.data('rid')
      let uid = row.data('uid')
      let username = row.data('username')
      app.ajax.get(
        baseUrl + '/adminApi/assignRoleToUser/' + uid + '/' + rid
      ).then(function (response) {
        let inList = '';
        inList += '<div class="row align-items-center list" data-rid="' + rid + '" data-uid="' + uid + '" data-username="' + username + '">';
        inList += '<div class="col-sm-6">' + username + '</div>';
        inList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
        inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Deassign</button>';
        inList += '</div></div>';
        inList += '</div>';
        $('#modal-role-user .in-list').prepend(inList);
        row.slideUp();
      })
    })
    $('#modal-role-user').on('click', '.bt-remove', function () {
      let row = $(this).closest('.row');
      let rid = row.data('rid')
      let uid = row.data('uid')
      let username = row.data('username')
      app.ajax.get(
        baseUrl + '/adminApi/deassignRoleFromUser/' + uid + '/' + rid
      ).then(function (response) {
        let notInList = '';
        notInList += '<div class="row align-items-center list" data-rid="' + rid + '" data-uid="' + uid + '" data-username="' + username + '">';
        notInList += '<div class="col-sm-6">' + username + '</div>';
        notInList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
        notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Assign</button>';
        notInList += '</div></div>';
        notInList += '</div>';
        $('#modal-role-user .not-in-list').prepend(notInList);
        row.slideUp();
      })
    })
  }

  handleUserEvent(app) {
    $('#bt-refresh-user').on('click', function () {
      app.ajax.get(
        baseUrl + 'adminApi/getUsersWithRole'
      ).then(function (users) {
        let userList = '';
        users.forEach((user) => {
          let role = user.role_id ? ' <span class="badge badge-warning">' + user.role_id + '</span>' : '';
          userList += '<div class="item" data-uid="' + user.uid + '" data-name="' + user.name + '">';
          userList += '<div class="align-items-center list d-flex justify-content-between">';
          userList += '<div class="item-row">'
          userList += '<span class="badge badge-info">' + user.uid + '</span> ' + user.name + role;
          userList += '</div>';
          userList += '<div class="text-right"><div class="btn-group btn-sm">';
          if (user.enabled == '1')
            userList += '<button class="bt-enable btn btn-sm btn-outline-success"><i class="fas fa-check-circle"></i></button>';
          else if (user.enabled == '0')
            userList += '<button class="bt-enable btn btn-sm btn-outline-danger"><i class="fas fa-times-circle"></i></button>';
          userList += '<button class="bt-edit btn btn-sm btn-outline-warning"><i class="fas fa-pencil-alt"></i></button>';
          userList += '<button class="bt-delete btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>';
          userList += '</div></div>';
          userList += '</div>';
          userList += '</div>';
        });
        $('#user-list').html(userList);
        $('.input-keyword-user').trigger('keyup');
      });
    });
    $('#bt-filter-user').on('click', function () {
      let row = $(this).closest('.row').siblings('.filter').slideToggle({
        duration: 100,
        complete: function () {
          $('.input-keyword-user').focus();
        }
      });
    });
    $('.input-keyword-user').on('keyup', function (e) {
      let users = $('#user-list').children();
      let keyword = $(this).val().trim();
      users.each((index, user) => {
        if ($(user).data('name').toString().match(new RegExp(keyword, 'gi')))
          $(user).show();
        else $(user).hide();
      })
    })
    $('#bt-import-user').on('click', function (e) {
      e.preventDefault();
      $('#csvform input[type="file"]').click();
    })
    $('#bt-download-template').on('click', function (e) {
      e.preventDefault();
      let confirm = app.gui.confirm('Download username and password template file?', {
        'positive-callback': function () {
          window.location.href = assetsUrl + 'files/user-pass.csv';
          confirm.modal('hide');
        }
      });
    })
    $('#bt-upload-csv').on('click', function (e) {
      e.preventDefault();
      var filename = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '')
      // console.log(filename.trim().length);
      if (filename.trim().length == 0) {
        app.gui.dialog('Please select CSV file.');
        return;
      }
      let confirm = app.gui.confirm('Upload file and insert user data?', {
        'positive-callback': function () {
          confirm.modal('hide');
          var form = $('#csvform')[0];
          var data = new FormData(form);
          $("#bt-upload-csv").prop("disabled", true);
          $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: baseUrl + "adminApi/uploadUserCSV",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
              // console.log(data);
              if (typeof data == 'object') {
                if (data.error) {
                  app.gui.notify(data.error, {
                    type: 'danger'
                  });
                } else {
                  let result = data.result ? data.result : [];
                  if (result.length) app.gui.notify(result.length + ' data imported succesfully.', {
                    type: 'success'
                  });
                  // console.log("SUCCESS : ", data);
                }
              } else app.gui.notify(data);
              $("#bt-upload-csv").prop("disabled", false);
            },
            error: function (e) {
              app.gui.notify(e.responseText, {
                type: 'danger'
              });
              $("#result").text(e.responseText);
              // console.log("ERROR : ", e);
              $("#bt-upload-csv").prop("disabled", false);
            }
          });
        }
      })
    })
    $('#csvform input[type="file"]').on('change', function () {
      var filename = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '')
      // console.log(filename);
      if (filename.trim() != '') $('#bt-import-user .filename').html(filename);
      else $('#bt-import-user .filename').html('Import CSV');
    })
    $('#bt-create-user').on('click', function (e) {
      e.preventDefault();
      $('#modal-create-user').modal('show');
      $('#modal-create-user .input-user-username').val('').focus();
      $('#modal-create-user .input-user-password').val('');
      $('#modal-create-user .input-user-name').val('');
    });
    let createUser = function (username, password, name) {
      app.ajax.post(
        baseUrl + 'adminApi/createUser', {
          username: username,
          password: password,
          name: name
        }
      ).then(function (data) {
        $('#modal-create-user').modal('hide');
        $('#bt-refresh-user').click();
      });
    };
    $('#modal-create-user').on('click', '.bt-generate-password', function () {
      let username = $('#modal-create-user .input-user-username').val();
      $('#modal-create-user .input-user-password').val(md5(username).substr(0, 4));
      $('#modal-create-user .input-user-password').prop('type', 'text');
    });
    $('#modal-create-user').on('click', '.bt-ok', function () {
      let username = $('#modal-create-user .input-user-username').val();
      let password = $('#modal-create-user .input-user-password').val();
      let name = $('#modal-create-user .input-user-name').val();
      if (username.trim().length > 0) {
        createUser(username.trim(), password.trim(), name.trim());
      }
    });
    $('#user-list').on('click', '.bt-enable', function () {
      let command = $(this).hasClass('btn-outline-success') ? 'disableUser' : 'enableUser';
      let row = $(this).closest('.item');
      let uid = row.data('uid');
      let button = $(this);
      let icon = $(this).find('> i');
      app.ajax.post(
        baseUrl + 'adminApi/' + command, {
          uid: uid
        }
      ).then(function (data) {
        button.toggleClass('btn-outline-success').toggleClass('btn-outline-danger');
        icon.toggleClass('fa-check-circle').toggleClass('fa-times-circle');
      });
    });
    $('#user-list').on('click', '.bt-delete', function () {
      let row = $(this).closest('.item');
      let name = row.data('name');
      let uid = row.data('uid');
      let confirmDialog = app.gui.confirm('Delete this user: ' + name + ' ?', {
        'positive-callback': function () {
          app.ajax.post(
            baseUrl + 'adminApi/deleteUser', {
              uid: uid
            }
          ).then(function (data) {
            confirmDialog.modal('hide');
            row.slideUp();
          }).finally(function () {
            confirmDialog.modal('hide');
          });
        }
      });
    });
    $('#user-list').on('click', '.bt-edit', function () {
      let row = $(this).closest('.item');
      let uid = row.data('uid');
      app.ajax.get(
        baseUrl + 'adminApi/getUser/' + uid
      ).then(function (user) {
        if (user) {
          let name = user != null ? user.name : '';
          let username = user != null ? user.username : '';
          $('#modal-edit-user').modal('show');
          $('#modal-edit-user .input-user-name').val(name);
          $('#modal-edit-user .input-user-name').data('uid', uid);
          $('#modal-edit-user .input-user-username').val(username).focus();
          $('#modal-edit-user .input-user-password').val('');
        }
      });
      $('#modal-edit-user').on('click', '.bt-generate-password', function () {
        let username = $('#modal-edit-user .input-user-username').val();
        $('#modal-edit-user .input-user-password').val(md5(username).substr(0, 4));
        $('#modal-edit-user .input-user-password').prop('type', 'text');
      });
    });
    let updateUser = function (uid, username, password, name) {
      app.ajax.post(
        baseUrl + 'adminApi/updateUser', {
          username: username,
          password: password,
          name: name,
          uid: uid,
        }
      ).then(function (data) {
        app.gui.notify('User updated.', {
          type: 'success'
        });
        $('#modal-edit-user').modal('hide');
        $('#bt-refresh-user').click();
      });
    };
    $('#modal-edit-user').on('click', '.bt-ok', function () {
      let name = $('#modal-edit-user .input-user-name').val();
      let uid = $('#modal-edit-user .input-user-name').data('uid');
      let username = $('#modal-edit-user .input-user-username').val();
      let password = $('#modal-edit-user .input-user-password').val();
      if (name.trim().length > 0) {
        updateUser(uid, username.trim(), password.trim(), name.trim());
      }
    });
  }

}

$(function () {
  let admin = new AdminApp();
})