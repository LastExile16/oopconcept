var BRIDGE = {}

class AdminMap {

  constructor(options) {

    // Default settings
    let defaultSettings = {}

    // Override default settings by options
    this.settings = Object.assign({}, defaultSettings, options);

    this.gui = new GUI();
    this.ajax = Ajax.ins(this.gui);
    this.eventListeners = [];

    this.handleGoalmapEvent(this);
    $('#bt-refresh-material').click();

  }

  // Goalmap

  handleGoalmapEvent(kit) {
    $('#bt-refresh-material').on('click', function () {
      kit.ajax.get(
        baseUrl + 'adminApi/getMaterialsWithQsetExtras'
      ).then(function (materials) {
        let materialList = '';
        materials.forEach((material) => {
          
          let fid = material.fid ? ' <span class="badge badge-warning">' + material.fid + '</span>' : '';
          materialList += '<div class="row pt-2 pb-2 align-items-center list list-hover list-pointer" data-mid="' + material.mid + '" data-fid="' + material.fid + '" data-name="' + material.name + '">';
          materialList += '<div class="col-sm-12">';
          materialList += '<span class="badge badge-info">' + material.mid + '</span> '
          materialList += material.name + fid;
          materialList += '</div>';
          materialList += '</div>';
        });
        $('#material-list').html(materialList);
      });
    });
    $('#material-list').on('click', '.row', function () {
      let mid = $(this).data('mid');
      $('#bt-refresh-goalmap').attr('data-mid', mid);
      $('#bt-refresh-goalmap').click();
    });
    $('#bt-refresh-goalmap').on('click', function() {
      let mid = $(this).attr('data-mid');
      if(!mid) {
        kit.gui.notify('Select a material.', {type: 'warning'});
        return;
      }
      kit.ajax.get(baseUrl + 'adminApi/getGoalmaps/' + mid).then(function (goalmaps) {
        let goalmapList = '';
        goalmaps.forEach((goalmap) => { 
          goalmapList += '<div class="row align-items-center list" data-gmid="' + goalmap.gmid + '" data-name="' + goalmap.name + '" data-type="' + goalmap.type + '">';
          goalmapList += '<div class="col-sm-6">';
          goalmapList += '<span class="badge badge-info">' + goalmap.gmid + '</span> ' + goalmap.name + ' ';
          goalmapList += '<span class="badge-kit-count badge badge-info">L:' + goalmap.links_count + ' C:' + goalmap.concepts_count + '</span> ';
          goalmapList += '<span class="badge-learnermap-count badge badge-warning">LM:' + goalmap.learnermaps_count + '</span> ';
          if(goalmap.type == 'kit')
            goalmapList += '<span class="badge-type badge badge-success">' + goalmap.type + '</span>';
          if(goalmap.type == 'draft')
            goalmapList += '<span class="badge-type badge badge-secondary">' + goalmap.type + '</span>';
          if(goalmap.type == 'fix')
            goalmapList += '<span class="badge-type badge badge-primary">' + goalmap.type + '</span>';    
          goalmapList += '</div>';
          goalmapList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
          if(goalmap.type == 'fix')
            goalmapList += '<button class="bt-set-kit btn btn-sm btn-outline-success"><i class="fas fa-puzzle-piece"></i></button>';
          goalmapList += '<button class="bt-edit btn btn-sm btn-outline-warning"><i class="fas fa-pencil-alt"></i></button>';
          goalmapList += '<button class="bt-delete btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>';
          goalmapList += '</div></div>';
          goalmapList += '</div>';
        });
        $('#goalmap-list').html(goalmapList);
      });
    });
    $('#bt-filter-goalmap').on('click', function () {
      let filter = $(this).closest('.row').siblings('.filter').slideToggle({
        duration: 100,
        complete: function () {
          $('.input-keyword-goalmap').focus();
        }
      });
    });
    $('.input-keyword-goalmap').on('keyup', function (e) {
      let goalmaps = $('#goalmap-list').children();
      let keyword = $(this).val().trim();
      goalmaps.each((index, goalmap) => {
        if ($(goalmap).data('name').match(new RegExp(keyword, 'gi')))
          $(goalmap).show();
        else $(goalmap).hide();
      })
    })
    $('#goalmap-list').on('click', '.bt-delete', function () {
      let row = $(this).closest('.row');
      let name = row.data('name');
      let gmid = row.data('gmid');
      let confirmDialog = kit.gui.confirm('Delete this goalmap: ' + name + ' ?', {
        'positive-callback': function () {
          kit.ajax.post(
            baseUrl + 'adminApi/deleteGoalmap', {
              gmid: gmid
            }
          ).then(function (data) {
            confirmDialog.modal('hide');
            row.slideUp();
          })
        }
      });
    });
    $('#goalmap-list').on('click', '.bt-set-kit', function () { 
      let row = $(this).closest('.row');
      let gmid = row.data('gmid');
      let button = $(this);
      let icon = row.find('.badge-type');
      kit.ajax.post(
        baseUrl + 'adminApi/setGoalmapAsKit', {
          gmid: gmid
        }
      ).then(function (data) {
        button.fadeOut();
        icon.removeClass('badge-primary').addClass('badge-success').html('kit');
        row.attr('data-type', 'kit');
      });
    });
    $('#goalmap-list').on('click', '.bt-edit', function () {
      let row = $(this).closest('.row');
      let gmid = row.data('gmid');
      let name = row.data('name');
      let type = row.data('type');
      $('#modal-edit-goalmap').modal('show');
      $('#modal-edit-goalmap .input-goalmap-name').val(name).focus();
      $('#modal-edit-goalmap .input-goalmap-name').data('gmid', gmid);
      $('#modal-edit-goalmap .input-goalmap-type').removeAttr('checked');
      $('#modal-edit-goalmap .input-goalmap-type[value="'+type+'"]').attr('checked', 'checked');
    });
    let updateGoalmap = function (gmid, name, type) {
      kit.ajax.post(
        baseUrl + 'adminApi/updateGoalmap', {
          goalmap: name,
          gmid: gmid,
          type: type
        }
      ).then(function (data) {
        kit.gui.notify('Goalmap updated.', {
          type: 'success'
        });
        $('#modal-edit-goalmap').modal('hide');
        $('#bt-refresh-goalmap').click();
      });
    };
    $('#modal-edit-goalmap').on('click', '.bt-ok', function () {
      let name = $('#modal-edit-goalmap .input-goalmap-name').val();
      let gmid = $('#modal-edit-goalmap .input-goalmap-name').data('gmid');
      let type = $('#modal-edit-goalmap .input-goalmap-type:checked').val();
      
      if (name.trim().length > 0) {
        updateGoalmap(gmid, name.trim(), type);
      }
    });
    $('#modal-edit-goalmap .input-goalmap-name').on('keyup', function (e) {
      if (e.keyCode === 13) {
        let name = $(this).val();
        let gmid = $(this).data('gmid');
        let type = $('#modal-edit-goalmap .input-goalmap-type:checked').val();
        if (name.trim().length > 0) {
          updateGoalmap(gmid, name.trim(), type);
        }
      }
    });

    // Goalmap-User Handler

    $('#goalmap-list').on('click', '.bt-goalmap-user', function () {
      let gmid = $(this).closest('.row').data('gmid');
      let name = $(this).closest('.row').data('name');
      $('#modal-goalmap-user .goalmap-name').html('<strong>' + name + '</strong>')
      kit.ajax.get(
        baseUrl + 'adminApi/getUserGoalmap/' + gmid
      ).then(function (users) {
        let inList = '';
        users.in.forEach((user) => {
          inList += '<div class="row align-items-center list" data-gmid="' + gmid + '" data-uid="' + user.uid + '" data-username="' + user.username + '">';
          inList += '<div class="col-sm-6">' + user.username + '</div>';
          inList += '<div class="col-sm-6 text-right"><div class="btn-goalmap btn-sm">';
          inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
          inList += '</div></div>';
          inList += '</div>';
        });
        $('#modal-goalmap-user .in-list').html(inList);
        let notInList = '';
        users.notin.forEach((user) => {
          notInList += '<div class="row align-items-center list" data-gmid="' + gmid + '" data-uid="' + user.uid + '" data-username="' + user.username + '">';
          notInList += '<div class="col-sm-6">' + user.username + '</div>';
          notInList += '<div class="col-sm-6 text-right"><div class="btn-goalmap btn-sm">';
          notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
          notInList += '</div></div>';
          notInList += '</div>';
        });
        $('#modal-goalmap-user .not-in-list').html(notInList);
        kit.gui.modal('#modal-goalmap-user', {
          width: 'wider'
        });
      });
    })

    $('#modal-goalmap-user').on('click', '.bt-add', function () {
      let row = $(this).closest('.row');
      let gmid = row.data('gmid')
      let uid = row.data('uid')
      let username = row.data('username')
      kit.ajax.get(
        baseUrl + '/adminApi/addUserToGoalmap/' + uid + '/' + gmid
      ).then(function (response) {
        let inList = '';
        inList += '<div class="row align-items-center list" data-gmid="' + gmid + '" data-uid="' + uid + '" data-username="' + username + '">';
        inList += '<div class="col-sm-6">' + username + '</div>';
        inList += '<div class="col-sm-6 text-right"><div class="btn-goalmap btn-sm">';
        inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> Remove</button>';
        inList += '</div></div>';
        inList += '</div>';
        $('#modal-goalmap-user .in-list').prepend(inList);
        row.slideUp();
      })
    })

    $('#modal-goalmap-user').on('click', '.bt-remove', function () {
      let row = $(this).closest('.row');
      let gmid = row.data('gmid')
      let uid = row.data('uid')
      let username = row.data('username')
      kit.ajax.get(
        baseUrl + '/adminApi/removeUserFromGoalmap/' + uid + '/' + gmid
      ).then(function (response) {
        let notInList = '';
        notInList += '<div class="row align-items-center list" data-gmid="' + gmid + '" data-uid="' + uid + '" data-username="' + username + '">';
        notInList += '<div class="col-sm-6">' + username + '</div>';
        notInList += '<div class="col-sm-6 text-right"><div class="btn-goalmap btn-sm">';
        notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> Add</button>';
        notInList += '</div></div>';
        notInList += '</div>';
        $('#modal-goalmap-user .not-in-list').prepend(notInList);
        row.slideUp();
      })
    })
  }
}


$(function(){
  BRIDGE.app = new AdminMap();
})