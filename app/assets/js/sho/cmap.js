class CmapApp {

  constructor(canvas) {
    CmapApp.currentState = CmapApp.Init;
    this.canvas = canvas;
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.analyzerLib = new AnalyzerLib();

    this.l = Language.instance();
    Language.instance();

    this.controller = 'sho';

    this.user = {
      uid: null,
      username: null,
      name: null,
      rid: null,
      gids: []
    }

    this.material = {
      mid: null,
      name: null,
      content: null
    }

    this.goalmap = null;
    this.learnermap = null;

    this.eventListeners = [];
    this.logger = null;

    this.reset();
    // this.resetUI();
    this.handleEvent();
    this.canvas.attachEventListener(this);
    this.handleRefresh();
  }

  handleRefresh() {
    let app = this;
    app.session.get('color', function (color) {
      if (color) app.canvas.toolbar.setColor(color);
    })
    app.session.get('user', function (u) {
      if (u != null) {
        let gids = u.gids.trim() ? u.gids.split(",") : [];
        app.signIn(u.uid, u.username, u.name, u.role_id, gids);
        app.logger.log('reload-browser', {
          uid: u.uid
        });
        app.session.get('state', function (target) {
          CmapApp.targetState = target;
          switch (app.user.role) {
            case 'CON':
              app.stateChange(CmapApp.ConceptMapping, null, function () {
                app.session.getAll(function (sessions) {
                  if (sessions.mid) {
                    app.loadMaterial(sessions.mid, function (material) {
                      // console.log(material);
                      app.logger.setMid(material.mid);
                      app.unsetMaterial();
                      app.setMaterial(material.mid, material.name, material.content);
                      app.enableSaveAs();
                      app.canvas.enableNodeCreation();
                      $('#bt-close-map').prop('disabled', false);
                    })
                  }
                  if (sessions.gmid) {
                    app.loadGoalmap(sessions.gmid, function (goalmap) {
                      app.logger.setMid(goalmap.goalmap.mid);
                      app.setGoalmap(goalmap);
                      app.enableSave();
                      if (sessions.kid) {
                        app.stateChange(CmapApp.KitMaking, null, function () {
                          app.loadKitmap(sessions.kid, function (kitmap) {
                            app.setKitmap(kitmap);
                            app.showMap(goalmap, true, function () {
                              app.showKit(kitmap, function () {
                                // $('#bt-center').click();
                                app.jumpCenterCamera();
                              })
                            })
                          })
                        })
                        return;
                      }
                      app.showMap(goalmap, false, function () {
                        // $('#bt-center').click();
                        app.jumpCenterCamera();
                      });
                    });
                  }
                });
              });
              break;
            case 'KIT':
              app.stateChange(CmapApp.KitBuilding, null, function () {
                app.session.getAll(function (sessions) {
                  if (sessions.mid) {
                    app.loadMaterial(sessions.mid, function (material) {
                      app.unsetMaterial();
                      app.setMaterial(material.mid, material.name, material.content);
                    })
                  }
                  if (sessions.gmid) {
                    app.loadGoalmap(sessions.gmid, function (goalmap) {
                      app.enableUpload();
                      app.setGoalmap(goalmap);
                      app.showMap(goalmap, true, function () {
                        app.logger.setMid(undefined);
                        app.logger.setGmid(goalmap.goalmap.gmid);
                        if (sessions.kid) {
                          app.loadKitmap(sessions.kid, function (kitmap) {
                            // let enabled = parseInt(kitmap.kitmap.isDirected);
                            // app.canvas.getToolbar().enableDirection(enabled)
                            app.canvas.getToolbar().enableDirection(false)
                            app.setKitmap(kitmap);
                            if (sessions.lmid) {
                              app.loadLearnermap(sessions.lmid, function (learnermap) {
                                app.setLearnermap(learnermap);
                                app.showLearnermap(learnermap, function () {
                                  // $('#bt-center').click();
                                  app.jumpCenterCamera();
                                  app.InitializeREdgeLimit();
                                });
                                if (sessions.uploaded) {
                                  app.stateChange(CmapApp.Compare, null, function () {
                                    app.logger.setMid(undefined);
                                    app.logger.setGmid(undefined);
                                    $('#bt-compare').click();
                                  })
                                }
                              })
                            } else app.showKit(kitmap, function () {
                              // $('#bt-center').click();
                              app.jumpCenterCamera();
                              app.InitializeREdgeLimit();
                            });
                          })
                        }
                      });
                    })
                  }
                })
              });
              break;
          }
        })
      }
    });
  }

  onCanvasEvent(event, data) {
    let app = this;
    let edges = null;
    switch (event) {
      case 'color-change': // console.error(data);
        app.session.setMulti({
          'color': data.color
        });
        break;
      case 'move-link':
        if (CmapApp.currentState == CmapApp.Compare) return;
        edges = app.canvas.getCy().nodes('#' + data.id).connectedEdges();
        edges.data('link', 'match');
        edges.removeData('link match')
        break;
      case 'disconnect-left':
      case 'disconnect-right':
        edges = app.canvas.getCy().nodes('#' + data.source).connectedEdges();
        edges.data('link', 'match');
        edges.removeData('link match')
        break;
      case 'change-connect-left':
      case 'change-connect-right':
        edges = app.canvas.getCy().nodes('#' + data.from.source).connectedEdges();
        edges.data('link', 'match');
        edges.removeData('link match')
        break;
    }
  }

  reset() {
    this.user = {
      uid: null,
      username: null,
      name: null,
      rid: null,
      gids: []
    }

    this.material = {
      mid: null,
      name: null,
      content: null
    }

    this.goalmap = null;
    this.learnermap = null;
  }

  resetUI() {
    let app = this;
    app.stateChange(CmapApp.Init);
  }

  getCanvas() {
    return this.canvas;
  }

  getGUI() {
    return this.gui;
  }

  getSession() {
    return this.session;
  }

  attachEventListener(listener) {
    this.eventListeners.push(listener);
  }

  attachLogger(logger) {
    this.logger = logger;
  }

  handleEvent() {
    let app = this;

    $('#select-language .language').on('click', function () {
      let currentLang = $('#select-language .active-lang').html().toLowerCase();
      let lang = $(this).data('lang');
      if (currentLang == lang) return;
      let fullLang = lang == 'jp' ? '日本語' : 'English';
      app.session.set('lang', lang, function () {
        $('#select-language .active-lang').html(lang.toUpperCase());
        if (app.canvas.getCy().nodes().length) {
          app.gui.dialog('Language is set to: <strong>' + fullLang + '</strong>. The browser needs to be reloaded for the language settings to apply. Save your work and reload the web browser afterwards.');
          return;
        }
        let dialog = app.gui.dialog('Language is set to: <strong>' + fullLang + '</strong>. The browser needs to be reloaded for the language settings to apply. Reload now?', {
          'positive-callback': function () {
            dialog.modal('hide');
            location.reload();
          }
        });

      })
    })

    $('#bt-login').on('click', function (e) {
      $('#modal-login').modal('show');
      $('#modal-login .input-username').focus();
    });
    let doSignIn = function () {
      let username = $('#modal-login .input-username').val().trim();
      let password = $('#modal-login .input-password').val().trim();
      app.ajax.post(
        baseUrl + 'kitbuildApi/signIn', {
          username: username,
          password: password
        }
      ).then(function (user) { // console.log(user);
        let uid = user.uid;
        let rid = user.role_id;
        let name = user.name;
        let gids = user.gids ? user.gids.split(",") : [];
        app.session.setMulti({
          user: user
        }, () => {
          app.signIn(uid, username, name, rid, gids);
          let welcome = app.gui.dialog(app.l.get('welcome-to-kb', app.user.name));
          setTimeout(function () {
            welcome.modal('hide');
          }, 3000);
          app.logger.setSeq(0);
          app.logger.log('sign-in', {
            uid: user.uid,
            username: user.username,
            name: user.name,
            rid: rid,
            gids: gids
          });
          if (rid == 'CON') app.stateChange(CmapApp.ConceptMapping);
          if (rid == 'KIT') app.stateChange(CmapApp.KitBuilding);
        });
        $('#modal-login').modal('hide');
        $('#input-username').val('');
        $('#input-password').val('');
      })
    }
    $('#modal-login').on('click', '.bt-ok', doSignIn);
    $('#modal-login .input-password').on('keyup', function (e) {
      if (e.keyCode == 13) doSignIn();
    });
    $('#bt-logout').on('click', function (e) {
      let confirmDialog = app.gui.confirm(app.l.get('do-you-want-logout'), {
        'positive-callback': function () {
          confirmDialog.modal('hide');
          app.session.destroy(function () {
            app.logger.log('sign-out', {
              uid: app.user.uid,
              username: app.user.username
            });
            app.logger.setSeq(0);
            let currentLang = $('#select-language .active-lang').html().toLowerCase();
            app.session.set('lang', currentLang, function () {
              app.signOut();
              app.reset();
            })
          })
          // window.location.href = baseUrl + app.controller + '/signOut';
        }
      });
    });
    $('#bt-open-map').on('click', function () {
      if (!app.user.role) return;
      if (!app.user.gids) return;
      let requestUrl = (app.user.role == 'KIT') ?
        baseUrl + 'kitbuildApi/getMaterialsWithGids' :
        baseUrl + 'adminApi/getMaterialsWithGids';
      $('#modal-kit .bt-open').removeAttr('data-mid');
      $('#modal-kit .bt-open').removeAttr('data-gmid');
      $('#modal-kit .bt-new').removeAttr('data-mid')
      $('#modal-kit .bt-new').removeClass('d-none');
      app.ajax.post(requestUrl, {
        gids: app.user.gids,
        enabled: app.user.role == 'KIT'
      }).then(materials => {
        let materialList = (!materials || materials.length == 0) ?
          '<em>' + app.l.get('no-topic-found') + '</em>' :
          '';
        materials.forEach((material) => {
          materialList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pl-2 pr-2 pt-1 pb-1" data-mid="' + material.mid + '" data-name="' + material.name + '">'
          materialList += '<span class="material-name">' + material.name + '</span>'
          materialList += '<i class="fas fa-check text-primary d-none"></i>'
          materialList += '</div>'
        });
        $('#modal-kit .material-list').html(materialList);
        $('#modal-kit .goalmap-list').html('');
        app.gui.modal('#modal-kit', {
          'width': '600px'
        });
      });
    });
    $('#modal-kit .material-list').on('click', '.row', function () {
      let mid = $(this).data('mid');
      $('#modal-kit .bt-open').removeAttr('data-gmid');
      $('#modal-kit .bt-open').attr('data-mid', mid);
      $('#modal-kit .bt-new').attr('data-mid', mid);
      $('#modal-kit .material-list .row').find('i').addClass('d-none');
      $('#modal-kit .material-list .row').removeClass('selected');
      $(this).find('i').removeClass('d-none');
      $(this).addClass('selected')

      if (app.user.role == 'CON') {
        app.loadGoalmaps(mid, function (goalmaps) {

          let goalmapList = (!goalmaps || goalmaps.length == 0) ? '<small>' + app.l.get('no-map-kit-found') + '</small>' : '';
          if (goalmaps.length) {
            goalmaps.forEach(goalmap => {
              goalmapList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pl-2 pr-2 pt-1 pb-1" data-gmid="' + goalmap.gmid + '" data-name="' + goalmap.name + '">'
              goalmapList += '<span class="goalmap-name">' + goalmap.name + '</span>'
              goalmapList += '<span class="goalmap-type badge badge-info">' + goalmap.type + '</span>'
              goalmapList += '<i class="fas fa-check text-primary d-none"></i>'
              goalmapList += '</div>'
            });
          }
          $('#modal-kit .goalmap-list').html(goalmapList)
        });
      }
      if (app.user.role == 'KIT') {
        app.loadKitmaps(mid, function (kitmaps) {
          let kitmapList = (!kitmaps || kitmaps.length == 0) ? '<small>' + app.l.get('no-map-kit-found') + '</small>' : '';
          if (kitmaps.length) {
            kitmaps.forEach(kitmap => {
              kitmapList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pl-2 pr-2 pt-1 pb-1" data-gmid="' + kitmap.gmid + '" data-kid="' + kitmap.kid + '" data-name="' + kitmap.name + '">'
              kitmapList += '<span class="goalmap-name">' + kitmap.name + '</span>'
              kitmapList += '<span class="goalmap-type badge badge-info">' + kitmap.gmname + '</span>'
              kitmapList += '<i class="fas fa-check text-primary d-none"></i>'
              kitmapList += '</div>'
            });
          }
          $('#modal-kit .goalmap-list').html(kitmapList)
        })
      }
    });
    $('#modal-kit .goalmap-list').on('click', '.row', function () {
      let gmid = $(this).data('gmid');
      let kid = $(this).data('kid');
      if (kid) $('#modal-kit .bt-open').attr('data-kid', kid);
      $('#modal-kit .bt-open').attr('data-gmid', gmid);
      $('#modal-kit .bt-new').attr('data-gmid', gmid);
      $('#modal-kit .goalmap-list .row').find('i').addClass('d-none');
      $('#modal-kit .goalmap-list .row').removeClass('selected');
      $(this).find('i').removeClass('d-none');
      $(this).addClass('selected')
    });
    $('#modal-kit').on('click', '.bt-new', function () {
      let mid = $(this).attr('data-mid');
      if (!mid) {
        app.gui.notify(app.l.get('please-select-topic'), {
          type: 'warning'
        });
        return;
      }
      app.loadMaterial(mid, function (material) {
        if (!material) {
          app.gui.notify(app.l.get('unable-load-topic'), {
            type: 'danger'
          })
          return;
        }
        let doLoadMaterial = function () {
          $('#modal-kit').modal('hide');
          app.unsetMaterial();
          app.setMaterial(material.mid, material.name, material.content);
          app.session.set('mid', material.mid);
          app.canvas.enableNodeCreation();
          app.enableCloseMap();
          app.enableSaveAs();
          app.setGoalmap(null);
          app.logger.setMid(material.mid);
          app.logger.log('begin-goalmapping', {
            mid: material.mid
          });
        }
        if (app.canvas.getCy().nodes().length) {
          let confirm = app.gui.confirm(app.l.get('map-exists-topic-clear'), {
            'positive-callback': function () {
              app.canvas.clearCanvas();
              app.canvas.reset();
              confirm.modal('hide');
              doLoadMaterial();
            }
          })
        } else doLoadMaterial();
      });
    });
    $('#modal-kit').on('click', '.bt-open', function () {
      let mid = $(this).attr('data-mid');
      let gmid = $(this).attr('data-gmid');
      let kid = $(this).attr('data-kid');
      if (!mid) {
        app.gui.notify(app.l.get('please-select-topic'), {
          type: 'warning'
        });
        return;
      }
      if (!gmid) {
        app.gui.notify(app.l.get('please-select-kit'), {
          type: 'warning'
        });
        return;
      }
      app.loadMaterial(mid, function (material) {
        app.canvas.enableNodeCreation();
        app.enableCloseMap();
        if (!material) {
          app.gui.notify(app.l.get('unable-load-topic'), {
            type: 'danger'
          })
          return;
        }
        app.loadGoalmap(gmid, function (goalmap) {
          if (!goalmap) {
            app.gui.notify(app.l.get('unable-load-kit'), {
              type: 'danger'
            })
            return;
          }
          let doLoadGoalmap = function () {
            $('#modal-kit').modal('hide');
            app.session.setMulti({
              'mid': material.mid,
              'gmid': goalmap.goalmap.gmid
            });
            app.unsetMaterial();
            app.setMaterial(material.mid, material.name, material.content);
            app.setGoalmap(goalmap);
            app.enableSave();
            app.enableSaveAs();
            switch (app.user.role) {
              case 'CON':
                app.showMap(goalmap, false, function () {
                  // $('#bt-center').click();
                  app.jumpCenterCamera();
                });
                app.logger.setMid(goalmap.goalmap.mid);
                app.logger.setGmid(undefined);
                app.logger.log('open-saved-goalmap', {
                  gmid: goalmap.goalmap.gmid
                })
                break;
              case 'KIT':
                app.canvas.enableNodeCreation(false);
                app.showMap(goalmap, true, function () {
                  app.loadKitmap(kid, function (kitmap) {
                    app.kitmap = kitmap;
                    app.session.set('kid', kitmap.kitmap.kid);
                    app.session.unset('lmid');
                    app.showKit(kitmap);
                    app.enableUpload();
                    app.logger.setMid(undefined);
                    app.logger.setGmid(kitmap.kitmap.gmid);
                    app.logger.log('open-kit', {
                      gmid: kitmap.kitmap.gmid,
                      kid: kitmap.kitmap.kid
                    })
                    // $('#bt-center').click();
                    app.jumpCenterCamera();
                    app.InitializeREdgeLimit();
                  })
                });
                app.setLearnermap(null);
                break;
            }
            app.enableSaveAs();
            app.enableCloseMap();
          }
          if (app.canvas.getCy().nodes().length) {
            let confirm = app.gui.confirm(app.l.get('map-exists-map-clear'), {
              'positive-callback': function () {
                app.canvas.clearCanvas();
                app.canvas.reset();
                confirm.modal('hide');
                doLoadGoalmap();
              }
            })
          } else doLoadGoalmap();

        });
      });
    });
    $('#modal-kit').on('click', '.bt-topic-management', function () {
      app.ajax.get(baseUrl + "adminApi/getMaterials").then(function (materials) {
        // console.log(materials)
        let materialList = '';
        materials.forEach((material) => {
          materialList += '<div class="item list" data-mid="' + material.mid + '" data-fid="' + material.fid + '" data-name="' + material.name + '">';
          materialList += '<div class="align-items-center d-flex justify-content-between">';
          materialList += '<div class="item-row">';
          materialList += material.name + '<br>';
          if (material.fid) materialList += '<span class="badge badge-warning">' + material.fid + '</span>';
          if (!parseInt(material.enabled)) materialList += ' <span class="badge badge-danger">Disabled</span> '
          materialList += ' <span class="badge badge-info">' + material.mid + '</span> '
          materialList += '</div>';
          materialList += '<div class="text-right">';
          materialList += '<div class="btn-group btn-sm">';
          if (material.enabled == '1')
            materialList += '<button class="bt-enable btn btn-sm btn-outline-success"><i class="fas fa-check-circle"></i></button>';
          else if (material.enabled == '0')
            materialList += '<button class="bt-enable btn btn-sm btn-outline-danger"><i class="fas fa-times-circle"></i></button>';
          materialList += '<button class="bt-group btn btn-sm btn-outline-secondary"><i class="fas fa-user-check"></i></button>';
          materialList += '<button class="bt-edit btn btn-sm btn-outline-warning"><i class="fas fa-pencil-alt"></i></button>';
          materialList += '<button class="bt-delete btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>';
          materialList += '</div></div>';
          materialList += '</div>';
          materialList += '</div>';
        });
        $('#modal-material-list .material-list').html(materialList);
        $('#modal-kit').modal('hide');
        $('#modal-material-list').modal('show');
      });
    });
    $('#modal-material-list .bt-new').on('click', function (e) {
      app.gui.modal('#modal-create-material');
      $('#modal-create-material .input-material-name').val('').focus();
      $('#modal-create-material .input-material-fid').val('');
    });
    $('#modal-material-list .bt-close').on('click', function (e) {
      $('#bt-open-map').click();
    });
    let createMaterial = function (name, fid, content) {
      app.ajax.post(
        baseUrl + 'adminApi/createMaterial', {
          name: name,
          fid: fid,
          content: content
        }
      ).then(function (data) {
        $('#modal-kit .bt-topic-management').click();
        $('#modal-create-material').modal('hide');
      });
    };
    $('#modal-create-material').on('click', '.bt-ok', function () {
      let name = $('#modal-create-material .input-material-name').val();
      let fid = $('#modal-create-material .input-material-fid').val();
      let content = '';
      if (name.trim().length > 0) {
        createMaterial(name.trim(), fid.trim().toUpperCase(), content.trim());
      }
    });
    $('#modal-create-material').on('click', '.bt-cancel', function () {
      $('#modal-kit .bt-topic-management').click();
    })
    $('#modal-material-list').on('click', '.bt-delete', function () {
      let row = $(this).closest('.item');
      let name = row.data('name');
      let mid = row.data('mid');
      let confirmDialog = app.gui.confirm(app.l.get('confirm-delete-material', name), {
        'positive-callback': function () {
          app.ajax.post(
            baseUrl + 'adminApi/deleteMaterial', {
              mid: mid
            }
          ).then(function (data) {
            if (data) {
              row.slideUp();
            }
            confirmDialog.modal('hide');
          })
        },
        width: '400px'
      });
    });
    $('#modal-material-list').on('click', '.bt-enable', function () {
      let command = $(this).hasClass('btn-outline-success') ? 'disableMaterial' : 'enableMaterial';
      let row = $(this).closest('.item');
      let mid = row.data('mid');
      let button = $(this);
      let icon = $(this).find('> i');
      // let confirmDialog = app.gui.confirm('Delete this material: ' + name + ' ?', {
      //   'positive-callback': function () {
      app.ajax.post(
        baseUrl + 'adminApi/' + command, {
          mid: mid
        }
      ).then(function (data) {
        button.toggleClass('btn-outline-success').toggleClass('btn-outline-danger');
        icon.toggleClass('fa-check-circle').toggleClass('fa-times-circle');
      });
    });
    $('#modal-material-list').on('click', '.bt-edit', function () {
      let row = $(this).closest('.item');
      let mid = row.data('mid');
      let name = row.data('name');
      let fid = row.data('fid');
      $('#modal-material-list').modal('hide');
      app.gui.modal('#modal-edit-material');
      $('#modal-edit-material .input-material-name').val(name).focus();
      $('#modal-edit-material .input-material-name').data('mid', mid);
      $('#modal-edit-material .input-material-fid').val(fid);
    });
    let updateMaterial = function (mid, fid, name, content) {
      app.ajax.post(
        baseUrl + 'adminApi/updateMaterial', {
          material: name,
          fid: fid,
          content: content,
          mid: mid
        }
      ).then(function (data) {
        app.gui.notify('Material updated.', {
          type: 'success'
        });
        $('#modal-kit .bt-topic-management').click();
        $('#modal-edit-material').modal('hide');
      });
    };
    $('#modal-edit-material').on('click', '.bt-ok', function () {
      let name = $('#modal-edit-material .input-material-name').val();
      let content = '';
      let fid = $('#modal-edit-material .input-material-fid').val();
      let mid = $('#modal-edit-material .input-material-name').data('mid');
      if (name.trim().length > 0) {
        updateMaterial(mid, fid.trim().toUpperCase(), name.trim(), content.trim());
      }
    });
    $('#modal-edit-material').on('click', '.bt-cancel', function () {
      $('#modal-kit .bt-topic-management').click();
    })
    $('#modal-material-list').on('click', '.bt-group', function () {
      let row = $(this).closest('.item');
      let mid = row.data('mid');
      let name = row.data('name');
      let fid = row.data('fid');
      // $('#modal-material-list').modal('hide');
      app.ajax.get(
        baseUrl + 'adminApi/getGroupMaterial/' + mid
      ).then(function (groups) {
        let inList = '';
        groups.in.forEach((group) => {
          inList += '<div class="d-flex item align-items-center justify-content-between list" data-mid="' + mid + '" data-gid="' + group.gid + '" data-name="' + group.name + '" data-type="' + group.type + '" data-class="' + group.class + '" data-grade="' + group.grade + '">';
          inList += '<span><span class="badge badge-info">' + group.type + '</span> ';
          if (group.grade) inList += '<span class="badge badge-warning">' + group.grade + '</span> ';
          if (group.class) inList += '<span class="badge badge-primary">' + group.class + '</span> ';
          inList += '<br>' + group.name + '</span>'
          inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> ' + app.l.get('remove') + '</button>';
          inList += '</div>';
        });
        $('#modal-material-group .in-list').html(inList);
        let notInList = '';
        groups.notin.forEach((group) => {
          notInList += '<div class="d-flex item align-items-center justify-content-between list" data-mid="' + mid + '" data-gid="' + group.gid + '" data-name="' + group.name + '" data-type="' + group.type + '" data-class="' + group.class + '" data-grade="' + group.grade + '">';
          notInList += '<span><span class="badge badge-info">' + group.type + '</span> ';
          if (group.grade) notInList += '<span class="badge badge-warning">' + group.grade + '</span> ';
          if (group.class) notInList += '<span class="badge badge-primary">' + group.class + '</span> ';
          notInList += '<br>' + group.name + '</span>'
          notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> ' + app.l.get('add') + '</button>';
          notInList += '</div>';
        });
        $('#modal-material-group .not-in-list').html(notInList);
        app.gui.modal('#modal-material-group', {
          width: '600px'
        });
      });
    });
    $('#modal-material-group').on('click', '.bt-add', function () {
      let row = $(this).closest('.item');
      let mid = row.data('mid')
      let group = {};
      group.gid = row.data('gid');
      group.name = row.data('name');
      group.type = row.data('type');
      group.class = row.data('class');
      group.grade = row.data('grade');
      app.ajax.get(
        baseUrl + '/adminApi/addGroupToMaterial/' + group.gid + '/' + mid
      ).then(function (response) { // console.log(response);
        if (!parseInt(response)) return;
        let inList = '';
        inList += '<div class="d-flex item align-items-center justify-content-between list" data-mid="' + mid + '" data-gid="' + group.gid + '" data-name="' + group.name + '" data-type="' + group.type + '" data-class="' + group.class + '" data-grade="' + group.grade + '">';
        inList += '<span><span class="badge badge-info">' + group.type + '</span> ';
        if (group.grade) inList += '<span class="badge badge-warning">' + group.grade + '</span> ';
        if (group.class) inList += '<span class="badge badge-primary">' + group.class + '</span> ';
        inList += '<br>' + group.name + '</span>'
        inList += '<button class="bt-remove btn btn-sm btn-outline-danger"><i class="fas fa-long-arrow-alt-right"></i> ' + app.l.get('remove') + '</button>';
        inList += '</div>';
        $('#modal-material-group .in-list').prepend(inList);
        row.slideUp({
          complete: function () {
            $(this).removeClass('d-flex')
          }
        });
      })
    })
    $('#modal-material-group').on('click', '.bt-remove', function () {
      let row = $(this).closest('.item');
      let mid = row.data('mid')
      let group = {};
      group.gid = row.data('gid');
      group.name = row.data('name');
      group.type = row.data('type');
      group.class = row.data('class');
      group.grade = row.data('grade');
      app.ajax.get(
        baseUrl + '/adminApi/removeGroupFromMaterial/' + group.gid + '/' + mid
      ).then(function (response) { // console.log(response);
        if (!parseInt(response)) return;
        let notInList = '';
        notInList += '<div class="d-flex item align-items-center justify-content-between list" data-mid="' + mid + '" data-gid="' + group.gid + '" data-name="' + group.name + '" data-type="' + group.type + '" data-class="' + group.class + '" data-grade="' + group.grade + '">';
        notInList += '<span><span class="badge badge-info">' + group.type + '</span> ';
        if (group.grade) notInList += '<span class="badge badge-warning">' + group.grade + '</span> ';
        if (group.class) notInList += '<span class="badge badge-primary">' + group.class + '</span> ';
        notInList += '<br>' + group.name + '</span>'
        notInList += '<button class="bt-add btn btn-sm btn-outline-success"><i class="fas fa-long-arrow-alt-left"></i> ' + app.l.get('add') + '</button>';
        notInList += '</div>';
        $('#modal-material-group .not-in-list').prepend(notInList);
        row.slideUp({
          complete: function () {
            $(this).removeClass('d-flex')
          }
        });
      })
    })












    $('#bt-close-map').on('click', function () {
      let confirm = app.gui.confirm(app.l.get('close-map-ask'), {
        'positive-callback': function () {
          app.session.unset('gmid');
          app.session.unset('mid');
          app.goalmap = null;
          app.material = {
            mid: null,
            name: null,
            content: null
          }
          app.canvas.clearCanvas();
          app.canvas.reset();
          app.canvas.enableNodeCreation(false);
          app.enableCloseMap(false);
          app.enableSaveAs(false);
          app.enableSave(false);
          app.unsetMaterial();
          app.logger.setMid(undefined);
          $(".map-name").html(app.l.get('map-is-not-loaded'));
          $(".last-update").html(app.l.get('n-a'));
          confirm.modal('hide');
          if ($('#popup-material-content').is(':visible'))
            $('#popup-material-content').hide();
        }
      })
    })

    // CMM - Map Creation Support 

    $('#bt-support').on('click', function () {
      let nlp = app.material.nlp;
      let mid = app.material.mid;
      if (mid == null) {
        app.gui.notify('Invalid topic.<br>Please create new/open a map to select a topic.', {
          type: 'warning'
        });
        return;
      }
      if (nlp == undefined || nlp == null || !app.support) {
        app.ajax.get(baseUrl + 'kitbuildApi/getCMMMaterialByMid/' + mid).then(function (cmmMaterial) {
          // console.log(cmmMaterial);
          if (cmmMaterial.nlp == null) {
            app.gui.notify('This material has no NLP support data, therefore support feature cannot be used.', {
              type: 'warning'
            });
            return;
          }
          app.setMaterialContentNlp(cmmMaterial.content, cmmMaterial.nlp);
          if(!app.support) {
            app.support = new Support(cmmMaterial.content, cmmMaterial.nlp)
              .setCanvas(app.canvas)
              .setGui(app.gui);
          } else app.support.setContentNLP(cmmMaterial.content, cmmMaterial.nlp);
          app.support.show();
        })
      } else app.support.show();
      // console.log(app.material);
    })

    $('#bt-content').on('click', function () {
      let visible = $('#popup-material-content').is(':visible');
      let mid = app.material.mid;
      let content = app.material.content;
      // console.log(mid, content);
      if (mid == null || mid == undefined) {
        app.gui.notify('Please open a topic/create a new map.', {
          type: 'warning'
        });
        return;
      }
      if (!visible) {
        $('#popup-material-content .material-title').html(app.material.name);
        if (content == null || content == null) {
          app.ajax.get(baseUrl + 'kitbuildApi/getCMMMaterialByMid/' + mid).then(function (cmmMaterial) {
            // console.log(cmmMaterial);
            if (cmmMaterial.content == null) {
              app.gui.notify('This material has no content data.', {
                type: 'warning'
              });
              return;
            }
            app.setMaterialContentNlp(cmmMaterial.content, cmmMaterial.nlp);
            $('#popup-material-content .material-content').html(app.gui.nl2br(cmmMaterial.content));
          })
        } else $('#popup-material-content .material-content').html(app.gui.nl2br(content));
      }
      $('#popup-material-content').toggle({
        duration: 0,
        complete: function () {
          // let v = $('#popup-material-content').is(':visible');
          // console.error(v);
        }
      }).draggable({
        cursor: 'move',
        handle: '.move-handler'
      }).resizable();
    });

    $('#popup-material-content .bt-close').on('click', function () {
      $('#popup-material-content').hide();
    })

    $('#popup-material-content .bt-top').on('click', function () {
      $('#popup-material-content .material-content-container').animate({
        scrollTop: 0
      })
    })

    $('#popup-material-content .bt-more').on('click', function () {
      let scrollTop = $('#popup-material-content .material-content-container').scrollTop();
      let h = $('#popup-material-content .material-content-container').height();
      h -= app.gui.em2px() + 3;
      $('#popup-material-content .material-content-container').animate({
        scrollTop: scrollTop + h
      });
    })


    $('#bt-save').on('click', function () {
      let mid = app.material.mid;
      let uid = app.user.uid;
      let gmid = app.goalmap ? app.goalmap.goalmap.gmid : null

      if (!gmid) {
        $('#bt-save-as').click();
        return;
      }
      app.saveGoalmapOverwrite(gmid, 'draft', function (gmid) {
          app.gui.notify(app.l.get('map-saved'), {
            type: 'success'
          })
        },
        function (error) {
          console.error(error);
          app.gui.notify('Error occured during saving your map data. Your map data may not be saved', {
            type: 'danger'
          });
        });
    });

    $('#bt-set-as-kit').on('click', function () {
      let mid = app.material.mid;
      let uid = app.user.uid;
      let gmid = app.goalmap ? app.goalmap.goalmap.gmid : null

      if (!gmid) {
        $('#bt-save-as').click();
        return;
      }
      let confirm = app.gui.confirm(app.l.get('ask-set-map-as-kit'), {
        'positive-callback': function () {
          app.saveGoalmapOverwrite(gmid, 'kit', function (gmid) {
            app.gui.notify(app.l.get('map-saved-as-kit'), {
              type: 'success'
            })
          }, function (type, error) {
            app.gui.notify(app.l.get('unable-save-map'), {
              type: 'danger',
              delay: 0
            });
            app.gui.notify(app.l.get('tag-set-to', type), {
              type: 'success',
              delay: 0
            });
          });
          confirm.modal('hide');
        },
        'negative-callback': function () {
          confirm.modal('hide');
        }
      })
    });

    $('#bt-save-as').on('click', function () {
      let mid = app.material.mid;
      let uid = app.user.uid;
      if (mid != null && uid != null) {
        let name = 'draft-m' + mid + '-u' + uid + '-' + app.user.username;
        if (app.goalmap) name = app.goalmap.goalmap.name + ' (copy)';
        $('#modal-map-name').modal('show');
        $('#modal-map-name .map-name-input').val(name).focus().select();
      } else app.gui.notify(app.l.get('invalid-user-topic'), {
        type: 'danger'
      });
    });

    $('#bt-create-kit').on('click', function () { // console.error(this);
      let gmid = app.goalmap ? app.goalmap.goalmap.gmid : null
      if (!gmid) {
        app.gui.notify(app.l.get('please-open-or-save-map-first'), {
          type: 'warning'
        })
        return;
      }
      let confirm = app.gui.confirm(app.l.get('confirm-create-kit-from-goalmap'), {
        'positive-callback': function () {
          app.logger.log('begin-create-kit', {
            gmid: app.goalmap.goalmap.gmid
          });
          $('#bt-save').click();
          confirm.modal('hide');
          app.stateChange(CmapApp.KitMaking, null, function () {
            app.session.set('state', CmapApp.KitMaking);
            app.gui.notify(app.l.get('you-in-kit-creation-mode'));
          });
        }
      })

    })

    $('#bt-concept-mapping').on('click', function () {
      let confirm = app.gui.confirm(app.l.get('confirm-return-to-cmap'), {
        'positive-callback': function () {
          app.stateChange(CmapApp.ConceptMapping, null, function () {
            app.session.set('state', CmapApp.ConceptMapping);
            // app.showMap(app.goalmap, false, function () {
            app.loadGoalmap(app.goalmap.goalmap.gmid, function (goalmap) {
              app.gui.notify(app.l.get('goalmap-is-loaded'));
              app.showMap(goalmap);
              $('#bt-center').click();
              confirm.modal('hide');
            });
            app.logger.log('return-concept-mapping', {
              gmid: app.goalmap.goalmap.gmid
            });
          });
        },
        'negative-callback': function () {
          confirm.modal('hide');
        }
      });
    })

    $('#popup-map').on('click', '.bt-cancel', function () {
      $('#popup-map').hide();
    });

    $('#popup-map').on('click', '.bt-download', function () {
      let mid = app.material.mid;
      let uid = app.user.uid;
      var a = document.createElement("a"); //Create <a>
      a.href = $('#popup-map').find('img.cmap').attr('src');
      a.download = "concept-map-m" + mid + "-u" + uid + ".png"; //File name Here
      a.click(); //Downloaded file
    })

    $('#popup-map').on('click', '.bt-continue', function () {
      let mid = app.material.mid;
      let uid = app.user.uid;
      if (mid != null && uid != null) {
        $('#modal-map-name').modal('show');
        $('#modal-map-name .map-name-input').val('draft-m' + mid + '-u' + uid + '-' + app.user.username).focus().select();
      } else app.gui.notify(app.l.get('invalid-user-topic'), {
        type: 'danger'
      });
    });

    $('#modal-map-name').on('click', '.bt-ok', function () {
      let mid = app.material.mid;
      let uid = app.user.uid;
      if (mid != null && uid != null) {
        let name = $('#modal-map-name .map-name-input').val().trim()
        if (name == '')
          app.gui.dialog(app.l.get('enter-map-name'))
        else {
          app.saveGoalmapDraft(mid, uid, name, function (gmid) {
            $('#modal-map-name').modal('hide');
            $('#popup-map').hide();
            app.gui.notify(app.l.get('concept-map-saved'), {
              type: 'success'
            })
            app.loadGoalmap(gmid, function (goalmap) {
              app.setGoalmap(goalmap)
              app.enableSave();
              app.enableSetAsKit();
            });
            app.eventListeners.forEach(listener => {
              if (listener && typeof listener.onAppEvent == 'function') {
                listener.onAppEvent('save-map', {
                  gmid: gmid
                });
              }
            })
          });
        }
      }
    });






    // Kit-Making

    $('#bt-reset-all').on('click', function () {
      let confirm = app.gui.confirm(app.l.get('confirm-reset-all'), {
        'positive-callback': function () {
          confirm.modal('hide')
          app.showMap(app.goalmap, false, function () {
            $('#bt-center').click();
          });
          app.logger.log('reset-all', EventListener.addMapStateData(app.canvas.getCy(), {
            gmid: app.goalmap.goalmap.gmid
          }));
        },
      });
    })

    $('#bt-reset-all-edges').on('click', function () {
      let confirm = app.gui.confirm(app.l.get('confirm-reset-all-links'), {
        'positive-callback': function () {
          app.canvas.getCy().edges().remove();
          app.showPropositions();
          confirm.modal('hide')
          app.logger.log('restore-all-edges', EventListener.addMapStateData(app.canvas.getCy(), {
            gmid: app.goalmap.goalmap.gmid
          }));
        },
        'negative-callback': function () {
          confirm.modal('hide')
        }
      })
    })

    $('#bt-remove-all-edges').on('click', function () {
      let confirm = app.gui.confirm(app.l.get('confirm-remove-all-connections'), {
        'positive-callback': function () {
          app.canvas.getCy().edges().remove();
          confirm.modal('hide')
          app.logger.log('remove-all-edges', EventListener.addMapStateData(app.canvas.getCy(), {
            gmid: app.goalmap.goalmap.gmid
          }));
          app.canvas.getCanvasTool().clear();
          app.canvas.getCy().nodes(":selected").unselect();
        },
        'negative-callback': function () {
          confirm.modal('hide')
        }
      })
    })

    $('#bt-remove-left-edge').on('click', function () {
      let len = app.canvas.getCy().edges('[type="left"]').length;
      let dialog = (len ? app.l.get('confirm-remove-all-outgoing-connections') : app.l.get('confirm-restore-all-outgoing-connections'))
      let confirm = app.gui.confirm(dialog, {
        'positive-callback': function () {
          if (len) {
            app.canvas.getCy().edges('[type="left"]').remove();
            app.logger.log('remove-left-edges', EventListener.addMapStateData(app.canvas.getCy(), {
              gmid: app.goalmap.goalmap.gmid
            }));
          } else {
            app.showPropositions('left');
            app.logger.log('restore-left-edges', EventListener.addMapStateData(app.canvas.getCy(), {
              gmid: app.goalmap.goalmap.gmid
            }));
          }
          confirm.modal('hide')
        },
        'negative-callback': function () {
          confirm.modal('hide')
        }
      })
    })

    $('#bt-remove-right-edge').on('click', function () {
      let len = app.canvas.getCy().edges('[type="right"]').length;
      let dialog = (len ? app.l.get('confirm-remove-all-incoming-connections') : app.l.get('confirm-restore-all-incoming-connections'))
      let confirm = app.gui.confirm(dialog, {
        'positive-callback': function () {
          if (len) {
            app.canvas.getCy().edges('[type="right"]').remove();
            app.logger.log('remove-right-edges', EventListener.addMapStateData(app.canvas.getCy(), {
              gmid: app.goalmap.goalmap.gmid
            }));
          } else {
            app.showPropositions('right');
            app.logger.log('restore-right-edges', EventListener.addMapStateData(app.canvas.getCy(), {
              gmid: app.goalmap.goalmap.gmid
            }));
          }
          confirm.modal('hide')
        },
        'negative-callback': function () {
          confirm.modal('hide')
        }
      })
    })

    $('#bt-save-kit').on('click', function () {
      if (!app.kitmap) {
        $('#bt-save-kit-as').click();
        return;
      }
      let isDirected = (app.canvas.settings.isDirected) ? app.l.get('directed') : app.l.get('undirected');
      let color = (app.canvas.settings.isDirected) ? 'success' : 'danger';
      $('#proposition-direction .is-directed').html(app.l.get('is-directed', color, isDirected));
      $('#modal-kit-name input.kit-name-input').val(app.kitmap.kitmap.name);
      $('#modal-kit-name .bt-ok').attr('data-kid', app.kitmap.kitmap.kid);
      $('input[type="radio"][name="layout"][value="' + app.kitmap.kitmap.layout + '"]').prop('checked', 'checked');
      if (parseInt(app.kitmap.kitmap.enabled)) $('input.kit-input-enabled').prop('checked', 'checked');
      else $('input.kit-input-enabled').prop('checked', false);
      $('#modal-kit-name').modal('show');

    })

    $('#bt-save-kit-as').on('click', function () {
      let dialogMessage = app.l.get('confirm-save-kit-as');
      let confirm = app.gui.confirm(dialogMessage, {
        'positive-callback': function () {
          confirm.modal('hide');
          let isDirected = (app.canvas.settings.isDirected) ? app.l.get('directed') : app.l.get('undirected');
          let color = (app.canvas.settings.isDirected) ? 'success' : 'danger';
          $('#proposition-direction .is-directed').html(app.l.get('is-directed', color, isDirected));
          $('#modal-kit-name .bt-ok').removeAttr('data-kid');
          if (app.kitmap) {
            $('#modal-kit-name input.kit-name-input').val(app.kitmap.kitmap.name + ' (copy)');
            $('input[type="radio"][name="layout"][value="' + app.kitmap.kitmap.layout + '"]').prop('checked', 'checked');
          } else {
            $('input[type="radio"][name="layout"][value="preset"]').prop('checked', 'checked');
          }
          $('#modal-kit-name').modal('show');
        },
        'negative-callback': function () {
          confirm.modal('hide');
        }
      })
    });

    $('#modal-kit-name').on('click', '.bt-ok', function () {
      let dialog = this;
      let kid = $(this).attr('data-kid');
      let gmid = app.goalmap.goalmap.gmid;
      let uid = app.user.uid;
      let name = $('#modal-kit-name .kit-name-input').val().trim();
      if (name == '') name = undefined;
      let layout = $('#modal-kit-name input[type="radio"][name="layout"]:checked').val();
      let directed = app.canvas.settings.isDirected;
      let enabled = $('#modal-kit-name input.kit-input-enabled:checked').val();

      app.saveKitmap(gmid, uid, name, layout, directed, enabled, kid, function (kid) {

        app.kid = kid;
        app.session.set('kid', kid, function () {
          app.loadKitmap(kid, function (kitmap) {
            app.kitmap = kitmap;
            app.showKit(kitmap);
            $('#modal-kit-name').modal('hide');
            let what = app.l.get('kit');
            let message = app.l.get('object-is-saved-successfully', what);

            app.gui.notify(message, {
              type: 'success'
            });
          })
        });
      })
    })

    $('#bt-open-saved-kit').on('click', function () {
      if (!app.goalmap) {
        app.gui.notify(app.l.get('invalid-goalmap'), {
          type: 'danger',
          delay: 0
        });
        return;
      }
      let gmid = app.goalmap.goalmap.gmid;
      app.ajax.get(baseUrl + 'kitbuildApi/getGoalmapKits/' + gmid + '/false').then(function (kits) {
        let kitList = kits.length ? '' : '<em>' + app.l.get('no-saved-kits-available') + '</em>';
        for (let kit of kits) {
          kitList += '<div class="list list-hover list-pointer pt-1 pb-1 pl-2 pr-2 ml-2 mr-2" data-kid="' + kit.kid + '" data-name="' + kit.name + '">'
          kitList += '<span class="d-block">' + kit.name + '</span> ';
          kitList += parseInt(kit.enabled) ? '<span class="badge badge-success">Enabled</span> ' :
            '<span class="badge badge-danger">Disabled</span> ';
          kitList += ' <span class="badge badge-info">' + kit.authorname + '</span> ';
          kitList += '<span class="badge badge-warning">' + kit.update_time + '</span> ';
          kitList += '</div>';
        }
        $('#modal-open-kit-list .kit-list').html(kitList)
        $('#modal-open-kit-list').modal('show');
      })

    })

    $('#modal-open-kit-list').on('click', '.list', function () {
      let kid = $(this).data('kid');
      let confirmDialog = app.gui.confirm(app.l.get('confirm-open-this-kit', $(this).data('name')), {
        'positive-callback': function () {
          app.loadKitmap(kid, function (kitmap) {
            app.kitmap = kitmap;
            app.session.set('kid', kid);
            app.showKit(kitmap, function () {
              $('#modal-open-kit-list').modal('hide');
              confirmDialog.modal('hide');
              $('#bt-center').click();
              app.gui.notify(app.l.get('kit-loaded'), {
                type: 'success'
              });
              app.logger.log('open-kit', {
                kid: kid,
                gmid: kitmap.kitmap.gmid
              })
            });
          })
        },
        'width': '300px'
      })
    })









    // Kit-Building


    $('.kit-builder-toolbar .bt-open-kit').on('click', function () {
      if (!app.user) return;
      if (app.user.role == 'KIT') $('#modal-kit .bt-new').addClass('d-none');
      app.ajax.post(
        baseUrl + 'kitbuildApi/getMaterialsWithGids', {
          gids: app.user.gids
        }
      ).then(function (materials) {
        let materialList = (!materials || materials.length == 0) ?
          '<small><em class="text-secondary">' + app.l.get('no-topic-available') + '</em></small>' :
          '';
        materials.forEach((material) => {
          materialList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pt-1 pb-1 pl-2 pr-2" data-mid="' + material.mid + '" data-name="' + material.name + '">'
          materialList += '<span class="material-name">' + material.name + '</span>'
          materialList += '<i class="fas fa-check text-primary d-none"></i>'
          materialList += '</div>'
        });
        $('#modal-kit .material-list').html(materialList);
        $('#modal-kit .goalmap-list').html('');
        app.gui.modal('#modal-kit', {
          'width': '600px'
        });
      })
    });

    let saveLearnerMap = function (callback) {
      if (!app.kitmap || !app.goalmap) {
        app.gui.notify(app.l.get('please-open-kit'), {
          type: 'warning'
        })
        return;
      }
      let gmid = app.goalmap.goalmap.gmid;
      let uid = app.user.uid;
      let lmid = app.learnermap ? app.learnermap.learnermap.lmid : null;
      let kid = app.kitmap.kitmap.kid;


      if (gmid != null && uid != null && kid != null) {
        app.saveLearnermapOverwrite(gmid, kid, uid, 'draft', lmid, function (learnermap) {
          app.setLearnermap(learnermap);
          if (typeof callback == "function") {
            callback(learnermap);
          }
          app.session.set('lmid', learnermap.learnermap.lmid, function () {
            app.gui.notify(app.l.get('concept-map-saved'), {
              type: 'success'
            })
          });
        });
      }
    }

    $('.kit-builder-toolbar .bt-save').on('click', saveLearnerMap);

    $('.kit-builder-toolbar .bt-load-draft').on('click', function () {
      if (!app.goalmap || !app.kitmap) {
        app.gui.notify(app.l.get('please-open-kit'), {
          type: 'warning'
        })
        return;
      }
      let gmid = app.goalmap.goalmap.gmid;
      let kid = app.kitmap.kitmap.kid;
      let uid = app.user.uid;
      let lmid = app.learnermap ? app.learnermap.learnermap.lmid : null;

      if (gmid != null && uid != null && kid != null) {
        app.loadSavedLearnermap(gmid, kid, uid, 'draft', function (learnermap) {
          if (!learnermap) {
            app.gui.notify(app.l.get('previous-map-not-found'), {
              type: 'warning'
            })
            return;
          }
          let doShowLearnermap = function () {
            app.showLearnermap(learnermap, () => {
              app.canvas.canvasTool.clear();
              $('#bt-center').click();
              app.setLearnermap(learnermap);
              app.canvas.toolbar.getAction().clearStack();
              app.session.set('lmid', learnermap.learnermap.lmid, function () {
                app.gui.notify(app.l.get('map-loaded'), {
                  type: 'success'
                })
              });
            });
          }
          if (app.canvas.getCy().edges().length > 0) {
            let confirm = app.gui.confirm(app.l.get('do-you-want-load-saved-maps'), {
              'positive-callback': function () {
                doShowLearnermap();
                confirm.modal('hide');
              }
            })
          } else doShowLearnermap();
        });
      }
    })

    $('#bt-feedback').on('click', function () {
      if (!app.goalmap) {
        app.gui.notify(app.l.get('please-open-kit'), {
          type: 'warning'
        })
        return;
      }
      console.warn("app.getCanvas().getCy().edges()")
      console.warn(app.getCanvas().getCy().edges())
      app.getCanvas().getCy().edges().data("link", "normal")
    let giveFeedback = function (lm) {
      let concepts = app.canvas.getCy().nodes('[type="concept"]');
        let lConcept = 0;
        for (let concept of concepts) {
          if (concept.connectedEdges().length == 0) lConcept++;
        }
        let learnermap = app.analyzerLib.buildLearnermap(app.canvas.getCy());
        let compare = app.analyzerLib.compare(learnermap, app.goalmap);
        console.log("learner map from bt-feedback")
        console.table(learnermap)
        let message = '';
        if (compare.excess.length > 0) {
          // message += '<p><i class="fas fa-exclamation-triangle text-warning"></i> ' + app.l.get('you-have-excess', compare.excess.length); + '</em></p>';
          for (let e of compare.excess) {
            let link = app.canvas.getCy().nodes('#l-' + e.lid);
            if (link.length) {
              link.connectedEdges().data('link', 'hint');
            }
          }
        }
        // excessive propositions
        if (compare.excessP.length > 0) {
          for (let e of compare.excessP) {
            let link = app.canvas.getCy().nodes('#l-' + e.lid);
            if (link.length) {
              // link.connectedEdges("[target='c-3'], [target='c-1']").data('link', 'hint');
              link.connectedEdges("[target='c-"+e.target+"']").data('link', 'hint');
            }
          }
        }
        if (compare.nExcessP>0) {
          message += '<p><i class="fas fa-exclamation-triangle text-warning"></i> ' + app.l.get('you-have-excess', compare.nExcessP); + '</em></p>';
        }
        if (lConcept) message += '<p><i class="fas fa-exclamation-triangle text-warning"></i> ' + app.l.get('you-have-unconnected-concept', lConcept) + '</p>';
        // if (compare.leave.length) message += '<p><i class="fas fa-exclamation-triangle text-warning"></i> ' + app.l.get('you-have-unconnected-link', compare.leave.length) + '</p>';
        if (compare.nLeaveP) message += '<p><i class="fas fa-exclamation-triangle text-warning"></i> ' + app.l.get('you-have-unconnected-link', compare.nLeaveP) + '</p>';
        if (message.trim() != '') app.gui.dialog(message);
        app.logger.setKid(app.kitmap.kitmap.kid);
        app.eventListeners.forEach(listener => {
          if (listener && typeof listener.onAppEvent == 'function') {
            listener.onAppEvent('get-feedback', {
              excess: compare.nExcessP,
              leave: compare.nLeaveP,
              compare: compare,
              lmid: lm.learnermap.lmid
            });
          }
        })
    }
/*
      match: matchLinks,
      leave: leaveLLinks,
      nLeaveP: numberOfLeavePropositions,
      excess: excessLLinks,
      excessP: excessLPropositions,
      nExcessP: numberOfExcessPropositions,
      miss: missGLinks,
      matchP: matchGPropositions
      */
    if (app.learnermap) {
        giveFeedback(app.learnermap);
      }else {
        saveLearnerMap(function(lm) {
          giveFeedback(lm);
        })

      }
    })

    $('#bt-upload').on('click', function () {
      if (!app.goalmap || !app.kitmap) {
        app.gui.notify(app.l.get('please-open-kit'), {
          type: 'warning'
        })
        return;
      }
      let confirm = app.gui.confirm(app.l.get('upload-end'), {
        'positive-callback': function () {
          let gmid = app.goalmap.goalmap.gmid;
          let kid = app.kitmap.kitmap.kid;
          let uid = app.user.uid;
          let lmid = app.learnermap ? app.learnermap.learnermap.lmid : null;
          if (gmid != null && uid != null) {
            app.saveLearnermapOverwrite(gmid, kid, uid, 'fix', lmid, function (learnermap) {
              confirm.modal('hide');
              app.setLearnermap(learnermap);
              app.logger.log('learnermap-uploaded', {
                lmid: learnermap.lmid,
                gmid: gmid,
                kid: kid
              })
              app.session.setMulti({
                'lmid': learnermap.learnermap.lmid,
                'uploaded': true
              }, function () {
                app.gui.notify(app.l.get('concept-map-uploaded'), {
                  type: 'success'
                });
                app.stateChange(CmapApp.Compare, null, function () {
                  $('#bt-compare').click();
                  app.logger.log('begin-compare', {
                    lmid: learnermap.lmid,
                    gmid: gmid,
                    kid: kid
                  });
                })
              });
            });
          }
        }
      })
    });

    $('#bt-compare').on('click', function () {

      if (!app.learnermap || app.learnermap.learnermap.type != 'fix') {
        app.gui.notify(app.l.get('please-upload-map-to-analyze'), {
          type: 'warning'
        });
        return;
      }
      app.canvas.canvasTool.clear();
      app.canvas.canvasTool.enableNodeTools(false);
      app.canvas.canvasTool.enableConnectionTools(false);

      // console.log("learner map from bt-compare")
      // console.table("learnermap")
      // console.table(app.learnermap)
      // console.table("build Learnermap")
      // console.table(app.analyzerLib.buildLearnermap(app.canvas.getCy()))
      app.compareResult = app.analyzerLib.compare(app.learnermap, app.goalmap);
      // let learnermap = app.analyzerLib.buildLearnermap(app.canvas.getCy());
      // app.compareResult = app.analyzerLib.compare(learnermap, app.goalmap);
      /*
      match: matchLinks,
      leave: leaveLLinks,
      nLeaveP: numberOfLeavePropositions,
      excess: excessLLinks,
      excessP: excessLPropositions,
      nExcessP: numberOfExcessPropositions,
      miss: missGLinks,
      matchP: matchGPropositions
      */
      app.analyzerLib.drawAnalysisLinks(app.canvas.getCy(),
        app.compareResult.match, app.compareResult.matchP, app.compareResult.leave, app.compareResult.excess, app.compareResult.excessP, app.compareResult.miss, {
          match: $('#bt-matching-links').is(':checked'),
          leave: $('#bt-leaving-links').is(':checked'),
          excess: $('#bt-excessive-links').is(':checked'),
          miss: $('#bt-missing-links').is(':checked')
        },
        function () {
          app.showAnalyzerToolbar(true);
          app.updateAnalysisDrawing();
          if (app.analyzerLib.isGraphOutOfView(app.canvas.getCy())) {
            $('#bt-fit').click();
          };
        })
      // app.drawIndividualMap(compareResult.match, compareResult.leave, compareResult.excess, compareResult.miss);
    })

    $('#bt-matching-links').on('change', function () {
      if (app.learnermap) app.updateAnalysisDrawing();
      else app.updateGroupAnalysis();
    })
    $('#bt-leaving-links').on('change', function () {
      if (app.learnermap) app.updateAnalysisDrawing();
      else app.updateGroupAnalysis();
    })
    $('#bt-excessive-links').on('change', function () {
      if (app.learnermap) app.updateAnalysisDrawing();
      else app.updateGroupAnalysis();
    })
    $('#bt-missing-links').on('change', function () {
      if (app.learnermap) app.updateAnalysisDrawing();
      else app.updateGroupAnalysis();
    })

    $('#toolbar-analyzer .bt-help').on('click', function () {
      let content = '<p><span class="badge badge-success">&nbsp;&nbsp;&nbsp;</span> <strong>Matching Links</strong><br>' + app.l.get('prop-match') + '</p>';
      content += '<p><span class="badge badge-danger">&nbsp;&nbsp;&nbsp;</span> <strong>Missing Links </strong><br>' + app.l.get('prop-miss') + '</p>';
      content += '<p><span class="badge" style="background-color: #5bc0eb">&nbsp;&nbsp;&nbsp;</span> <strong>Excessive Links </strong><br>' + app.l.get('prop-excess') + '</p>';
      content += '<p><span class="badge" style="background-color: #ccc">&nbsp;&nbsp;&nbsp;</span> <strong>Leaving Links </strong><br>' + app.l.get('leaving-links') + '</p>';
      app.gui.dialog(content);
    })
  }

  loadMaterial(mid, callback) {
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/getMaterialByMid/' + mid
    ).then(function (material) {
      let visible = $('#popup-material-content').is(':visible');
      if(visible) {
        $('#popup-material-content').fadeOut({ // refreshing content.
          complete: function() {
            $('#bt-content').click();
          }
        });
      }
      if(app.support) {
        app.support.reprocess();
        app.support.hide();
      }
      if (typeof callback == 'function') callback(material);
    }, function () {
      app.gui.notify(app.l.get('unable-load-topic'), {
        type: 'danger'
      })
    })
  }

  setMaterial(mid, name, content) {
    let app = this;
    app.material.mid = mid;
    app.material.name = name;
    app.material.content = content;
    app.material.nlp = null;
    $('#toolbar-material .material-name').html(name);
  }

  unsetMaterial() {
    let app = this;
    this.material.mid = null;
    this.material.name = null;
    this.material.content = null;
    this.material.nlp = null;
    app.canvas.toolbar.enableNodeCreation(false);
    app.enableSaveAs(false);
    $('#toolbar-material .material-name').html(app.l.get('topic-is-not-loaded'));
  }

  setMaterialContentNlp(content, nlp) {
    this.material.content = content;
    this.material.nlp = nlp;
  }

  loadGoalmaps(mid, callback) {
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/getGoalmaps/' + mid + (app.user.role == 'KIT' ? "/true" : '')
    ).then(function (goalmaps) {
      if (typeof callback == 'function') callback(goalmaps);
    })
  }

  loadGoalmap(gmid, callback) {
    let app = this;
    app.ajax.get(baseUrl + 'kitbuildApi/openGoalmap/' + gmid)
      .then(function (goalmap) {
        if (typeof callback == 'function') callback(goalmap);
      })
  }

  loadKitmaps(mid, callback) {
    let app = this;
    app.ajax.get(baseUrl + 'kitbuildApi/getMaterialKits/' + mid + '/true')
      .then(function (kitmaps) {
        if (typeof callback == 'function') callback(kitmaps);
      })
  }


  loadLearnermap(lmid, callback) {
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/getLearnermap/' + lmid
    ).then(function (learnermap) {
      if (typeof callback == 'function') callback(learnermap);
    })
  }

  setGoalmap(goalmap) {
    let app = this;
    app.goalmap = goalmap;
    if (goalmap) {
      $('#toolbar-material .map-name').html(goalmap.goalmap.name);
      $('#toolbar-material .last-update').html(goalmap.goalmap.update_time);
    } else {
      $('#toolbar-material .map-name').html(app.l.get('map-is-not-loaded'));
      $('#toolbar-material .last-update').html(app.l.get('n-a'));
    }
  }

  setKitmap(kitmap) {
    let app = this;
    app.kitmap = kitmap;
    if (kitmap) {
      $('#toolbar-material .kit-name').html(kitmap.kitmap.name);
      $('#toolbar-material .kit-last-update').html(kitmap.kitmap.update_time);
    } else {
      $('#toolbar-material .kit-name').html(app.l.get('kit-is-not-loaded'));
      $('#toolbar-material .kit-last-update').html(app.l.get('n-a'));
    }
  }

  setLearnermap(learnermap) {
    let app = this;
    app.learnermap = learnermap;
    if (learnermap) $('#toolbar-material .last-update').html(learnermap.learnermap.create_time);
    else $('#toolbar-material .last-update').html(app.l.get('n-a'));
  }

  showMap(cmap, asKit = false, callback) {
    let app = this;
    let goalmap = cmap.goalmap;
    let concepts = cmap.concepts;
    let links = cmap.links;
    let targets = cmap.targets;
    let eles = [];
    concepts.forEach((c) => {
      let jData = JSON.parse(c.data);
      let jExtra = JSON.parse(c.extra);
      jData.textColor = jData.textColor && jData.textColor != '' ? jData.textColor : undefined;
      let data = Object.assign({
        id: 'c-' + c.cid,
        name: c.label,
        type: 'concept',
        extras: jExtra
      }, jData);
      eles.push({
        group: 'nodes',
        data: data,
        position: {
          x: parseInt(c.locx),
          y: parseInt(c.locy)
        }
      })
    })
    links.forEach((l) => {
      let jData = JSON.parse(l.data);
      let jExtra = JSON.parse(l.extra);
      let data = Object.assign({
        id: 'l-' + l.lid,
        name: l.label,
        type: 'link',
        extras: jExtra
      }, jData)
      eles.push({
        group: 'nodes',
        data: data,
        position: {
          x: parseInt(l.locx),
          y: parseInt(l.locy)
        }
      })
      if (!asKit) {
        if (l.source != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.source,
              type: 'left'
            }
          })
        }
        if (cmap.targets != "undefined" && cmap.targets != null && cmap.targets.length != null && cmap.targets.length > 0) {
          // console.table(targets);
          targets.forEach((t) => {
            if (t.lid == l.lid) {
              eles.push({
                group: 'edges',
                data: {
                  source: 'l-' + l.lid,
                  target: 'c-' + t.cid,
                  type: 'right'
                }
              })
            }
          })
        }else{
          console.log("empty targets ");
          console.table(targets);
        }
      }
    })
    app.canvas.clearCanvas();
    app.canvas.getCy().add(eles);
    app.canvas.getCanvasTool().clear();
    app.canvas.getCanvasTool().resetState();

    /*if (asKit) {
      app.getCanvas().getCy().nodes().layout({
        name: 'cose-bilkent',
        fit: false,
        stop: () => {
          if (typeof callback == 'function') callback();
        }
      }).run();
    } else {
      if (typeof callback == 'function') callback();
    }*/
      if (typeof callback == 'function') callback();

    app.eventListeners.forEach(listener => {
      if (listener && typeof listener.onAppEvent == 'function') {
        listener.onAppEvent('load-concept-map', {
          gmid: parseInt(cmap.goalmap.gmid)
        });
      }
    })
  }

  showKit(kitmap, callback) {
    let app = this;
    let concepts = kitmap.concepts;
    let links = kitmap.links;
    let targets = kitmap.targets;
    let eles = [];
    concepts.forEach((c) => {
      app.canvas.getCy().nodes('#c-' + c.cid).position({
        x: parseInt(c.locx),
        y: parseInt(c.locy)
      })
    })
    links.forEach((l) => {
      app.canvas.getCy().nodes('#l-' + l.lid).position({
        x: parseInt(l.locx),
        y: parseInt(l.locy)
      })
      if (l.source != null) {
        eles.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.source,
            type: 'left'
          }
        })
      }
      /*if (l.target != null) {
        eles.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.target,
            type: 'right'
          }
        })
      }*/
      if (targets != "undefined" && targets != null && targets.length != null && targets.length > 0) {
          // console.table(targets);
          targets.forEach((t) => {
            if (t.lid == l.lid) {
              eles.push({
                group: 'edges',
                data: {
                  source: 'l-' + l.lid,
                  target: 'c-' + t.cid,
                  type: 'right'
                }
              })
            }
          })
        }else{
          console.log("empty targets ");
          console.table(targets);
        }
    })
    app.canvas.getCy().edges().remove(); // edges only
    app.canvas.getCy().add(eles); // edges only
    $('#toolbar-material .kit-name').html(kitmap.kitmap.name);
    $('#toolbar-material .kit-last-update').html(kitmap.kitmap.update_time);
    let isDirected = parseInt(kitmap.kitmap.directed);
    app.canvas.setDirected(!!isDirected);
    if (typeof callback == 'function') callback();
  }

  showPropositions(which = 'all') {
    let app = this;
    let links = app.goalmap.links;
    let targets = app.goalmap.targets;
    let eles = [];
    links.forEach((l) => {
      let jData = JSON.parse(l.data);
      let jExtra = JSON.parse(l.extra);
      let data = Object.assign({
        id: 'l-' + l.lid,
        name: l.label,
        type: 'link',
        extras: jExtra
      }, jData)
      eles.push({
        group: 'nodes',
        data: data,
        position: {
          x: parseInt(l.locx),
          y: parseInt(l.locy)
        }
      })
      if ((which == 'all' || which == 'left') && l.source != null) {
        eles.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.source,
            type: 'left'
          }
        })
      }
      if ((which == 'all' || which == 'right') && targets != null) {
        targets.forEach((t) => {
          if (t.lid == l.lid) {
            eles.push({
              group: 'edges',
              data: {
                source: 'l-' + l.lid,
                target: 'c-' + t.cid,
                type: 'right'
              }
            })
          }
        })
      }
    })
    app.canvas.getCy().add(eles);
    app.canvas.getCanvasTool().clear();
    app.canvas.getCanvasTool().resetState();
  }

  showLearnermap(cmap, callback) {
    let app = this;
    let concepts = cmap.concepts;
    let links = cmap.links;
    let targets = cmap.targets;
    let eles = [];
    concepts.forEach((c) => {
      app.canvas.getCy().nodes('#c-' + c.cid).position({
        x: parseInt(c.locx),
        y: parseInt(c.locy)
      })
    })
    links.forEach((l) => {
      app.canvas.getCy().nodes('#l-' + l.lid).position({
        x: parseInt(l.locx),
        y: parseInt(l.locy)
      })
      if (l.source != null) {
        eles.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.source,
            type: 'left'
          }
        })
      }
      /*if (l.target != null) {
        eles.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.target,
            type: 'right'
          }
        })
      }*/
      if (targets != "undefined" && targets != null && targets.length != null && targets.length > 0) {
          // console.table(targets);
          targets.forEach((t) => {
            if (t.lid == l.lid) {
              eles.push({
                group: 'edges',
                data: {
                  source: 'l-' + l.lid,
                  target: 'c-' + t.cid,
                  type: 'right'
                }
              })
            }
          })
        }else{
          console.log("empty targets ");
          console.table(targets);
        }
    })
    app.canvas.getCy().edges().remove(); // edges only
    app.canvas.getCy().add(eles); // edges only
    if (typeof callback == 'function') callback();
  }

  saveGoalmap(mid, uid, type, name, callback) {
    let app = this;
    let goalmap = {
      name: name ? name : type + '-m' + mid + '-u' + uid,
      type: type,
      mid: mid,
      creator_id: uid,
      updater_id: uid,
      concepts: [],
      links: [],
    }
    let cs = app.canvas.getNodes('[type="concept"]');
    let ls = app.canvas.getNodes('[type="link"]');
    let es = app.canvas.getEdges();

    cs.forEach(c => {
      goalmap.concepts.push({
        cid: c.data.id.substr(2),
        gmid: null,
        label: c.data.name,
        locx: c.position.x,
        locy: c.position.y,
        extras: c.data
      });
    });
    ls.forEach(l => {
      let tl = {
        lid: l.data.id.substr(2),
        gmid: null,
        label: l.data.name,
        locx: l.position.x,
        locy: l.position.y,
        source: null,
        target: [],
        extras: l.data
      };
      es.forEach((e) => {
        if (e.data.source == l.data.id) {
          if (e.data.type == 'right') tl.target.push(e.data.target.substr(2)); //c-1 substr --> 1
          if (e.data.type == 'left') tl.source = e.data.target.substr(2);
          return;
        }
      });
      goalmap.links.push(tl);
    })
    app.ajax.post(
      baseUrl + 'kitbuildApi/saveGoalmap',
      goalmap
    ).then(function (gmid) {
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('map-save-' + type, {
            gmid: gmid
          });
        }
      })
      if (typeof callback == 'function') callback(gmid)
    })
  }

  saveGoalmapOverwrite(gmid, type = 'draft', callback, callbackError) {

    let app = this;
    let goalmap = {
      type: type,
      gmid: gmid,
      updater_id: app.user.uid,
      concepts: [],
      links: []
    }
    let cs = app.canvas.getNodes('[type="concept"]');
    let ls = app.canvas.getNodes('[type="link"]');
    let es = app.canvas.getEdges();

    cs.forEach(c => {
      goalmap.concepts.push({
        cid: c.data.id.substr(2),
        gmid: null,
        label: c.data.name,
        locx: c.position.x,
        locy: c.position.y,
        extras: c.data
      });
    });
    ls.forEach(l => {
      let tl = {
        lid: l.data.id.substr(2),
        gmid: null,
        label: l.data.name,
        locx: l.position.x,
        locy: l.position.y,
        source: null,
        target: [],
        extras: l.data
      };
      es.forEach((e) => {
        if (e.data.source == l.data.id) {
          if (e.data.type == 'right') tl.target.push(e.data.target.substr(2)); //c-1 substr --> 1
          if (e.data.type == 'left') tl.source = e.data.target.substr(2);
          return;
        }
      });
      goalmap.links.push(tl);
    })
    app.ajax.post(
      baseUrl + 'kitbuildApi/saveGoalmap',
      goalmap
    ).then(function (gmid) {
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('map-save-overwrite', {
            gmid: gmid
          });
        }
      })
      if (typeof callback == 'function') callback(gmid)
    }, function (error) {
      if (typeof callbackError == 'function') callbackError(type, error);
    })
  }

  saveGoalmapDraft(mid, uid, name, callback) {
    this.saveGoalmap(mid, uid, 'draft', name, callback)
  }

  saveKitmap(gmid, uid, name, layout = 'preset', directed, enabled = false, kid = null, callback) {
    let app = this;
    let kitmap = {
      kid: kid ? kid : undefined,
      name: name ? name : 'kit-g' + gmid + '-u' + uid,
      gmid: gmid,
      author: uid,
      layout: layout,
      directed: directed,
      enabled: enabled,
      concepts: [],
      links: [],
    }
    let cs = app.canvas.getNodes('[type="concept"]');
    let ls = app.canvas.getNodes('[type="link"]');
    let es = app.canvas.getEdges();

    cs.forEach(c => {
      kitmap.concepts.push({
        cid: c.data.id.substr(2),
        kid: null,
        gmid: gmid,
        locx: c.position.x,
        locy: c.position.y
      });
    });
    ls.forEach(l => {
      let tl = {
        lid: l.data.id.substr(2),
        kid: null,
        gmid: gmid,
        locx: l.position.x,
        locy: l.position.y,
        source: null,
        target: [],
      };
      es.forEach((e) => {
        if (e.data.source == l.data.id) {
          if (e.data.type == 'right') tl.target.push(e.data.target.substr(2));
          if (e.data.type == 'left') tl.source = e.data.target.substr(2);
          return;
        }
      });
      // console.table(tl.target)
      kitmap.links.push(tl);
    })
    app.ajax.post(
      baseUrl + 'kitbuildApi/saveKitmap',
      kitmap
    ).then(function (kid) {
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('kit-save-' + layout, {
            kid: kid
          });
        }
      })
      if (typeof callback == 'function') callback(kid)
    })
  }

  loadKitmap(kid, callback) {
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/getKitmap/' + kid
    ).then(function (kitmap) {
      if (typeof callback == 'function') callback(kitmap);
    })
  }

  saveLearnermapOverwrite(gmid, kid, uid, type = 'draft', lmid = null, callback) {
    let app = this;
    let learnermaps = {
      lmid: lmid,
      type: type,
      gmid: gmid,
      kid: kid,
      uid: uid,
      concepts: [],
      links: []
    }

    let cs = app.canvas.getNodes('[type="concept"]');
    let ls = app.canvas.getNodes('[type="link"]');
    let es = app.canvas.getEdges();

    cs.forEach(c => {
      learnermaps.concepts.push({
        cid: c.data.id.substr(2),
        gmid: gmid,
        locx: c.position.x,
        locy: c.position.y
      });
    });
    ls.forEach(l => {
      let tl = {
        lid: l.data.id.substr(2),
        gmid: gmid,
        locx: l.position.x,
        locy: l.position.y,
        source: null,
        target: []
      };
      es.forEach((e) => {
        if (e.data.source == l.data.id) {
          if (e.data.type == 'right') tl.target.push(e.data.target.substr(2));
          if (e.data.type == 'left') tl.source = e.data.target.substr(2);
          return;
        }
      });
      learnermaps.links.push(tl);
    })
    app.ajax.post(
      baseUrl + 'kitbuildApi/saveLearnermapOverwrite',
      learnermaps
    ).then(function (learnermap) {
      app.setLearnermap(learnermap);
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('learnermap-save-overwrite', {
            lmid: learnermap.learnermap.lmid
          });
        }
      })
      if (typeof callback == 'function') callback(learnermap)
    })
  }

  loadSavedLearnermap(gmid, kid, uid, type = 'draft', callback) {
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/getLastLearnermap/' + gmid + "/" + kid + "/" + uid + '/' + type
    ).then(function (cmap) {
      if (!cmap) {
        callback(cmap);
        return;
      }
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('load-saved-learnermap', {
            lmid: parseInt(cmap.learnermap.lmid)
          });
        }
      })
      if (typeof callback == 'function') callback(cmap);
    })
  }



  // Analysis

  updateAnalysisDrawing() {
    let app = this;
    if (app.compareResult) {

      if (app.goalmap.removedNodes) {
        for (let rNode of app.goalmap.removedNodes) {
          rNode.restore();
        }
      }
      app.analyzerLib.drawAnalysisLinks(app.canvas.getCy(),
        app.compareResult.match, app.compareResult.matchP,  app.compareResult.leave, app.compareResult.excess, app.compareResult.excessP, app.compareResult.miss, {
          match: $('#bt-matching-links').is(':checked'),
          leave: $('#bt-leaving-links').is(':checked'),
          excess: $('#bt-excessive-links').is(':checked'),
          miss: $('#bt-missing-links').is(':checked')
        },
        function () {
          let nodes = app.canvas.getCy().nodes('[link!="leave"]');
          app.goalmap.removedNodes = [];
          for (let n of nodes) {
            let edges = n.connectedEdges();
            if (edges.length == 0) {
              app.goalmap.removedNodes.push(n);
              n.remove();
            }
          }
        })
    } else app.gui.notify(app.l.get('compare-required'), {
      type: 'warning'
    });
  }

  // UI Manipulation

  enableCloseMap(enabled = true) {
    $('#bt-close-map').attr('disabled', !enabled);
  }

  enableSetAsKit(enabled = true) {
    $('#bt-set-as-kit').attr('disabled', !enabled);
  }

  enableFeedback(enabled = true) {
    $('#bt-feedback').attr('disabled', !enabled);
  }
  
  showKitBuilderToolbar(show = true) {
    if (show) $('.kit-builder-toolbar').removeClass('d-none');
    else $('.kit-builder-toolbar').addClass('d-none');
  }

  enableUpload(enabled = true) {
    $('#bt-upload').attr('disabled', !enabled);
  }
  enableCompare(enabled = true) {
    $('#bt-compare').attr('disabled', !enabled);
  }
  enableOpenKit(enabled = true) {
    $('.kit-builder-toolbar .bt-open-kit').attr('disabled', !enabled);
  }
  enableSaveLearnermap(enabled = true) {
    $('.kit-builder-toolbar .bt-save').attr('disabled', !enabled);
  }
  enableLoadLearnermap(enabled = true) {
    $('.kit-builder-toolbar .bt-load-draft').attr('disabled', !enabled);
  }

  showAnalyzerToolbar(show = true) {
    if (show) $('#toolbar-analyzer').removeClass('d-none');
    else $('#toolbar-analyzer').addClass('d-none');
  }

  // Commanding Kit-Build App Interface

  signIn(uid, username, name, role, gids) {
    let app = this;
    app.user.uid = uid;
    app.user.username = username;
    app.user.name = name;
    app.user.role = role;
    app.user.gids = gids;
    app.logger.setUid(uid); // console.log(app.logger);
    $('#status-bar .user-name').html(app.user.name);
  };

  signOut() {
    let app = this;
    this.reset();
    this.resetUI();
    app.logger.setMid(undefined);
    app.logger.setGmid(undefined);
    $('#status-bar .user-name').html('');
  }

  enableOpenMap(enabled = true) {
    $('#bt-open-map').prop('disabled', !enabled);
  }

  enableConceptMapperToolbar(enabled = true) {
    if (enabled) $('.concept-mapper-toolbar').removeClass('d-none').addClass('d-inline-block');
    else $('.concept-mapper-toolbar').addClass('d-none').removeClass('d-inline-block');
  }

  enableKitBuilderToolbar(enabled = true) {
    if (enabled) $('.kit-builder-toolbar').removeClass('d-none');
    else $('.kit-builder-toolbar').addClass('d-none');
    $('.kit-builder-toolbar .bt-open-kit').prop('disabled', !enabled);
    $('.kit-builder-toolbar .bt-save').prop('disabled', !enabled);
  }

  enableKitCreationToolbar(enabled = false) {
    if (enabled) $('.kit-creation-toolbar').removeClass('d-none')
    else $('.kit-creation-toolbar').addClass('d-none')
  }

  enableMaterial(enabled = true) {
    $('#bt-show-material').prop('disabled', !enabled);
  }

  enableSave(enable = true) {
    $('#bt-save').prop('disabled', !enable);
    $('#bt-load-draft').prop('disabled', !enable);
  }

  enableSaveAs(enable = true) {
    $('#bt-save-as').prop('disabled', !enable);
  }

  enableFinalize(enable = true) {
    $('#bt-finalize').prop('disabled', !enable);
  }

  InitializeREdgeLimit() {
    let app = this
    app.ajax.post(baseUrl + 'MapApi/ReadREdgeLimit/' + app.goalmap.goalmap.gmid).then(e => {
      app.canvas.getCanvasTool().REdgeLimitList = e;
      // console.log("app.canvas.tool");
      // console.log(app.canvas.canvasTool.REdgeLimitList);
    });
  }

  jumpCenterCamera() {
    let app = this;
    if (app.canvas.getCy().nodes().length > 0) {
      app.canvas.getCy().animate({
        center: {
          eles: app.canvas.getCy(),
          padding: 50
        },
        duration: 0
      });
    }
  }

  // State

  stateChange(state, params, callback) {
    let app = this;

    switch (CmapApp.currentState) {
      case CmapApp.ConceptMapping:
        if (state == CmapApp.KitMaking) {
          $('.kit-creation-toolbar').removeClass('d-none');
          $('.concept-mapper-toolbar').addClass('d-none').removeClass('d-block d-inline-block');
          $('#bt-reset-all').prop('disabled', false);
          $('#bt-reset-all-edges').prop('disabled', false);
          $('#bt-remove-all-edges').prop('disabled', false);
          $('#bt-remove-left-edge').prop('disabled', false);
          $('#bt-remove-right-edge').prop('disabled', false);
          $('#bt-open-saved-kit').prop('disabled', false);
          $('#bt-save-kit').prop('disabled', false);
          $('#bt-save-kit-as').prop('disabled', false);
          $('#material-modal .kit-name').html(app.l.get('kit-not-loaded'));
          $('#material-modal .kit-last-update').html(app.l.get('n-a'));
          app.canvas.toolbar.enableNodeCreation(false);
          app.canvas.toolbar.enableColor(false);
          app.canvas.toolbar.action.clearStack();
          app.canvas.canvasTool.enableNodeCreationTools(false);
          app.canvas.canvasTool.enableNodeModificationTools(false);
          app.canvas.canvasTool.enableNodeTools(false);
          app.canvas.canvasTool.enableConnectionTools(true);
          app.canvas.canvasTool.enableREdgeLimit(true);
          app.InitializeREdgeLimit();
          app.canvas.getCanvasTool().clear();
          app.canvas.getCy().nodes(":selected").unselect();
        }
        break;
      case CmapApp.KitMaking:
        if (state == CmapApp.ConceptMapping) {
          app.kitmap = null;
          app.session.unset('kid');
          $('.kit-creation-toolbar').addClass('d-none');
          $('.concept-mapper-toolbar').removeClass('d-none').addClass('d-inline-block');
          $('#bt-reset-all').prop('disabled', true);
          $('#bt-reset-all-edges').prop('disabled', true);
          $('#bt-remove-all-edges').prop('disabled', true);
          $('#bt-remove-left-edge').prop('disabled', true);
          $('#bt-remove-right-edge').prop('disabled', true);
          $('#bt-open-saved-kit').prop('disabled', true);
          $('#bt-save-kit').prop('disabled', true);
          $('#bt-save-kit-as').prop('disabled', true);
          $('#material-modal .kit-name').html(app.l.get('kit-not-loaded'));
          $('#material-modal .kit-last-update').html(app.l.get('n-a'));
          app.canvas.toolbar.enableColor(true);
          app.canvas.toolbar.enableNodeCreation();
          app.canvas.toolbar.action.clearStack();
          app.canvas.canvasTool.enableNodeCreationTools(true);
          app.canvas.canvasTool.enableNodeModificationTools(true);
          app.canvas.canvasTool.enableNodeTools(true);
          app.canvas.canvasTool.enableREdgeLimit(false);
        }
        break;
      case CmapApp.KitBuilding:
        if (state == CmapApp.Compare) {
          app.enableUpload(false);
          app.enableCompare(true);
          app.enableOpenKit(false);
          app.enableSaveLearnermap(false);
          app.enableLoadLearnermap(false);
          app.showAnalyzerToolbar(true);
          app.enableFeedback(false);
          app.canvas.toolbar.enableNodeCreation(false);
          app.canvas.toolbar.action.clearStack();
          app.logger.setGmid(undefined);
          app.logger.setMid(undefined);
        }
        break;
      case CmapApp.Init:
        $('#bt-login').addClass('d-none')
        $('#bt-logout').removeClass('d-none')
        app.enableOpenMap();
        if (state == CmapApp.ConceptMapping) {
          app.enableConceptMapperToolbar();
          app.enableKitBuilderToolbar(false);
          app.canvas.canvasTool.enableNodeTools();
          app.canvas.canvasTool.enableNodeModificationTools();
          app.canvas.canvasTool.enableConnectionTools();
          app.canvas.toolbar.enableClearCanvas();
          app.canvas.toolbar.enableDirection(false);
          app.canvas.toolbar.enableNodeCreation(false);
          app.canvas.setDirected(true);
          app.canvas.canvasTool.enableREdgeLimit(false);
        }
        if (state == CmapApp.KitBuilding) {
          app.enableKitBuilderToolbar();
          app.enableConceptMapperToolbar(false);
          app.canvas.canvasTool.enableNodeModificationTools(false);
          app.canvas.canvasTool.enableConnectionTools(true); // disabled on upload
          app.canvas.toolbar.enableClearCanvas(false);
          app.canvas.toolbar.enableNodeCreation(false);
          app.canvas.toolbar.enableDirection(false); // depends on settings
          app.canvas.setDirected(true);
          // disabled by upload, re-enable it.
          app.enableSaveLearnermap();
          app.enableLoadLearnermap();
          app.enableOpenKit();
          app.enableUpload(false);
          app.enableCompare(false);
          app.enableFeedback(true);

          app.canvas.canvasTool.enableREdgeLimit(true);
          // app.InitializeREdgeLimit() // call it when opening the goalmapkits
        }
        break;
      default:

        break;
    }

    if (state == CmapApp.Init) {
      $('.goalmap-list').html('')
      $('.material-list').html('')
      $('#bt-login').removeClass('d-none')
      $('#bt-logout').addClass('d-none')
      this.enableOpenMap(false);
      this.enableSave(false);
      this.enableCloseMap(false);
      this.enableConceptMapperToolbar(false);
      this.enableKitBuilderToolbar(false);
      this.enableKitCreationToolbar(false);
      this.canvas.clearCanvas();
      this.canvas.reset();
      this.canvas.getCanvasTool().clear();
      this.canvas.getCanvasTool().resetState();
      this.canvas.enableNodeCreation(false);
      this.canvas.toolbar.getAction().clearStack();
      $('#toolbar-material .material-name').html(app.l.get('topic-is-not-loaded'));
      $('#toolbar-material .map-name').html(app.l.get('map-is-not-loaded'));
      $('#toolbar-material .last-update').html(app.l.get('n-a'));
      this.showAnalyzerToolbar(false);

      $.each($('.bt-new').data(), function (i) {
        $(".bt-new").removeAttr("data-" + i);
      });
      $.each($('.bt-open').data(), function (i) {
        $(".bt-open").removeAttr("data-" + i);
      });
    }

    CmapApp.currentState = state;
    app.session.set('state', CmapApp.currentState, function () {
      if (typeof callback == 'function') callback(true);
    })
    return true;
  }

}


// static variables ES6 Support
CmapApp.Init = 'INIT';
CmapApp.ConceptMapping = 'CMAP';
CmapApp.KitMaking = 'KIT';
CmapApp.KitBuilding = 'KB';
CmapApp.Compare = 'COMPARE';
CmapApp.currentState;
CmapApp.targetState;

var BRIDGE = {};

$(function () {

  var canvas = new Canvas('cy', {
    language: 'en',
    // isDirected: false,
    // enableToolbar: false,
    enableNodeCreation: false,
    // enableConceptCreation: false,
    // enableLinkCreation: false,
    // enableUndoRedo: false,
    // enableZoom: false,
    // enableAutoLayout: false,
    // enableSaveImage: false,
    // enableClearCanvas: false
  }).init();

  let app = new CmapApp(canvas); // the app
  let logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
  let eventListener = new EventListener(logger, canvas);
  canvas.attachEventListener(eventListener);
  canvas.attachEventListener(app);
  app.attachEventListener(eventListener);
  app.attachLogger(logger);
  BRIDGE.app = app;

});

/*
bt-feedback builds the learner map using buildLearnermap method of analyzer.lib.js then calls compare function
bt-compare uses the cached learnermap in app.learnermap that is setted previously when loading the map using ajax then calls compare function.
when building the learnermap in both cases I made it to include the targets in link.target instead of making it a seperated targets array.
*/