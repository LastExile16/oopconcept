class AnalyzerApp {

  constructor(canvas) {
    this.l = Language.instance();
    // Language.instance();

    this.canvas = canvas;
    this.gui = new GUI();
    this.analyzerLib = new AnalyzerLib();
    this.ajax = Ajax.ins(this.gui);
    this.session = new Session(baseUrl);
    this.goalmap = {
      goalmap: null,
      concepts: [],
      links: []
    }
    this.learnermaps = [];

    this.handleEvent();

    this.learnermap = null;
    this.compareResult = null;
    this.gCompare = null;

    let app = this;

    $('input[type="range"]').rangeslider();
    $('input[type="range"]').on('input', function (e) {
      let minVal = $('input[data-id="min-range"]').val();
      let maxVal = $('input[data-id="max-range"]').val();
      let id = $(this).data('id');
      let over = false;
      if (id == 'min-range' && parseInt(this.value) > parseInt(maxVal)) {
        $(this).val(maxVal).change();
        over = true;
      } else if (id == 'max-range' && parseInt(this.value) < parseInt(minVal)) {
        $(this).val(minVal).change();
        over = true;
      }
      if (over) return;
      // console.log(minVal, maxVal);
      $('.min-val').html(minVal);
      $('.max-val').html(maxVal);
      app.updateGroupAnalysis();
    });

  }

  getCanvas() {
    return this.canvas;
  }

  handleEvent() {

    let app = this;
    $('#app-menu').removeClass('d-none').addClass('d-inline-block');
    $('.concept-mapper-toolbar').addClass('d-none');
    $('#modal-kit .bt-new').addClass('d-none');

    $('#select-language .language').on('click', function () {
      let currentLang = $('#select-language .active-lang').html().toLowerCase();
      let lang = $(this).data('lang');
      if (currentLang == lang) return;
      let fullLang = lang == 'jp' ? '日本語' : 'English';
      app.session.set('lang', lang, function () {
        $('#select-language .active-lang').html(lang.toUpperCase());
        if (app.canvas.getCy().nodes().length) {
          app.gui.dialog('Language is set to: <strong>' + fullLang + '</strong>. The browser needs to be reloaded for the language settings to apply. Save your work and reload the web browser afterwards.');
          return;
        }
        let dialog = app.gui.dialog('Language is set to: <strong>' + fullLang + '</strong>. The browser needs to be reloaded for the language settings to apply. Reload now?', {
          'positive-callback': function () {
            dialog.modal('hide');
            location.reload();
          }
        });

      })
    })

    $('#bt-open-kit').on('click', function () {
      app.gui.loading('#modal-kit .material-list');
      $.ajax({
        url: baseUrl + 'kitbuildApi/getMaterials',
        method: 'post'
      }).done(function (response) { // console.log(response)
        if (!response.status) {
          app.gui.notify(response.error ? response.error : response, {
            type: 'danger'
          });
          return;
        }
        let materials = response.result;
        let materialList = (!materials || materials.length == 0) ?
          '<em class="text-secondary">No material available</em>' :
          '';
        materials.forEach((material) => {
          materialList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pt-2 pb-2 pl-2 pr-2" data-mid="' + material.mid + '" data-name="' + material.name + '">'
          materialList += material.name
          materialList += '<i class="fas fa-check text-primary" style="display:none"></i>'
          materialList += '</div>'
        });
        $('#modal-kit .material-list').html(materialList);
        app.gui.modal('#modal-kit', {
          'width': '600px'
        });
      }).fail(function (response) {
        app.gui.notify(response, {
          type: 'danger'
        });
      })
    });
    $('#modal-kit .material-list').on('click', '.row', function () {
      if ($(this).hasClass('selected')) return;
      $('#modal-kit .material-list .row').find('i').hide();
      $('#modal-kit .material-list .row').removeClass('selected');
      $(this).find('i').show();
      $(this).addClass('selected')
      let mid = $(this).data('mid');
      let name = $(this).data('name');
      $('#modal-kit .bt-open').data('mid', mid);
      $('#modal-kit .bt-open').data('name', name);
      $('#modal-kit .bt-open').data('gmid', null);
      $('#modal-kit .bt-open').data('gmname', null);
      app.gui.loading('#modal-kit .goalmap-list', {text: 'Loading goalmaps...'});
      app.ajax.get(
        baseUrl + 'kitbuildApi/getGoalmaps/' + mid
      ).then(function (goalmaps) {
        let goalmapList = (!goalmaps || goalmaps.length == 0) ?
          '<em class="text-secondary">No goalmap available</em>' :
          '';
        goalmaps.forEach((goalmap) => {
          goalmapList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pt-2 pb-2 pl-2 pr-2" data-gmid="' + goalmap.gmid + '" data-name="' + goalmap.name + '">'
          goalmapList += '<span>' + goalmap.name + ' '
          goalmapList += '<span class="badge ' + (parseInt(goalmap.learnermaps_count) ? 'badge-success' : 'badge-danger') + '" title="Learnermaps Count">LM: ' + goalmap.learnermaps_count + '</span>'
          goalmapList += '</span>'
          goalmapList += '<i class="fas fa-check text-primary" style="display:none"></i>'
          goalmapList += '</div>'
        });
        $('#modal-kit .goalmap-list').html(goalmapList);
      })
    });
    $('#modal-kit .goalmap-list').on('click', '.row', function () {
      if ($(this).hasClass('selected')) return;
      $('#modal-kit .goalmap-list .row').find('i').hide();
      $('#modal-kit .goalmap-list .row').removeClass('selected');
      $(this).find('i').show();
      $(this).addClass('selected')
      let gmid = $(this).data('gmid');
      let name = $(this).data('name');
      $('#modal-kit .bt-open').data('gmid', gmid);
      $('#modal-kit .bt-open').data('gmname', name);
    });
    $('#modal-kit').on('click', '.bt-open', function () {
      let name = ($(this).data('name'))
      let mid = ($(this).data('mid'))
      let gmid = ($(this).data('gmid'))
      let gmname = ($(this).data('gmname'))
      if (!gmid) {
        app.gui.dialog('Please select a Kit.', {
          width: '300px'
        });
        return;
      }
      let confirm = app.gui.confirm('Open kit <strong>\'' + gmname + '\' of \'' + name + '\'</strong>?', {
        'positive-callback': function () {
          app.loadGoalmap(gmid, function (goalmap, concepts, links, targets) {
            app.setGoalmap(goalmap, concepts, links, targets);
            app.analyzerLib.showGoalmap(app.canvas.getCy(), concepts, links, function () {
              app.canvas.getCy().fit(app.canvas.getCy().nodes(), 50);
            });
            app.loadKits(gmid, function(kits){
              let kitList = '<option value="">All</option>';
              for(let kit of kits) {
                kitList += '<option value="' + kit.kid + '">' + kit.kid + ' - ' + kit.name +  '</option>';
              }
              $('#kit-list').html(kitList);
            });
            app.loadGroups(goalmap.mid, function(groups) {
              let groupList = '<option value="">All</option>';
              for(let group of groups) {
                groupList += '<option value="' + group.gid + '">' + group.gid + ' - ' + group.name +  '</option>';
              }
              $('#group-list').html(groupList);
            });
            // loading learner maps
            app.loadLearnermaps(gmid, function (learnermaps) {
              app.learnermaps = learnermaps;
              app.gCompare = app.analyzerLib.groupCompare(app.learnermaps, app.goalmap, app.canvas.getCy());
              console.log("app.gCompare")
              console.log(app.gCompare)
              app.showLearnermapList(learnermaps);
              let range = app.analyzerLib.getRange(app.gCompare);
              $('input[type="range"]').attr('max', range.max);
              $('input[type="range"]').attr('min', range.min);
              $('input[type="range"][data-id="min-range"]').val(range.min).change();
              $('input[type="range"][data-id="max-range"]').val(range.max).change();
              $('input[type="range"]').change();
              $('.min-val').html(range.min);
              $('.max-val').html(range.max);
              console.log("learnermaps[0]")
              console.log(learnermaps)
              // throw "loadLearnermaps"
            });
            confirm.modal('hide');
            $('#modal-kit').modal('hide');
          });
        },
        'width': '300px'
      })
    });
    $('#kit-list').on('change', function(){
      let kid = $(this).val();
      app.showLearnermapList(app.learnermaps ? app.learnermaps : []);
      if(app.learnermap == null) $('#bt-show-groups-map').click();
    })
    $('#group-list').on('change', function(){
      let gid = $(this).val();
      app.showLearnermapList(app.learnermaps ? app.learnermaps : []);
      if(app.learnermap == null) $('#bt-show-groups-map').click();
    })
    $('#learner-maps-list-container').on('click', '.item', function () {
      let lmid = $(this).data('lmid');
      let lmItem = $(this);
      app.learnermap = app.getLearnermap(lmid); // console.log(app.learnermap);
      app.compareResult = app.analyzerLib.compare(app.learnermap, app.goalmap); // console.log(compareResult);

      if($('#goalmap-layout').is(':checked')) {
        $('#preset-layout').prop('checked', false);
        for(let c of app.goalmap.concepts) 
          app.canvas.getCy().nodes('#c-'+c.cid).position({
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          })
        for(let l of app.goalmap.links) 
          app.canvas.getCy().nodes('#l-'+l.lid).position({
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          });
        app.canvas.getCy().fit(50);
      } else if($('#preset-layout').is(':checked')) {
        for(let c of app.learnermap.concepts) 
          app.canvas.getCy().nodes('#c-'+c.cid).position({
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          })
        for(let l of app.learnermap.links) 
          app.canvas.getCy().nodes('#l-'+l.lid).position({
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          });
        app.canvas.getCy().fit(50);
      }
    
      app.analyzerLib.drawAnalysisLinks(app.canvas.getCy(),
        app.compareResult.match, app.compareResult.matchP, app.compareResult.leave, app.compareResult.excess, app.compareResult.excessP, app.compareResult.miss, {
          match: $('#bt-matching-links').is(':checked'),
          leave: $('#bt-leaving-links').is(':checked'),
          excess: $('#bt-excessive-links').is(':checked'),
          miss: $('#bt-missing-links').is(':checked')
        },
        function () {
          $('#bt-fit').click();
        })
    })
    $('#bt-matching-links').on('change', function () {
      if (app.learnermap) app.updateAnalysisDrawing();
      else app.updateGroupAnalysis();
    })
    $('#bt-leaving-links').on('change', function () {
      if (app.learnermap) app.updateAnalysisDrawing();
      else app.updateGroupAnalysis();
    })
    $('#bt-excessive-links').on('change', function () {
      if (app.learnermap) app.updateAnalysisDrawing();
      else app.updateGroupAnalysis();
    })
    $('#bt-missing-links').on('change', function () {
      if (app.learnermap) app.updateAnalysisDrawing();
      else app.updateGroupAnalysis();
    })
    $('#bt-show-groups-map').on('click', function () {
      if (!app.gCompare) return;
      let learnermaps = [];
      let kid = $('#kit-list').val();
      if(!kid) learnermaps = app.learnermaps;
      else {
        learnermaps = [];
        for(let l of app.learnermaps) {
          if(l.kid == kid) learnermaps.push(l);
        }
      }
      app.gCompare = app.analyzerLib.groupCompare(learnermaps, app.goalmap, app.canvas.getCy());
      app.learnermap = null;
      app.updateGroupAnalysis();
    })
    $('#bt-show-teacher-map').on('click', function () {
      if (app.goalmap.goalmap) {
        app.showGoalmap(null, function(){
          app.canvas.getCy().fit(app.canvas.getCy().nodes(), 50);
        });
      }
      else $('#bt-open-kit').click();
    });

    $('#bt-logout').on('click', function (e) {
      let confirmDialog = app.gui.confirm(app.l.get('do-you-want-logout'), {
        'positive-callback': function () {
          confirmDialog.modal('hide');
          app.session.destroy(function () {
            // app.logger.log('sign-out', {
            //   uid: app.user.uid,
            //   username: app.user.username
            // });
            // app.logger.setSeq(0);
            let currentLang = $('#select-language .active-lang').html().toLowerCase();
            app.session.set('lang', currentLang, function () {
              window.location.href = baseUrl;
            })
          })
          // window.location.href = baseUrl + app.controller + '/signOut';
        }
      });
    });
  }

  updateAnalysisDrawing() {
    let app = this;
    if (app.compareResult)
      app.analyzerLib.drawAnalysisLinks(app.canvas.getCy(),
        app.compareResult.match, app.compareResult.matchP, app.compareResult.leave, app.compareResult.excess, app.compareResult.excessP, app.compareResult.miss, {
          match: $('#bt-matching-links').is(':checked'),
          leave: $('#bt-leaving-links').is(':checked'),
          excess: $('#bt-excessive-links').is(':checked'),
          miss: $('#bt-missing-links').is(':checked')
        })
  }

  updateGroupAnalysis() {
    let app = this;
    if (app.gCompare)
      app.analyzerLib.drawGroupAnalysis(app.canvas.getCy(),
        app.gCompare.match, app.gCompare.leave, app.gCompare.excess, app.gCompare.miss, {
          match: $('#bt-matching-links').is(':checked'),
          leave: $('#bt-leaving-links').is(':checked'),
          excess: $('#bt-excessive-links').is(':checked'),
          miss: $('#bt-missing-links').is(':checked'),
          min: $('input[data-id="min-range"]').val(),
          max: $('input[data-id="max-range"]').val()
        },
        function () {})
  }

  loadGoalmap(gmid, callback) {
    let app = this;
    app.ajax.get(baseUrl + 'kitbuildApi/openKit/' + gmid,
    ).then(function (kit) { // console.log(response)
      let goalmap = kit.goalmap;
      let concepts = kit.concepts;
      let links = kit.links;
      let targets = kit.targets;
      if (typeof callback == 'function') callback(goalmap, concepts, links, targets);
    })
  }

  setGoalmap(goalmap, concepts, links, targets) {
    this.goalmap.goalmap = goalmap;
    this.goalmap.concepts = concepts;
    this.goalmap.links = links;
    this.goalmap.targets = targets;
    this.goalmap.propositionCount = 0;
    links.forEach(l => {
      if (l.source && l.target.length) this.goalmap.propositionCount += l.target.length;
    })
  }
/*
      match: matchLinks,
      leave: leaveLLinks,
      nLeaveP: numberOfLeavePropositions,
      excess: excessLLinks,
      excessP: excessLPropositions,
      nExcessP: numberOfExcessPropositions,
      miss: missGLinks,
      matchP: matchGPropositions
      */
  showLearnermapList(learnermaps = []) {
    let app = this;
    let kid = $('#kit-list').val();
    let gid = $('#group-list').val();
    // console.log(kid, gid);
    this.learnermaps = learnermaps;
    console.log("learnermaps[0]")
    console.log(learnermaps)
    let learnermapList = learnermaps.length == 0 ? '<p><small><em class="text-secondary">No learnermaps available</em></small></p>' : '';
    app.gui.loading('#learner-maps-list-container');
    for (let learnermap of learnermaps) {
      if (kid && learnermap.kid != kid) continue;
      if (gid && learnermap.gids.indexOf(gid) == -1) continue;
      learnermapList += '<div class="d-flex item list list-hover list-pointer p-1" data-lmid="' + learnermap.lmid + '" data-kid="' + learnermap.kid + '" style="width:0; min-width: 100%; flex: 1; align-items:center; border-radius: 0.2rem;">'
      if(!kid)
      learnermapList += '<span class="badge badge-info mr-2">K:' + learnermap.kid + '</span>';
      if(!gid)
      learnermapList += '<span class="badge badge-warning mr-2">G:' + learnermap.gids.join(",") + '</span>';
      learnermapList += '<span class="name pr-1" style="white-space: nowrap; overflow: hidden; flex: 1; text-overflow: ellipsis;">' + learnermap.name + '</span>'
      learnermapList += '<span>' + (learnermap.compare.nMatchP / this.goalmap.propositionCount * 100).toFixed(2) + '%</span>'
      learnermapList += '</div>'
    }
    $('#learner-maps-list-container').html(learnermapList);
  }

  loadKits(gmid, callback) {
    let app = this;
    app.ajax.get(baseUrl + 'kitbuildApi/getKitmaps/' + gmid + '/false').then(function (kitmaps) {
      if (typeof callback == 'function') callback(kitmaps);
    })
  }

  loadGroups(mid, callback) {
    let app = this;
    app.ajax.get(baseUrl + 'kitbuildApi/getMaterialGroups/' + mid).then(function (groups) {
      if (typeof callback == 'function') callback(groups);
    })
  }

  loadLearnermaps(gmid, callback) {
    let app = this;
    app.ajax.get(baseUrl + 'kitbuildApi/getLearnermapsWithGroups/' + gmid).then(function (learnermaps) {
      learnermaps.forEach(l => {
        l.gids = l.gids.split(",");
      });
      if (typeof callback == 'function') callback(learnermaps);
    })
  }

  getLearnermap(lmid) {
    for (let learnermap of this.learnermaps) {
      if (learnermap.lmid == lmid) return learnermap;
    };
  }

  showGoalmap(options, callback) {
    let app = this;
    let settings = Object.assign({
      useElementPosition: true
    }, options)
    let links = app.goalmap.links;
    let edges = [];
    links.forEach(l => {
      if(settings.useElementPosition) {
        app.getCanvas().getCy().nodes('#l-' + l.lid).position({
          x: parseInt(l.locx),
          y: parseInt(l.locy)
        });
      }
      if (l.source)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.source,
            type: 'left',
            label: ''
          }
        })
      if ( Array.isArray(l.target) && l.target.length != 0)
        l.target.forEach(tgt => {
          edges.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + tgt,
              type: 'right'
            }
          })
      })
    })

    app.getCanvas().getCy().remove('edge');
    app.getCanvas().getCy().remove('[link="leave"]');
    app.getCanvas().getCy().add(edges);

    if(settings.useElementPosition) {
      let concepts = app.goalmap.concepts;
      for(let c of concepts) {
        app.getCanvas().getCy().nodes('#c-' + c.cid).position({
          x: parseInt(c.locx),
          y: parseInt(c.locy)
        });
      }
    }
    if(typeof callback == 'function') callback();
  }

}

var BRIDGE = {};

$(function () {

  var canvas = new Canvas('cy', {
    // isDirected: false,
    // enableToolbar: false,
    enableNodeCreation: false,
    // enableConceptCreation: false,
    // enableLinkCreation: false,
    // enableUndoRedo: false,
    // enableZoom: false,
    // enableAutoLayout: false,
    // enableSaveImage: false,
    enableClearCanvas: false,
    enableNodeTools: false,
    enableNodeModificationTools: false,
    enableConnectionTools: false,
  }).init();

  let app = new AnalyzerApp(canvas);

});