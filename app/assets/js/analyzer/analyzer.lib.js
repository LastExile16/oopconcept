class AnalyzerLib {

  constructor() {}
  composePropositions(learnermap, goalmap) {
    let lm = learnermap;
    let gm = goalmap;
    let lProps = [];
    let lids = [];
    let gids = [];
    if (lm.props == undefined || true) { // console.log('the lm loop');
      lm.links.forEach(l => {
        if (l.target) {
          l.target.forEach(t => {
            let p = {
              source: null,
              link: l.label,
              lid: l.lid,
              target: null
            }
            // find the source and target
            for (let c of gm.concepts) {
              if (c.cid == l.source) {
                p.source = c.label;
                p.sid = c.cid;
              }
              if (c.cid == t) {
                p.target = c.label;
                p.tid = c.cid;
              }
              if (p.source && p.target) break;
            }
            lProps.push(p);
            lids.push(l.lid);
          })

        } else {

          let p = {
            source: null,
            link: l.label,
            lid: l.lid,
            target: null
          }

          if(l.source) {
            for (let c of gm.concepts) {
              if (c.cid == l.source) {
                p.source = c.label;
                p.sid = c.cid;
              }
              if (p.source) break;
            }
          }
          lProps.push(p);
          lids.push(l.lid);
        }

      });
      lm.props = lProps;
    } else lProps = lm.props;

    let gLabels = [];
    let gProps = [];
    if (gm.props == undefined || true) { // console.log('the gm loop');
      gm.links.forEach(l => {
        l.target.forEach(t => {
          let p = {
            source: null,
            link: l.label,
            lid: l.lid,
            target: null
          }

          // find the source and target
          for (let c of gm.concepts) {
            if (c.cid == l.source) {
              p.source = c.label;
              p.sid = c.cid;
            }
            if (c.cid == t) {
              p.target = c.label;
              p.tid = c.cid;
            }
            if (p.source && p.target) break;
          }
          gProps.push(p);
          gids.push(l.lid);
          // console.warn(gids)
        });
          gLabels[l.lid] = l.label;
      });
      gm.props = gProps;
    } else gProps = gm.props;

    // console.log(gids, lids)
    let lackingLinks = this.difference(gids, lids);
    console.log(learnermap, goalmap)
    // console.log(lackingLinks)
    lackingLinks.forEach(lid => {
      let p = {
            source: null,
            link: gLabels[lid],
            lid: lid,
            target: null
          }
      lProps.push(p);
    })
  }
  clone = function(o) {
    return JSON.parse(JSON.stringify(o));
  }
  compare(learnermap, goalmap, flag=0) {
    // let matchLinks = [];
    // let matchGPropositions = [];
    // let missGLinks = [];
    // let leaveLLinks = [];
    // let leaveLPropositions = [];
    // let excessLLinks = [];
    // let excessLPropositions = [];

    let match = [];
    let leave = [];
    let excess = [];
    let miss = [];
    let numberOfLeavePropositions = 0;
    let numberOfExcessPropositions = 0;
    let numberOfMatchPropositions = 0;

    this.composePropositions(learnermap, goalmap);

    let totalGLinks = goalmap.props.length;
    let totalLLinks = learnermap.props.length;
    try {
      // compose proposition list from both map
      this.composePropositions(learnermap, goalmap);

      // clone it, so the comparison process only modifies the clone.
      let lProps = this.clone(learnermap.props);
      let gProps = this.clone(goalmap.props);

      let llen = lProps.length;
      while (llen--) {
        let lp = lProps[llen];
        let glen = gProps.length;

        // is leaving link?
        if (lp.source == null || lp.target == null) {
          // yes
          let lvp = lProps.splice(llen, 1).pop();
          // move to leaving bucket
          leave.push(lvp);
          continue;
        }

        while (glen--) { // console.log(lp.link, gp.link);
          let gp = gProps[glen];

          // what if goalmap's proposition itself is leaving/incomplete?
          // WHAT TO DO?

          // compare link with the same label
          if (lp.link == gp.link) { // console.log(gp, lp);

            // if match?
            if ((lp.source.trim().toLowerCase() == gp.source.trim().toLowerCase() &&
                lp.target.trim().toLowerCase() == gp.target.trim().toLowerCase()) /*||
              (lp.source.trim().toLowerCase() == gp.target.trim().toLowerCase() &&
                lp.target.trim().toLowerCase() == gp.source.trim().toLowerCase())*/) {
              // match!
              let mgp = gProps.splice(glen, 1).pop(); // throw away prop at gprops
              let mp = lProps.splice(llen, 1).pop(); // move prop to match bucket
              match.push(mp); // console.warn(mp, mgp);
              break;
            }

          }
        }
      }

      // assign to excess and miss for the remaining propositions
      excess = lProps;
      miss = gProps;

      // console.log(match, leave, excess, miss);
      // console.log(lProps, gProps);

    } catch (e) {
      console.log(e);
    }

    // find the missplaced missing links paired with matching links
    for (let mi of miss) {

      // is there any missing link paired with matching links
      // red paired with green
      if (this.linkInLinks(mi, match)) { // yes, there is...
        // console.error(mi)

        // lets try if this link should be bound to excessive links
        let found = false;
        for (let ex of excess) {

          //   // is there any excessive links with the same label?
          //   // if the label is the same, move it.
          if (mi.link.trim().toLowerCase() == ex.link.trim().toLowerCase()) { // yes it is
            // but check if this excessive link is NOT already paired with missing link
            if (!this.linkInLinks(ex, miss)) {
              ;
              // move it!
              mi.lid = ex.lid;
              found = true;
              // console.warn('resolved in excess', ex);
              break;
            } // else find another one.

          }
        }

        // if not found on excess
        if (!found) {
          for (let lv of leave) { // search the leaving
            // is there any leaving links with the same label?
            // if the label is the same, move it.
            if (mi.link.trim().toLowerCase() == lv.link.trim().toLowerCase()) { // yes it is
              // but check if this leaving link is NOT previously paired with missing link
              if (!this.linkInLinks(lv, miss)) {
                // move it!
                mi.lid = lv.lid;
                // console.warn('resolved in leave', lv);
                break;
              } // else find another one.

            }
          }
        }
      }
    }

    // recheck there are should be no miss in match anymore.
    /*for (let mi of miss) {
      if (this.linkInLinks(mi, match)) // yes, there is...
        console.log(mi)
    }*/

    numberOfMatchPropositions = match.length;
    numberOfExcessPropositions = excess.length;
    numberOfLeavePropositions = leave.length;
    // recombine the decomposed propositions
    for(let i=0; i<match.length; i++) {
      // console.log("i: "+ i + " => ", JSON.parse(JSON.stringify(match)))
      if(match[i] == null) continue;
      // change it into an array
      match[i].target = match[i].target?match[i].target.split():null;
      match[i].tid = match[i].tid?match[i].tid.split():null;
      let mlength = match.length;
      while(--mlength) {
        if (match[mlength] != null && mlength != i && match[i].lid == match[mlength].lid) {
          match[i].target.push(match[mlength].target);
          match[i].tid.push(match[mlength].tid);
          match[mlength] = null;
          // console.warn(match[i])
        }
      }
    }
  match = $.map(match, function(val) { if (val != null) return val; });
  
    for(let i=0; i<leave.length; i++) {
      if(leave[i] == null || leave[i].target == null) continue;
      // change it into an array
      leave[i].target = leave[i].target?leave[i].target.split():null;
      leave[i].tid = leave[i].tid?leave[i].tid.split():null;
      let mlength = leave.length;
      while(--mlength) {
        if (leave[mlength] != null && mlength != i && leave[i].lid == leave[mlength].lid) {
          leave[i].target.push(leave[mlength].target);
          leave[i].tid.push(leave[mlength].tid);
          leave[mlength] = null;
        }
      }
    }
  leave = $.map(leave, function(val) { if (val != null) return val; });
  
    for(let i=0; i<excess.length; i++) {
      if(excess[i] == null) continue;
      // change it into an array
      excess[i].target = excess[i].target?excess[i].target.split():null;
      excess[i].tid = excess[i].tid?excess[i].tid.split():null;
      let mlength = excess.length;
      while(--mlength) {
        if (excess[mlength] != null && mlength != i && excess[i].lid == excess[mlength].lid) {
          excess[i].target.push(excess[mlength].target);
          excess[i].tid.push(excess[mlength].tid);
          // excess.splice(mlength, 1);
          excess[mlength] = null;
        }
      }
    }
  excess = $.map(excess, function(val) { if (val != null) return val; });

    for(let i=0; i<miss.length; i++) {
      if(miss[i] == null) continue;
      // change it into an array
      miss[i].target = miss[i].target?miss[i].target.split():null;
      miss[i].tid = miss[i].tid?miss[i].tid.split():null;
      let mlength = miss.length;
      while(--mlength) {
        if (miss[mlength] != null && mlength != i && miss[i].lid == miss[mlength].lid) {
          miss[i].target.push(miss[mlength].target);
          miss[i].tid.push(miss[mlength].tid);
          // miss.splice(mlength, 1);
          miss[mlength] = null;
        }
      }
    }
  miss = $.map(miss, function(val) { if (val != null) return val; });
// should only be used for the teachers group analyzer
    if (flag) {
          // console.error("miss",miss)
      for (let ex of excess) {
        let linkInLinksSearch = this.linkInLinks2(ex, miss, "source+label");
        if (linkInLinksSearch) {
          // console.error("ex", ex, linkInLinksSearch)
          ex.lid = linkInLinksSearch.lid;
        }
      }
    }
    console.log("+++++++++++++++++++++++++++++++++++++++")
    // console.log("llinks_copy remaining: ")
    // console.log(JSON.parse(JSON.stringify(llinks_copy)))
    // console.log("glinks_copy remaining: ")
    // console.log(JSON.parse(JSON.stringify(glinks_copy)))
    // console.log("excessLLinks: ")
    // console.log(JSON.parse(JSON.stringify(excessLLinks)))
    // console.log("excessLPropositions: ")
    // console.log(JSON.parse(JSON.stringify(excessLPropositions)))
    console.log("match: ")
    console.log(JSON.parse(JSON.stringify(match)))
    console.log("leave: ")
    console.log(JSON.parse(JSON.stringify(leave)))
    console.log("excess: ")
    console.log(JSON.parse(JSON.stringify(excess)))
    console.log("miss: ")
    console.log(JSON.parse(JSON.stringify(miss)))
    // console.log(JSON.parse(JSON.stringify(leaveLLinks)))
    console.log("numberOfLeavePropositions: " + numberOfLeavePropositions)
    console.log("numberOfExcessPropositions: " + numberOfExcessPropositions)
    console.log("numberOfMatchPropositions: " + numberOfMatchPropositions)
    console.log("+++++++++++++++++++++++++++++++++++++++")


      let excessLPropositions = []
      let matchGPropositions = []
    learnermap.compare = {
      match: match,
      leave: leave,
      nLeaveP: numberOfLeavePropositions,
      excess: excess,
      excessP: excessLPropositions,
      nExcessP: numberOfExcessPropositions,
      miss: miss,
      matchP: matchGPropositions,
      nMatchP: numberOfMatchPropositions
    }
    return learnermap.compare;
  }

  groupCompare(learnermaps, goalmap, cy) {
    let app = this;
    let gmatches = [];
    let gleaves = [];
    let gexcesses = [];
    let gmisses = [];
    let nMatchP = 0;
    // console.warn(learnermaps[0])
    // for (let link of learnermaps[0].links) {
    //   console.log(link.target + " ===> " + Array.isArray(link.target))
    // }
    // console.log("-----------------------")
    learnermaps.forEach(learnermap => {
      let compare = app.compare(learnermap, goalmap, 1);
      // if(learnermap.lmid == "556") console.warn(compare)
      nMatchP += compare.nMatchP;
      /* match, leave, nLeaveP, excess, excessP, nExcessP, miss, matchP, nMatchP,     */
      // let mergedMatch = compare.matchP.length>0?compare.match.concat(compare.matchP):compare.match;
      // console.warn(mergedMatch)
      // for (let match of mergedMatch) {
      //   console.log(match.target + " ===> " + Array.isArray(match.target))
      // }
      for (let match of compare.match) {
        let exists = false;
        for (let gmatch of gmatches) {
          if (match.link == gmatch.link && match.source == gmatch.source) {
            for(let $j=0; $j < match.target.length; $j++) {
              if (gmatch.target == match.target[$j]) {
                exists = true;
                gmatch.count++;
                match.target.splice($j, 1);
                match.tid.splice($j, 1);
                // --$j;
                break;
              }
            }
          }
        }
        if (match.tid.length > 0) {
          // console.warn("match target in gCompare");
          // console.warn(match);
          for(let [tindex, t] of match.target.entries()) {
            gmatches.push({
              count: 1,
              // gmid: match.gmid,
              link: match.link,
              lid: match.lid,
              source: match.source,
              sid: match.sid,
              tid: match.tid[tindex],
              target: t
            });
          };

          // console.warn("gmatches");
          // console.log(JSON.parse(JSON.stringify(gmatches)));
        }
      }

      for (let leave of compare.leave) {
        let exists = false;
        for (let gleave of gleaves) {
          // if (leave.link == gleave.link && (leave.source == gleave.source && leave.target == gleave.target) ||
          //   (leave.source == gleave.target && leave.target == gleave.source)) {
          if (leave.link == gleave.link) {
            exists = true;

            gleave.count++;
            break;
          }
        }
        if (!exists) {
          leave.count = 1;
          gleaves.push(leave);
        }
      }

      // let mergedExcess = compare.excessP.length>0?compare.excess.concat(compare.excessP):compare.excess;
      for (let excess of compare.excess) {
        let exists = false;
        for (let gexcess of gexcesses) {
          if (excess.link == gexcess.link && excess.source == gexcess.source) {
            for(let $j=0; $j < excess.target.length; $j++) {
              if (gexcess.target == excess.target[$j]) {
                exists = true;
                gexcess.count++;
                excess.target.splice($j, 1);
                excess.tid.splice($j, 1);
                // --$j;
                break;
              }
            }
          }
        }
        if (excess.tid.length > 0) {
          // console.log("excess target in gCompare");
          // console.log(excess);
          for(let [tindex, t] of excess.target.entries()) {
            gexcesses.push({
              count: 1,
              // gmid: excess.gmid,
              link: excess.link,
              lid: excess.lid,
              source: excess.source,
              sid: excess.sid,
              tid: excess.tid[tindex],
              target: t
            });
          };
          // console.log("gexcesses");
          // console.log(JSON.parse(JSON.stringify(gexcesses)));
        }
      }
      // let x=0;
      for (let miss of compare.miss) {
        // x++;
        let exists = false;
        for (let gmiss of gmisses) {
            // console.warn("miss"+x, JSON.parse(JSON.stringify(miss)))
            // console.warn("gmiss", JSON.parse(JSON.stringify(gmiss)))
          if (miss.link == gmiss.link && miss.source == gmiss.source) {
            for(let $j=0; $j < miss.target.length; $j++) {
              if (gmiss.target == miss.target[$j]) {
                exists = true;
                gmiss.count++;
                miss.target.splice($j, 1);
                miss.tid.splice($j, 1);
                // --$j;
                break;
              }
            }
          }
        }
        if (miss.tid.length > 0) {
          console.log("missing target in gCompare");
          console.log(miss);
          for(let [tindex, t] of miss.target.entries()) {
            gmisses.push({
              count: 1,
              // gmid: miss.gmid,
              link: miss.link,
              lid: miss.lid,
              source: miss.source,
              sid: miss.sid,
              tid: miss.tid[tindex],
              target: t
            });
          };
          // console.log("gmisses");
          // console.log(JSON.parse(JSON.stringify(gmisses)));
        }
      }

    });


    return {
      match: gmatches,
      leave: gleaves,
      excess: gexcesses,
      miss: gmisses,
      nMatchP: nMatchP
    }
  }

  showGoalmapKit(cy, concepts, links, callback) {
    // concepts and links in database format
    let nodes = [];
    let edges = [];
    concepts.forEach(c => {
      nodes.push({
        group: "nodes",
        data: {
          id: "c-" + c.cid,
          name: c.label,
          type: 'concept'
        },
        position: {
          x: parseInt(c.locx),
          y: parseInt(c.locy)
        }
      })
    })
    links.forEach(l => {
      nodes.push({
        group: "nodes",
        data: {
          id: "l-" + l.lid,
          name: l.label,
          type: 'link'
        },
        position: {
          x: parseInt(l.locx),
          y: parseInt(l.locy)
        }
      })
    })

    cy.reset();
    cy.elements().remove();
    cy.add(nodes);

    cy.layout({
      name: 'preset',
      fit: false,
      zoom: 1.0,
      stop: (typeof callback == 'function') ? callback : function () {}
    }).run();
  }

  showGoalmap(cy, concepts, links, callback) {
    // concepts and links in database format
    // let app = this;
    let nodes = [];
    let edges = [];
    concepts.forEach(c => {
      let extras = c.data ? JSON.parse(c.data) : null;
      nodes.push({
        group: "nodes",
        data: {
          id: "c-" + c.cid,
          name: c.label,
          type: 'concept',
          color: extras ? extras.color : undefined,
          textColor: extras ? extras.textColor : undefined
        },
        position: {
          x: parseInt(c.locx),
          y: parseInt(c.locy)
        }
      })
    })
    links.forEach(l => {
      nodes.push({
        group: "nodes",
        data: {
          id: "l-" + l.lid,
          name: l.label,
          type: 'link'
        },
        position: {
          x: parseInt(l.locx),
          y: parseInt(l.locy)
        }
      })
      if (l.source)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.source,
            type: 'left'
          }
        })
      if (Array.isArray(l.target) && l.target.length != 0)
        l.target.forEach(tgt => {
          edges.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + tgt,
              type: 'right'
            }
          })
      })

      cy.reset();
    })
    cy.elements().remove();
    cy.add(nodes);
    cy.add(edges);

    cy.layout({
      name: 'preset',
      fit: false,
      zoom: 1.0,
      stop: (typeof callback == 'function') ? callback : function () {}
    }).run();
  }

  getRange(links) {
    // console.log("getRange")
    // console.log(links)
    // throw "asdasd"
    let max = 1;
    if (links.match && links.match.length) {
      links.match.forEach(l => {
        if (l.count > max) max = l.count
      })
    }
    if (links.leave && links.leave.length) {
      links.leave.forEach(l => {
        if (l.count > max) max = l.count
      })
    }
    if (links.excess && links.excess.length) {
      links.excess.forEach(l => {
        if (l.count > max) max = l.count
      })
    }
    if (links.miss && links.miss.length) {
      links.miss.forEach(l => {
        if (l.count > max) max = l.count
      })
    }
    return {
      min: 1,
      max: max
    }
  }

  drawAnalysisLinks(cy, match, matchP, leave, excess, excessP, miss, options, callback) {
    let settings = Object.assign({
      match: true,
      leave: true,
      excess: true,
      miss: true
    }, options);

    let edges = [];
    let nodes = [];
    if (settings.excess) excess.forEach(l => {
      if (l.source)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.sid,
            type: 'left',
            link: 'ex',
            match: 5,
            label: ''
          }
        })
      if (Array.isArray(l.target) && l.target.length != 0)
        l.tid.forEach(tgt => {
          edges.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + tgt,
              type: 'right',
              link: 'ex',
              match: 5,
              label: ''
            }
          })
        })
    });
    let srcExcess = []
    if (settings.excess) excessP.forEach(l => {
      // console.log(l);
    // throw "aaaaa"
      if (l.source && !srcExcess.includes(l.lid))
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.sid,
            type: 'left',
            link: 'ex',
            match: 5,
            label: ''
          }
        })
      srcExcess.push(l.lid)
      if (l.target)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.target,
            type: 'right',
            link: 'ex',
            match: 5,
            label: ''
          }
        })
    });
    if (settings.miss) miss.forEach(l => {
      if (l.source)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.sid,
            type: 'left',
            link: 'miss',
            match: 5,
            label: ''
          }
        })
      if (Array.isArray(l.target) && l.target.length != 0)
        l.tid.forEach(tgt => {
          edges.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + tgt,
              type: 'right',
              link: 'miss',
              match: 5,
              label: ''
            }
          })
        })
    });
    if (settings.match) match.forEach(l => {
      if (l.source)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.sid,
            type: 'left',
            link: 'match',
            match: 5,
            label: ''
          }
        })
      if (Array.isArray(l.target) && l.target.length != 0)
        l.tid.forEach(tgt => {
          edges.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + tgt,
              type: 'right',
              link: 'match',
              match: 5,
              label: ''
            }
          })
        })
    });
    let srcMatch = []
    if (settings.match) matchP.forEach(l => {
      // console.log(l);
    // throw "aaaaa"
      if (l.source && !srcMatch.includes(l.lid))
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.sid,
            type: 'left',
            link: 'match',
            match: 5,
            label: ''
          }
        })
      srcMatch.push(l.lid)
      if (l.target)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.target,
            type: 'right',
            link: 'match',
            match: 5,
            label: ''
          }
        })
    });
    if (settings.leave) leave.forEach(l => {
      nodes.push({
        group: "nodes",
        data: {
          id: "ll-" + l.lid,
          name: l.link,
          type: 'link',
          link: 'leave'
        },
        position: {
          x: parseInt(l.locx),
          y: parseInt(l.locy)
        }
      })
    })
    console.log("edges created in drawAnalysisLinks")
    console.log(edges)
    cy.edges().remove();
    cy.add(edges);
    // get bounding box BEFORE adding leaving links
    cy.remove('[link="leave"]');
    let bb = cy.nodes().boundingBox();
    let leavingNodes = cy.add(nodes);

    // positioning leaving nodes
    leavingNodes.layout({
      name: 'grid',
      fit: false,
      condense: true,
      cols: 1,
      boundingBox: {
        x1: bb.x2,
        y1: bb.y1,
        w: 100,
        h: bb.y2 - bb.y1
      },
      stop: function () {
        if (callback) callback();
      }
    }).run();
  }

  drawGroupAnalysis(cy, match, leave, excess, miss, options, callback) {

    let range = this.getRange({
      match: match,
      leave: leave,
      excess: excess,
      miss: miss
    })

    let settings = Object.assign({
      match: true,
      leave: true,
      excess: true,
      miss: true,
      min: range.min,
      max: range.max,
      minRange: range.min,
      maxRange: range.max
    }, options);

    let edges = [];
    let nodes = [];
    if (settings.excess) excess.forEach(l => {
      if (l.sid && l.count >= settings.min && l.count <= settings.max)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.sid,
            type: 'left',
            link: 'ex',
            match: l.count,
            label: l.count
          }
        })
      if (l.tid.length && l.count >= settings.min && l.count <= settings.max)
        // l.tid.forEach(tgt => {
          edges.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.tid,
              type: 'right',
              link: 'ex',
              match: l.count,
              label: l.count
            }
          })
        // })
    });
    if (settings.miss) miss.forEach(l => {
      if (l.sid && l.count >= settings.min && l.count <= settings.max)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.sid,
            type: 'left',
            link: 'miss',
            match: l.count,
            label: l.count
          }
        })
      if (l.tid && l.count >= settings.min && l.count <= settings.max)
        // l.tid.forEach(tgt => {
          edges.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.tid,
              type: 'right',
              link: 'miss',
              match: l.count,
              label: l.count
            }
          })
        // })
    });
    if (settings.match) match.forEach(l => {
      if (l.sid && l.count >= settings.min && l.count <= settings.max)
        edges.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.sid,
            type: 'left',
            link: 'match',
            match: l.count,
            label: l.count
          }
        })
      if (l.tid.length && l.count >= settings.min && l.count <= settings.max)
        // l.tid.forEach(tgt => {
          edges.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.tid,
              type: 'right',
              link: 'match',
              match: l.count,
              label: l.count
            }
          })
        // })
    });
    if (settings.leave) leave.forEach(l => {
      if (l.count >= settings.min && l.count <= settings.max)
        nodes.push({
          group: "nodes",
          data: {
            id: "ll-" + l.lid,
            name: l.link + ' (' + l.count + ')',
            type: 'link',
            link: 'leave'
          },
          position: {
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          }
        })
    })
    cy.edges().remove();
    cy.add(edges);
    // get bounding box BEFORE adding leaving links
    cy.remove('[link="leave"]');
    let bb = cy.nodes().boundingBox();
    let leavingNodes = cy.add(nodes);

    // positioning leaving nodes
    leavingNodes.layout({
      name: 'grid',
      fit: false,
      condense: true,
      cols: 1,
      boundingBox: {
        x1: bb.x2,
        y1: bb.y1,
        w: 100,
        h: bb.y2 - bb.y1
      },
      stop: function () {
        if (callback) callback();
      }
    }).run();
  }

  buildLearnermap(cy) {
    if (!cy) return null;
    let learnermap = {
      concepts: [],
      links: []
    }
    let concepts = cy.nodes('[type="concept"]');
    let links = cy.nodes('[type="link"]');
    for (let c of concepts) {
      learnermap.concepts.push({
        cid: c.id().substr(2),
        label: c.data('name')
      })
    }
    for (let l of links) {
      let left = l.connectedEdges('[type="left"]');
      let right = l.connectedEdges('[type="right"]');
      let source = null,
         target = [];
      if (left.length == 1) source = left.connectedNodes('[type="concept"]')
      if (right.length >= 1) {
        right.forEach(r => {
          target.push(r.connectedNodes('[type="concept"]').id().substr(2))
        })
      }
      learnermap.links.push({
        lid: l.id().substr(2),
        label: l.data('name'),
        source: source ? source.id().substr(2) : null,
        target: target
        // target: target ? target.id().substr(2) : null // aryo version
      });
    }
    return learnermap;
  }

  isGraphOutOfView(cy) {
    let width = cy.width();
    let height = cy.height();
    let rbb = cy.elements().renderedBoundingbox();
    return rbb.x1 < 0 || rbb.y1 < 0 || rbb.x2 > width || rbb.y2 > height;
  }

  /*FROM https://stackoverflow.com/questions/39810641/get-difference-between-two-arrays-including-duplicates*/
  difference(a, b) {
    return [...b.reduce( (acc, v) => acc.set(v, (acc.get(v) || 0) - 1),
            a.reduce( (acc, v) => acc.set(v, (acc.get(v) || 0) + 1), new Map() ) 
    )].reduce( (acc, [v, count]) => acc.concat(Array(Math.abs(count)).fill(v)), [] );
  }
  difference_both(a, b) {
    return a.filter(function(v) {
        return !this.get(v) || !this.set(v, this.get(v) - 1);
    }, b.reduce( (acc, v) => acc.set(v, (acc.get(v) || 0) + 1), new Map() ));
  }
  array_diff(arr1) { // eslint-disable-line camelcase
      //  discuss at: https://locutus.io/php/array_diff/
      // original by: Kevin van Zonneveld (https://kvz.io)
      // improved by: Sanjoy Roy
      //  revised by: Brett Zamir (https://brett-zamir.me)
      //   example 1: array_diff(['Kevin', 'van', 'Zonneveld'], ['van', 'Zonneveld'])
      //   returns 1: {0:'Kevin'}

      var retArr = []
      var argl = arguments.length
      var k1 = ''
      var i = 1
      var k = ''
      var arr = {}

      arr1keys: for (k1 in arr1) { // eslint-disable-line no-labels
        for (i = 1; i < argl; i++) {
          arr = arguments[i]
          for (k in arr) {
            if (arr[k] === arr1[k1]) {
              // If it reaches here, it was found in at least one array, so try next value
              continue arr1keys // eslint-disable-line no-labels
            }
          }
          retArr.push(arr1[k1])
        }
      }

      return retArr
  }
  // from https://locutus.io/php/array/array_diff/
  array_diff_asObj(arr1) { // eslint-disable-line camelcase
    //  discuss at: https://locutus.io/php/array_diff/
    // original by: Kevin van Zonneveld (https://kvz.io)
    // improved by: Sanjoy Roy
    //  revised by: Brett Zamir (https://brett-zamir.me)
    //   example 1: array_diff(['Kevin', 'van', 'Zonneveld'], ['van', 'Zonneveld'])
    //   returns 1: {0:'Kevin'}

    var retArr = {}
    var argl = arguments.length
    var k1 = ''
    var i = 1
    var k = ''
    var arr = {}

    arr1keys: for (k1 in arr1) { // eslint-disable-line no-labels
      for (i = 1; i < argl; i++) {
        arr = arguments[i]
        for (k in arr) {
          if (arr[k] === arr1[k1]) {
            // If it reaches here, it was found in at least one array, so try next value
            continue arr1keys // eslint-disable-line no-labels
          }
        }
        retArr[k1] = arr1[k1]
      }
    }

    return retArr
  }
  // from https://www.geeksforgeeks.org/find-the-length-of-a-javascript-object/
  countObj(obj) {
    var key, count = 0;

    // Check if every key has its own property
    for (key in obj) {
        if (obj.hasOwnProperty(key))

            // If the key is found, add it to the total length
            count++;
    }
    return count;
  }

  linkInLinks(link, links, by = 'id') {
    for (let l of links) {
      switch (by) {
        case 'label':
          if (link.link.trim().toLowerCase() == l.link.trim().toLowerCase())
            return true;
          break;
        case 'both':
          if (link.link.trim().toLowerCase() == l.link.trim().toLowerCase() &&
            link.lid == l.lid)
            return true;
          break;
        default:
          if (link.lid == l.lid) return true;
          break;
      }
    }
    return false;
  }

  linkInLinks2(link, links, by = 'id') {
    for (let l of links) {
      switch (by) {
        case 'label':
          if (link.link.trim().toLowerCase() == l.link.trim().toLowerCase())
            return l;
          break;
        case 'source':
          if (link.source == l.source)
            return l;
          break;
        case 'source+label':
          if (link.link.trim().toLowerCase() == l.link.trim().toLowerCase() && link.source == l.source)
            return l;
          break;
        case 'label+id':
          if (link.link.trim().toLowerCase() == l.link.trim().toLowerCase() &&
            link.lid == l.lid)
            return l;
          break;
        case 'source+id':
          if (link.source == l.source &&
            link.lid == l.lid)
            return l;
          break;
        default:
          if (link.lid == l.lid) return l;
          break;
      }
    }
    return false;
  }
}