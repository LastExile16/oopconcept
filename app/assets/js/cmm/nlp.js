var CDM = {};
CDM.stopLists = [ 
  "the", "and", "of", "a", "in", "is", "for", "to", "we", "this", "are", "with", "as", "on", "it", "an", "that", "which", "by", "using", "can", "paper", "from", "be", "based", "has", "was", "have", "or", "at", "such", "also", "but", "results", "proposed", "show", "new", "these", "used", "however", "our", "were", "when", "one", "not", "two", "study", "present", "its", "sub", "both", "then", "been", "they", "all", "presented", "if","each", "approach", "where", "may", "some", "more", "use", "between", "into", "1", "under", "while", "over", "many", "through", "addition", "well", "first", "will", "there", "propose", "than", "their", "2", "most", "sup", "developed", "particular", "provides", "including", "other", "how", "without", "during", "article", "application", "only", "called","what", "since", "order", "experimental", "any" ];

CDM.stopwords = ["a", "about", "above", "above", "across", "after", "afterwards", "again", "against", "all", "almost", "alone", "along", "already", "also", "although", "always", "am", "among", "amongst", "amoungst", "amount", "an", "and", "another", "any", "anyhow", "anyone", "anything", "anyway", "anywhere", "are", "around", "as", "at", "back", "be", "became", "because", "become", "becomes", "becoming", "been", "before", "beforehand", "behind", "being", "below", "beside", "besides", "between", "beyond", "bill", "both", "bottom", "but", "by", "call", "can", "cannot", "cant", "co", "con", "could", "couldnt", "cry", "de", "describe", "detail", "do", "done", "down", "due", "during", "each", "eg", "eight", "either", "eleven", "else", "elsewhere", "empty", "enough", "etc", "even", "ever", "every", "everyone", "everything", "everywhere", "except", "few", "fifteen", "fify", "fill", "find", "fire", "first", "five", "for", "former", "formerly", "forty", "found", "four", "from", "front", "full", "further", "get", "give", "go", "had", "has", "hasnt", "have", "he", "hence", "her", "here", "hereafter", "hereby", "herein", "hereupon", "hers", "herself", "him", "himself", "his", "how", "however", "hundred", "ie", "if", "in", "inc", "indeed", "interest", "into", "is", "it", "its", "itself", "keep", "last", "latter", "latterly", "least", "less", "ltd", "made", "many", "may", "me", "meanwhile", "might", "mill", "mine", "more", "moreover", "most", "mostly", "move", "much", "must", "my", "myself", "name", "namely", "neither", "never", "nevertheless", "next", "nine", "no", "nobody", "none", "noone", "nor", "not", "nothing", "now", "nowhere", "of", "off", "often", "on", "once", "one", "only", "onto", "or", "other", "others", "otherwise", "our", "ours", "ourselves", "out", "over", "own", "part", "per", "perhaps", "please", "put", "rather", "re", "same", "see", "seem", "seemed", "seeming", "seems", "serious", "several", "she", "should", "show", "side", "since", "sincere", "six", "sixty", "so", "some", "somehow", "someone", "something", "sometime", "sometimes", "somewhere", "still", "such", "system", "take", "ten", "than", "that", "the", "their", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "therefore", "therein", "thereupon", "these", "they", "thickv", "thin", "third", "this", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "top", "toward", "towards", "twelve", "twenty", "two", "un", "under", "until", "up", "upon", "us", "very", "via", "was", "we", "well", "were", "what", "whatever", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whoever", "whole", "whom", "whose", "why", "will", "with", "within", "without", "would", "yet", "you", "your", "yours", "yourself", "yourselves", "the", "-lrb-", "-rrb-"];

class Tree {

  constructor() {
    this.nodes = [];
    this.label = null;
    this.text = null;
  }

  addNode(node) {
    this.nodes.push(node);
  }

}

// let pop = 0;

class Cosim {
  static calculateTf(document) {
    document.terms = [];
    // console.log(document);
    for(let i in document.lemmas) {
      let lemma = document.lemmas[i];
      let tf = Cosim.getFrequency(lemma, document.lemmas);
      document.terms[lemma.toString()] = {
        term: lemma,
        tf: tf,
        tfn: tf/document.lemmas.length 
      };
    }
  }

  static calculateIdf(document, idfs, n) {
    for(let i in document.terms) {
      let term = document.terms[i];
      // let defidf = 1 + Math.log((n+1));
      // console.log(term, n);
      term.idf = idfs[term.term] != undefined ? 
        idfs[term.term] : 
        Math.log((n+1))
        // 1 + Math.log((n+1));
      term.tfidf = term.tfn * term.idf;
    }
  }

  static calculateTfIdf(document, idfs, n) {
    Cosim.calculateTf(document);
    Cosim.calculateIdf(document, idfs, n);
  }

  static getFrequency(word, words) {
    let f = 0;
    for(let i in words) {
      if(word == words[i]) f++;
    }
    return f;
  }

  static getIdfs(uwords, documents) {
    let idfs = {};
    for(let i in uwords) {
      let word = uwords[i];
      // let idf = 1 + 
      let idf = 
        Math.log(documents.length / Cosim.getNumDocs(word, documents));
      idfs[word] = idf;
    }
    return idfs;
  }

  static getNumDocs(word, documents) {
    let num = 0;
    for(let i in documents) {
      let doc = documents[i];
      if(doc.lemmas.includes(word)) {
        // console.log(doc.originalText);
        // console.log(doc.text);
        // console.log(doc.lemmas);
        num++;
      }
    }
    // console.log(word + ": " + num);
    return num;
  }

  static cosineSimilarity(query, document) {

    let dp = 0; // dot product
    let qsum = 0;
    let dsum = 0;
    // console.log('comparing :');
    // console.log(query.text +": "+ query.lemmas.join(" "));
    // console.log(document.originalText + ": " + document.lemmas.join(" "));
    for(let i in query.terms) {
      let qterm = query.terms[i];
      let dterm = document.terms[qterm.term] ? 
        document.terms[qterm.term] : { tfidf: 0 };
      dp += qterm.tfidf * dterm.tfidf;
      // console.log('qt');
      // console.log(qterm);
      // console.log('dt');
      // console.log(dterm);
      qsum += Math.pow(qterm.tfidf, 2);
      dsum += Math.pow(dterm.tfidf, 2);
    }
    
    let sim = 0;
    if(dp != 0) {
      let qbar = Math.sqrt(qsum); // |q|
      let dbar = Math.sqrt(dsum); // |d|
      sim = dp / (qbar * dbar);
    }
    // console.log('----' + dp + ";" + qsum + ";" + dsum);
    // console.log('-- sim --' + ); 
    // if(sim > 0) console.log(query.text + " : " + document.originalText + " : " + sim);
    return sim;
  }

  static getSimilarConceptKeyword(concept, documents) {
    // let intact = { originalText: concept }
    // console.log(documents.length);
    for(let i in documents) {
      let doc = documents[i];

      // Kalau text-ya sama dengan teks concept yang dicari
      // DAN punya similarity dengan keywords!
      if(doc.originalText == concept && doc.sims.length) {
        let mostSim = null;
        // console.log(doc);
        for(let j in doc.sims) {
          let sim = doc.sims[j];
          // console.log(sim);
          if(sim.sim > 0.5) {
            if(mostSim == null || mostSim.sim < sim.sim)
              mostSim = sim;
          }
        }
        // if(mostSim) console.log(mostSim);
        return mostSim ? mostSim.query : doc;
      }
    }

    return null;

  }

  // static createConcept(concept, tokens, sentenceIndex) {
  //   // not found
  //   // console.log(concept);
  //   let words = concept.split(" ");
  //   let lemmas = [];
  //   for(let i in words) {
  //     let word = words[i];

  //     if(kb.stopwords.includes(word.toLowerCase())) continue;

  //     let lemmaCandidate = null;
  //     for(let j in tokens) {
  //       let token = tokens[j];
  //       if(token.originalText == word) {
  //         lemmaCandidate = token.lemma;
  //         if(token.sentenceIndex == sentenceIndex) {
  //           lemmaCandidate = token.lemma;
  //           break;
  //         }
  //       }
  //     }
  //     if(lemmaCandidate != null) lemmas.push(lemmaCandidate);
  //     else {
  //       // TODO: this should be obtained 
  //       // from word lemmatization process
  //       lemmas.push(word.toLowerCase());
  //     }
  //   }
  //   let newDoc = {
  //     id: null,
  //     lemmas: lemmas,
  //     originalText: concept,
  //     sentenceIndex: sentenceIndex,
  //     text: lemmas.join(" "),
  //     score: 0
  //   };
  //   // console.log('new');
  //   // console.log(newDoc);
  //   return newDoc;
  // }

}

class Nlp {

  static parseTree(text) {
    let stringArray = text.split(/(\s+)/);
    let queue = stringArray.filter(v=>v.trim()!=''); // queue
    let stack = [];
    let tree = null;
    let index = 0;
    while (queue.length > 0) {
      if(stack.length == 0) {
        let label = queue.shift();
        let node = new Tree();
        node.label = label.substr(1);
        stack.push(node);
        continue;
      } 
      let label = queue.shift();
      if(label[0] == '(') {
        let node = new Tree();
        let parent = stack.pop();
        node.label = label.substr(1);
        node.parent = parent;
        parent.addNode(node);
        stack.push(parent);
        stack.push(node);
      } else {
        let labels = label.split(")");
        let parent = stack.pop();
        parent.text = labels.shift();
        parent.index = ++index;
        labels.pop();
        while(labels.length) {
          tree = stack.pop();
          labels.pop();
        }
      }
    }
    return tree;
    // console.log(tree);
  }

  static find(index, tree) {
    for(let i = 0; i < tree.nodes.length; i++) {
      let t = tree.nodes[i];
      if('index' in t && t.index == index) return true; 
      for(let j = 0; j < t.nodes.length; j++) {
        let found = find(index, t.nodes[j]);
        if(found) return true;
      }
    }
    return false;
  }

  static toString(words) {
    let text = '';
    for(let i = 0; i < words.length; i++) {
      let t = words[i].originalText;
      let pt = (i>0)? words[i-1].originalText : '';
      if(i==0) text += t;
      else {
        if(['.',',',')'].includes(t)) text += t + " ";
        else if(["''"].includes(words[i].word)) text += t + " ";
        else if(['.',',','(',')','"'].includes(pt))
          text += t;
        else text += " " + t;
      }
    }
    return text.trim();
  }

  static getNounPhrases(tree) {
    if(tree.text != null) return [];
    let candidates = [];
    for(let i = 0; i < tree.nodes.length; i++) {
      let childNps = Nlp.getNounPhrases(tree.nodes[i]);
      if(childNps.length == 0
        && tree.nodes[i].label == "NP") {
        candidates.push(tree.nodes[i]);
      } else candidates.push(...childNps);
    }
    return candidates;
  }

  static getPhrases(tree, words = []) {
    if(tree.text) {
      if(![",",".","-LRB-","-RRB-","POS","``","''"].includes(tree.label)) {
        // console.log(tree.label + ": " + tree.text);
        words.push({
          text: tree.text,
          index: tree.index,
          label: tree.label
        });
        // console.log(tree);
      }
    }
    for(let tr in tree.nodes) {
      Nlp.getPhrases(tree.nodes[tr], words);
    }
    return words;
  }

  static getLeaves(tree, words = []) {
    if(tree.text) {
      if(!["DT",",",".","-LRB-","-RRB-","POS","``","''"].includes(tree.label)) {
        // console.log(tree.label + ": " + tree.text);
        words.push(tree.text)
        // console.log(tree);
      }
    }
    for(let tr in tree.nodes) {
      Nlp.getLeaves(tree.nodes[tr], words);
    }
    return words;
  }

  static getLemmas(nounPhrases, tokens) {
    let lemmas = [];
    for(let n = 0; n < nounPhrases.length; n++) {
      
      let np = nounPhrases[n];
      let token = tokens[np.index-1];
      // console.log(token);
      if(!["DT",",",".","-LRB-","-RRB-","POS","``","''"].includes(token.pos))
        lemmas.push(token.lemma.toLowerCase());
    }
    return lemmas;
  }

  static getLemmasFromTokens(label, tokens) {
    let words = label.split(/(\s+)/);
    let lemmas = [];
    for(let x = 0; x < words.length; x++){ 
      if ( words[x].toString().trim() == '' ) words.splice(x, 1);
    }
    for(let j = 0; j < words.length; j++) {
      let word = words[j];
      if(CDM.stopLists.includes(word.toLowerCase()))
        continue;
      let found = false;  
      for(let i = 0; i < tokens.length; i++) { 
        let token = tokens[i];
        // console.log(word, token);
        if(token.originalText.toString().toLowerCase().trim() ==  word.toString().toLowerCase().trim()) {
          lemmas.push(token.lemma.toLowerCase());
          found = true;
          break;
        }
      }
      if(!found)
        lemmas.push(word.toString().toLowerCase().trim());
    }
    // console.log("lemmas", lemmas);
    return lemmas;
  }

  static postProcess(sentences) {
    let rake = new Rake();
    let tokens = [];
    
    for(let i = 0; i < sentences.length; i++) {
      
      let sent = sentences[i];
      sent.tree = Nlp.parseTree(sent.parse);
      sent.string = Nlp.toString(sent.tokens);
      sent.nps = Nlp.getNounPhrases(sent.tree);

      sent.keywordWords = []; // single word list storage
      sent.keywords = []; // keyword list storage

      var getKey = function(word, tokens) {
        for(let j = 0; j < tokens.length; j++) {
          if(word == tokens[j].originalText)
            return tokens[j].lemma;
        }
        return word;
      }
      
      // untuk setiap NP yang ada di dalam sentence
      for(let j = 0; j < sent.nps.length; j++) {

        // ambil leaves-nya
        let keywordWords = Nlp.getLeaves(sent.nps[j]);
        // console.log(keywordWords);

        // kalau nggak ada, skip
        if(keywordWords.length == 0) continue;
        
        // join words NP-nya jadi keyword
        let keyword = keywordWords.join(" ").trim();
        for(let k in keywordWords) {
          let key = getKey(keywordWords[k], sent.tokens);
          // console.log(key);

          // buat object word dari keyword 
          // yang menyimpan text dan lemma-nya sebagai key
          let keywordWord = {
            text: keywordWords[k],
            key: key
          }

          // masukkan ke dalam daftar single word untuk sentence ini
          sent.keywordWords.push(keywordWord);
        }
        
        // tambahkan keyword ke dalam daftar keyword 
        // untuk sentence ke-i
        // siapkan atribut tambahan dari post-process RAKE
        sent.keywords.push({
          originalText: keyword, 
          score: 0, 
          sentenceIndex: i,
          words: []
        });
        // console.log(keywordWords);
        // console.log('----' + j);
        
      } // end of noun-phrases loop

      // console.log(sent.tokens);
      // masukkan token ke dalam daftar token 
      // untuk dicari lemma-nya sebagai key
      tokens.push(sent.tokens);
      
      rake.setCandidates(sent.keywords, sent.keywordWords);
      rake.setTokens(tokens);

      // console.log(rake);

      // console.log(sent);
    }
    rake.process();
    return sentences;
  }

}

class RakeWord {

  constructor(word) {
    this.word = word;
    this.frequency = 1;
    this.degree = 0;
    this.ratio = 0;
    this.cooccurences = [];
  }

  getRatio() {
    return this.ratio;
  }

  getDegree() {
    return this.degree;
  }

  calculateDegree() {
    let keys = Object.keys(this.cooccurences);
    for(let i = 0; i < keys.length; i++) {
      this.degree += this.cooccurences[keys[i]];
    }
    this.ratio = this.degree / this.frequency;
  }

  updateCooccurences(anotherWord, keywordWords) {
    let cooc = 0;
    let anotherKey = anotherWord.toLowerCase();
    if(this.cooccurences[anotherKey])
      cooc = this.cooccurences[anotherKey];
    else this.cooccurences[anotherKey] = 0;
    if(keywordWords.includes(this.word) &&
      keywordWords.includes(anotherWord)) {
        cooc++;
        this.cooccurences[anotherKey] = cooc;
      }
    return cooc;
  }
    
}

class Rake {

  constructor(){
    this.keywords = [];
    this.rakeWords = [];
    this.tokens = [];
  }

  setCandidates(keywords, keywordWords) {
    // Builds RAKE word list
    this.keywords.push(...keywords);
    // this.tokens
    for(let i = 0; i < keywordWords.length; i++) {
      // console.log(keywordWords[i]);
      let key = keywordWords[i].key;
      let rakeWord = null;
      if(this.rakeWords[key]) {
        rakeWord = this.rakeWords[key];
        rakeWord.frequency++;
      } else {
        rakeWord = new RakeWord(keywordWords[i].text);
        this.rakeWords[key] = rakeWord;
      }
    }
    // console.log(this);
  }

  setTokens(tokens) {
    this.tokens = tokens;
  }

  getKey(word, tokens) {
    for(let j = 0; j < tokens.length; j++) {
      if(tokens[j].originalText == word) {
        return tokens[j].lemma;
      }
    }
    return word.toLowerCase();
  }

  process() {
    // Calculate cooccurences matrix
    let keys = Object.keys(this.rakeWords);
    for(let i = 0; i < keys.length; i++) {
      for(let j = 0; j < keys.length; j++) {
        let vWord = this.rakeWords[keys[i]];
        let hWord = this.rakeWords[keys[j]];
        for(let k = 0; k < this.keywords.length; k++) {
          let words = this.keywords[k].originalText.split(" ");
          vWord.updateCooccurences(hWord.word, words);
        }
      }
    }

    // console.log(this.rakeWords);
    // return;

    // Calculate degree and ratio
    for(let i = 0; i < keys.length; i++) {
      let rakeWord = this.rakeWords[keys[i]];
      rakeWord.calculateDegree();
    }

    // Calculate keywords score
    for(let i = 0; i < this.keywords.length; i++) {
      let keyword = this.keywords[i];
      // console.log(keyword);
      keyword.score = 0;
      keyword.length = 0;
      let words = keyword.originalText.split(" ");
      for(let j = 0; j < words.length; j++) {
        let key = this.getKey(words[j], 
          this.tokens[keyword.sentenceIndex]);

        // valid word ?
        if((this.rakeWords[key] == undefined || (this.rakeWords[key].word == undefined)) 
        || (CDM.stopLists.includes(this.rakeWords[key].word.toLowerCase())))
          continue;

        keyword.score += this.rakeWords[key].ratio;
        keyword.length++;
        keyword.words.push({
          lemma: key,
          text: this.rakeWords[key].word,
          ratio: this.rakeWords[key].ratio,
          degree: this.rakeWords[key].degree,
          frequency: this.rakeWords[key].frequency,
        });
      }

      keyword.score = keyword.score / keyword.length;
      
    }
  }

}

class ReVerb {

  static getVerbs(tokens = []) {

    let verbs = [];
    var reVB = new RegExp('^vb.*','gi'); // Verb
    var reRP = new RegExp('^rp$','gi'); // Particle
    var reRB = new RegExp('^rb.*','gi'); // Adverb
    var reNN = new RegExp('^nn.*','gi'); // Noun
    var reJJ = new RegExp('^jj.*','gi'); // Adjective
    var rePR = new RegExp('^prp.*','gi'); // Pronoun
    var reDT = new RegExp('^dt','gi'); // Determiner
    var reIN = new RegExp('^in','gi'); // Preposition
    var reTO = new RegExp('^to','gi'); // Information Marker
  
    let flag = false;
    
    let v;
    let mwords = [];
    let p;
  
    function reset(i = 1) {
      mwords = [];
      flag = false;
      return i - 1;
    }
    
    // let s = '';
    // for(let i = 0; i < tokens.length; i++) {  
    //   let token = tokens[i];
    //   s += '(' + token.originalText + ':'+token.pos+') ';
    // }
    // console.log(s);
  
    for(let i = 0; i < tokens.length; i++) {
  
      let token = tokens[i]; //console.log(token.pos, token.originalText);
      
      if(!flag) { // beginning search (or after finding verbs)
        if(reVB.exec(token.pos)) {
          // console.warn('found start V:', token.pos, token.originalText);
          v = {
            word: token.word,
            index: i
          }
          flag = true;
          continue;
        }
      } else { // verb has been found, check the sequences
        let min = reIN.exec(token.pos);
        let mrp = reRP.exec(token.pos);
        let mto = reTO.exec(token.pos);
        if(min || mrp || mto) { // P: Prep / Particle / Inf Marker
          p = {
           word: token.word, 
           index: i
          };
          // console.warn('found end P:', token.pos, token.originalText);
          let mwrd = [];
          for(let j = 0; j < mwords.length; j++) 
            mwrd.push({word: mwords[j].word});
          // console.warn('verbs:', v, mwrd, p );
          //mrels = [];
          
          if(v.index > 0 && reTO.exec(tokens[v.index - 1].pos))
            verbs.push({s: v.index-1, e: p.index});
          else verbs.push({s: v.index, e: p.index});
          
          i = reset(i);
  
        } else {
  
          let mvb = new RegExp('^vb.*','gi').exec(token.pos);
          let mnn = reNN.exec(token.pos); // Noun
          let mjj = reJJ.exec(token.pos); // Adjective
          let mrb = reRB.exec(token.pos); // Adverb
          let mpr = rePR.exec(token.pos); // Pronoun
          let mdt = reDT.exec(token.pos); // Determiner
          
  
          // console.log(token.pos.match(/vb.*/ig));
          // console.log(reVB.exec(token.pos));
          // console.log({m:mv});
  
          if(mnn || mjj || mrb || mpr || mdt || mvb ) {
            mwords.push({ word: token.word, pos: token.pos, index: i });
            // console.warn('push mid W:', token.pos, token.originalText);
          } else {
            
            // console.warn('found nothing:', token.pos, token.originalText);
            // console.warn('verbs:', v );
            // console.log('discard middle words');//, mwords);
  
            if(mwords.length) {
              let vOnly = true;
              for(let m in mwords) {
                if(!mwords[m].pos.match(/^vb.*/ig)) { vOnly = false; break; }
              }
              if(vOnly) {
                // console.warn('found chain of verbs:', tokens[token.index - 2].pos, tokens[token.index - 2].word );
                if(v.index > 0 && reTO.exec(tokens[v.index - 1].pos))
                  verbs.push({s: v.index-1, e: mwords[mwords.length-1].index});
                else verbs.push({s: v.index, e: mwords[mwords.length-1].index});
                i = reset(i);
                continue;
              }
            }
            
            if(v.index > 0 && reTO.exec(tokens[v.index - 1].pos))
                verbs.push({s: v.index-1, e: v.index});
            else verbs.push({s: v.index, e: v.index});
            
            i = reset(i);
          }
        }
          
      }
      
      // console.log(token.pos, token.originalText, token.pos.match('/VB.*/gi'));
      // if(token.pos.match('/vb/gi'))
      // {
      //   verbs.push(token);
      // }
    }
  
    return verbs;
  }

  static getTriples(sentence = null, sentenceIndex, idfs, n) {
    let verbs = ReVerb.getVerbs(sentence.tokens);
    let tree = Nlp.parseTree(sentence.parse);
    let nps = Nlp.getNounPhrases(tree);
    let triples = [];

    for(let i in verbs) {
      let rel = verbs[i];
      let triple = {
        subject: {
          lemmas: [],
          originalText: '',
          sentenceIndex: sentenceIndex,
          text: ''
        }, 
        snp: false, 
        relation: '', 
        object: {
          lemmas: [],
          originalText: '',
          sentenceIndex: sentenceIndex,
          text: ''        
        }, 
        onp: false
      };
      
      let verbWords = []
      for(let d = rel.s; d <= rel.e; d++)
        verbWords.push(sentence.tokens[d].word);
      triple.relation = verbWords.join(" ");
      
      let pNounIndex = -1;
      let nNounIndex = -1;
      for(let p = rel.s-1; p >= 0; p--) {
        let token = sentence.tokens[p];
        // console.log(token.word, token.pos, token.index);
        if(token.pos.match(/nn.*/ig)) {
          pNounIndex = token.index;
          triple.subject.originalText = token.originalText;
          triple.subject.lemmas.push(token.lemma);
          triple.subject.text = token.lemma;
          break;
        }
      }

      for(let p = rel.e+1; p < sentence.tokens.length; p++) {
        let token = sentence.tokens[p];
        // console.log(token.word, token.pos, token.index);
        if(token.pos.match(/nn.*/ig)) {
          nNounIndex = token.index;
          triple.object.originalText = token.originalText;
          triple.object.lemmas.push(token.lemma);
          triple.object.text = token.lemma;
          break;
        }
      }

      // cari NP yang sesuai dengan subject dan object
      for(let q = 0; q < nps.length; q++) {
        if(!triple.snp && Nlp.find(pNounIndex, nps[q])) {
          let sNps = Nlp.getPhrases(nps[q]);
          triple.subject.originalText = sNps.map(e => e.text).join(' ');
          triple.snp = true;
          triple.subject.lemmas = Nlp.getLemmas(sNps, sentence.tokens);
          triple.subject.text = triple.subject.lemmas.join(" ");
          // console.warn('previous NP:', triple.s);
          // break;
        }
        if(!triple.onp && Nlp.find(nNounIndex, nps[q])) {
          let oNps = Nlp.getPhrases(nps[q]);
          triple.object.originalText = oNps.map(e => e.text).join(' ');
          triple.onp = true;
          triple.object.lemmas = Nlp.getLemmas(oNps, sentence.tokens);
          triple.object.text = triple.object.lemmas.join(" ");
          // console.warn('next NP:', );
          // break;
        }
        if(triple.snp && triple.onp) break;
      }
      // console.log(pNounIndex, nNounIndex);
      delete triple.snp;
      delete triple.onp;

      Cosim.calculateTfIdf(triple.subject, idfs, n);
      Cosim.calculateTfIdf(triple.object, idfs, n);
      // console.log(triple);
      // console.error(r);
      triples.push(triple);
    }
    // console.log(sentence.tokens);
    // console.log(tree);
    // console.log(nps);
    // console.log(jsonText.sentences[sent].parse);
    // break;
    return triples;
  }

}
