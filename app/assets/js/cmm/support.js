class Support {

  constructor(content, nlp, options = {}) {
    this.setContentNLP(content, nlp);
    this.settings = Object.assign({
      reset: false
    }, options);

    // External references
    this.canvas = null;
    this.gui = null;

    this.sentences; // post-process sentences cache
    
    this.resetSupport();
    this.handleEvent();
  }

  reprocess() {
    this.process = true;
  }

  setContentNLP(content, nlp) {
    this.content = content;
    this.nlp = JSON.parse(nlp);
    return this;
  }

  resetSupport() {
    this.process = true;
    this.tokens = [];
    this.documents = [];
    this.keywords = [];
    this.sentenceIndex = 0;
    return this;
  }

  setCanvas(canvas) { // console.log(canvas)
    this.canvas = canvas;
    return this;
  }

  setGui(gui) {
    this.gui = gui;
    return this;
  }

  handleEvent() {
    
    let support = this;
    $('#exp-collab-sentence-selection').on('click', '.nav-next', function () {
      // console.log(support.sentences);
      if (support.sentenceIndex < support.sentences.length - 1) {
        support.sentenceIndex++;
        support.getRecommendationOfSentence(support.sentenceIndex);
        // if (typeof Logger != 'undefined') Logger.log('click-bt-next-sentence', { sidx: support.sentenceIndex });
      } else support.gui.notify('You have reached the end of the reading.', { type: 'info' });
    });
  
    $('#exp-collab-sentence-selection').on('click', '.nav-prev', function () {
      if (support.sentenceIndex > 0) {
        support.sentenceIndex--;
        support.getRecommendationOfSentence(support.sentenceIndex);
        // if (typeof Logger != 'undefined') Logger.log('click-bt-prev-sentence', { sidx: support.sentenceIndex });
      } else support.gui.notify('You have reached the beginning of the reading.', { type: 'info' });
    });
  
    $('#exp-collab').on('click', '.bt-add-keyword-as-concept', function () {
  
      // TODO: Add check whether kit to be added is already exists.
      let existingNode = support.canvas.findNode($(this).data('label'));
      if (existingNode != null) {
        support.gui.notify('Keyword <strong>' + $(this).data('label') + '</strong> is already exists on the map.', { type: 'warning' });
        return;
      }
  
      let node = support.canvas.createNode({
        type: $(this).data('type'),
        label: $(this).data('label')
      });
  
      /* Preparing Undo and Log */
  
      // let nodeData = node.json();
      // delete nodeData.classes;
      // delete nodeData.grabbable;
      // delete nodeData.locked;
      // delete nodeData.removed;
      // delete nodeData.selectable;
      // delete nodeData.selected;
      // nodeData.position.x = parseInt(nodeData.position.x);
      // nodeData.position.y = parseInt(nodeData.position.y);
  
      // // console.log(nodeData);
      // UndoCommand.push(new Create(nodeData));
  
      support.gui.notify('Keyword <strong>' + $(this).data('label') + '</strong> added to the map as concept', { type: 'success' });
      // if (typeof Logger != 'undefined') Logger.log("add-keyword-to-map", {
      //   mid: support.mid,
      //   type: $(this).data('type'),
      //   label: $(this).data('label')
      // });
  
    });
  
    $('#exp-collab').on('click', '.bt-add-proposition', function () {
  
      let sentenceIndex = $(this).data('sentence');
  
      // let subject = $(this).data('subject');
      // let object = $(this).data('object');
      let relation = $(this).data('relation');
      let ttype = $(this).data('ttype');
      // console.log($(this).siblings('.prop-group'));
      let s = $(this).siblings('.prop-group').find('.concept-subject')[0];
      let o = $(this).siblings('.prop-group').find('.concept-object')[0];
      let subject = $(s).html();
      let object = $(o).html();
      // console.log($(s).html(), relation, $(o).html());
  
      let results =
        support.addProposition(subject, relation, object, sentenceIndex);
      if (results != null) {
        // results.collection.layout({ name: 'cose' }).run();
        // support.canvas.zoomToFit();
        // cy.animate({ fit: { eles: cy, padding: 30 } });
        // if (typeof Logger != 'undefined') Logger.log("add-proposition-" + ttype, {
        //   mid: support.mid,
        //   subject: subject,
        //   relation: relation,
        //   object: object,
        //   sentenceIndex: sentenceIndex,
        //   ttype: ttype
        // });
      } else {
        support.gui.notify('Proposition <strong>"' + subject + '" - "' + relation + '" - "' + object + '"</strong> is already exists', { type: 'warning' });
      }
  
    });
  
    $('#exp-collab').on('click', '.bt-edit-proposition', function () {
      // console.log(this);
  
      let propGroup = $(this).siblings('.prop-group');
      let conceptA = $(propGroup.find('.concept-subject')[0]).html().toString().trim();
      let conceptB = $(propGroup.find('.concept-object')[0]).html().toString().trim();
      let relation = $(propGroup.find('.relation')[0]).html().toString().trim();
  
      let modal = support.showManualProposition({
        subject: conceptA,
        relation: relation,
        object: conceptB
      });
  
      // // console.log(propGroup, conceptA, relation, conceptB);
  
      // $('#exp-manual-konsep-a').val(conceptA);
      // $('#exp-manual-konsep-b').val(conceptB);
      // $('#exp-manual-link').val(relation);
  
      // if (typeof Logger != 'undefined') Logger.log('edit-proposition', {
      //   ca: conceptA,
      //   cb: conceptB,
      //   rel: relation
      // });
  
      // // var elm = $('#form-exp-proposition-manual');
      // // console.log(elm);
      // // var newone = elm[0].cloneNode(true);
      // // elm[0].parentNode.replaceChild(newone, elm[0]);
  
      // // $(newone)
      // $('#form-exp-proposition-manual')
      //   .css({ 'background-color': '#ffc107' })
      //   .animate({ 'background-color': 'transparent' }, 150, function () {
      //     $(this).css({ 'background-color': '#ffc107' })
      //       .animate({ 'background-color': 'transparent' }, 500);
      //   });
  
    });
  
    $('#exp-collab').on('click', '#bt-show-hide-more-suggestions', function () {
      // if (typeof Logger != 'undefined') Logger.log('toggle-more-suggestions');
    });
  }


  initializeSentences(sentences) {
    this.sentences = sentences;
    this.documents = []; // list of concept from all sentences and empty it!
    this.keywords = []; // empty bag of keywords!
    for (let i in this.sentences) {
      // console.log(this.sentences[i]);
      let sent = this.sentences[i];
      // sent.read = false;
      let triples = sent.openie;
      this.keywords.push(...sent.keywords);

      for (let t in sent.tokens) {
        let token = sent.tokens[t];
        token.sentenceIndex = i;
        this.tokens.push(token);
      }

      for (let j in triples) {
        let triple = triples[j];

        // Process subject's lemmas
        let subjectLemmas = [];
        let subjectWords = triple.subject.toLowerCase().split(" ");
        for (let k = triple.subjectSpan[0]; k < triple.subjectSpan[1]; k++) {
          if (CDM.stopwords.includes(sent.tokens[k].lemma.toLowerCase()))
            continue;
          if (subjectWords.includes(sent.tokens[k].word.toLowerCase()))
            subjectLemmas.push(sent.tokens[k].lemma.toLowerCase());
        }
        this.documents.push({
          originalText: triple.subject,
          text: subjectLemmas.join(" "),
          lemmas: subjectLemmas,
          sentenceIndex: i
        });

        // Process object's lemmas
        let objectLemmas = [];
        let objectWords = triple.object.toLowerCase().split(" ");
        for (let k = triple.objectSpan[0]; k < triple.objectSpan[1]; k++) {
          if (CDM.stopwords.includes(sent.tokens[k].lemma.toLowerCase()))
            continue;
          if (objectWords.includes(sent.tokens[k].word.toLowerCase()))
            objectLemmas.push(sent.tokens[k].lemma.toLowerCase());
        }
        this.documents.push({
          originalText: triple.object,
          text: objectLemmas.join(" "),
          lemmas: objectLemmas,
          sentenceIndex: i
        });
      } // end triples loop
    } // end sentences loop
    // console.log(this.documents);
    // for(let i in this.documents) {
    //   if(this.documents[i].originalText == 'located')
    //     console.log(this.documents[i]);
    // }

    let queries = [];
    for (let i in this.keywords) {

      let keyword = this.keywords[i];
      keyword.lemmas = [];
      for (let j in keyword.words) {
        // skip word which is in stopwords list
        if (CDM.stopwords.includes(keyword.words[j].lemma))
          continue;
        keyword.lemmas.push(keyword.words[j].lemma);
      }
      // change text value to joined lemmas  
      keyword.text = keyword.lemmas.join(" ");

      // add keyword to list of this.documents
      this.documents.push(keyword);

      // // add keyword to list of keywords
      queries.push(keyword);
    }


    // extract words from this.documents
    let uwords = [];
    for (let i in this.documents) uwords.push(...this.documents[i].lemmas);

    // remove all duplicate words
    uwords = Array.from(new Set(uwords)); // console.log(uwords);

    // calculate idfs for all (unique) words in all this.documents
    this.idfs = Cosim.getIdfs(uwords, this.documents); //console.log(idfs);
    let numDocuments = this.documents.length;

    // Calculate all tf and tf-idf on all documents
    // Including keywords (queries)
    for (let i in this.documents) {
      this.documents[i].id = i;
      Cosim.calculateTf(this.documents[i]);
      Cosim.calculateIdf(this.documents[i], this.idfs, numDocuments);
    }

    // Calculate similarity with other documents
    for (let i in this.documents) {
      let doc = this.documents[i];
      doc.sims = [];
      for (let j in queries) {
        let que = queries[j];
        // Only calculate documents similarity 
        // which is in the same sentence as keywords (queries)
        if (que.sentenceIndex != doc.sentenceIndex) continue;

        let similarity = Cosim.cosineSimilarity(que, doc);
        if (similarity > 0) {
          doc.sims.push({
            query: que,
            sim: similarity
          });
        }
      }
    }

    // console.log(this.keywords);
    // console.log(this.documents);
    // console.log(this.keywords);
  }

  getSentenceText(tokens) {
    let text = '';
    for (let i = 0; i < tokens.length; i++) {
      text += tokens[i].originalText + tokens[i].after;
    }
    return text;
  }

  processSentence(sent, index) {
    // let sentences = this.getSentences();
    // let index = $(this).find(":selected").data('index');
    // let sent = sentences[index];
    let keywords = sent.keywords;
    sent.read = true;

    // referencing UI list containers
    let keywordList = $('#exp-keyword-list');
    let tripleList = $('#exp-triple-list');
    let tripleKeywordSimList = $('#exp-triple-keyword-sim-list');
    let tripleReverbList = $('#exp-triple-reverb-list');

    $('#selected-sentence').html('' + sent.string + '');
    // console.log(localStorage);

    keywordList.html('');
    for (let i in keywords) {
      let keyword = keywords[i];
      keywordList.append('<li>' + keyword.originalText + ' - <span class="badge badge-info keyword-score" data-tippy-content="Keyword score">' + keyword.score.toFixed(2) + '</span> &nbsp; <a class="bt-add-keyword-as-concept badge badge-warning" data-type="concept" data-label="' + keyword.originalText + '"><i class="fas fa-plus"></i> Add as Concept</a></li>');
    }

    let ts = tippy('.keyword-score', {
      arrow: true,
      arrowType: 'round', // or 'sharp' (default)
      animation: 'fade',
    });

    let triples = sent.openie;
    tripleList.html(triples.length ? '' : '<li style="text-align:center; color:#999999; padding:0; margin: 0; list-style:none"><em>Sorry, we could not find any more suggestions in this sentence for you.</em></li>');
    tripleKeywordSimList.html('');
    tripleReverbList.html('');

    for (let i in triples) {
      let triple = triples[i];

      tripleList.append('<li><span class="prop-group" data-subject="' + triple.subject + '" data-relation="' + triple.relation + '" data-object="' + triple.object + '" data-sentence="' + index + '"><span class="concept-ref"><span class="concept concept-subject" data-triple-type="subject">' + triple.subject + '</span> <i class="fas fa-caret-up text-primary"></i></span> &nbsp; <i class="fas fa-long-arrow-alt-right text-success"></i> &nbsp; <span class="relation">' + triple.relation + '</span> &nbsp; <i class="fas fa-long-arrow-alt-right text-success"></i> &nbsp; <span class="concept-ref"><span class="concept concept-object" data-triple-type="object">' + triple.object + '</span> <i class="fas fa-caret-up text-primary"></i></span></span> &nbsp; <a class="bt-edit-proposition badge badge-warning"><i class="fas fa-pen"></i> Edit</a> <a class="bt-add-proposition badge badge-primary text-white" data-ttype="openie" data-type="concept" data-subject="' + triple.subject + '" data-relation="' + triple.relation + '" data-object="' + triple.object + '" data-sentence="' + index + '"><i class="fas fa-plus"></i> Add Proposition</a></li>');

    }

    let keywordSimTriples = this.getSimilarTriples(triples);

    for (let i in keywordSimTriples) {

      let triple = keywordSimTriples[i]; // console.log(triple);
      tripleKeywordSimList.append('<li><span class="prop-group" data-subject="' + triple.subjectReplacement.originalText + '" data-relation="' + triple.relation + '" data-object="' + triple.objectReplacement.originalText + '" data-sentence="' + index + '"><span class="concept-ref"><span class="concept concept-subject" data-triple-type="subject">' + triple.subjectReplacement.originalText + '</span> <i class="fas fa-caret-up text-primary"></i></span> &nbsp; <i class="fas fa-long-arrow-alt-right text-success"></i> &nbsp; <span class="relation">' + triple.relation + '</span> &nbsp; <i class="fas fa-long-arrow-alt-right text-success"></i> &nbsp; <span class="concept-ref"><span class="concept concept-object" data-triple-type="object">' + triple.objectReplacement.originalText + '</span> <i class="fas fa-caret-up text-primary"></i></span></span> &nbsp; <a class="bt-edit-proposition badge badge-warning"><i class="fas fa-pen"></i> Edit</a> <a class="bt-add-proposition badge badge-primary text-white"  data-ttype="openie-sim" data-type="concept" data-subject="' + triple.subjectReplacement.originalText + '" data-relation="' + triple.relation + '" data-object="' + triple.objectReplacement.originalText + '" data-sentence="' + index + '"><i class="fas fa-plus"></i> Add Proposition</a></li>');

    }

    let reverbTriples = ReVerb.getTriples(sent, index, this.idfs, this.documents.length);
    // console.log(reverbTriples);

    for (let i in reverbTriples) {
      let triple = reverbTriples[i];
      // console.log(triple);
      tripleReverbList.append('<li><span class="prop-group" data-subject="' + triple.subject.originalText + '" data-relation="' + triple.relation + '" data-object="' + triple.object.originalText + '" data-sentence="' + index + '"><span class="concept-ref"><span class="concept concept-subject" data-triple-type="subject">' + triple.subject.originalText + '</span> <i class="fas fa-caret-up text-primary"></i></span> &nbsp; <i class="fas fa-long-arrow-alt-right text-success"></i> &nbsp; <span class="relation">' + triple.relation + '</span> &nbsp; <i class="fas fa-long-arrow-alt-right text-success"></i> &nbsp; <span class="concept-ref"><span class="concept concept-object" data-triple-type="object">' + triple.object.originalText + '</span> <i class="fas fa-caret-up text-primary"></i></span></span> &nbsp; <a class="bt-edit-proposition badge badge-warning"><i class="fas fa-pen"></i> Edit</a> <a class="bt-add-proposition badge badge-primary text-white" data-ttype="reverb" data-type="concept" data-subject="' + triple.subject.originalText + '" data-relation="' + triple.relation + '" data-object="' + triple.object.originalText + '" data-sentence="' + index + '"><i class="fas fa-plus"></i> Add Proposition</a></li>');

    }

    if (reverbTriples.length > 0) $('#exp-triple-reverb').show();
    else $('#exp-triple-reverb').hide();

    if (keywordSimTriples.length > 0) $('#exp-triple-keyword-sim').show();
    else $('#exp-triple-keyword-sim').hide();

    // if(triples.length > 0) $('#exp-triple').show(); 
    // else $('#exp-triple').hide();

    let ref = '.concept-ref';
    let canvas = this.canvas;
    let support = this;
    let a = tippy(ref, {
      // theme: 'light',
      arrow: true,
      arrowType: 'round', // or 'sharp' (default)
      boundary: 'viewport',
      // animation: 'fade',
      content: 'A',
      interactive: true,
      distance: 5,
      animateFill: false,
      trigger: 'click',
      maxWidth: 650,
      onShow: function (e) {

        let concepts = support.canvas.getNodes('[type="concept"]');
        // console.log(concepts);
        let concept = $(e.reference).find('.concept');
        // console.log(concept);
        let type = $(concept).data('triple-type');
        let def = $(e.reference).parent('.prop-group').data(type);
        // console.log(def);
        let contents = '<a class="dropdown-item bt-replace" data-name="' + def + '">' + def + ' <span class="badge badge-success">&nbsp;</span></a>';
        for (let i = 0; i < concepts.length; i++) {
          if (def == concepts[i].data.name) continue;
          contents = '<a class="dropdown-item bt-replace" data-id="' + concepts[i].data.id + '" data-name="' + concepts[i].data.name + '">' + concepts[i].data.name + '</a>'
            + contents;
        }
        for (let i = 0; i < a.length; i++) {
          a[i].setContent('<div class="tippy-wrapper" style="max-height: calc(vh - 1rem); overflow: scroll">' + contents + '</div>');
        }

        // console.log(e.reference._tippy);

        $('body').off('click').on('click', '.bt-replace', function () {
          let c = $(e.reference).find('.concept');
          // console.log($(c).html());
          $(c).html($(this).data('name'));
          // if (typeof Logger != 'undefined') Logger.log('replace-concept', {
          //   t: type,
          //   df: def,
          //   to: $(this).data('name')
          // });
          e.reference._tippy.hide();
        });

        // $(a)._tippy.setContent('Yo!');
      }
    });
    // console.log(a);
    // console.log($('.concept'), $('#alt-concept'));
    // $('.concept').click(function(){
    //   $('#alt-concept').toggle();
    //   var popper = new Popper($('.concept'), $('#alt-concept'), {
    //     placement: 'top'
    //   });
    //   popper.scheduleUpdate();
    //   // console.log(popper);
    // });

    // console.log('sentence-processed', index);
    // if (typeof Logger != 'undefined') Logger.log('sentence-processed', {mid: this.mid});

    let animateShowHideButton = function () {
      $('#bt-show-hide-more-suggestions')
        .removeClass('animated pulse')
        .addClass('animated pulse');

      var elm = $('#bt-show-hide-more-suggestions'); // console.log(elm);
      var newone = elm[0].cloneNode(true);
      elm[0].parentNode.replaceChild(newone, elm[0]);
      setTimeout(animateShowHideButton, 5000);
    }
    if (!$('#bt-show-hide-more-suggestions').hasClass('animated'))
      animateShowHideButton();
    // setTimeout(animateShowHideButton, 5000);

    return {
      sentence: sent,
      keywords: keywords,
      openieTriples: triples,
      simTriples: keywordSimTriples,
      reverbTriples: reverbTriples
    }

  }

  getSimilarTriples(triples) {

    // console.log(triples);

    let keywordSimTriples = [];
    for (let i in triples) {
      let triple = triples[i];
      // console.log(triple);
      triple.subjectReplacement =
        Cosim.getSimilarConceptKeyword(triple.subject, this.documents);
      triple.objectReplacement =
        Cosim.getSimilarConceptKeyword(triple.object, this.documents);

      if (triple.subjectReplacement == null ||
        triple.objectReplacement == null) continue;

      let subSame, preSame, objSame;
      let conSameTriple, index = -1;
      for (let j in keywordSimTriples) {
        conSameTriple = null;
        let keywordSimTriple = keywordSimTriples[j];
        subSame = keywordSimTriple.subjectReplacement.originalText ==
          triple.subjectReplacement.originalText;
        objSame = keywordSimTriple.objectReplacement.originalText ==
          triple.objectReplacement.originalText;
        preSame = keywordSimTriple.relation == triple.relation;
        if (subSame && preSame && objSame) break;
        if (subSame && objSame) {
          conSameTriple = keywordSimTriple;
          index = j;
          break;
        }
        // console.log(subSame);
        // console.log(preSame);
        // console.log(objSame);
        // console.log(index);
      }

      // if not equal in any way with previous triple...
      if (!(subSame && preSame && objSame)) {

        // if only both concepts are equal
        if (conSameTriple != null) {
          if (conSameTriple.relation.length < triple.relation.length) {
            keywordSimTriples.splice(index, 1);
            keywordSimTriples.push(triple);
          }
        } else { // otherwise... add to triples list
          keywordSimTriples.push(triple);
        }

      }

      // console.log(triple);
    }
    return keywordSimTriples;
  }

  updateProgress() {
    let max = this.sentences.length;
    let read = 0;
    for (let i = 0; i < this.sentences.length; i++)
      if (this.sentences[i].read) read++;

    let now = this.sentenceIndex + 1;
    let valeur = parseInt(read / max * 100);
    $('#exp-collab .exp-progress .progress-bar').css('width', valeur + '%').attr('aria-valuenow', valeur);
    $('#exp-progress-text').html('<span>Sentence ' + now + ' / ' + max + '</span> <span>' + read + " sentence(s) read (" + valeur + "%)</span>");
    if (valeur >= 99) $('#bt-finalize').removeClass('disabled');
  }

  addProposition(subject, relation, object, sentenceIndex) {
    let support = this;

    if (subject == '') this.support.gui.notify('First concept can not empty', { type: 'warning' });
    if (relation == '') this.support.gui.notify('Link can not empty', { type: 'warning' });
    if (object == '') this.support.gui.notify('Second concept can not empty', { type: 'warning' });

    if (subject == '' || relation == '' || object == '') return null;

    let results =
      support.canvas.addProposition(subject, relation, object, sentenceIndex);

    if (results != null) {
      // results.collection.layout({ name: 'cose-bilkent' }).run();
      support.gui.notify('Proposition of <strong>"' + subject + '" - "' + relation + '" - "' + object + '"</strong> is successfully added.', { type: 'success' });
    }
    // this.support.canvas.updateCanvasInfo();
    return results;
  }

  showSentences() {
    let support = this;
    let sentencesList = $('#collab-sentences-list').html('');
    // console.log(collab.sentences.length);
    for (let i = 0; i < support.sentences.length; i++) {
      let sent = support.sentences[i]; // console.log(i, sent.read);
      sentencesList.append('<tr>' +
        '<td>' + this.getSentenceText(sent.tokens) + '</td>' +
        '<td>' + (sent.read ? '<span class="badge badge-success">Read</span>' : '<span class="badge badge-warning">Unread</span>') + '</td>' +
        '<td><button id="bt-select-sentence" data-index="' + i + '" class="btn btn-sm btn-outline-primary">Read</button></td>' +
        '</tr>');
    }
    
    $('#modal-sentences').off('click').on('click', '#bt-select-sentence', function () {
      // console.log(this);
      support.getRecommendationOfSentence($(this).data('index'))
      // if (typeof Logger != 'undefined') Logger.log('select-sentence', { sidx: support.sentenceIndex });
      $('#modal-sentences').modal('hide');
    });
    $('#modal-sentences').modal('show');
  }

  showManualProposition(proposition = null) {
    if (proposition) {
      $('#modal-proposition .modal-title').html('Edit Proposition');
      $('#modal-proposition #exp-manual-konsep-a').val(proposition.subject);
      $('#modal-proposition #exp-manual-link').val(proposition.relation);
      $('#modal-proposition #exp-manual-konsep-b').val(proposition.object);
    } else {
      $('#modal-proposition .modal-title').html('Create Proposition');
      $('#modal-proposition #exp-manual-konsep-a').val('');
      $('#modal-proposition #exp-manual-link').val('');
      $('#modal-proposition #exp-manual-konsep-b').val('');
    }
    let modal = $('#modal-proposition').modal('show');
    return modal;
  }

  getRecommendationOfSentence(sentenceIndex) {
    let sent = this.sentences[sentenceIndex];
    $('#exp-collab-sentence').text(this.getSentenceText(sent.tokens));
    let suggestions = this.processSentence(sent, sentenceIndex);
    let data = {
      sentence: suggestions.sentence.string,
      keywords: [],
      reverbs: [],
      sims: [],
    }
    suggestions.keywords.forEach(k => {
      data.keywords.push({
        keyword: k.originalText,
        score: k.score.toFixed(2)
      })
    });
    suggestions.reverbTriples.forEach(t => {
      data.reverbs.push({
        subject: t.subject.originalText,
        relation: t.relation,
        object: t.object.originalText
      });
    })
    suggestions.simTriples.forEach(t => {
      data.sims.push({
        subject: t.subjectReplacement.originalText,
        relation: t.relation,
        object: t.objectReplacement.originalText
      });
    })
    this.updateProgress();
    this.sentenceIndex = sentenceIndex;
    // if (typeof Logger != 'undefined') Logger.log('begin-construct-sentence', 
    //   { mid: this.mid, sentenceIndex: sentenceIndex });
    return data;
  }








  // Modal Handling

  /* Modal for Exp CKB Collab Dialog */

  show(collab, options = null) {

    let support = this;

    support.settings = Object.assign(support.settings, options);

    if(support.process) {
      support.sentences = Nlp.postProcess(support.nlp.sentences);
      support.initializeSentences(support.sentences); // console.log(sentences);
      if (support.sentenceIndex == null) support.sentenceIndex = 0;
    }

    if(support.sentences.length == 0) {
      support.gui.notify('Invalid post-processed sentences.');
      return;
    }

    support.getRecommendationOfSentence(support.sentenceIndex);

    // let data = [];
    // for(let i = 0; i < sentences.length; i++) {
    //   data.push(support.goToSentence(i));
    // }
    // console.log(JSON.stringify(data));

    support.updateProgress();
    // if(collab.sentenceIndex == 0 && $('button.sw-btn-next.disabled').length == 1) {
    // if (typeof Logger != 'undefined') Logger.log('begin-construct', { mid: collab.mid });
    // if (typeof Logger != 'undefined') Logger.log('begin-construct-sentence', { mid: collab.mid, sidx: collab.sentenceIndex });
    // }
    $('#bt-sentences-list').off('click').on('click', function () {
      support.showSentences();
      // if (typeof Logger != 'undefined') Logger.log('show-sentence-list');
    });

    $('#bt-manual-proposition').off('click').on('click', function () {
      support.showManualProposition();
      // if (typeof Logger != 'undefined') Logger.log('show-manual-proposition-dialog');
    });

    $('#bt-add-manual-proposition').off('click').on('click', function (e) {
      e.preventDefault();

      let subject = $('#exp-manual-konsep-a').val().toString().trim();
      let relation = $('#exp-manual-link').val().toString().trim();
      let object = $('#exp-manual-konsep-b').val().toString().trim();

      if (subject == '') {
        $('#exp-manual-konsep-a').addClass('is-invalid');
      } else $('#exp-manual-konsep-a').removeClass('is-invalid');
      if (object == '') {
        $('# exp-manual-konsep-b').addClass('is-invalid');
      } else $('#exp-manual-konsep-b').removeClass('is-invalid');
      if (relation == '') {
        $('#exp-manual-link').addClass('is-invalid');
      } else $('#exp-manual-link').removeClass('is-invalid');

      if (subject == '' || object == '' || relation == '') {
        collab.support.gui.notify('First concept, second concept, and link may not be empty', { type: 'warning' });
        return false;
      }

      let result = support.addProposition(subject, relation, object, 0);
      if (!result) {
        support.gui.notify('Proposition <strong>' + subject + ' - ' + relation + ' - ' + object + '</strong> already exist.', { type: 'warning' });
      }
      // console.log(result);

      cy.animate({ fit: { eles: cy, padding: 50 } });

      // if (typeof Logger != 'undefined') Logger.log('add-manual-proposition', {
      //   s: subject, r: relation, o: object
      // });

    });

    $('#modal-support').modal('show');
    this.process = false; // already cached, so don't post-process again.

  }

  hide() {
    $('#modal-support').modal('hide');
  }

  /* Reflection Modal */

  showReflection() {
    $('#modal-reflection').modal('show');
  }

}