  <?php $this->language('kbui/en'); ?>
  <?php if(isset($_SESSION['lang']) && $_SESSION['lang'] == 'jp') $this->language('kbui/jp'); ?>
  <div class="kb-container">
    <div id="<?php echo isset($canvasId) ? $canvasId : 'cy'; ?>" class="kb-cy"></div>
    <?php $this->view('kbui/kbui.toolbar.php'); ?>
    <?php $this->view('kbui/kbui.modal.php'); ?>
  </div>
