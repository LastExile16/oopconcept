<?php $this->view('base/header.php');?>
<?php 
  $this->language('general.en'); 
  $this->language('analyzer/en');
?>
<?php if(isset($_SESSION['lang']) && $_SESSION['lang'] == 'jp') {
  $this->language('general.jp');
  $this->language('analyzer/jp'); 
}
?>
<div id="main-container">
  <div id="app-toolbar">
    <?php $this->view('sho/cmap.lang.php'); ?>
    <?php $this->view('sho/cmap.nav.php'); ?>
    <div class="btn-group">
      <button id="bt-open-kit" data-tippy-content="Open Kit" title="Kit" class="btn btn-sm btn-primary">
        <i class="fas fa-folder-open"></i> <?php echo $this->l('cmap-open-teacher-map'); ?>
      </button>
    </div>
    <button id="bt-logout" class="btn btn-sm btn-danger ml-3" data-tippy-content="Sign Out">
      <i class="fas fa-sign-out-alt"></i> Sign Out      <!-- Sign Out <span id="bt-logout-username" class="font-weight-bold"></span> -->
    </button>
  </div>

  <div id="panel-container" style="display: flex; flex: 1">
    <div class="col-3 pl-0 pr-2 d-flex" style="min-width: 280px;">
      <?php $this->view('sho/analyzer.sidebar.php'); ?>
    </div>
    <div class="col pl-0 pr-0" style="flex: 1; display:flex;">
      <?php $this->view('kbui/kbui.canvas.php'); ?>
    </div>
  </div> <!-- /panel-container -->

</div> <!-- /Main Container -->

<?php $this->view('sho/modal.php');?>
<?php // $this->view('kbui/kbui.modal.php'); ?>
<?php $this->view('general/general.ui.php');?>
<?php $this->view('base/footer.php');?>