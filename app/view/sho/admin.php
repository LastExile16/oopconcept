<?php $this->view('base/header.php'); ?>
<?php 
  $this->language('general.en'); 
  $this->language('cmap/en');
  $this->language('admin/en');
?>
<?php if(isset($_SESSION['lang']) && $_SESSION['lang'] == 'jp') {
  $this->language('general.jp');
  $this->language('cmap/jp');
  $this->language('admin/jp'); 
}
?>
<div id="main-container" class="container">

  <div class="row">

    <?php $this->view('sho/admin.nav.php'); ?>

    <div class="col mb-3">
      <h2><?php echo $this->l('system-administration'); ?> <span><?php $this->view('sho/cmap.nav.php'); ?></span></h2>
      <hr>
    </div>

    <!-- <div class="col-md-6 mb-3">
      <div class="row justify-content-between mr-3 ml-1">
        <h4>Room 
          <span id="bt-refresh-room" class="btn btn-sm btn-outline-secondary"><i class="fas fa-sync"></i></span>
          <span id="bt-filter-room" class="btn btn-sm btn-outline-secondary"><i class="fas fa-filter"></i></span>
        </h4>
        <div class="btn-group btn-sm">
          <button id="bt-create-room" class="btn btn-sm btn-outline-primary"><i class="fas fa-plus"></i> Create Room</button>
          <button class="bt-publish-room btn btn-sm btn-outline-primary"><i class="fas fa-broadcast-tower"></i></button>
        </div>
      </div>
      <div class="row filter mt-2 ml-2 mr-2" style="display: none">
        <input type="text" autocomplete="new-password" class="form-control form-control-sm input-keyword-room" name="keyword" class="input-filter" />
      </div>
      <hr>
      <div id="room-list" class="list-container"></div>
    </div>

    <div class="col-md-6 mb-3">
      <div class="row justify-content-between mr-3 ml-1">
        <h4>Lobby <span id="bt-refresh-lobby" class="btn btn-sm btn-outline-secondary"><i class="fas fa-sync"></i></span></h4>
      </div>
      <hr>
      <div id="lobby-list" class="list-container"></div>
    </div> -->

  </div>

  <div class="row">
    <div class="col-md-6 mb-3">
      <div class="row justify-content-between mr-3 ml-1">
        <h4><?php echo $this->l('admin-user'); ?> 
          <span id="bt-refresh-user" class="btn btn-sm btn-outline-secondary"><i class="fas fa-sync"></i></span>
          <span id="bt-filter-user" class="btn btn-sm btn-outline-secondary"><i class="fas fa-filter"></i></span>
        </h4>
        <form method="POST" enctype="multipart/form-data" id="csvform">
        <div class="btn-group btn-sm">
          <button id="bt-download-template" class="btn btn-sm btn-outline-warning text-dark"><i class="fas fa-file-csv"></i> <span class="filename">CSV</span></button>
          <button id="bt-import-user" class="btn btn-sm btn-outline-warning text-dark"><i class="fas fa-file-import"></i> <span class="filename">Import CSV</span></button>
          <input type="file" name="csv" class="d-none">
          <button id="bt-upload-csv" class="btn btn-sm btn-outline-warning text-dark"><i class="fas fa-file-upload"></i></button>
          <button id="bt-create-user" class="btn btn-sm btn-outline-primary"><i class="fas fa-plus"></i> Create User</button>
        </div>
        </form>
      </div>
      <div class="row filter mt-2 ml-2 mr-2" style="display: none">
        <input type="text" autocomplete="new-password" class="form-control form-control-sm input-keyword-user" name="keyword" class="input-filter" />
      </div>
      <hr>
      <div id="user-list" class="list-container"></div>
    </div>
    <div class="col-md-6 mb-3">
      <div class="row justify-content-between mr-3 ml-1">
        <h4><?php echo $this->l('admin-role'); ?>  
          <span id="bt-refresh-role" class="btn btn-sm btn-outline-secondary"><i class="fas fa-sync"></i></span>
          <span id="bt-filter-role" class="btn btn-sm btn-outline-secondary"><i class="fas fa-filter"></i></span>
        </h4>
        <div class="btn-group btn-sm">
          <button id="bt-create-role" class="btn btn-sm btn-outline-primary"><i class="fas fa-plus"></i> Create Role</button>
        </div>
      </div>
      <div class="row filter mt-2 ml-2 mr-2" style="display: none">
        <input type="text" autocomplete="new-password" class="form-control form-control-sm input-keyword-role" name="keyword" class="input-filter" />
      </div>
      <hr>
      <div id="role-list" class="list-container"></div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6 mb-3">
      <div class="row justify-content-between mr-3 ml-1">
        <h4>Group 
          <span id="bt-refresh-group" class="btn btn-sm btn-outline-secondary"><i class="fas fa-sync"></i></span>
          <span id="bt-filter-group" class="btn btn-sm btn-outline-secondary"><i class="fas fa-filter"></i></span>
        </h4>
        <div class="btn-group btn-sm">
          <button id="bt-create-group" class="btn btn-sm btn-outline-primary"><i class="fas fa-plus"></i> Create Group</button>
        </div>
      </div>
      <div class="row filter mt-2 ml-2 mr-2" style="display: none">
        <input type="text" autocomplete="new-password" class="form-control form-control-sm input-keyword-group" name="keyword" class="input-filter" />
      </div>
      <hr>
      <div id="group-list" class="list-container"></div>
    </div>
    <div class="col-md-6 mb-3">
      <div class="row justify-content-between mr-3 ml-1">
        <h4>Material 
          <span id="bt-refresh-material" class="btn btn-sm btn-outline-secondary"><i class="fas fa-sync"></i></span>
          <span id="bt-filter-material" class="btn btn-sm btn-outline-secondary"><i class="fas fa-filter"></i></span>
        </h4>
        <div class="btn-group btn-sm">
          <button id="bt-create-material" class="btn btn-sm btn-outline-primary"><i class="fas fa-plus"></i> Create Material</button>
        </div>
      </div>
      <div class="row filter mt-2 ml-2 mr-2" style="display: none">
        <input type="text" autocomplete="new-password" class="form-control form-control-sm input-keyword-material" name="keyword" class="input-filter" />
      </div>
      <hr>
      <div id="material-list" class="list-container"></div>
    </div>
    <div class="col-md-6 mb-3">
      <div class="row justify-content-between mr-3 ml-1">
        <h4>Support: Material Content and NLP 
          <span id="bt-refresh-material-cmm" class="btn btn-sm btn-outline-secondary"><i class="fas fa-sync"></i></span>
          <span id="bt-filter-material-cmm" class="btn btn-sm btn-outline-secondary"><i class="fas fa-filter"></i></span>
        </h4>
      </div>
      <div class="row filter mt-2 ml-2 mr-2" style="display: none">
        <input type="text" autocomplete="new-password" class="form-control form-control-sm input-keyword-material-cmm" name="keyword" class="input-filter" />
      </div>
      <hr>
      <div id="material-cmm-list" class="list-container"></div>
    </div>
  </div>

</div>
<?php $this->view('sho/admin.modal.php'); ?>
<?php $this->language('general.en'); ?>
<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>