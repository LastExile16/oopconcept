<!-- Modal Login -->
<div class="modal" id="modal-login" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form">
          <div class="form-group row">
            <label class="col-sm-4 col-form-label text-right">Username</label>
            <div class="col-sm-6">
              <input class="form-control input-username" placeholder="Username">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-4 col-form-label text-right">Password</label>
            <div class="col-sm-6">
              <input type="password" class="form-control input-password" placeholder="Password">
            </div>
          </div>
        </div>
        <div class="text-center"><small class="modal-helper-text">Enter username and passsword. Both fields cannot
            empty.</small></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">&nbsp; &nbsp; Sign-In &nbsp;
          &nbsp;</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- New Material Form -->
<div class="modal" id="modal-create-material" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false"
  data-backdrop="static">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4>Create New Material</h4>
        <hr>
        <!-- <form id="form-create-material"> -->
        <input type="hidden" name="uid" class="input-material-creator-id" value="">
        <div class="form-group">
          <input type="text" maxlength="150" name="name" class="form-control input-material-name"
            placeholder="Material Title">
        </div>
        <div class="form-group">
          <input type="text" maxlength="50" name="name" class="form-control input-material-fid"
            placeholder="Friendly ID (Optional)">
        </div>
        <!-- </form> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">OK</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- /New Material Form -->

<!-- Edit Material Form -->
<div class="modal" id="modal-edit-material" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false"
  data-backdrop="static">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4>Edit Material</h4>
        <hr>
        <!-- <form id="form-create-material"> -->
        <input type="hidden" name="uid" class="input-material-creator-id" value="">
        <div class="form-group">
          <input type="text" maxlength="150" name="name" class="form-control input-material-name"
            placeholder="Material Title">
        </div>
        <div class="form-group">
          <input type="text" maxlength="50" name="name" class="form-control input-material-fid"
            placeholder="Friendly ID">
        </div>
        <!-- </form> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">OK</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- /Edit Material Form -->

<!-- Edit Material CMM Form -->
<div class="modal" id="modal-edit-material-cmm" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false"
  data-backdrop="static">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4>Edit Support Data for Material</h4>
        <h5><span class="material-mid badge badge-warning"></span>
          <span class="material-name font-italic">Loading...</span></h5>
        <hr>
        <!-- <form id="form-create-material"> -->
        <input type="hidden" name="uid" class="input-material-cmm-creator-id" value="">
        <div class="form-group">
          <!-- <input type="text" maxlength="150" name="name" class="form-control input-material-cmm-name"
            placeholder="Material Title"> -->
          <label for="content">Contents</label>
          <textarea name="content" class="form-control input-material-cmm-content mb-2" rows="7"></textarea>
          <button class="btn btn-sm btn-info bt-process-nlp pl-2 pr-2"><i class="fas fa-code"></i> Process in CoreNLP
            Server</button>
          <button class="btn btn-sm btn-primary bt-save-content pl-2 pr-2"><i class="fas fa-save"></i> Save
            Contents</button>
        </div>
        <hr>
        <div class="form-group">
          <!-- <input type="text" maxlength="50" name="name" class="form-control input-material-cmm-fid"
            placeholder="Friendly ID"> -->
          <label for="content">NLP Data Structure</label>
          <textarea name="nlp" class="form-control input-material-cmm-nlp mb-2 text-monospace" rows="7"></textarea>
          <button class="btn btn-sm btn-primary bt-save-nlp pl-2 pr-2"><i class="fas fa-save"></i> Save NLP Data
            Structure</button>
        </div>
        <!-- </form> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- /Edit Material Form -->

<!-- New User Form -->
<div class="modal" id="modal-create-user" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4>New User</h4>
        <hr>
        <div class="form-row">
          <div class="col">
            <label>Username</label>
            <input type="text" name="username" class="form-control input-user-username" placeholder="Username">
          </div>
          <div class="col">
            <label>Password <span class="bt-generate-password badge badge-warning" style="cursor: pointer"><i
                  class="fas fa-magic"></i></span></label>
            <input type="password" name="username" class="form-control input-user-password" placeholder="Password">
          </div>
        </div>
        <hr>
        <div class="form-row">
          <div class="col">
            <label>User Name</label>
            <input type="text" name="name" class="form-control input-user-name" placeholder="Name">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">OK</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- /New User Form -->

<!-- Edit User Form -->
<div class="modal" id="modal-edit-user" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4>Edit User</h4>
        <hr>
        <div class="form-row">
          <div class="col">
            <label>Username</label>
            <input type="text" name="username" class="form-control input-user-username" placeholder="Username">
          </div>
          <div class="col">
            <label>Password <span class="bt-generate-password badge badge-warning" style="cursor: pointer"><i
                  class="fas fa-magic"></i></span></label>
            <input type="password" name="username" class="form-control input-user-password" placeholder="Password">
          </div>
        </div>
        <hr>
        <div class="form-row">
          <div class="col">
            <label>User Name</label>
            <input type="text" name="name" class="form-control input-user-name" placeholder="Name">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">OK</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- /Edit User Form -->

<!-- Modal Create Room -->
<div class="modal" id="modal-create-room" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Create New Group</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-inline">
          <div class="form-group mb-2">
            <span class="modal-input-label">Room Name</span>
          </div>
          <div class="form-group mx-sm-3 mb-2" style="position:relative">
            <input type="text" class="form-control input-room-name" style="width:300px;" required>
            <div class="invalid-tooltip">
              Please provide a room name.
            </div>
          </div>
        </div>
        <small class="modal-helper-text">Enter a room name. Room name cannot empty.</small>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">OK</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Create Room -->

<!-- Modal Edit Room -->
<div class="modal" id="modal-edit-room" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Room</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-inline">
          <div class="form-group mb-2">
            <span class="modal-input-label">Room Name</span>
          </div>
          <div class="form-group mx-sm-3 mb-2" style="position:relative">
            <input type="text" class="form-control input-room-name" style="width:300px;" required>
            <div class="invalid-tooltip">
              Please provide a room name.
            </div>
          </div>
        </div>
        <small class="modal-helper-text">Enter a room name. Room name cannot empty.</small>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">OK</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Edit Room -->

<!-- Modal Create Group -->
<div class="modal" id="modal-create-group" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Create New Group</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-inline">
          <div class="form-group mb-2 col-sm-4">
            <span class="modal-input-label">Name</span>
          </div>
          <div class="form-group mx-sm-3 mb-2 col-sm-8" style="position:relative">
            <input type="text" class="form-control input-group-name" style="width:300px;" required>
            <div class="invalid-tooltip">
              Please provide a group name.
            </div>
          </div>
        </div>
        <div class="form-inline">
          <div class="form-group mb-2 col-sm-4">
            <span class="modal-input-label">Friendly ID</span>
          </div>
          <div class="form-group mx-sm-3 mb-2 col-sm-8" style="position:relative">
            <input type="text" class="form-control input-group-fid" style="width:300px;" required>
          </div>
        </div>
        <small class="modal-helper-text">Enter a group name. Group name cannot empty.</small>
        <hr>
        <div class="form-inline">
          <div class="form-group mb-2">
            <span class="modal-input-label">Group Type</span>
          </div>
          <div class="form-group mx-sm-3 mb-2" style="position:relative">
            <div class="form-check form-check-inline">
              <input class="form-check-input input-group-type input-group-type-regular" type="radio"
                name="input-group-create-type" id="inlineRadio4" value="regular" checked="checked">
              <label class="form-check-label" for="inlineRadio4">Regular</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input input-group-type input-group-type-pilot" type="radio"
                name="input-group-create-type" id="inlineRadio1" value="pilot">
              <label class="form-check-label" for="inlineRadio1">Pilot</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input input-group-type input-group-type-practice" type="radio"
                name="input-group-create-type" id="inlineRadio2" value="practice">
              <label class="form-check-label" for="inlineRadio2">Practice</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input input-group-type input-group-type-experimental" type="radio"
                name="input-group-create-type" id="inlineRadio3" value="experimental">
              <label class="form-check-label" for="inlineRadio3">Experimental</label>
            </div>
          </div>
        </div>
        <hr>
        <div class="form-row">
          <div class="col">
            <input type="text" placeholder="Grade" class="form-control input-group-grade">
          </div>
        </div>
        <hr>
        <div class="form-row">
          <div class="col">
            <input type="text" placeholder="Class" class="form-control input-group-class">
          </div>
        </div>
        <hr>
        <div class="form-row">
          <div class="col">
            <textarea type="text" placeholder="Note" class="form-control input-group-note"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">OK</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Create Group -->

<!-- Modal Edit Group -->
<div class="modal" id="modal-edit-group" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Group</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-inline">
          <div class="form-group mb-2 col-sm-4">
            <span class="modal-input-label">Name</span>
          </div>
          <div class="form-group mx-sm-3 mb-2 col-sm-8" style="position:relative">
            <input type="text" class="form-control input-group-name" style="width:300px;" required>
            <div class="invalid-tooltip">
              Please provide a group name.
            </div>
          </div>
        </div>
        <div class="form-inline">
          <div class="form-group mb-2 col-sm-4">
            <span class="modal-input-label">Friendly ID</span>
          </div>
          <div class="form-group mx-sm-3 mb-2 col-sm-8" style="position:relative">
            <input type="text" class="form-control input-group-fid" style="width:300px;">
          </div>
        </div>
        <small class="modal-helper-text">Enter a group name. Group name cannot empty.</small>
        <hr>
        <div class="form-inline">
          <div class="form-group mb-2">
            <span class="modal-input-label">Group Type</span>
          </div>
          <div class="form-group mx-sm-3 mb-2" style="position:relative">
            <div class="form-check form-check-inline">
              <input class="form-check-input input-group-type input-group-type-regular" type="radio"
                name="input-group-edit-type" id="inlineRadio8" value="regular" checked="checked">
              <label class="form-check-label" for="inlineRadio8">Regular</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input input-group-type input-group-type-pilot" type="radio"
                name="input-group-edit-type" id="inlineRadio5" value="pilot">
              <label class="form-check-label" for="inlineRadio5">Pilot</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input input-group-type input-group-type-practice" type="radio"
                name="input-group-edit-type" id="inlineRadio6" value="practice">
              <label class="form-check-label" for="inlineRadio6">Practice</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input input-group-type input-group-type-experimental" type="radio"
                name="input-group-edit-type" id="inlineRadio7" value="experimental">
              <label class="form-check-label" for="inlineRadio7">Experimental</label>
            </div>
          </div>
        </div>
        <hr>
        <div class="form-row">
          <div class="col">
            <input type="text" placeholder="Grade" class="form-control input-group-grade">
          </div>
        </div>
        <hr>
        <div class="form-row">
          <div class="col">
            <input type="text" placeholder="Class" class="form-control input-group-class">
          </div>
        </div>
        <hr>
        <div class="form-row">
          <div class="col">
            <textarea type="text" placeholder="Note" class="form-control input-group-note"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">OK</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Edit Group -->

<!-- Modal Create Role -->
<div class="modal" id="modal-create-role" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Create New Role</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-row">
          <div class="col-sm-3">
            <input type="text" placeholder="Role ID" class="form-control input-role-rid">
          </div>
          <div class="col">
            <input type="text" placeholder="Role Name" class="form-control input-role-name">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">OK</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Create Role -->

<!-- Modal Edit Role -->
<div class="modal" id="modal-edit-role" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Role</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-row">
          <div class="col-sm-3">
            <input type="text" placeholder="Role ID" class="form-control input-role-rid">
          </div>
          <div class="col">
            <input type="text" placeholder="Role Name" class="form-control input-role-name">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">OK</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Edit Role -->

<?php $this->view('sho/modal.map-management.php'); ?>
<?php $this->view('sho/admin.modal.relation.php'); ?>