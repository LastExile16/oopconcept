
<!-- Map Management -->

<!-- Modal Material Goalmaps -->
<div class="modal" id="modal-material-goalmap" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><span class="material-name"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="d-flex">
          <div style="flex-basis:50%">
            <div class="d-flex align-items-center justify-content-between"><strong>Goalmaps</strong> <span
                class="btn btn-sm btn-outline-primary bt-refresh mr-2"><i class="fas fa-sync"></i> Refresh</span></div>
            <hr class="mt-1 mb-1">
            <div class="goalmap-list"></div>
          </div>
          <div style="flex-basis:50%">
            <div class="d-flex align-items-center justify-content-between"><strong>Kits</strong><span
                class="btn btn-sm mr-2"><i class="fas fa-null"></i></span></div>
            <hr class="mt-1 mb-1">
            <div class="kit-list"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Material Goalmaps -->

<!-- Modal Move Goalmaps -->
<div class="modal" id="modal-goalmap-move-material" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Move Goalmap to Materials/Topic</span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="d-flex">
          <div class="material-list w-100"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">OK</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Move Goalmaps -->

<!-- Modal Rename Goalmap -->
<div class="modal" id="modal-goalmap-rename" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Rename Goalmap</span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="d-flex">
          <div class="w-100">
            <input type="text" class="goalmap-name form-control" />
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">OK</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Rename Goalmap -->

<!-- Modal Edit Kit -->
<div class="modal" id="modal-kit-edit" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Kit</span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-row pr-2 pl-2">
          <label>Kit Name</label>
          <input type="text" class="form-control kit-name-input">
        </div>
        <div class="form-row pr-2 pl-2 mt-3">
          <label>Layout</label>
        </div>
        <div class="form-check">
          <input type="radio" name="layout" value="preset" id="kit-option-position-preset" class="form-check-input">
          <label for="kit-option-position-preset" class="form-check-label">Preset Position</label>
        </div>
        <div class="form-check">
          <input type="radio" name="layout" value="random" id="kit-option-position-random" class="form-check-input">
          <label for="kit-option-position-random" class="form-check-label">Randomize Position</label>
        </div>
        <div class="form-row pr-2 pl-2 mt-3">
          <label>Proposition Direction</label>
        </div>
        <div class="form-check">
          <input type="radio" name="directed" value="1" id="kit-option-directed" class="form-check-input">
          <label for="kit-option-directed" class="form-check-label">Directed</label>
        </div>
        <div class="form-check">
          <input type="radio" name="directed" value="0" id="kit-option-undirected" class="form-check-input">
          <label for="kit-option-undirected" class="form-check-label">Undirected</label>
        </div>
        <div class="form-row pr-2 pl-2 mt-3">
          <label>Enabled</label>
        </div>
        <div class="form-check">
          <input class="form-check-input kit-input-enabled" type="checkbox" value="1" id="kit-status-enabled">
          <label class="form-check-label" for="kit-status-enabled">
            Enabled
          </label>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">OK</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Edit Kit -->

<!-- Modal Learnermaps -->
<div class="modal" id="modal-learnermap" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Learnermaps: <span class="kit-name font-italic"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="learnermap-list"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">OK</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Learnermaps -->


