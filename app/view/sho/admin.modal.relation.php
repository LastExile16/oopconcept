<!-- Room-User Form -->
<div class="modal" id="modal-room-user" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4>Users in Room: <span class="room-name"></span></h4>
        <hr>
        <div class="row">
          <div class="col-sm-6">
            <h5>In Room</h5>
            <hr>
            <div class="in-list"></div>
          </div>
          <div class="col-sm-6">
            <h5>Not in Room</h5>
            <hr>
            <div class="not-in-list"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
<!-- /Room-User Form -->

<!-- Material-Qset Form -->
<div class="modal" id="modal-material-qset" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4>Materials in Group: <span class="material-name"></span></h4>
        <hr>
        <div class="row">
          <div class="col-sm-6">
            <h5>In Group</h5>
            <hr>
            <div class="in-list"></div>
          </div>
          <div class="col-sm-6">
            <h5>Not in Group</h5>
            <hr>
            <div class="not-in-list"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
<!-- /Material-Qset Form -->

<!-- Group-User Form -->
<div class="modal" id="modal-group-user" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4>Users in Group: <span class="group-name"></span></h4>
        <hr>
        <div class="row">
          <div class="col-sm-6">
            <h5>In Group</h5>
            <hr>
            <div class="in-list"></div>
          </div>
          <div class="col-sm-6">
            <h5>Not in Group</h5>
            <hr>
            <div class="not-in-list"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
<!-- /Group-User Form -->

<!-- Group-Material Form -->
<div class="modal" id="modal-group-material" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4>Materials in Group: <span class="group-name"></span></h4>
        <hr>
        <div class="row">
          <div class="col-sm-6">
            <h5>In Group</h5>
            <hr>
            <div class="in-list"></div>
          </div>
          <div class="col-sm-6">
            <h5>Not in Group</h5>
            <hr>
            <div class="not-in-list"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
<!-- /Group-Material Form -->

<!-- Group-Room Form -->
<div class="modal" id="modal-group-room" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4>Rooms in Group: <span class="group-name"></span></h4>
        <hr>
        <div class="row">
          <div class="col-sm-6">
            <h5>In Group</h5>
            <hr>
            <div class="in-list"></div>
          </div>
          <div class="col-sm-6">
            <h5>Not in Group</h5>
            <hr>
            <div class="not-in-list"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
<!-- /Group-Room Form -->

<!-- Group-Qset Form -->
<div class="modal" id="modal-group-qset" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4>Qset in Group: <span class="group-name"></span></h4>
        <hr>
        <div class="row">
          <div class="col-sm-6">
            <h5>In Group</h5>
            <hr>
            <div class="in-list"></div>
          </div>
          <div class="col-sm-6">
            <h5>Not in Group</h5>
            <hr>
            <div class="not-in-list"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
<!-- /Group-Qset Form -->

<!-- Role-User Form -->
<div class="modal" id="modal-role-user" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4>Users with Role: <span class="role-name"></span></h4>
        <hr>
        <div class="row">
          <div class="col-sm-6">
            <h5>In Group</h5>
            <hr>
            <div class="in-list"></div>
          </div>
          <div class="col-sm-6">
            <h5>Not in Group</h5>
            <hr>
            <div class="not-in-list"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>
<!-- /Role-User Form -->