<div class="btn-group kit-builder-toolbar d-none">
  <button class="bt-open-kit btn btn-sm btn-primary" data-tippy-content="<?php echo $this->l('cmap-open-kit'); ?>">
    <i class="fas fa-folder-open"></i> <?php echo $this->l('cmap-open-kit'); ?>
  </button>
  <button class="bt-save btn btn-sm btn-outline-info" data-tippy-content="<?php echo $this->l('cmap-save-map'); ?>">
    <i class="fas fa-save"></i> <span class="d-none d-sm-inline-block"><?php echo $this->l('save'); ?></span>
  </button>
  <button class="bt-load-draft btn btn-sm btn-outline-info"
    data-tippy-content="<?php echo $this->l('cmap-load-saved-map'); ?>">
    <i class="fas fa-upload"></i> <span
      class="d-none d-sm-inline-block"><?php echo $this->l('cmap-load-saved-map'); ?></span>
  </button>
</div>

<div class="btn-group kit-builder-toolbar d-none">
  <button id="bt-feedback" class="btn btn-sm btn-warning text-dark slim"
    data-tippy-content="<?php echo $this->l('cmap-get-feedback'); ?>">
    <i class="fas fa-life-ring text-danger"></i> <?php echo $this->l('cmap-get-feedback'); ?>
  </button>
  <button id="bt-upload" class="btn btn-sm btn-danger" data-tippy-content="<?php echo $this->l('upload'); ?>">
    <i class="fas fa-cloud-upload-alt"></i> <?php echo $this->l('cmap-upload-and-final'); ?>
  </button>
  <button id="bt-compare" class="btn btn-sm btn-outline-warning text-dark"
    data-tippy-content="<?php echo $this->l('cmap-compare-map'); ?>">
    <i class="fas fa-stethoscope"></i> <?php echo $this->l('cmap-compare'); ?>
  </button>
</div>