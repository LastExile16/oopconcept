<div class="concept-mapper-toolbar d-none">
  <div class="d-inline-block">
    <span class="btn-group">
      <button id="bt-open-map" data-tippy-content="<?php echo $this->l('cmap-new-map-open-material'); ?>"
        class="btn btn-sm btn-primary" disabled><i class="fas fa-folder-open"></i>
        <?php echo $this->l('cmap-new-open-map'); ?></button>
      <button id="bt-close-map" data-tippy-content="<?php echo $this->l('cmap-close-map'); ?>"
        class="btn btn-sm btn-outline-danger" disabled><i class="fas fa-folder"></i>
        <?php echo $this->l('cmap-close-map'); ?></button>
    </span>
    <span class="btn-group">
      <button id="bt-content" class="btn btn-sm btn-warning pl-2 pr-2" data-tippy-content="Reading Text">
        <i class="far fa-file-alt" style="margin-left: 3px;"></i> <span class="d-none d-sm-inline-block"></span>
      </button>
      <button id="bt-support" class="btn btn-sm btn-warning pl-2 pr-2" data-tippy-content="Support">
        <i class="fas fa-life-ring"></i> <span class="d-none d-sm-inline-block"></span>
      </button>
      <!-- <button id="bt-reflection" class="btn btn-sm btn-outline-secondary pl-2 pr-2" data-tippy-content="Feedback">
        <i class="fas fa-check-circle"></i> <span class="d-none d-sm-inline-block"></span>
      </button> -->
    </span>
    <span class="btn-group">
      <button id="bt-save" class="btn btn-sm btn-outline-info"
        data-tippy-content="<?php echo $this->l('cmap-save-map'); ?>" disabled>
        <i class="fas fa-save"></i> <span class="d-none d-sm-inline-block"><?php echo $this->l('save'); ?></span>
      </button>
      <button id="bt-save-as" class="btn btn-sm btn-outline-info"
        data-tippy-content="<?php echo $this->l('cmap-save-as'); ?>" disabled>
        <i class="far fa-save"></i> <span
          class="d-none d-sm-inline-block"><?php echo $this->l('cmap-save-as'); ?></span>
      </button>
      <!-- <button id="bt-set-as-kit" class="btn btn-sm btn-info" data-tippy-content="<?php echo $this->l('cmap-save-and-set-kit'); ?>" disabled>
          <i class="fas fa-grip-horizontal"></i> <span class="d-none d-sm-inline-block"><?php echo $this->l('cmap-save-as-kit'); ?></span>
        </button> -->
      <!-- <button id="bt-show-material" data-tippy-content="Show Material" class="btn btn-sm btn-outline-primary"
          disabled><i class="fas fa-eye"></i> <span class="d-none d-sm-inline-block">Material</span></button> -->
    </span>
  </div>
</div>

<!-- <button id="bt-submap" data-tippy-content="<?php echo $this->l('cmap-create-kit-from-goalmap'); ?>"
  class="btn btn-sm btn-warning concept-mapper-toolbar d-none">
  <i class="fas fa-sitemap"></i>
</button> -->

<button id="bt-create-kit" data-tippy-content="<?php echo $this->l('cmap-create-kit-from-goalmap'); ?>"
  class="btn btn-sm btn-info concept-mapper-toolbar d-none">
  <i class="fas fa-th-list mr-2"></i>
  <span><?php echo $this->l('cmap-create-kit'); ?></span>
  <i class="fas fa-chevron-right ml-2"></i>
</button>

<button id="bt-concept-mapping" class="btn btn-sm btn-primary kit-creation-toolbar d-none"
  data-tippy-content="<?php echo $this->l('cmap-return-to-cmap'); ?>">
  <i class="fas fa-chevron-left mr-2"></i>
  <i class="fas fa-bezier-curve mr-2"></i>
  <span><?php echo $this->l('cmap-return'); ?></span>
</button>

<div class="btn-group kit-creation-toolbar d-none">
  <button class="btn btn-sm btn-outline-secondary" disabled><span><?php echo $this->l('cmap-link'); ?></span>
  </button>
  <button id="bt-reset-all-edges" class="btn btn-sm btn-outline-danger slim"
    data-tippy-content="<?php echo $this->l('cmap-restore-all-edges'); ?>" disabled>
    <i class="fas fa-undo"></i>
  </button>
  <button id="bt-remove-all-edges" class="btn btn-sm btn-outline-danger slim"
    data-tippy-content="<?php echo $this->l('cmap-remove-all-edges'); ?>" disabled>
    <i class="fas fa-times"></i>
  </button>
  <button id="bt-remove-right-edge" class="btn btn-sm btn-outline-danger slim"
    data-tippy-content="<?php echo $this->l('cmap-remove-incoming-edge'); ?>" disabled>
    <i class="fas fa-sign-in-alt"></i>
  </button>
  <button id="bt-remove-left-edge" class="btn btn-sm btn-outline-danger slim"
    data-tippy-content="<?php echo $this->l('cmap-remove-outgoing-edge'); ?>" disabled>
    <i class="fas fa-sign-out-alt"></i>
  </button>
</div>

<div class="btn-group kit-creation-toolbar d-none">
  <button id="bt-open-saved-kit" class="btn btn-sm btn-warning"
    data-tippy-content="<?php echo $this->l('cmap-open-kit'); ?>" disabled>
    <i class="fas fa-folder-open"></i> <span
      class="d-none d-sm-inline-block"><?php echo $this->l('cmap-open-kit'); ?></span>
  </button>
  <button id="bt-save-kit" class="btn btn-sm btn-warning" data-tippy-content="<?php echo $this->l('cmap-save-kit'); ?>"
    disabled>
    <i class="fas fa-save"></i> <span class="d-none d-sm-inline-block"><?php echo $this->l('save'); ?></span>
  </button>
  <button id="bt-save-kit-as" class="btn btn-sm btn-warning"
    data-tippy-content="<?php echo $this->l('cmap-save-kit-as'); ?>" disabled>
    <i class="far fa-save"></i> <span class="d-none d-sm-inline-block"><?php echo $this->l('save-as'); ?></span>
  </button>
</div>