<div id="app-menu" class="dropdown concept-mapper-toolbar d-none">
  <button class="btn btn-sm btn-outline-secondary dropdown-toggle slim" type="button" id="ddAppMenu"
    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-tippy-content="<?php echo $this->l('cmap-application-menu'); ?>">
    <i class="fas fa-bars"></i>
  </button>
  <div class="dropdown-menu" aria-labelledby="ddAppMenu">
    <a class="dropdown-item" href="<?php echo $this->location(''); ?>"><?php echo $this->l('cmap-concept-mapping'); ?></a>
    <a class="dropdown-item" href="<?php echo $this->location('home/analyzer'); ?>"><?php echo $this->l('cmap-analyzer'); ?></a>
    <a class="dropdown-item" href="<?php echo $this->location('home/admin'); ?>"><?php echo $this->l('cmap-system-administration'); ?></a>
  </div>
</div>