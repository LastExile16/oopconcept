<div id="analyzer-sidebar-container" class="d-flex flex-column" style="padding:0.5em; flex-grow: 1">
  <div class="text-secondary" style="margin-bottom:0.5em;display: flex; justify-content: space-between;">
    <div class="title">
      <i class="fas fa-users"></i>
      <?php echo $this->l('cmap-groups-map'); ?>
    </div>
  </div>
  <div id="sidebar-content" class="d-flex flex-column" style="height: 100%;">
    <div class="custom-control custom-switch form-control-sm">
      <input type="checkbox" class="custom-control-input" id="bt-matching-links" checked>
      <label class="custom-control-label" for="bt-matching-links"><?php echo $this->l('cmap-matching-links'); ?>
        <span class="badge badge-success">&nbsp;</span>
      </label>
    </div>
    <div class="custom-control custom-switch form-control-sm">
      <input type="checkbox" class="custom-control-input" id="bt-excessive-links" checked>
      <label class="custom-control-label" for="bt-excessive-links"><?php echo $this->l('cmap-excessive-links'); ?>
        <span class="badge badge-primary">&nbsp;</span>
      </label>
    </div>
    <div class="custom-control custom-switch form-control-sm">
      <input type="checkbox" class="custom-control-input" id="bt-missing-links" checked>
      <label class="custom-control-label" for="bt-missing-links"><?php echo $this->l('cmap-missing-links'); ?>
        <span class="badge badge-danger">&nbsp;</span>
      </label>
    </div>
    <div class="custom-control custom-switch form-control-sm">
      <input type="checkbox" class="custom-control-input" id="bt-leaving-links" checked>
      <label class="custom-control-label" for="bt-leaving-links"><?php echo $this->l('cmap-leaving-links'); ?></label>
      <span class="badge badge-secondary">&nbsp;</span>
    </div>



    <div class="btn-group btn-group-sm mt-2 mb-1">
      <button id="bt-show-teacher-map" class="btn btn-outline-primary btn-sm"
        data-tippy-content="<?php echo $this->l('cmap-show-teacher-map'); ?>">
        <i class="fas fa-chalkboard-teacher"></i> <?php echo $this->l('cmap-teacher-map'); ?>
      </button>
    </div>
    <div class="btn-group btn-group-sm mt-2 mb-1">
      <button id="bt-show-groups-map" class="btn btn-outline-primary btn-sm"
        data-tippy-content="<?php echo $this->l('cmap-show-groups-map'); ?>">
        <i class="fas fa-users"></i> <?php echo $this->l('cmap-groups-map'); ?>
      </button>
      <button class="btn btn-sm btn-outline-secondary" disabled><?php echo $this->l('cmap-groups-range'); ?></button>
    </div>
    <div class="d-flex"><span style="min-width: 30px">Min</span>
      <div style="display: flex; flex-direction: row; flex-grow: 2; margin-left: 10px;"><input data-id="min-range" type="range" min="1" max="1" value="1"
          step="1" style="flex-grow: 1;"><span class="min-val" style="width: 35px;text-align: center;">1</span></div>
    </div>
    <div class="d-flex"><span style="min-width: 30px">Max</span>
      <div style="display: flex; flex-direction: row; flex-grow: 2; margin-left: 10px;"><input data-id="max-range" type="range" min="1" max="1" value="1"
          step="1" style="flex-grow: 1;"><span class="max-val" style="width: 35px;text-align: center;">1</span></div>
    </div>
    <hr style="width: 100%;">

    <div class="input-group input-group-sm mt-1 mb-1">
      <div class="input-group-prepend">
        <div class="input-group-text"><i class="fas fa-project-diagram"></i> &nbsp; <?php echo $this->l('cmap-kits'); ?></div>
      </div>
      <select id="kit-list" class="form-control"></select>
    </div>
  

    <hr style="width: 100%;">
    <div class="text-secondary mb-2"><i class="fas fa-user-friends"></i> <?php echo $this->l('cmap-learners-maps'); ?>
    </div>

    <div class="custom-control custom-switch form-control-sm">
      <input type="checkbox" class="custom-control-input" id="goalmap-layout">
      <label class="custom-control-label"
        for="goalmap-layout"><?php echo $this->l('cmap-use-goalmap-positioning'); ?></label>
    </div>

    <div class="custom-control custom-switch form-control-sm">
      <input type="checkbox" class="custom-control-input" id="preset-layout">
      <label class="custom-control-label"
        for="preset-layout"><?php echo $this->l('cmap-use-preset-positioning'); ?></label>
    </div>

    <div class="input-group input-group-sm mt-1 mb-1">
      <div class="input-group-prepend">
        <div class="input-group-text">
        <i class="fas fa-users"></i> &nbsp; <?php echo $this->l('cmap-groups'); ?></div>
      </div>
      <select id="group-list" class="form-control"></select>
    </div>

    <div id="learner-maps-heading"
      style="display:flex; justify-content:space-between; border-bottom: 1px solid #cccccc">
      <span><small><?php echo $this->l('cmap-name'); ?></small></span>
      <span><small><?php echo $this->l('cmap-score'); ?></small></span>
    </div>

    <div id="learner-maps-container" class="d-flex"
      style="display: flex; flex-direction: column; flex: 1 1 auto; overflow-y: auto; height: 0; min-height: 50px;">
      <div id="learner-maps-list-container">
        <small><em><?php echo $this->l('cmap-no-data-open-kit'); ?></em></small>
      </div>
    </div>
  </div>
</div>