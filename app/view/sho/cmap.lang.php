<div id="select-language" class="dropdown d-inline-block">
  <button class="btn btn-sm btn-outline-secondary dropdown-toggle slim" type="button" id="dropdownMenuButton"
    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-tippy-content="<?php echo $this->l('cmap-language'); ?>">
    <span class="active-lang"><?php echo isset($_SESSION['lang']) ? strtoupper($_SESSION['lang']) : 'EN'; ?></span>
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item language" data-lang="en" href="#">English</a>
    <a class="dropdown-item language" data-lang="jp" href="#">日本語</a>
  </div>
</div>