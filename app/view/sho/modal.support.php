<!-- content window -->
<div id="popup-material-content" style="display: none; border: 2px solid #ccc; border-radius: 0.5rem;">
  <div class="row move-handler justify-content-between pl-3 pr-3">
    <span class="material-title h4"></span>
    <span class="text-secondary align-items-center">
      <!-- <input id="popup-material-check-open" type="checkbox" class="check-open mr-2"> -->
      <!-- <label for="popup-material-check-open"><small><i class="fas fa-eye-slash"></i> Keep open </small></label> -->
      <span class="bt-close badge badge-danger ml-3">
        <i class="fas fa-times"></i> Close
      </span>
    </span>
  </div>
  <div>
    <hr>
  </div>
  <div class="material-content-container">
    <div class="material-content"></div>
  </div>
  <div class="content-nav">
  <button class="btn btn-sm btn-danger mt-3 bt-top"><i class="fas fa-chevron-up"></i> Top</button>
  <button class="btn btn-sm btn-primary mt-3 bt-more"><i class="fas fa-chevron-down"></i> More</button>
  </div>
</div>

<!-- Support Main Modal -->
<div class="modal" id="modal-support" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Proposition Support</h4>
        <!-- <button id="bt-sentences-list" class="btn btn-outline-primary">
          <i class="fas fa-file-alt"></i> Sentences List
        </button> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div id="exp-collab">

          <div id="exp-collab-sentence-selection">
            <div class="nav nav-prev">
              <i class="fas fa-angle-left" id="bt-exp-collab-sentence-prev"></i>
            </div>
            <div id="exp-collab-sentence" class="card card-body bg-light">Loading...</div>
            <div class="nav nav-next">
              <i class="fas fa-angle-right" id="bt-exp-collab-sentence-next"></i>
            </div>
          </div>

          <div id="exp-progress">
            <div class="progress exp-progress">
              <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0"
                aria-valuemax="100"></div>
            </div>
            <span id="exp-progress-text">~/~</span>
          </div>

          <div id="exp-collab-suggestions">



            <div id="exp-collab-propositions">

              <div id="exp-triple-reverb">
                <hr>
                <ul id="exp-triple-reverb-list"></ul>
              </div>
              <div id="exp-triple-keyword-sim">
                <hr>
                <ul id="exp-triple-keyword-sim-list"></ul>
              </div>
              <div style="display:flex; justify-content:center; margin-bottom:1rem;">
                <button id="bt-show-hide-more-suggestions" class="btn btn-sm btn-outline-danger" data-toggle="collapse"
                  data-target="#exp-triple">&lsaquo; Show-hide more suggestions &rsaquo; </button>
              </div>
              <div class="collapse" id="exp-triple">
                <ul id="exp-triple-list" style="margin:0 !important;"></ul>
              </div>
              <div id="exp-keyword">
                <hr>
                <ul id="exp-keyword-list"></ul>
              </div>
            </div>

            <div id="exp-collab-nav">
              <button id="bt-sentences-list" class="btn btn-primary">
                <i class="fas fa-file-alt"></i> Sentences List
              </button>
              <button id="bt-manual-proposition" class="btn btn-primary">
                <i class="fas fa-edit"></i> Manual Proposition</button>
            </div>

          </div>

          <!-- <hr> -->

          <!-- <h5>Manual Proposition</h5>

          <form id="form-exp-proposition-manual" class="form-sm form-inline">
            <label class="sr-only">Concept</label>
            <input type="text" class="form-control mb-2 mr-sm-2" id="exp-manual-konsep-a" placeholder="First Concept">

            <label class="sr-only">Link</label>
            <input type="text" class="form-control mb-2 mr-sm-2" id="exp-manual-link" placeholder="Link">

            <label class="sr-only">Concept</label>
            <input type="text" class="form-control mb-2 mr-sm-2" id="exp-manual-konsep-b" placeholder="Second Concept">

            <button id="bt-add-manual-proposition" class="btn btn-primary mb-2">
              <i class="fas fa-plus"></i> Add Proposition</button>
          </form> -->

        </div> <!-- /exp-collab-->

      </div><!-- /modal-body -->
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary bt-close" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary bt-ok">&nbsp; &nbsp; OK &nbsp; &nbsp;</button> -->
      </div>

    </div>
  </div>
</div>


<!-- Sentences Modal -->
<div class="modal" id="modal-sentences" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Sentences</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <table class="table table-sm">
          <tbody id="collab-sentences-list"></tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary bt-ok" data-dismiss="modal">&nbsp; &nbsp; OK &nbsp; &nbsp;</button>
      </div>
    </div>
  </div>
</div>

<!-- Manual/Edit Proposition Modal -->
<div class="modal" id="modal-proposition" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Proposition</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form id="form-exp-proposition-manual" class="">

          <div class="form-row">
            <div class="form-group col-md-4">
              <label>Concept</label>
              <input type="text" class="form-control" id="exp-manual-konsep-a">
            </div>
            <div class="form-group col-md-4">
              <label>Link</label>
              <input type="text" class="form-control" id="exp-manual-link">
            </div>
            <div class="form-group col-md-4">
              <label>Concept</label>
              <input type="text" class="form-control" id="exp-manual-konsep-b">
            </div>
          </div>

        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary bt-close" data-dismiss="modal">Cancel</button>
        <button id="bt-add-manual-proposition" class="btn btn-primary">
          <i class="fas fa-plus"></i> Add Proposition
        </button>
      </div>
    </div>
  </div>
</div>