<!-- Modal Login -->
<div class="modal" id="modal-login" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo $this->l('cmap-signin'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form">
          <div class="form-group row">
            <label class="col-sm-4 col-form-label text-right"><?php echo $this->l('cmap-username'); ?></label>
            <div class="col-sm-6">
              <input class="form-control input-username" placeholder="<?php echo $this->l('cmap-username'); ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-4 col-form-label text-right"><?php echo $this->l('cmap-password'); ?></label>
            <div class="col-sm-6">
              <input type="password" class="form-control input-password"
                placeholder="<?php echo $this->l('cmap-password'); ?>">
            </div>
          </div>
        </div>
        <div class="text-sm-center font-italic text-secondary"><span
            class="modal-helper-text text-sm"><?php echo $this->l('cmap-enter-user-pass'); ?></span></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">&nbsp; &nbsp;
          <?php echo $this->l('cmap-login'); ?> &nbsp;
          &nbsp;</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">&nbsp; &nbsp;
          <?php echo $this->l('close'); ?> &nbsp;
          &nbsp;</button>
      </div>
    </div>
  </div>
</div>

<!-- /Modal Login -->

<!-- Material Modal -->
<div class="modal" id="modal-kit" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="d-flex justify-content-between concept-mapper-toolbar">
          <h5><?php echo $this->l('cmap-topic-and-concept-map'); ?></h5>
          <button class="btn btn-sm btn-primary bt-topic-management concept-mapper-toolbar">
            <i class="fas fa-file-alt"></i>
            <?php echo $this->l('cmap-topic-management'); ?>
          </button>
        </div>

        <hr>
        <div class="row">
          <div class="col">
            <h6 class="label-title-material"><?php echo $this->l('cmap-topic'); ?></h6>
            <hr>
            <div class="material-list list-container"></div>
          </div>
          <div class="col">
            <h6 class="label-title-goalmap"><?php echo $this->l('cmap-goalmap-kit'); ?></h6>
            <hr>
            <div class="goalmap-list list-container"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button"
          class="btn btn-sm btn-success bt-dialog bt-new"><?php echo $this->l('cmap-new-map'); ?></button>
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-open"><?php echo $this->l('open'); ?></button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-cancel"
          data-dismiss="modal"><?php echo $this->l('cancel'); ?></button>
      </div>
    </div>
  </div>
</div>
<!-- /Material Modal -->

<!-- Map Name Modal -->
<div class="modal" id="modal-map-name" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="form-row pr-2 pl-2">
          <label><?php echo $this->l('cmap-please-give-name'); ?></label>
          <input type="text" class="form-control map-name-input">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button"
          class="btn btn-sm btn-primary bt-dialog bt-ok pl-5 pr-5"><?php echo $this->l('ok'); ?></button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-cancel pl-5 pr-5"
          data-dismiss="modal"><?php echo $this->l('cancel'); ?></button>
      </div>
    </div>
  </div>
</div>
<!-- /Map Name Modal -->

<!-- Open Map Modal -->
<div class="modal" id="modal-open-map" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h5><?php echo $this->l('cmap-open-saved-maps'); ?></h5>
        <hr>
        <div class="row">
          <div class="col">
            <div class="map-list list-container"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-cancel"
          data-dismiss="modal"><?php echo $this->l('cancel'); ?></button>
      </div>
    </div>
  </div>
</div>
<!-- /Open Map Modal -->

<!-- Kit Name Modal -->
<div class="modal" id="modal-kit-name" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="form-row pr-2 pl-2">
          <label><?php echo $this->l('cmap-please-give-kit-name'); ?></label>
          <input type="text" class="form-control kit-name-input">
        </div>
        <div class="form-row pr-2 pl-2 mt-3">
          <label><?php echo $this->l('cmap-layout'); ?></label>
        </div>
        <div class="form-check">
          <input type="radio" name="layout" value="preset" id="kit-option-preset" class="form-check-input">
          <label for="kit-option-preset"
            class="form-check-label"><?php echo $this->l('cmap-preset-as-positioned'); ?></label>
        </div>
        <div class="form-check">
          <input type="radio" name="layout" value="random" id="kit-option-random" class="form-check-input">
          <label for="kit-option-random" class="form-check-label"><?php echo $this->l('cmap-randomize'); ?></label>
        </div>
        <p class="mt-3" id="proposition-direction"><?php echo $this->l('cmap-proposition-direction'); ?> <span
            class="is-directed"></span>
          <br>
          <small><em><?php echo $this->l('cmap-enable-from-toolbar'); ?></em></small>
        </p>
        <div class="form-check">
          <input class="form-check-input kit-input-enabled" type="checkbox" value="1" id="kit-enabled">
          <label class="form-check-label" for="kit-enabled">
            <?php echo $this->l('enabled'); ?>
          </label>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok"><?php echo $this->l('ok'); ?></button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-cancel"
          data-dismiss="modal"><?php echo $this->l('cancel'); ?></button>
      </div>
    </div>
  </div>
</div>
<!-- /Kit Name Modal -->

<!-- Kit List Modal -->
<div class="modal" id="modal-open-kit-list" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h5><?php echo $this->l('cmap-open-kit'); ?></h5>
        <hr>
        <div class="kit-list"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-cancel"
          data-dismiss="modal"><?php echo $this->l('cancel'); ?></button>
      </div>
    </div>
  </div>
</div>
<!-- /Kit List Modal -->

<!-- Material List Modal -->
<div class="modal" id="modal-material-list" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h5><?php echo $this->l('cmap-material-list'); ?></h5>
        <hr>
        <div class="material-list" style="max-height: 400px; overflow-y: scroll"></div>
        <div class="text-right">
          <small><em><?php echo $this->l('cmap-disabled-material-not-shown'); ?></em></small>
        </div>
      </div>
      <div class="modal-footer d-flex justify-content-between">
        <button type="button" class="btn btn-sm btn-success bt-dialog bt-new" data-dismiss="modal"><i
            class="fas fa-plus"></i> <?php echo $this->l('cmap-new-topic'); ?></button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close"
          data-dismiss="modal"><?php echo $this->l('close'); ?></button>
      </div>
    </div>
  </div>
</div>
<!-- /Material List Modal -->

<!-- New Material Form -->
<div class="modal" id="modal-create-material" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false"
  data-backdrop="static">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4><?php echo $this->l('cmap-create-new-material'); ?></h4>
        <hr>
        <!-- <form id="form-create-material"> -->
        <input type="hidden" name="uid" class="input-material-creator-id" value="">
        <div class="form-group">
          <input type="text" maxlength="150" name="name" class="form-control input-material-name"
            placeholder="<?php echo $this->l('cmap-material-name'); ?>">
        </div>
        <div class="form-group">
          <input type="text" maxlength="50" name="name" class="form-control input-material-fid"
            placeholder="<?php echo $this->l('cmap-friendly-id-optional'); ?>">
        </div>
        <!-- </form> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok"><?php echo $this->l('ok'); ?></button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-cancel"
          data-dismiss="modal"><?php echo $this->l('cancel'); ?></button>
      </div>
    </div>
  </div>
</div>
<!-- /New Material Form -->

<!-- Edit Material Form -->
<div class="modal" id="modal-edit-material" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false"
  data-backdrop="static">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4><?php echo $this->l('cmap-edit-material'); ?></h4>
        <hr>
        <!-- <form id="form-create-material"> -->
        <input type="hidden" name="uid" class="input-material-creator-id" value="">
        <div class="form-group">
          <input type="text" maxlength="150" name="name" class="form-control input-material-name"
            placeholder="<?php echo $this->l('cmap-material-name'); ?>">
        </div>
        <div class="form-group">
          <input type="text" maxlength="50" name="name" class="form-control input-material-fid"
            placeholder="<?php echo $this->l('cmap-friendly-id-optional'); ?>">
        </div>
        <!-- </form> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok"><?php echo $this->l('ok'); ?></button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-cancel"
          data-dismiss="modal"><?php echo $this->l('cancel'); ?></button>
      </div>
    </div>
  </div>
</div>
<!-- /Edit Material Form -->

<!-- Material-Group Form -->
<div class="modal" id="modal-material-group" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4><?php echo $this->l('cmap-material-group'); ?> <span class="group-name"></span></h4>
        <hr>
        <div class="row">
          <div class="col-sm-6">
            <h5><?php echo $this->l('cmap-assigned-group'); ?></h5>
            <hr>
            <div class="in-list"></div>
          </div>
          <div class="col-sm-6">
            <h5><?php echo $this->l('cmap-unassigned-group'); ?></h5>
            <hr>
            <div class="not-in-list"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok"
          data-dismiss="modal"><?php echo $this->l('ok'); ?></button>
      </div>
    </div>
  </div>
</div>
<!-- /Group-Material Form -->

<?php $this->view('sho/modal.map-management.php'); ?>

<!-- Sub-map Modal -->
<div class="modal" id="modal-submap" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="kb-container" style="position:relative">
          <div id="sub-cy" class="kb-cy" style="height:500px; border:1px solid #ddd"></div>
          <?php $this->view('kbui/kbui.toolbar.php'); ?>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok"
          data-dismiss="modal"><?php echo $this->l('ok'); ?></button>
      </div>
    </div>
  </div>
</div>
<!-- /Sub-map Modal -->
<?php $this->view('sho/modal.support.php'); ?>