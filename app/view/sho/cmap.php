<?php $this->view('base/header.php');?>
<?php 
  $this->language('general.en'); 
  $this->language('cmap/en');
?>
<?php if(isset($_SESSION['lang']) && $_SESSION['lang'] == 'jp') {
  $this->language('general.jp');
  $this->language('cmap/jp'); 
}
?>
<div id="main-container">
  <div id="app-toolbar">

    <?php $this->view('sho/cmap.lang.php'); ?>
    <?php $this->view('sho/cmap.nav.php'); ?>
    <div class="concept-mapper-toolbar d-none">
      <div class="d-inline-block">
        <span class="btn-group">
          <button id="bt-open-map" data-tippy-content="<?php echo $this->l('cmap-new-map-open-material'); ?>"
            class="btn btn-sm btn-primary" disabled><i class="fas fa-folder-open"></i>
            <?php echo $this->l('cmap-new-open-map'); ?></button>
          <button id="bt-close-map" data-tippy-content="<?php echo $this->l('cmap-close-map'); ?>"
            class="btn btn-sm btn-outline-danger" disabled><i class="fas fa-folder"></i>
            <?php echo $this->l('cmap-close-map'); ?></button>
        </span>
        <!-- <span class="btn-group"> -->
          <!-- <button id="bt-content" class="btn btn-sm btn-warning pl-2 pr-2" data-tippy-content="Reading Text">
            <i class="far fa-file-alt" style="margin-left: 3px;"></i> <span class="d-none d-sm-inline-block"></span>
          </button>
          <button id="bt-support" class="btn btn-sm btn-warning pl-2 pr-2" data-tippy-content="Support">
            <i class="fas fa-life-ring"></i> <span class="d-none d-sm-inline-block"></span>
          </button> -->
          <!-- <button id="bt-reflection" class="btn btn-sm btn-outline-secondary pl-2 pr-2" data-tippy-content="Feedback">
            <i class="fas fa-check-circle"></i> <span class="d-none d-sm-inline-block"></span>
          </button> -->
        <!-- </span> -->
        <span class="btn-group">
          <button id="bt-save" class="btn btn-sm btn-outline-info"
            data-tippy-content="<?php echo $this->l('cmap-save-map'); ?>" disabled>
            <i class="fas fa-save"></i> <span class="d-none d-sm-inline-block"><?php echo $this->l('save'); ?></span>
          </button>
          <button id="bt-save-as" class="btn btn-sm btn-outline-info"
            data-tippy-content="<?php echo $this->l('cmap-save-as'); ?>" disabled>
            <i class="far fa-save"></i> <span
              class="d-none d-sm-inline-block"><?php echo $this->l('cmap-save-as'); ?></span>
          </button>
          <!-- <button id="bt-set-as-kit" class="btn btn-sm btn-info" data-tippy-content="<?php echo $this->l('cmap-save-and-set-kit'); ?>" disabled>
          <i class="fas fa-grip-horizontal"></i> <span class="d-none d-sm-inline-block"><?php echo $this->l('cmap-save-as-kit'); ?></span>
        </button> -->
          <!-- <button id="bt-show-material" data-tippy-content="Show Material" class="btn btn-sm btn-outline-primary"
          disabled><i class="fas fa-eye"></i> <span class="d-none d-sm-inline-block">Material</span></button> -->
        </span>
      </div>
    </div>

    <button id="bt-create-kit" data-tippy-content="<?php echo $this->l('cmap-create-kit-from-goalmap'); ?>"
      class="btn btn-sm btn-info concept-mapper-toolbar d-none">
      <i class="fas fa-th-list mr-2"></i>
      <span><?php echo $this->l('cmap-create-kit'); ?></span>
      <i class="fas fa-chevron-right ml-2"></i>
    </button>

    <button id="bt-concept-mapping" class="btn btn-sm btn-primary kit-creation-toolbar d-none"
      data-tippy-content="<?php echo $this->l('cmap-return-to-cmap'); ?>">
      <i class="fas fa-chevron-left mr-2"></i>
      <i class="fas fa-bezier-curve mr-2"></i>
      <span><?php echo $this->l('cmap-return'); ?></span>
    </button>

    <div class="btn-group kit-creation-toolbar d-none">
      <button class="btn btn-sm btn-outline-secondary" disabled><span><?php echo $this->l('cmap-link'); ?></span>
      </button>
      <button id="bt-reset-all" class="btn btn-sm btn-outline-danger slim"
        data-tippy-content="<?php echo $this->l('cmap-reset-all'); ?>">
        <i class="fas fa-sync"></i>
      </button>
      <button id="bt-reset-all-edges" class="btn btn-sm btn-outline-danger slim"
        data-tippy-content="<?php echo $this->l('cmap-restore-all-edges'); ?>" disabled>
        <i class="fas fa-undo"></i>
      </button>
      <button id="bt-remove-all-edges" class="btn btn-sm btn-outline-danger slim"
        data-tippy-content="<?php echo $this->l('cmap-remove-all-edges'); ?>" disabled>
        <i class="fas fa-times"></i>
      </button>
      <button id="bt-remove-right-edge" class="btn btn-sm btn-outline-danger slim"
        data-tippy-content="<?php echo $this->l('cmap-remove-incoming-edge'); ?>" disabled>
        <i class="fas fa-sign-in-alt"></i>
      </button>
      <button id="bt-remove-left-edge" class="btn btn-sm btn-outline-danger slim"
        data-tippy-content="<?php echo $this->l('cmap-remove-outgoing-edge'); ?>" disabled>
        <i class="fas fa-sign-out-alt"></i>
      </button>
    </div>

    <div class="btn-group kit-creation-toolbar d-none">
      <button id="bt-open-saved-kit" class="btn btn-sm btn-warning"
        data-tippy-content="<?php echo $this->l('cmap-open-kit'); ?>" disabled>
        <i class="fas fa-folder-open"></i> <span
          class="d-none d-sm-inline-block"><?php echo $this->l('cmap-open-kit'); ?></span>
      </button>
      <button id="bt-save-kit" class="btn btn-sm btn-warning"
        data-tippy-content="<?php echo $this->l('cmap-save-kit'); ?>" disabled>
        <i class="fas fa-save"></i> <span class="d-none d-sm-inline-block"><?php echo $this->l('save'); ?></span>
      </button>
      <button id="bt-save-kit-as" class="btn btn-sm btn-warning"
        data-tippy-content="<?php echo $this->l('cmap-save-kit-as'); ?>" disabled>
        <i class="far fa-save"></i> <span class="d-none d-sm-inline-block"><?php echo $this->l('save-as'); ?></span>
      </button>
    </div>

    <div class="btn-group kit-builder-toolbar d-none">
      <button class="bt-open-kit btn btn-sm btn-primary" data-tippy-content="<?php echo $this->l('cmap-open-kit'); ?>">
        <i class="fas fa-folder-open"></i> <?php echo $this->l('cmap-open-kit'); ?>
      </button>
      <button class="bt-save btn btn-sm btn-outline-info" data-tippy-content="<?php echo $this->l('cmap-save-map'); ?>">
        <i class="fas fa-save"></i> <span class="d-none d-sm-inline-block"><?php echo $this->l('save'); ?></span>
      </button>
      <button class="bt-load-draft btn btn-sm btn-outline-info"
        data-tippy-content="<?php echo $this->l('cmap-load-saved-map'); ?>">
        <i class="fas fa-upload"></i> <span
          class="d-none d-sm-inline-block"><?php echo $this->l('cmap-load-saved-map'); ?></span>
      </button>
    </div>
    <div class="btn-group kit-builder-toolbar d-none">
      <button id="bt-feedback" class="btn btn-sm btn-outline-warning text-danger slim"
        data-tippy-content="<?php echo $this->l('cmap-get-feedback'); ?>">
        <i class="fas fa-life-ring"></i> <?php echo $this->l('cmap-get-feedback'); ?>
      </button>
      <button id="bt-upload" class="btn btn-sm btn-warning" data-tippy-content="<?php echo $this->l('upload'); ?>">
        <i class="fas fa-cloud-upload-alt"></i> <?php echo $this->l('upload'); ?>
      </button>
      <button id="bt-compare" class="btn btn-sm btn-outline-warning text-dark"
        data-tippy-content="<?php echo $this->l('cmap-compare-map'); ?>">
        <i class="fas fa-stethoscope"></i> <?php echo $this->l('cmap-compare'); ?>
      </button>
    </div>

    <button id="bt-login" class="btn btn-sm btn-primary ml-3"
      data-tippy-content="<?php echo $this->l('cmap-signin'); ?>">
      <i class="fas fa-sign-in-alt"></i> <?php echo $this->l('cmap-signin'); ?>
      <!-- Sign In -->
    </button>

    <button id="bt-logout" class="btn btn-sm btn-danger ml-3 d-none"
      data-tippy-content="<?php echo $this->l('cmap-signout'); ?>">
      <i class="fas fa-sign-out-alt"></i> <?php echo $this->l('cmap-signout'); ?>
      <!-- Sign Out <span id="bt-logout-username" class="font-weight-bold"></span> -->
    </button>

  </div>

  <?php $this->view('kbui/kbui.canvas.php');?>

  <div id="toolbar-material">
    <small>
      <i class="far fa-file-alt ml-2 text-danger"></i> <span
        class="material-name text-secondary"><?php echo $this->l('cmap-topic-not-loaded'); ?></span>
      <i class="fas fa-share-alt ml-2 text-primary"></i> <span
        class="map-name text-secondary"><?php echo $this->l('cmap-map-not-loaded'); ?></span>
      <i class="fas fa-clock ml-2 text-primary"></i> <span
        class="last-update text-secondary"><?php echo $this->l('cmap-n-a'); ?></span>
    </small>
    <small id="toolbar-kit" class="kit-creation-toolbar">
      <i class="fas fa-share-alt ml-2 text-info"></i> <span
        class="kit-name text-secondary"><?php echo $this->l('cmap-kit-not-loaded'); ?></span>
      <i class="fas fa-clock ml-2 text-info"></i> <span
        class="kit-last-update text-secondary"><?php echo $this->l('cmap-n-a'); ?></span>
    </small>
  </div>

  <div id="status-bar">
    <span class="user-name"></span>
  </div>

  <div id="toolbar-analyzer" class="d-none">
    <p class="material-title h6 pt-2"><?php echo $this->l('cmap-compare-analysis-tool'); ?></p>
    <div class="custom-control custom-switch form-control-sm">
      <input type="checkbox" class="custom-control-input" id="bt-matching-links" checked>
      <label class="custom-control-label" for="bt-matching-links">
        <span class="badge badge-success">&nbsp;</span>
        <?php echo $this->l('cmap-matching-links'); ?>
      </label>
    </div>
    <div class="custom-control custom-switch form-control-sm">
      <input type="checkbox" class="custom-control-input" id="bt-excessive-links" checked>
      <label class="custom-control-label" for="bt-excessive-links">
        <span class="badge" style="background-color: #5BC0EB">&nbsp;</span>
        <?php echo $this->l('cmap-excessive-links'); ?>
      </label>
    </div>
    <div class="custom-control custom-switch form-control-sm">
      <input type="checkbox" class="custom-control-input" id="bt-missing-links" checked>
      <label class="custom-control-label" for="bt-missing-links">
        <span class="badge badge-danger">&nbsp;</span>
        <?php echo $this->l('cmap-missing-links'); ?>
      </label>
    </div>
    <div class="custom-control custom-switch form-control-sm">
      <input type="checkbox" class="custom-control-input" id="bt-leaving-links" checked>
      <label class="custom-control-label" for="bt-leaving-links">
        <span class="badge" style="background-color: #ccc">&nbsp;</span>
        <?php echo $this->l('cmap-leaving-links'); ?>
      </label>
    </div>
    <hr>
    <button class="bt-help btn btn-sm btn-outline-info"><i class="fas fa-life-ring"></i>
      <?php echo $this->l('cmap-info'); ?></button>
  </div>

  <div id="popup-map" class="popup p-3" style="display: none; min-width: 500px; background: #fff">
    <div class="row justify-content-between pl-3 pr-3">
      <h4 class="material-title"><?php echo $this->l('cmap-concept-map'); ?></h4>
    </div>
    <hr>
    <div class="text-center">
      <img class="cmap" style="margin: inherit auto; max-width: 800px; max-height:400px;">
    </div>
    <hr>
    <div class="row justify-content-end p-3">
      <button class="bt-download btn btn-sm btn-secondary mr-2"><i class="fas fa-download"></i>
        <?php echo $this->l('download'); ?></button>
      <button class="bt-continue btn btn-sm btn-primary mr-2"><i class="fas fa-save"></i>
        <?php echo $this->l('save'); ?></button>
      <button class="bt-cancel btn btn-sm btn-danger"><?php echo $this->l('cancel'); ?></button>
    </div>
  </div>

  <div id="popup-selection" class="" style="display: none;">
    <div class="menu-item" data-type="concept"><?php echo $this->l('cmap-create-new-concept'); ?><strong
        class="selected-text"></strong></div>
    <div class="menu-item" data-type="link"><?php echo $this->l('cmap-create-new-link'); ?><strong
        class="selected-text"></strong></div>
  </div>

</div> <!-- /Main Container -->

<?php $this->view('sho/modal.php');?>
<?php $this->view('general/general.ui.php');?>
<?php $this->view('base/footer.php');?>