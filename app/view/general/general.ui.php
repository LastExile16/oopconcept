  <!-- Dialog Modal -->
  <div class="modal" id="modal-dialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div id="modal-dialog-content"></div>
          <hr>
          <div style="text-align:right">
            <button type="button" class="btn btn-primary btn-positive" style="padding: 0.25rem 2rem"><?php $this->l('yes'); ?></button>
            <button type="button" class="btn btn-secondary btn-negative" style="padding: 0.25rem 2rem"><?php $this->l('no'); ?></button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /Dialog Modal -->

  <!-- Loading Modal -->
  <div class="modal" id="loading-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-loading modal-dialog-centered" role="document">
      <div class="modal-content" style="box-shadow: 0px 0px 100px #777777; border: 3px solid #77777777;">
        <div class="modal-body">
          <div class="modal-loading" style="text-align:center">
            <img src="<?php echo $this->assets('images/loading.svg'); ?>" />
            <br><span id="modal-loading-text"><?php $this->l('loading'); ?></span>
          </div>
          <div id="modal-content"></div>
        </div>
      </div>
    </div>
  </div>
  <!-- /Loading Modal -->