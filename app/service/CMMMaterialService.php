<?php

class CMMMaterialService extends MaterialService {

  public function selectMaterials() {
    $db     = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('materials m')
      ->leftJoin('materials_cmm mc', 'mc.mid', 'm.mid')
      ->select(array('m.mid', 'm.fid', 'm.name', 'm.creator_id', 'm.create_time', 'm.enabled', QB::raw('IF(LENGTH(mc.content), 1, NULL) AS content'), QB::raw('IF(LENGTH(mc.nlp), 1, NULL) AS nlp'), 'mc.state'))
      ->executeQuery(true);
    return $result;
  }

  public function getMaterials($enabled = true) {
    $db = $this->getInstance('kb');
    $qb = QB::instance($db)
      ->leftJoin('materials_cmm mc', 'mc.mid', 'm.mid')
      ->select(array('m.mid', 'm.fid', 'm.name', 'm.creator_id', 'm.create_time', 'm.enabled', QB::raw('IF(LENGTH(mc.content), 1, NULL) AS content'), QB::raw('IF(LENGTH(mc.nlp), 1, NULL) AS nlp'), 'mc.state'));
    if ($enabled) {
      $qb->where('m.enabled', 1);
    }
    $result = $qb->executeQuery(true);
    return $result;
  }

  public function getMaterialByMid($mid) {
    $db     = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('materials m')
      ->leftJoin('materials_cmm mc', 'mc.mid', 'm.mid')
      ->select(array('m.mid', 'm.fid', 'm.name', 'm.creator_id', 'm.create_time', 'm.enabled', 'mc.content', 'mc.nlp', 'mc.state'))
      ->where('m.mid', QB::esc($mid))
      ->limit(1)
      ->executeQuery(true);
    return count($result) ? $result[0] : null;
  }

  public function disableMaterial($mid) {
    $db = $this->getInstance('kb');
    $qb = QB::instance($db)
      ->table('materials_cmm')
      ->update(array('state' => 'disabled'))
      ->where('mid', QB::esc($mid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  public function enableMaterial($mid) {
    $db = $this->getInstance('kb');
    $qb = QB::instance($db)
      ->table('materials_cmm')
      ->update(array('state' => 'enabled'))
      ->where('mid', QB::esc($mid))
      ->execute();
    return $qb->getAffectedRows();
  }

  public function insertUpdateContent($mid, $content) {
    $db     = $this->getInstance('kb');

    $insert['mid'] = QB::esc($mid);
    $insert['content'] = empty($content) ? QBRaw::raw('NULL') : QB::esc($content);

    $update['content'] = $insert['content'];

    $result = QB::instance($db)
      ->table('materials_cmm mc')
      ->insertUpdate($insert, $update)
      ->execute()
      ->getAffectedRows();
    
    return $result;
  }

  public function insertUpdateNLP($mid, $nlp) {
    $db     = $this->getInstance('kb');

    $insert['mid'] = QB::esc($mid);
    $insert['nlp'] = empty($nlp) ? QBRaw::raw('NULL') : QB::esc($nlp);

    $update['nlp'] = $insert['nlp'];

    $result = QB::instance($db)
      ->table('materials_cmm mc')
      ->insertUpdate($insert, $update)
      ->execute()
      ->getAffectedRows();
    
    return $result;
  }

}