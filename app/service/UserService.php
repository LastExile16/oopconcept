<?php

class UserService extends CoreService
{

  public function getUidFromUsername($username) {
    $db = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('users')
      ->select(array('uid'))
      ->where('username',$username)
      ->executeQuery(true);
    return count($result) ? $result[0]->uid : '';
  }

  public function selectUsers()
  {
    $db     = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('users')
      ->select(array('uid', 'name', 'username', 'password', 'role_id', 'creator_id', 'create_time', 'enabled'))
      ->executeQuery(true);
    return $result;
  }

  public function selectUsersWithRole()
  {
    $db     = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('users u')
      ->leftJoin('roles r', 'u.role_id', 'r.rid')
      ->select(array('u.uid', 'u.name', 'u.username', 'u.password', 'u.role_id', 'u.creator_id', 'u.create_time', 'u.enabled', 'r.name AS rname'))
      ->executeQuery(true);
    return $result;
  }

  public function signIn($username, $password) {
    $db     = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('users u')
      ->leftJoin('users_in_grups ug', 'u.uid', 'ug.uid')
      ->select(array('u.uid', 'u.name', 'u.username', 'u.password', 'u.role_id', 'u.creator_id', 'u.create_time', 'u.enabled'))
      ->selectRaw('GROUP_CONCAT(ug.gid) AS gids')
      ->where('u.username', $username)
      ->where('u.password', md5($password))
      ->where('u.enabled', '<>', '0')
      ->groupBy(array('u.uid'))
      ->limit(1)
      ->executeQuery(true);
    return count($result) ? $result[0] : null;
  }

  public function signInRole($username, $password, $role_id) {
    $db     = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('users u')
      ->leftJoin('users_in_grups ug', 'u.uid', 'ug.uid')
      ->leftJoin('grups g', 'g.gid', 'ug.gid')
      ->select(array('u.uid', 'u.name', 'u.username', 'u.password', 'u.role_id', 'u.creator_id', 'u.create_time', 'u.enabled'))
      ->selectRaw('GROUP_CONCAT(ug.gid) AS gids')
      ->selectRaw('GROUP_CONCAT(g.name) AS grups')
      ->selectRaw('(SELECT MAX(seq) AS seq FROM logs_cmapping lc WHERE lc.uid = u.uid) AS seq')
      ->where('u.username', $username)
      ->where('u.password', md5($password))
      ->where('u.role_id', QB::esc($role_id))
      ->where('u.enabled', '<>', '0')
      ->groupBy(array('u.uid'))
      ->executeQuery(true);
    if(count($result)) return $result[0];
    return $result;
  }

  public function getUserById($uid)
  {
    $db     = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('users')
      ->select(array('uid', 'name', 'username', 'password', 'role_id', 'creator_id', 'create_time', 'enabled'))
      ->where('uid', $uid)
      ->executeQuery(true);
    return $result;
  }

  public function loadUser($uid)
  {
    $db     = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('users u')
      ->leftJoin('users_in_grups ug', 'u.uid', 'ug.uid')
      ->leftJoin('grups g', 'g.gid', 'ug.gid')
      ->select(array('u.uid', 'u.name', 'u.username', 'u.password', 'u.role_id', 'u.creator_id', 'u.create_time', 'u.enabled'))
      ->selectRaw('GROUP_CONCAT(ug.gid) AS gids')
      ->selectRaw('GROUP_CONCAT(g.name) AS grups')
      ->selectRaw('(SELECT MAX(seq) AS seq FROM logs_cmapping lc WHERE lc.uid = u.uid) AS seq')
      ->where('u.uid', QB::esc($uid))
      ->where('u.enabled', '<>', '0')
      ->groupBy(array('u.uid'))
      ->executeQuery(true);
    if(count($result)) return $result[0];
    return $result;
  }

  public function insertUser($username, $password, $name, $role = null)
  {
    $db               = $this->getInstance('kb');
    $cols['name']     = QB::esc($name);
    $cols['username'] = QB::esc($username);
    $cols['password'] = md5(QB::esc($password));
    $cols['role_id']  = !empty($role) ? QB::esc($role) : QB::raw('NULL');
    $qb               = QB::instance($db)
      ->table('users')
      ->insertIgnore($cols)
      ->execute(true);
    return $qb->insertId();
  }

  public function deleteUser($uid)
  {
    $db = $this->getInstance('kb');
    $qb = QB::instance($db)
      ->table('users')
      ->delete()
      ->where('uid', QB::esc($uid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  public function disableUser($uid)
  {
    $db = $this->getInstance('kb');
    $qb = QB::instance($db)
      ->table('users')
      ->update(array('enabled' => 0))
      ->where('uid', QB::esc($uid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  public function enableUser($uid)
  {
    $db = $this->getInstance('kb');
    $qb = QB::instance($db)
      ->table('users')
      ->update(array('enabled' => 1))
      ->where('uid', QB::esc($uid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  public function updateUser($uid, $username, $password, $name)
  {
    $update['name']     = QB::esc($name);
    $update['username'] = QB::esc($username);
    if (!empty(trim($password))) {
      $update['password'] = md5(QB::esc($password));
    }
    $db = $this->getInstance('kb');
    $qb = QB::instance($db)
      ->table('users')
      ->update($update)
      ->where('uid', QB::esc($uid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  // User-Role

  public function getUserWithRole($rid)
  {
    $db = $this->getInstance('kb');
    $cols = array('uid', 'username', 'password', 'name', 'role_id', 'creator_id', 'create_time', 'enabled');
    $qb = QB::instance($db);
    $result = new stdClass;
    $result->in = $qb->table('users')
      ->select($cols)
      ->where('role_id', $rid)
      ->executeQuery(true);
    $qb->clear();
    $result->notin = $qb->table('users')
      ->select($cols)
      ->whereNull('role_id')
      ->executeQuery(true);
    return $result;
  }

  public function assignRoleToUser($uid, $rid)
  {
    $db = $this->getInstance('kb');
    $update['role_id'] = QB::esc($rid);
    $qb = QB::instance($db)
      ->table('users')
      ->update($update)
      ->where('uid', QB::esc($uid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  public function deassignRoleFromUser($uid, $rid)
  {
    $db = $this->getInstance('kb');
    $update['role_id'] = null;
    $qb = QB::instance($db)
      ->table('users')
      ->update($update)
      ->where('uid', QB::esc($uid))
      ->execute(true);
    return $qb->getAffectedRows();
  }
}
