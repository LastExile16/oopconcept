<?php

class SessionService extends CoreService {

  function setSessionData($uid, $data) {
    $insert['uid'] = $uid;
    $insert['data'] = json_encode($data);
    $update['data'] = $insert['data'];
    $db = $this->getInstance('kb');
    $qb = QB::instance($db);
    $qb->clear();
    $result = $qb->table('users_sessions')
      ->insertUpdate($insert, $update)
      ->execute();
    return $result;
  }

  function getSessionData($uid) {
    $db = $this->getInstance('kb');
    $qb = QB::instance($db);
    $qb->clear();
    $result = $qb->table('users_sessions')
      ->select(array('data'))
      ->where('uid', QB::esc($uid))
      ->executeQuery(true);
    if(count($result)) return $result[0];
    return null;
  }

  function clearSessionData($uid) {
    $db = $this->getInstance('kb');
    $qb = QB::instance($db);
    $qb->clear();
    $result = $qb->table('users_sessions')
      ->delete()
      ->where('uid', $uid)
      ->execute();
    return $result->getAffectedRows();
  }

}