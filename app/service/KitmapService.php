<?php

class KitmapService extends CoreService {

  public function getKits($gmid, $enabled = true) {
    $db      = $this->getInstance('kb');
    $qb      = QB::instance($db);
    $kit = $qb->table('kit k')
      ->leftJoin('users u', 'u.uid', 'k.author')
      ->select(array('k.kid', 'k.name', 'k.layout', 'k.directed', 'k.create_time', 'k.update_time', 'k.enabled', 'k.gmid', 'k.author'))
      ->select(array('u.username', 'u.name as authorname'))
      ->selectRaw('(SELECT count(*) FROM learnermaps lm WHERE lm.kid = k.kid) AS learnermaps_count')
      ->where('k.gmid', QB::esc($gmid));
    if($enabled) $kit = $kit->where('k.enabled', $enabled ? '1' : '0');
    $kits = $kit->executeQuery(true);
    if (!count($kits)) return [];
    return $kits;
  }

  public function getKitmaps($gmid, $enabled = true) {
    $db      = $this->getInstance('kb');
    $qb      = QB::instance($db);
    $kit = $qb->table('kit k')
      ->leftJoin('users u', 'u.uid', 'k.author')
      ->select(array('k.kid', 'k.name', 'k.layout', 'k.directed', 'k.create_time', 'k.update_time', 'k.enabled', 'k.gmid', 'k.author'))
      ->select(array('u.username', 'u.name as authorname'))
      ->where('k.gmid', QB::esc($gmid));
    if($enabled) $kit = $kit->where('k.enabled', $enabled ? '1' : '0');
    $kit = $kit->executeQuery(true);
    if (!count($kit)) {
      return [];
    }
    for ($i = 0; $i < count($kit); $i++) {
      $kitmap = $kit[$i];
      
      $qb->clear();
      $concepts = $qb->table('kit_concepts kc')
        ->leftJoin('goalmaps_concepts gc', array('kc.cid' => 'gc.cid', 'kc.gmid' => 'gc.gmid'))
        ->select(array('kc.cid', 'kc.kid', 'kc.gmid', 'kc.locx', 'kc.locy', 'gc.label'))
        ->where('kc.kid', $kitmap->kid)
        ->executeQuery(true);
      ($kit[$i])->concepts = $concepts ? $concepts : [];
      
      $qb->clear();
      $links = $qb->table('kit_links kl')
        ->leftJoin('goalmaps_links gl', array('kl.lid' => 'gl.lid', 'kl.gmid' => 'gl.gmid'))
        ->select(array('kl.lid', 'kl.kid', 'kl.gmid', 'kl.locx', 'kl.locy', 'kl.source', 'gl.label'))
        ->where('kl.kid', $kitmap->kid)
        ->executeQuery(true);
      ($kit[$i])->links = $links ? $links : [];

      $qb->clear();
      $targets = $qb->table('kit_links_target t')
        ->select(array('t.kid', 't.lid', 't.gmid', 't.cid'))
        ->where('t.kid', $kitmap->kid)
        ->executeQuery(true);
      ($kit[$i])->targets = $targets ? $targets : [];
      $qb->clear();
    }
    return $kit;
  }

  public function getKitmapsByMid($mid, $enabled = true) {
    $db      = $this->getInstance('kb');
    $qb      = QB::instance($db);
    $kits = $qb->table('kit k')
      ->leftJoin('users u', 'u.uid', 'k.author')
      ->leftJoin('goalmaps gm', 'gm.gmid', 'k.gmid')
      ->select(array('k.kid', 'k.name', 'k.layout', 'k.directed', 'k.create_time', 'k.update_time', 'k.enabled', 'k.gmid', 'k.author'))
      ->select(array('u.username', 'u.name as authorname'))
      ->select(array('gm.name as gmname', 'gm.update_time'));
    if($enabled) $kits = $kits->where('k.enabled', '1');
    $kits = $kits->where('gm.mid', QB::esc($mid))
      ->executeQuery(true);
    return $kits;
  }

  public function insertKitmapOverwrite($name, $gmid, $author, $layout, $directed, $enabled, $concepts, $links, $kid = null) {
    $db                  = $this->getInstance('kb');
    $qb                  = QB::instance($db);
    $kitmaps['name']     = QB::esc($name);
    $kitmaps['gmid']     = QB::esc($gmid);
    $kitmaps['author']   = QB::esc($author);
    $kitmaps['layout']   = QB::esc($layout);
    $kitmaps['directed'] = QB::raw(QB::esc($directed));
    $kitmaps['enabled']  = QB::raw((int)QB::esc($enabled));
    try {
      $qb->begin();
      if ($kid) { // overwrite
        $cids = array();
        $lids = array();
        foreach($concepts as $c) $cids[] = $c['cid'];
        foreach($lids as $l) $lids[] = $l['lid'];
        if(count($cids)) $qb->table('kit_concepts')
          ->delete()
          ->where('kid', $kid)
          ->whereNotIn('cid', $cids)
          ->execute();
        $qb->clear();

        if(count($lids)) $qb->table('kit_links')
          ->delete()
          ->where('kid', $kid)
          ->whereNotIn('lid', $lids)
          ->execute();
        $qb->clear();

        /**
        * delete all of the targets previously stored for all of the links of this kit
        * as for the links that are still there, their data including the targets will be updated ~ further below
        */

        $qb->table('kit_links_target')
          ->delete()
          ->where('kid', $kid)
          ->where('gmid', $gmid)
          ->execute();
        $qb->clear();

        $qb->table('kit')
          ->update(array(
            'name'        => QB::esc($name),
            'layout'      => QB::esc($layout),
            'directed'    => QB::raw(QB::esc($directed)),
            'update_time' => QB::raw('NOW()'),
            'enabled'     => QB::raw((int)QB::esc($enabled))
          ))
          ->where('kid', $kid)
          ->execute();
        $qb->clear();

      } else {
        $kid = $qb->table('kit')
          ->insert($kitmaps)
          ->execute()
          ->getInsertId();
        $qb->clear();
      }
      $cs = [];
      $ls = [];
      foreach ($concepts as $k => $v) {
        $concepts[$k]['kid'] = $kid;
        $cs[]                = (object) $concepts[$k];
      }
      $targets = [];
      foreach ($links as $k => $v) {
        $links[$k]['kid'] = $kid;
        if ($links[$k]['source'] == "") {
          $links[$k]['source'] = null;
        }

        /*if ($links[$k]['target'] == "") {
          $links[$k]['target'] = null;
        }*/

        if (isset($links[$k]['target']) && !empty($links[$k]['target'])) {
          foreach ($links[$k]['target'] as $tcid) {
            $target = new stdClass;
            $target->kid = $kid;
            $target->lid = $links[$k]['lid'];
            $target->gmid = $gmid;
            $target->cid = $tcid;
            $targets[] = $target;
          }
        }
        $links[$k]['target'] = null;
        
        $ls[] = (object) $links[$k];
      }
      if (count($cs)) {
        $qb->clear();
        $qb->table('kit_concepts')
          ->insertUpdateModels($cs, null, array('locx','locy'))
          ->execute();
      }
      if (count($ls)) {
        $qb->clear();
        $qb->table('kit_links')
          ->insertUpdateModels($ls, null, array('locx','locy','source'))
          ->execute();
      }
      if (count($targets)) {
          $qb->clear();
          $qb->table('kit_links_target')
            ->insertModel($targets)
            ->execute();
          $qb->clear();
          
        }
      $qb->commit();
      return $kid;
    } catch (Exception $ex) {
      $qb->rollback();
      throw new Exception($ex->getMessage() . ". " . $qb->get());
    }
  }

  public function getKitmap($kid) {
    $db      = $this->getInstance('kb');
    $qb      = QB::instance($db);
    $result  = new stdClass;
    try {
      $kit = $qb->table('kit k')
        ->select(array('k.kid', 'k.name', 'k.layout', 'k.directed', 'k.create_time', 'k.update_time', 'k.enabled', 'k.gmid', 'k.author'))
        ->where('k.kid', QB::esc($kid))
        ->limit(1)
        ->executeQuery(true);
      if (count($kit)) {
        $result->kitmap = count($kit) ? $kit[0] : null;
      } else {
        return null;
      }

      $qb->clear();
      $concepts = $qb->table('kit_concepts kc')
        ->leftJoin('goalmaps_concepts gc', array('kc.cid' => 'gc.cid', 'kc.gmid' => 'gc.gmid'))
        ->select(array('kc.cid', 'kc.kid', 'kc.gmid', 'kc.locx', 'kc.locy', 'gc.label'))
        ->where('kc.kid', $result->kitmap->kid)
        ->executeQuery(true);
      $qb->clear();
      
      $links = $qb->table('kit_links kl')
        ->leftJoin('goalmaps_links gl', array('kl.lid' => 'gl.lid', 'kl.gmid' => 'gl.gmid'))
        ->select(array('kl.lid', 'kl.kid', 'kl.gmid', 'kl.locx', 'kl.locy', 'kl.source', 'gl.label'))
        ->where('kl.kid', $result->kitmap->kid)
        ->executeQuery(true);
      $qb->clear();
      $targets = $qb->table('kit_links_target t')
        ->select(array('t.kid', 't.lid', 't.gmid', 't.cid'))
        ->where('t.kid', $result->kitmap->kid)
        ->executeQuery(true);
      $qb->clear();
    } catch(Exception $ex) {
      throw new Exception($ex->getMessage() . ". " . $qb->get());
    }

    $result->concepts = $concepts ? $concepts : [];
    $result->links = $links ? $links : [];
    $result->targets = $targets ? $targets : [];
    $qb->clear();

    return $result;
  }

  public function disableKit($kid) {
    $db = $this->getInstance('kb');
    $qb = QB::instance($db)
      ->table('kit')
      ->update(array('enabled' => '0'))
      ->where('kid', QB::esc($kid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  public function enableKit($kid) {
    $db = $this->getInstance('kb');
    $qb = QB::instance($db)
      ->table('kit')
      ->update(array('enabled' => '1'))
      ->where('kid', QB::esc($kid))
      ->execute();
    return $qb->getAffectedRows();
  }

  public function updateKit($name, $layout, $directed, $enabled, $kid) {
    $db = $this->getInstance('kb');
    $qb = QB::instance($db)
      ->table('kit')
      ->update(array(
        'name'        => QB::esc($name),
        'layout'      => QB::esc($layout),
        'directed'    => QB::raw(QB::esc($directed)),
        'update_time' => QB::raw('NOW()'),
        'enabled'     => QB::raw((int)QB::esc($enabled))
      ))
      ->where('kid', $kid)
      ->execute();
    return $qb->getAffectedRows();
  }

  public function deleteKit($kid) {
    $db = $this->getInstance('kb');
    $qb = QB::instance($db)
      ->table('kit')
      ->delete()
      ->where('kid', QB::esc($kid))
      ->execute();
    return $qb->getAffectedRows();
  }

}
