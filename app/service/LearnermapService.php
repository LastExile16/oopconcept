<?php

class LearnermapService extends CoreService
{
  public function getLearnermaps($gmid) {
    $db       = $this->getInstance('kb');
    $qb       = QB::instance($db);
    $learnermaps = $qb->table('learnermaps l')
      ->leftJoin('users u', 'u.uid', 'l.uid')
      ->select(array('l.lmid', 'l.type', 'l.kid', 'l.gmid', 'l.uid', 'l.create_time', 'u.username', 'u.name'))
      ->where('l.type', 'fix')
      ->where('l.gmid', QB::esc($gmid))
      ->executeQuery(true);
    if (!count($learnermaps)) return $learnermaps;
    for($i = 0; $i < count($learnermaps); $i++) {
      $learnermap = $learnermaps[$i];
      $qb->clear();
      $concepts = $qb->table('learnermaps_concepts lc')
        ->leftJoin('goalmaps_concepts gc', array('lc.cid' => 'gc.cid', 'lc.gmid' => 'gc.gmid'))
        ->select(array('lc.cid', 'lc.lmid', 'lc.gmid', 'lc.locx', 'lc.locy', 'gc.label'))
        ->where('lc.lmid', $learnermap->lmid)
        ->executeQuery(true);
      ($learnermaps[$i])->concepts = $concepts ? $concepts : [];
      $qb->clear();
      $links = $qb->table('learnermaps_links ll')
        ->leftJoin('goalmaps_links gl', array('ll.lid' => 'gl.lid', 'll.gmid' => 'gl.gmid'))
        ->select(array('ll.lid', 'll.lmid', 'll.gmid', 'll.locx', 'll.locy', 'll.source', 'gl.label'))
        ->where('ll.lmid', $learnermap->lmid)
        ->executeQuery(true);
      ($learnermaps[$i])->links = $links ? $links : [];

      $qb->clear();
      $targets = $qb->table('learnermaps_links_target t')
        ->select(array('t.lmid', 't.lid', 't.gmid', 't.cid'))
        ->where('t.lmid', $learnermap->lmid)
        ->executeQuery(true);
      ($learnermaps[$i])->targets = $targets ? $targets : [];  
      $qb->clear();
    }
    return $learnermaps;
  }

  public function getLearnermapsWithGroups($gmid) {
    $db       = $this->getInstance('kb');
    $qb       = QB::instance($db);    
    $learnermaps = $qb->table('learnermaps l')
      ->leftJoin('users u', 'u.uid', 'l.uid')
      ->select(array('l.lmid', 'l.type', 'l.kid', 'l.gmid', 'l.uid', 'l.create_time', 'u.username', 'u.name'))
      ->selectRaw('(SELECT GROUP_CONCAT(ug.gid) FROM users_in_grups ug WHERE ug.uid = u.uid) AS gids')
      ->where('l.type', 'fix')
      ->where('l.gmid', QB::esc($gmid))
      ->executeQuery(true);

    $learnermapCount = count($learnermaps);
    if (!$learnermapCount) return $learnermaps;

    for($i = 0; $i < $learnermapCount; $i++) {
      // $learnermap = $learnermaps[$i];
      
      $qb->clear();
      $concepts = $qb->table('learnermaps_concepts lc')
        ->leftJoin('goalmaps_concepts gc', array('lc.cid' => 'gc.cid', 'lc.gmid' => 'gc.gmid'))
        ->select(array('lc.cid', 'lc.lmid', 'lc.gmid', 'lc.locx', 'lc.locy', 'gc.label'))
        ->where('lc.lmid', $learnermaps[$i]->lmid)
        ->executeQuery(true);
      ($learnermaps[$i])->concepts = $concepts ? $concepts : [];
      
      $qb->clear();
      $links = $qb->table('learnermaps_links ll')
        ->leftJoin('goalmaps_links gl', array('ll.lid' => 'gl.lid', 'll.gmid' => 'gl.gmid'))
        ->select(array('ll.lid', 'll.lmid', 'll.gmid', 'll.locx', 'll.locy', 'll.source', 'gl.label'))
        ->where('ll.lmid', $learnermaps[$i]->lmid)
        ->executeQuery(true);
      ($learnermaps[$i])->links = $links ? $links : [];

      $qb->clear();
      $targets = $qb->table('learnermaps_links_target t')
        ->select(array('t.lmid', 't.lid', 't.gmid', 't.cid'))
        ->where('t.lmid', $learnermaps[$i]->lmid)
        ->executeQuery(true);
      ($learnermaps[$i])->targets = $targets ? $targets : []; 
      
      $tLength = count($targets);
      foreach ($links as $l) {
        // $l->target = array();
        for ($j=0; $j<$tLength; $j++) {
          if ($l->lid == $targets[$j]->lid) {
            $l->target[] = $targets[$j]->cid;
            // array_push($l->target, $targets[$j]->cid);
          }
        }
      }
    }
    // print_r($learnermaps);
    // exit();
    return $learnermaps;
  }

  public function getLearnermapsByKid($kid, $type = null) {
    $db       = $this->getInstance('kb');
    $qb       = QB::instance($db);
    $qb = $qb->table('learnermaps l')
      ->leftJoin('users u', 'u.uid', 'l.uid')
      ->select(array('l.lmid', 'l.type', 'l.kid', 'l.gmid', 'l.uid', 'l.create_time', 'u.username', 'u.name'));
    if($type) $qb = $qb->where('l.type', 'fix');
    $learnermaps = $qb->where('l.kid', QB::esc($kid))
      ->executeQuery(true);
    if (count($learnermaps)) return $learnermaps;
    return [];
  }
  
  public function insertLearnermap($gmid, $uid, $concepts, $links, $type) {
    $db                  = $this->getInstance('kb');
    $qb                  = QB::instance($db);
    $learnermaps['gmid'] = QB::esc($gmid);
    $learnermaps['uid']  = QB::esc($uid);    
    $learnermaps['type'] = QB::esc($type);
    try {
      $qb->begin();
      $lmid = $qb->table('learnermaps')
        ->insert($learnermaps)
        ->execute()
        ->getInsertId();
      $cs = [];
      $ls = [];
      foreach ($concepts as $k => $v) {
        $concepts[$k]['lmid'] = $lmid;
        $cs[]                 = (object) $concepts[$k];
      }

      $targets = [];
      foreach ($links as $k => $v) {
        $links[$k]['lmid'] = $lmid;
        if ($links[$k]['source'] == "") {
          $links[$k]['source'] = null;
        }

        /*if ($links[$k]['target'] == "") {
          $links[$k]['target'] = null;
        }*/

        if (isset($links[$k]['target']) && !empty($links[$k]['target'])) {
          foreach ($links[$k]['target'] as $tcid) {
            $target = new stdClass;
            $target->lmid = $lmid;
            $target->lid = $links[$k]['lid'];
            $target->gmid = $gmid;
            $target->cid = $tcid;
            $targets[] = $target;
          }
        }
        $links[$k]['target'] = null;
        $ls[] = (object) $links[$k];
      }
      if (count($cs)) {
        $qb->clear();
        $qb->table('learnermaps_concepts')
          ->insertModel($cs)
          ->execute();
      }
      if (count($ls)) {
        $qb->clear();
        $qb->table('learnermaps_links')
          ->insertModel($ls)
          ->execute();
      }

      if (count($targets)) {
        $qb->clear();
        $qb->table('learnermaps_links_target')
          ->insertModel($targets)
          ->execute();
        $qb->clear();
        
      }

      if ($type == 'fix') {
        $qb->clear();
        $qb->table('learnermaps l')
          ->delete()
          ->where('l.type', 'draft')
          ->where('l.uid', $uid)
          ->execute();
      }
      
      $qb->commit();
      return $lmid;
    } catch (Exception $ex) {
      $qb->rollback();
      throw new Exception($ex->getMessage() . ". " . $qb->get());
    }
  }

  public function insertLearnermapOverwrite($gmid, $kid, $uid, $concepts, $links, $type, $lmid = null) {
    $db                  = $this->getInstance('kb');
    $qb                  = QB::instance($db);
    $learnermaps['gmid'] = QB::esc($gmid);
    $learnermaps['kid'] = QB::esc($kid);
    $learnermaps['uid']  = QB::esc($uid);    
    $learnermaps['type'] = QB::esc($type);
    try {
      $qb->begin();
      if($lmid) {
        // $qb->table('learnermaps_links')
        // ->delete()
        // ->where('lmid', $lmid)
        // ->execute();
        // $qb->clear();
        // $qb->table('learnermaps_concepts')
        // ->delete()
        // ->where('lmid', $lmid)
        // ->execute();
        // $qb->clear();
        $qb->table('learnermaps')
        ->update(array('type' => QB::esc($type), 'create_time' => QB::raw('NOW()')))
        ->where('lmid', $lmid)
        ->execute();
        $qb->clear();

        /**
        * delete all of the targets previously stored for all of the links of this learnermap
        * as for the links that are still there, their data including the targets will be updated ~ further below
        */

        $qb->table('learnermaps_links_target')
          ->delete()
          ->where('lmid', $lmid)
          ->where('gmid', $gmid)
          ->execute();
        $qb->clear();

      } else {
        $lmid = $qb->table('learnermaps')
        ->insert($learnermaps)
        ->execute()
        ->getInsertId();
        $qb->clear();
      }
      $cs = [];
      $ls = [];
      foreach ($concepts as $k => $v) {
        $concepts[$k]['lmid'] = $lmid;
        $cs[]                 = (object) $concepts[$k];
      }

      $targets = [];
      foreach ($links as $k => $v) {
        $links[$k]['lmid'] = $lmid;
        if ($links[$k]['source'] == "") {
          $links[$k]['source'] = null;
        }

        /*if ($links[$k]['target'] == "") {
          $links[$k]['target'] = null;
        }*/
        if (isset($links[$k]['target']) && !empty($links[$k]['target'])) {
          foreach ($links[$k]['target'] as $tcid) {
            $target = new stdClass;
            $target->lmid = $lmid;
            $target->lid = $links[$k]['lid'];
            $target->gmid = $gmid;
            $target->cid = $tcid;
            $targets[] = $target;
          }
        }
        $links[$k]['target'] = null;
        $ls[] = (object) $links[$k];
      }
      if (count($cs)) {
        $qb->clear();
        $qb->table('learnermaps_concepts')
          ->insertUpdateModels($cs, null, array('locx', 'locy'))
          ->execute();
      }
      if (count($ls)) {
        $qb->clear();
        $qb->table('learnermaps_links')
          ->insertUpdateModels($ls, null, array('locx', 'locy', 'source', 'target'))
          ->execute();
      }
      if (count($targets)) {
        $qb->clear();
        $qb->table('learnermaps_links_target')
          ->insertModel($targets)
          ->execute();
        $qb->clear();
        
      }

      if ($type == 'fix') {
        $qb->clear();
        $qb->table('learnermaps l')
          ->delete()
          ->where('l.type', 'draft')
          ->where('l.uid', $uid)
          ->where('l.gmid', $gmid)
          ->where('l.kid', $kid)
          ->execute();
      }
      $qb->commit();
      return $lmid;
    } catch (Exception $ex) {
      $qb->rollback();
      throw new Exception($ex->getMessage() . ". " . $qb->get());
    }
  }

  public function getLastDraftLearnermap($gmid, $uid) {
    $db       = $this->getInstance('kb');
    $qb       = QB::instance($db);
    $result   = new stdClass;
    $learnermaps = $qb->table('learnermaps l')
      ->select(array('l.lmid', 'l.type', 'l.gmid', 'l.uid', 'l.create_time'))
      ->where('l.type', 'draft')
      ->where('l.gmid', QB::esc($gmid))
      ->where('l.uid', QB::esc($uid))
      ->orderBy('l.lmid', QB::ORDER_DESC)
      ->limit(1)
      ->executeQuery(true);
    if (count($learnermaps)) {
      $result->learnermap = count($learnermaps) ? $learnermaps[0] : null;
    } else return null;
    $qb->clear();
    $concepts = $qb->table('learnermaps_concepts lc')
      ->leftJoin('goalmaps_concepts gc', array('lc.cid' => 'gc.cid', 'lc.gmid' => 'gc.gmid'))
      ->select(array('lc.cid', 'lc.lmid', 'lc.gmid', 'lc.locx', 'lc.locy', 'gc.label'))
      ->where('lc.lmid', $result->learnermap->lmid)
      ->executeQuery(true);
    $result->concepts = $concepts ? $concepts : [];
    $qb->clear();
    $links = $qb->table('learnermaps_links ll')
      ->leftJoin('goalmaps_links gl', array('ll.lid' => 'gl.lid', 'll.gmid' => 'gl.gmid'))
      ->select(array('ll.lid', 'll.lmid', 'll.gmid', 'll.locx', 'll.locy', 'll.source', 'gl.label'))
      ->where('ll.lmid', $result->learnermap->lmid)
      ->executeQuery(true);
    $result->links = $links ? $links : [];
    return $result;
  }

  public function getLastLearnermap($gmid, $kid, $uid, $type = 'draft') {
    $db       = $this->getInstance('kb');
    $qb       = QB::instance($db);
    $result   = new stdClass;
    $learnermaps = $qb->table('learnermaps l')
      ->select(array('l.lmid', 'l.type', 'l.gmid', 'l.kid', 'l.uid', 'l.create_time'))
      ->where('l.type', QB::esc($type))
      ->where('l.gmid', QB::esc($gmid))
      ->where('l.kid', QB::esc($kid))
      ->where('l.uid', QB::esc($uid))
      ->orderBy('l.lmid', QB::ORDER_DESC)
      ->limit(1)
      ->executeQuery(true);
      // throw new Exception("hey: " . $qb->get());
    if (count($learnermaps)) {
      $result->learnermap = count($learnermaps) ? $learnermaps[0] : null;
    } else return null;
    $qb->clear();
    $concepts = $qb->table('learnermaps_concepts lc')
      ->leftJoin('goalmaps_concepts gc', array('lc.cid' => 'gc.cid', 'lc.gmid' => 'gc.gmid'))
      ->select(array('lc.cid', 'lc.lmid', 'lc.gmid', 'lc.locx', 'lc.locy', 'gc.label'))
      ->where('lc.lmid', $result->learnermap->lmid)
      ->executeQuery(true);
    $result->concepts = $concepts ? $concepts : [];
    $qb->clear();
    $links = $qb->table('learnermaps_links ll')
      ->leftJoin('goalmaps_links gl', array('ll.lid' => 'gl.lid', 'll.gmid' => 'gl.gmid'))
      ->select(array('ll.lid', 'll.lmid', 'll.gmid', 'll.locx', 'll.locy', 'll.source', 'gl.label'))
      ->where('ll.lmid', $result->learnermap->lmid)
      ->executeQuery(true);
    $result->links = $links ? $links : [];

    $qb->clear();
    $targets = $qb->table('learnermaps_links_target t')
        ->select(array('t.lmid', 't.lid', 't.gmid', 't.cid'))
        ->where('t.lmid', $result->learnermap->lmid)
        ->executeQuery(true);
    $result->targets = $targets ? $targets : [];  
    return $result;
  }

  public function getLearnermap($lmid) {
    $db       = $this->getInstance('kb');
    $qb       = QB::instance($db);
    $result   = new stdClass;
    $learnermaps = $qb->table('learnermaps l')
      ->select(array('l.lmid', 'l.type', 'l.gmid', 'l.uid', 'l.create_time'))
      ->where('l.lmid', QB::esc($lmid))
      ->limit(1)
      ->executeQuery(true);
    if (count($learnermaps)) {
      $result->learnermap = count($learnermaps) ? $learnermaps[0] : null;
    } else return null;
    $qb->clear();
    $concepts = $qb->table('learnermaps_concepts lc')
      ->leftJoin('goalmaps_concepts gc', array('lc.cid' => 'gc.cid', 'lc.gmid' => 'gc.gmid'))
      ->select(array('lc.cid', 'lc.lmid', 'lc.gmid', 'lc.locx', 'lc.locy', 'gc.label'))
      ->where('lc.lmid', $result->learnermap->lmid)
      ->executeQuery(true);
    $result->concepts = $concepts ? $concepts : [];
    $qb->clear();
    $links = $qb->table('learnermaps_links ll')
      ->leftJoin('goalmaps_links gl', array('ll.lid' => 'gl.lid', 'll.gmid' => 'gl.gmid'))
      ->select(array('ll.lid', 'll.lmid', 'll.gmid', 'll.locx', 'll.locy', 'll.source', 'gl.label'))
      ->where('ll.lmid', $result->learnermap->lmid)
      ->executeQuery(true);
    $result->links = $links ? $links : [];

    $qb->clear();
    $targets = $qb->table('learnermaps_links_target t')
        ->select(array('t.lmid', 't.lid', 't.gmid', 't.cid'))
        ->where('t.lmid', $result->learnermap->lmid)
        ->executeQuery(true);
    $result->targets = $targets ? $targets : [];  
    $tLength = count($targets);
    foreach ($links as $l) {
      // $l->target = array();
      for ($j=0; $j<$tLength; $j++) {
        if ($l->lid == $targets[$j]->lid) {
          $l->target[] = $targets[$j]->cid;
          // array_push($l->target, $targets[$i]->cid);
        }
      }
    }
    return $result;
  }

  public function deleteLearnermap($lmid) {
    $db = $this->getInstance('kb');
    $qb = QB::instance($db)
      ->table('learnermaps')
      ->delete()
      ->where('lmid', QB::esc($lmid))
      ->execute();
    return $qb->getAffectedRows();
  }

}
