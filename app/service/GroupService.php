<?php

class GroupService extends CoreService {

  public function selectGroups() {
    $db     = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('grups g')
      ->select(array('g.gid', 'g.fid', 'g.name', 'g.type', 'g.grade', 'g.class', 'g.note', 'g.creator_id', 'g.create_time'))
      ->executeQuery(true);
    return $result;
  }

  public function selectGroupsByIds($ids = []) {
    $db     = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('grups g')
      ->select(array('g.gid', 'g.fid', 'g.name', 'g.type', 'g.grade', 'g.class', 'g.note', 'g.creator_id', 'g.create_time'))
      ->whereIn('g.gid', $ids)
      ->executeQuery(true);
    return $result;
  }

  public function selectGroupsWithCount() {
    $db     = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('grups g')
      ->select(array('g.gid', 'g.fid', 'g.name', 'g.type', 'g.grade', 'g.class', 'g.note', 'g.creator_id', 'g.create_time'))
      ->selectRaw("(SELECT COUNT(*) FROM users_in_grups WHERE gid = g.gid) AS cusers")
      ->selectRaw("(SELECT COUNT(*) FROM grups_has_materials WHERE gid = g.gid) AS cmaterials")
      ->executeQuery(true);
    return $result;
  }

  public function getGroupById($gid) {
    $db     = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('grups g')
      ->select(array('g.gid', 'g.fid', 'g.name', 'g.type', 'g.grade', 'g.class', 'g.note', 'g.creator_id', 'g.create_time'))
      ->where('gid', $gid)
      ->executeQuery(true);
    return $result;
  }

  public function getGroupByFid($fid) {
    $db     = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('grups g')
      ->select(array('g.gid', 'g.fid', 'g.name', 'g.type', 'g.grade', 'g.class', 'g.note', 'g.creator_id', 'g.create_time'))
      ->where('fid', $fid)
      ->executeQuery(true);
    if (count($result)) {
      return $result[0];
    } else {
      return null;
    }
  }

  public function insertGroup($name, $fid, $type, $grade, $class, $note) {
    $db              = $this->getInstance('kb');
    $groups['name']  = QB::esc($name);
    $groups['fid']   = !empty($fid) ? QB::esc($fid) : QB::raw('NULL');
    $groups['type']  = QB::esc($type);
    $groups['grade'] = QB::esc($grade);
    $groups['class'] = QB::esc($class);
    $groups['note']  = QB::esc($note);
    $qb              = QB::instance($db);
    try {
      $qb->table('grups')
        ->insert($groups)
        ->execute(true);
      return $qb->insertId();
    } catch (Exception $ex) {
      throw $ex;
    }
  }

  public function deleteGroup($gid) {
    $db = $this->getInstance('kb');
    $qb = QB::instance($db)
      ->table('grups')
      ->delete()
      ->where('gid', QB::esc($gid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  public function updateGroup($gid, $name, $fid, $type, $grade, $class, $note) {
    $db              = $this->getInstance('kb');
    $update['gid']   = QB::esc($gid);
    $update['name']  = QB::esc($name);
    $update['fid']   = !empty($fid) ? QB::esc($fid) : QB::raw('NULL');
    $update['type']  = QB::esc($type);
    $update['grade'] = QB::esc($grade);
    $update['class'] = QB::esc($class);
    $update['note']  = QB::esc($note);
    $qb              = QB::instance($db);
    try {
      $qb->table('grups')
        ->update($update)
        ->where('gid', QB::esc($gid))
        ->execute(true);
      return $qb->getAffectedRows();
    } catch (Exception $ex) {
      throw $ex;
    }
  }

  // User Group

  public function getUserGroup($gid) {
    $db        = $this->getInstance('kb');
    $gid       = QB::esc($gid);
    $qb        = QB::instance($db);
    $users     = new stdClass;
    $users->in = $qb->table('users u')
      ->selectRaw('u.uid, u.name, u.username')
      ->whereRaw("u.uid IN (SELECT ug.uid FROM users_in_grups ug WHERE ug.gid = '$gid')")
      ->executeQuery(true);
    $qb->clear();
    $users->notin = $qb->table('users u')
      ->selectRaw('u.uid, u.name, u.username')
      ->whereRaw("u.uid NOT IN (SELECT ug.uid FROM users_in_grups ug WHERE ug.gid = '$gid')")
      ->executeQuery(true);
    return $users;
  }

  public function addUserToGroup($uid, $gid) {
    $db            = $this->getInstance('kb');
    $uid           = QB::esc($uid);
    $gid           = QB::esc($gid);
    $qb            = QB::instance($db);
    $insert['uid'] = $uid;
    $insert['gid'] = $gid;
    $qb->table('users_in_grups ug')
      ->insert($insert, true)
      ->execute();
    return $qb->getAffectedRows();
  }

  public function removeUserFromGroup($uid, $gid) {
    $db  = $this->getInstance('kb');
    $uid = QB::esc($uid);
    $gid = QB::esc($gid);
    $qb  = QB::instance($db);
    $res = $qb->table('users_in_grups ug')
      ->delete()
      ->where('uid', $uid)
      ->where('gid', $gid)
      ->execute();
    return $qb->getAffectedRows();
  }

  // Material Group

  public function getMaterialGroup($gid) {
    $db            = $this->getInstance('kb');
    $gid           = QB::esc($gid);
    $qb            = QB::instance($db);
    $materials     = new stdClass;
    $materials->in = $qb->table('materials m')
      ->selectRaw('m.mid, m.name')
      ->whereRaw("m.mid IN (SELECT gm.mid FROM grups_has_materials gm WHERE gm.gid = '$gid')")
      ->executeQuery(true);
    $qb->clear();
    $materials->notin = $qb->table('materials m')
      ->selectRaw('m.mid, m.name')
      ->whereRaw("m.mid NOT IN (SELECT gm.mid FROM grups_has_materials gm WHERE gm.gid = '$gid')")
      ->executeQuery(true);
    return $materials;
  }

  public function addMaterialToGroup($mid, $gid) {
    $db            = $this->getInstance('kb');
    $mid           = QB::esc($mid);
    $gid           = QB::esc($gid);
    $qb            = QB::instance($db);
    $insert['mid'] = $mid;
    $insert['gid'] = $gid;
    $qb->table('grups_has_materials gm')
      ->insert($insert, true)
      ->execute();
    return $qb->getAffectedRows();
  }

  public function removeMaterialFromGroup($mid, $gid) {
    $db  = $this->getInstance('kb');
    $mid = QB::esc($mid);
    $gid = QB::esc($gid);
    $qb  = QB::instance($db);
    $qb->table('grups_has_materials gm')
      ->delete()
      ->where('mid', $mid)
      ->where('gid', $gid)
      ->execute();
    return $qb->getAffectedRows();
  }

}
