<?php

class MapService extends CoreService {
	public function ReadREdgeLimitDB($gmid) {
		try {
			$db = $this->getInstance('kb');
		    $qb = QB::instance($db);
		    $limits = $qb->table('goalmaps_links_target t')
		    	->select(array('lid'))
		    	-> count(QBRaw::raw('*'))
		    	->where('gmid', $gmid)
		    	->groupBy(array('lid'))
		    	->executeQuery(true);
		    // throw new Exception("Error Processing Request", 1);
		    $qb->clear();

		    $limit_array = array();
		    if (count($limits)>0) {
			    foreach ($limits as $limit) {
					$limit_array["l-" . $limit->lid] = $limit->count;
			    }
		    }
		    return $limit_array;
		} catch(Exception $ex) {
		    throw new Exception($ex->getMessage() . ". " . $qb->get());
	    }

	}
}