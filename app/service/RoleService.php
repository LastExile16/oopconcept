<?php

class RoleService extends CoreService {

  public function selectRoles() {
    $db = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('roles')
      ->select(array('rid', 'name'))
      ->executeQuery(true);
    return $result;
  }

  public function selectRole($rid) {
    $db = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('roles')
      ->select(array('rid', 'name'))
      ->where('rid', QB::esc($rid))
      ->limit(1)
      ->executeQuery(true);
    return count($result) ? $result[0] : null;
  }

  public function selectRolesWithCount() {
    $db = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('roles r')
      ->select(array('r.rid', 'r.name'))
      ->selectRaw("(SELECT COUNT(*) FROM users WHERE role_id = r.rid) AS cusers")
      ->executeQuery(true);
    return $result;
  }

  public function insertRole($rid, $name) {
    $db = $this->getInstance('kb');
    $cols['rid'] = QB::esc($rid);
    $cols['name'] = QB::esc($name);
    $qb = QB::instance($db)
      ->table('roles')
      ->insert($cols)
      ->execute(true);
    return $qb->insertId();
  }

  public function deleteRole($rid) {
    $db = $this->getInstance('kb');
    $qb = QB::instance($db)
      ->table('roles')
      ->delete()
      ->where('rid', QB::esc($rid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  public function updateRole($rid, $nrid, $name) {
    $db = $this->getInstance('kb');
    $update['rid'] = $nrid;
    $update['name'] = $name;
    $qb = QB::instance($db)
      ->table('roles')
      ->update($update)
      ->where('rid', QB::esc($rid))
      ->execute(true);
    return $qb->getAffectedRows();
  }
 
}