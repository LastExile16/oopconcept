<?php

class GoalmapService extends CoreService
{
  public function getGoalmaps($mid, $enabled = false) {
    $db       = $this->getInstance('kb');
    $qb       = QB::instance($db);
    $qb = $qb->table('goalmaps g')
      ->select(array('g.gmid', 'name', 'type', 'mid', 'creator_id', 'create_time', 'updater_id', 'update_time'))
      ->selectRaw('(SELECT COUNT(*) FROM goalmaps_concepts gc WHERE gc.gmid = g.gmid) AS concepts_count')
      ->selectRaw('(SELECT COUNT(*) FROM goalmaps_links gl WHERE gl.gmid = g.gmid) AS links_count')
      ->selectRaw('(SELECT COUNT(*) FROM learnermaps l WHERE l.type = \'fix\' AND l.gmid = g.gmid) AS learnermaps_count');
    if($enabled) $qb->where('g.enabled', QB::esc($enabled));
    $goalmaps = $qb->where('g.mid', QB::esc($mid))
      ->orderBy('update_time', QB::ORDER_DESC)
      ->executeQuery(true);
    return $goalmaps;
  }

  public function getUserGoalmaps($mid, $uid, $type = null) {
    $db       = $this->getInstance('kb');
    $qb       = QB::instance($db);
    $qb = $qb->table('goalmaps g')
      ->select(array('g.gmid', 'name', 'type', 'mid', 'creator_id', 'create_time', 'updater_id', 'update_time'))
      ->selectRaw('(SELECT COUNT(*) FROM goalmaps_concepts gc WHERE gc.gmid = g.gmid) AS concepts_count')
      ->selectRaw('(SELECT COUNT(*) FROM goalmaps_links gl WHERE gl.gmid = g.gmid) AS links_count')
      ->selectRaw('(SELECT COUNT(*) FROM learnermaps l WHERE l.type = \'fix\' AND l.gmid = g.gmid) AS learnermaps_count');
    if($type) $qb->where('g.type', QB::esc($type));
    $goalmaps = $qb->where('g.mid', QB::esc($mid))
      ->where('g.creator_id', QB::esc($uid))
      ->orderBy('update_time', QB::ORDER_DESC)
      ->executeQuery(true);
    return $goalmaps;
  }

  public function getGoalmapKit($gmid) {
    $db       = $this->getInstance('kb');
    $qb       = QB::instance($db);
    $result   = new stdClass;
    $goalmap  = $qb->table('goalmaps g')
      ->select(array('g.gmid', 'name', 'type', 'mid', 'creator_id', 'create_time', 'updater_id', 'update_time'))
      ->where('g.gmid', QB::esc($gmid))
      ->limit(1)
      ->executeQuery(true);
    $qb->clear();
    $concepts = $qb->table('goalmaps_concepts c')
      ->select(array('cid', 'label', 'gmid', 'locx', 'locy'))
      ->where('gmid', QB::esc($gmid))
      ->executeQuery(true);
    $qb->clear();
    
    $links = $qb->table('goalmaps_links l')
      ->select(array('lid', 'label', 'gmid', 'locx', 'locy', 'source', 'target'))
      ->where('gmid', QB::esc($gmid))
      ->executeQuery(true);
    $qb->clear();
    
    $targets = $qb->table('goalmaps_links_target t')
      ->select(array('t.lid', 't.gmid', 't.cid'))
      ->where('t.gmid', QB::esc($gmid))
      ->executeQuery(true);
    
    $result->goalmap = count($goalmap) ? $goalmap[0]: null;
    $result->concepts = $concepts;
    $result->links = $links;
    $result->targets = $targets;

    /*$j = 1;
    foreach ($links as $link) {
      $tlength = count($targets);
      // echo "\niteration: " . $j . "\n";
      // var_dump($targets);
      for ($i=0; $i < $tlength; $i++) {
        // echo "targets[{$i}]:: ";
        // print_r($targets[$i]);
        if ($link->lid == $targets[$i]->lid) {
          $link->target[] = $targets[$i]->cid;
          // array_splice($targets, $i, 1);
          unset($targets[$i]); // remove the element
        }
      }
      $targets = array_values($targets); // re-index the array
      // $j++;
      // print_r($targets);
    }*/
    $tLength = count($targets);
    foreach ($links as $l) {
      // $l->target = array();
      for ($j=0; $j<$tLength; $j++) {
        if ($l->lid == $targets[$j]->lid) {
          $l->target[] = $targets[$j]->cid;
          // array_push($l->target, $targets[$j]->cid);
        }
      }
    }
    // print_r($result->targets);
    return $result;
  } 

  public function getGoalmap($gmid, $withExtra = true) {
    $db       = $this->getInstance('kb');
    $qb       = QB::instance($db);
    $result   = new stdClass;
    $goalmap  = $qb->table('goalmaps g')
      ->select(array('g.gmid', 'name', 'type', 'mid', 'creator_id', 'create_time', 'updater_id', 'update_time'))
      ->where('g.gmid', QB::esc($gmid))
      ->limit(1)
      ->executeQuery(true);
    $qb->clear();
    $concepts = $qb->table('goalmaps_concepts c')
      ->leftJoin('goalmaps_concepts_extras e', array('e.cid' => 'c.cid', 'e.gmid' => 'c.gmid'))
      ->select(array('c.cid', 'c.label', 'c.gmid', 'c.locx', 'c.locy', 'e.data', 'e.extra'))
      ->where('c.gmid', QB::esc($gmid))
      ->executeQuery(true);
    $qb->clear();
    $links = $qb->table('goalmaps_links l')
      ->leftJoin('goalmaps_links_extras e', array('e.lid' => 'l.lid', 'e.gmid' => 'l.gmid'))
      ->select(array('l.lid', 'l.label', 'l.gmid', 'l.locx', 'l.locy', 'l.source', 'e.data', 'e.extra'))
      ->where('l.gmid', QB::esc($gmid))
      ->executeQuery(true);
    $qb->clear();
    $targets = $qb->table('goalmaps_links_target t')
      ->select(array('t.lid', 't.gmid', 't.cid'))
      ->where('t.gmid', QB::esc($gmid))
      ->executeQuery(true);
    $qb->clear();
    $result->goalmap = count($goalmap) ? $goalmap[0]: null;
    $result->concepts = $concepts;
    $result->links = $links;
    $result->targets = $targets;

    /*$j = 1;
    foreach ($links as $link) {
      $tlength = count($targets);
      // echo "\niteration: " . $j . "\n";
      // var_dump($targets);
      for ($i=0; $i < $tlength; $i++) {
        // echo "targets[{$i}]:: ";
        // print_r($targets[$i]);
        if ($link->lid == $targets[$i]->lid) {
          $link->target[] = $targets[$i]->cid;
          // array_splice($targets, $i, 1);
          unset($targets[$i]); // remove the element
        }
      }
      $targets = array_values($targets); // re-index the array
      // $j++;
      // print_r($targets);
    }*/
    $tLength = count($targets);
    foreach ($links as $l) {
      // $l->target = array();
      for ($j=0; $j<$tLength; $j++) {
        if ($l->lid == $targets[$j]->lid) {
          $l->target[] = $targets[$j]->cid;
          // array_push($l->target, $targets[$j]->cid);
        }
      }
    }
    // print_r($result->targets);
    return $result;
  } 

  public function insertGoalmap($name, $type, $mid, $creator_id, $updater_id, $concepts, $links) {
    $db                     = $this->getInstance('kb');
    $qb                     = QB::instance($db);
    $goalmaps['name']       = QB::esc($name);
    $goalmaps['type']       = QB::esc($type);
    $goalmaps['mid']        = QB::esc($mid);
    $goalmaps['creator_id'] = QB::esc($creator_id);
    $goalmaps['updater_id'] = QB::esc($updater_id);
    try {
      $qb->begin();
      $gmid = 2; //Q why?
      $gmid = $qb->table('goalmaps')
        ->insert($goalmaps)
        ->execute()
        ->getInsertId();
      $qb->clear();
      $cs = [];
      $ls = [];
      foreach ($concepts as $k => $v) {
        $concepts[$k]['gmid']  = $gmid;
        $concepts[$k]['label'] = QB::esc($concepts[$k]['label']);
        $cs[]                  = (object) $concepts[$k];
      }
      $targets = [];
      foreach ($links as $k => $v) {
        $links[$k]['gmid']  = $gmid;
        $links[$k]['label'] = QB::esc($links[$k]['label']);
        if ($links[$k]['source'] == "") {
          $links[$k]['source'] = null;
        }

        if (isset($links[$k]['target']) && !empty($links[$k]['target'])) {
            foreach ($links[$k]['target'] as $tcid) {
              $target = new stdClass;
              $target->lid = $links[$k]['lid'];
              $target->gmid = $gmid;
              $target->cid = $tcid;
              $targets[] = $target;
            }
          }
          $links[$k]['target'] = null;
          
        $ls[] = (object) $links[$k];
      }
         // var_dump($targets);
         // throw new Exception("Error Processing Request", 1);
      if (count($cs)) {
        $cleanC = array();
        $cleanE = array();
        foreach($cs as $c) {
          $data = $c->extras;
          unset($data['id']);
          unset($data['name']);
          unset($data['type']);
          unset($c->extras);
          array_push($cleanC, $c);
          $extras = null;
          if(isset($data['extras']) && !empty($data['extras'])) {
            $extras = $data['extras'];
            unset($data['extras']);
            unset($extras['id']);
            unset($extras['name']);
            unset($extras['type']);
            if($extras['extras'] == 'null') unset($extras['extras']);
          }
          if(!$extras && !count($data)) continue;
          $jdata = json_encode($data);
          $jextras = json_encode($extras);
          array_push($cleanE, (object) array(
            'cid' => $c->cid,
            'gmid' => $c->gmid,
            'data' => $jdata == '[]' ? QB::raw('NULL') : QB::esc($jdata),
            'extra' => $jextras == "null" ? QB::raw('NULL') : QB::esc($jextras) 
            )
          );
        }
        // var_dump($cleanC);
        // var_dump($cleanE);
        if(count($cleanC)) {
          $sql = $qb->table('goalmaps_concepts')
            ->insertModel($cleanC)
            ->execute();
          $qb->clear();
        }
        if(count($cleanE)) {
          $qb->table('goalmaps_concepts_extras')
            ->insertModel($cleanE)
            ->execute();
          $qb->clear();
        }
      }
      if (count($ls)) {
        $cleanL = array();
        $cleanE = array();
        foreach($ls as $l) {
          $data = $l->extras;
          unset($data['id']);
          unset($data['name']);
          unset($data['type']);
          unset($l->extras);
          array_push($cleanL, $l);
          $extras = null;
          if(isset($data['extras']) && !empty($data['extras'])) {
            $extras = $data['extras'];
            unset($data['extras']);
            unset($extras['id']);
            unset($extras['name']);
            unset($extras['type']);
            if($extras['extras'] == 'null') unset($extras['extras']);
          }
          if(!$extras && !count($data)) continue;
          $jdata = json_encode($data);
          $jextras = json_encode($extras);
          array_push($cleanE, (object) array(
            'lid' => $l->lid,
            'gmid' => $l->gmid,
            'data' => $jdata == '[]' ? QB::raw('NULL') : QB::esc($jdata),
            'extra' => $jextras == "null" ? QB::raw('NULL') : QB::esc($jextras) 
            )
          );
        }
        // var_dump($cleanL);
        // var_dump($cleanE);
      // throw new Exception("a custom error on purpose");
        if(count($cleanL)) {
          $qb->table('goalmaps_links')
            ->insertModel($cleanL)
            ->execute();
          $qb->clear();
        }
        if(count($cleanE)) {
          $qb->table('goalmaps_links_extras')
            ->insertModel($cleanE)
            ->execute();
          $qb->clear();
        }
        if (count($targets)) {
          $qb->table('goalmaps_links_target')
            ->insertModel($targets)
            ->execute();
          $qb->clear();
          
        }
      }


      // if (count($cs)) {
      //   $qb->clear();
      //   $qb->table('goalmaps_concepts')
      //     ->insertModel($cs)
      //     ->execute();
      // }
      // if (count($ls)) {
      //   $qb->clear();
      //   $qb->table('goalmaps_links')
      //     ->insertModel($ls)
      //     ->execute();
      // }
      // if ($type == 'fix') {
      //   $qb->clear();
      //   $qb->table('goalmaps g')
      //     ->delete()
      //     ->where('g.type', 'draft')
      //     ->where('g.mid', $mid)
      //     ->execute();
      // }
      $qb->commit();

      return $gmid;
    } catch (Exception $ex) {
      $qb->rollback();
      throw new Exception($ex->getMessage() . " . " . $qb->get()->getMessage());
    }
  }

  public function overwriteGoalmap($gmid, $updater_id, $concepts, $links, $type = 'draft') {
    $db                     = $this->getInstance('kb');
    $qb                     = QB::instance($db);
    $goalmaps['updater_id'] = QB::esc($updater_id);
    $goalmaps['type']       = QB::esc($type);
    $goalmaps['update_time'] = QB::raw('NOW()');
    try {
      $qb->begin();
      $qb->table('goalmaps')
          ->update($goalmaps)
          ->where('gmid', $gmid)
          ->execute();
      $qb->clear();

      // used to delete links and concepts
      $lids = array();
      $cids = array();

      //used to insert links and concepts
      $cs = [];
      $ls = [];
      $targets = [];

      //begin the actual work ;)
      foreach($links as $link) $lids[] = $link['lid'];
      foreach($concepts as $concept) $cids[] = $concept['cid'];
      $cids_empty = true;
      $lids_empty = true;
      if (count($lids)>0) {
        $lids_empty = false;
        $qb->table('goalmaps_links')
            ->delete()
            ->where('gmid', $gmid)
            ->whereNotIn('lid', $lids)
            ->execute();
        $qb->clear();
        // $tcounter = array();
        foreach ($links as $k => $v) {
          $links[$k]['gmid']  = $gmid;
          $links[$k]['label'] = QB::esc($links[$k]['label']);
          if ($links[$k]['source'] == "") {
            $links[$k]['source'] = null;
          }

          if (isset($links[$k]['target']) && !empty($links[$k]['target'])) {
            // $tcounter[$links[$k]['lid']] = count($links[$k]['target']);
            foreach ($links[$k]['target'] as $tcid) {
              $target = new stdClass;
              $target->lid = $links[$k]['lid'];
              $target->gmid = $gmid;
              $target->cid = $tcid;
              $targets[] = $target;

            }
          }
          $links[$k]['target'] = null;
          $ls[] = (object) $links[$k];
        }
          // echo $tcounter[4];
        /*$gtcounter = $qb->table('goalmaps_links_target gt')
            ->select(array('gt.lid'))
            ->count(QBRaw::raw('*'))
            ->where('gmid', $gmid)
            ->groupBy(array('gt.lid'))
            ->executeQuery();
        print_r($gtcounter);*/
        // $qb->clear();
        
      }else { // in case where all links are removed from the map. So, there are no new links to insert but there are links to delete
        $qb->table('goalmaps_links')
            ->delete()
            ->where('gmid', $gmid)
            ->execute();
        $qb->clear();
      }

      if (count($cids)>0) {
        $cids_empty = false;
        $qb->table('goalmaps_concepts')
            ->delete()
            ->where('gmid', $gmid)
            ->whereNotIn('cid', $cids)
            ->execute();
        $qb->clear();
        foreach ($concepts as $k => $v) {
          $concepts[$k]['gmid']  = $gmid;
          $concepts[$k]['label'] = QB::esc($concepts[$k]['label']);
          $cs[]                  = (object) $concepts[$k];
        }
      }else { // in case where all concepts are removed from the map. So, there are no new concepts to insert but there are concepts to delete
        $qb->table('goalmaps_concepts')
            ->delete()
            ->where('gmid', $gmid)
            ->execute();
        $qb->clear();
      }
      // delete all targets related to the current goal map then re-insert new targets below
      if (!$cids_empty && !$lids_empty) {
        $qb->table('goalmaps_links_target')
            ->delete()
            ->where('gmid', $gmid)
            ->whereIn('cid', $cids)
            ->whereIn('lid', $lids)
            ->execute();
        $qb->clear();
      }

      
      if (count($cs)) {
        $cleanC = array();
        $cleanE = array();
        foreach($cs as $c) {
          $data = $c->extras;
          unset($data['id']);
          unset($data['name']);
          unset($data['type']);
          unset($c->extras);
          array_push($cleanC, $c);
          $extras = null;
          if(isset($data['extras']) && !empty($data['extras'])) {
            $extras = $data['extras'];
            unset($data['extras']);
            unset($extras['id']);
            unset($extras['name']);
            unset($extras['type']);
            if($extras['extras'] == 'null') unset($extras['extras']);
          }
          if(!$extras && !count($data)) continue;
          $jdata = json_encode($data);
          $jextras = json_encode($extras);
          array_push($cleanE, (object) array(
            'cid' => $c->cid,
            'gmid' => $c->gmid,
            'data' => $jdata == '[]' ? QB::raw('NULL') : QB::esc($jdata),
            'extra' => $jextras == "null" ? QB::raw('NULL') : QB::esc($jextras) 
            )
          );
        }
        // var_dump($cleanC);
        // var_dump($cleanE);
        if(count($cleanC)) {
          $qb->table('goalmaps_concepts')
            ->insertUpdateModels($cleanC, null, array('label', 'locx','locy'))
            ->execute();
          $qb->clear();
        }
        if(count($cleanE)) {
          $qb->table('goalmaps_concepts_extras')
            ->insertUpdateModels($cleanE, null, array('extra', 'data'))
            ->execute();
          $qb->clear();
        }
      }
      if (count($ls)) {
        $cleanL = array();
        $cleanE = array();
        foreach($ls as $l) {
          $data = $l->extras;
          unset($data['id']);
          unset($data['name']);
          unset($data['type']);
          unset($l->extras);
          array_push($cleanL, $l);
          $extras = null;
          if(isset($data['extras']) && !empty($data['extras'])) {
            $extras = $data['extras'];
            unset($data['extras']);
            unset($extras['id']);
            unset($extras['name']);
            unset($extras['type']);
            if($extras['extras'] == 'null') unset($extras['extras']);
          }
          if(!$extras && !count($data)) continue;
          $jdata = json_encode($data);
          $jextras = json_encode($extras);
          array_push($cleanE, (object) array(
            'lid' => $l->lid,
            'gmid' => $l->gmid,
            'data' => $jdata == '[]' ? QB::raw('NULL') : QB::esc($jdata),
            'extra' => $jextras == "null" ? QB::raw('NULL') : QB::esc($jextras) 
            )
          );
        }
        // var_dump($cleanL);
        // var_dump($cleanE);
        if(count($cleanL)) {
          $qb->table('goalmaps_links')
            ->insertUpdateModels($cleanL, null, array('label', 'locx', 'locy', 'source'))
            ->execute();
          $qb->clear();
        }
        if(count($cleanE)) {
          $qb->table('goalmaps_links_extras')
            ->insertUpdateModels($cleanE, null, array('extra', 'data'))
            ->execute();
          $qb->clear();
        }
        if (count($targets)) {
          $qb->table('goalmaps_links_target')
            ->insertUpdateModels($targets, null, array('lid', 'cid'))
            ->execute();
          $qb->clear();
          
        }
      }
      $qb->commit();
      return $gmid;
    } catch (Exception $ex) {
      $qb->rollback();
      throw new Exception($ex->getMessage() . ". " . $qb->get());
    }
  }

  // used when the name or tags are changed
  public function updateGoalmap($gmid, $name, $type) {
    $db       = $this->getInstance('kb');
    $qb       = QB::instance($db);
    $result = $qb->table('goalmaps g')
      ->update(array('g.name' => QB::esc($name), 'g.type' => QB::esc($type)))
      ->where('g.gmid', QB::esc($gmid))
      ->execute();
    return $result;
  }

  public function deleteGoalmap($gmid) {
    $db       = $this->getInstance('kb');
    $qb       = QB::instance($db);
    $result = $qb->table('goalmaps g')
      ->delete()
      ->where('g.gmid', QB::esc($gmid))
      ->execute();
    return $result;
  }

  public function getLastDraftGoalmap($mid, $uid) {
    $db       = $this->getInstance('kb');
    $qb       = QB::instance($db);
    $result   = new stdClass;
    $goalmaps = $qb->table('goalmaps g')
      ->select(array('g.gmid', 'name', 'type', 'mid', 'creator_id', 'create_time', 'updater_id', 'update_time'))
      ->where('g.type', 'draft')
      ->where('g.mid', QB::esc($mid))
      ->where('g.creator_id', QB::esc($uid))
      ->orderBy('update_time', QB::ORDER_DESC)
      ->limit(1)
      ->executeQuery(true);
    $gmid = null;
    if ($goalmaps) {
      $goalmap         = $goalmaps[0];
      $gmid            = $goalmap->gmid;
      $result->goalmap = $goalmap;
    } else {
      return null;
    }

    $qb->clear();
    $concepts = $qb->table('goalmaps_concepts')
      ->select(array('cid', 'gmid', 'label', 'locx', 'locy'))
      ->where('gmid', $gmid)
      ->executeQuery(true);
    $result->concepts = $concepts ? $concepts : [];
    $qb->clear();
    $links = $qb->table('goalmaps_links')
      ->select(array('lid', 'gmid', 'label', 'locx', 'locy', 'source', 'target'))
      ->where('gmid', $gmid)
      ->executeQuery(true);
    $result->links = $links ? $links : [];

    $qb->clear();
    $targets = $qb->table('goalmaps_links_target t')
      ->select(array('t.lid', 't.gmid', 't.cid'))
      ->where('t.gmid', QB::esc($gmid))
      ->executeQuery(true);
      $result->targets = $targets ? $targets : [];

    /*$j = 1;
    foreach ($links as $link) {
      $tlength = count($targets);
      // echo "\niteration: " . $j . "\n";
      // var_dump($targets);
      for ($i=0; $i < $tlength; $i++) {
        // echo "targets[{$i}]:: ";
        // print_r($targets[$i]);
        if ($link->lid == $targets[$i]->lid) {
          $link->target[] = $targets[$i]->cid;
          // array_splice($targets, $i, 1);
          unset($targets[$i]); // remove the element
        }
      }
      $targets = array_values($targets); // re-index the array
      // $j++;
      // print_r($targets);
    }*/
    $tLength = count($targets);
      foreach ($links as $l) {
        // $l->target = array();
        for ($j=0; $j<$tLength; $j++) {
          if ($l->lid == $targets[$j]->lid) {
            $l->target[] = $targets[$j]->cid;
            // array_push($l->target, $targets[$j]->cid);
          }
        }
      }
    return $result;
  }

  function setGoalmapAsKit($gmid) {
    $db       = $this->getInstance('kb');
    $qb       = QB::instance($db);
    $result = $qb->table('goalmaps g')
      ->update(array('g.type' => 'kit'))
      ->where('g.gmid', QB::esc($gmid))
      ->execute();
    return $result;
  }

  function moveGoalmapToMaterial($gmid, $omid, $tmid) {
    $db       = $this->getInstance('kb');
    $qb       = QB::instance($db);
    $result = $qb->table('goalmaps g')
      ->update(array('g.mid' => $tmid))
      ->where('g.gmid', QB::esc($gmid))
      ->where('g.mid', QB::esc($omid))
      ->execute();
    return $result;
  }

  function renameGoalmap($gmid, $name) {
    $db       = $this->getInstance('kb');
    $qb       = QB::instance($db);
    $result = $qb->table('goalmaps g')
      ->update(array('g.name' => $name))
      ->where('g.gmid', QB::esc($gmid))
      ->execute();
    return $result;
  }

}
