<?php

class MaterialService extends CoreService {

  public function selectMaterials() {
    $db     = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('materials m')
      ->select(array('m.mid', 'm.fid', 'm.name', 'm.creator_id', 'm.create_time', 'm.enabled'))
      ->executeQuery(true);
    return $result;
  }

  public function selectMaterialsWithQsetExtras() {
    $db     = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('materials m')
      ->select(array('m.mid', 'm.fid', 'm.name', 'm.creator_id', 'm.create_time', 'm.enabled'))
      ->selectRaw('(SELECT COUNT(*) FROM question_sets qs WHERE qs.mid = m.mid) AS cqsets')
      ->executeQuery(true);
    return $result;
  }

  public function getMaterials($enabled = true) {
    $db = $this->getInstance('kb');
    $qb = QB::instance($db)
      ->table('materials m')
      ->select(array('m.mid', 'm.fid', 'm.name', 'm.creator_id', 'm.create_time', 'm.enabled'));
    if ($enabled) {
      $qb->where('m.enabled', 1);
    }

    $result = $qb->executeQuery(true);
    return $result;
  }

  public function getMaterialByMid($mid) {
    $db     = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('materials m')
      ->select(array('m.mid', 'm.fid', 'm.name', 'm.creator_id', 'm.create_time', 'm.enabled'))
      ->where('m.mid', QB::esc($mid))
      ->limit(1)
      ->executeQuery(true);
    return count($result) ? $result[0] : null;
  }

  public function getMaterialByFid($fid, $enabled = 1) {
    $db     = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('materials m')
      ->select(array('m.mid', 'm.fid', 'm.name', 'm.creator_id', 'm.create_time', 'm.enabled'))
      ->where('m.fid', QB::esc($fid));
    if ($enabled) {
      $result->where('m.enabled', 1);
    }

    $result = $result->limit(1)
      ->executeQuery(true);
    return count($result) ? $result[0] : null;
  }

  public function getMaterialByMidOrFid($id, $enabled = 1) {
    $db     = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('materials m')
      ->select(array('m.mid', 'm.fid', 'm.name', 'm.creator_id', 'm.create_time', 'm.enabled'))
      ->whereRaw("(m.fid = '" . QB::esc($id) . "' OR m.mid = '" . QB::esc($id) . "')");
    if ($enabled) {
      $result->where('m.enabled', 1);
    }

    $result = $result->limit(1)
      ->executeQuery(true);
    return count($result) ? $result[0] : null;
  }

  public function getMaterialsWithGids($gids, $enabled = 1) {
    if (!count($gids)) {
      return array();
    }

    $db     = $this->getInstance('kb');
    $result = QB::instance($db)
      ->table('materials m')
      ->leftJoin('grups_has_materials gm', 'gm.mid', 'm.mid')
      ->select(array('m.mid', 'm.fid', 'm.name', 'm.creator_id', 'm.create_time', 'm.enabled'))
      ->distinct();
    if ($enabled) {
      $result = $result->where('m.enabled', '1');
    }

    $result = $result->whereIn('gm.gid', $gids)
      ->executeQuery(true);
    return $result;
  }

  public function insertMaterial($name, $fid) {
    $db                = $this->getInstance('kb');
    $materials['name'] = QB::esc($name);
    $materials['fid']  = !empty($fid) ? QB::esc($fid) : QB::raw('NULL');
    $qb                = QB::instance($db);
    try {
      $qb->begin()
        ->table('materials')
        ->insert($materials)
        ->execute(true);
      $qb->commit();
      return $qb->getInsertId();
    } catch (Exception $ex) {
      $qb->rollback();
      throw $ex;
    }
  }

  public function deleteMaterial($mid) {
    $db = $this->getInstance('kb');
    $qb = QB::instance($db)
      ->table('materials')
      ->delete()
      ->where('mid', QB::esc($mid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  public function disableMaterial($mid) {
    $db = $this->getInstance('kb');
    $qb = QB::instance($db)
      ->table('materials')
      ->update(array('enabled' => 0))
      ->where('mid', QB::esc($mid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  public function enableMaterial($mid) {
    $db = $this->getInstance('kb');
    $qb = QB::instance($db)
      ->table('materials')
      ->update(array('enabled' => 1))
      ->where('mid', QB::esc($mid))
      ->execute();
    return $qb->getAffectedRows();
  }

  public function updateMaterial($mid, $fid, $name) {
    $db     = $this->getInstance('kb');
    $qb     = QB::instance($db);
    $update = array('name' => QB::esc($name), 'fid' => !empty($fid) ? QB::esc($fid) : QB::raw('NULL'));
    try {
      $qb->begin()
        ->table('materials')
        ->update($update)
        ->where('mid', QB::esc($mid))
        ->execute()
        ->commit();
      return $qb->getAffectedRows();
    } catch (Exception $ex) {
      $qb->rollback();
      throw $ex;
    }
  }

  // Group assign to Material

  public function getGroupMaterial($mid) {
    $db         = $this->getInstance('kb');
    $mid        = QB::esc($mid);
    $qb         = QB::instance($db);
    $groups     = new stdClass;
    $groups->in = $qb->table('grups g')
      ->selectRaw('g.gid, g.name, g.type, g.grade, g.class')
      ->whereRaw("g.gid IN (SELECT gm.gid FROM grups_has_materials gm WHERE gm.mid = '$mid')")
      ->executeQuery(true);
    $qb->clear();
    $groups->notin = $qb->table('grups g')
      ->selectRaw('g.gid, g.name, g.type, g.grade, g.class')
      ->whereRaw("g.gid NOT IN (SELECT gm.gid FROM grups_has_materials gm WHERE gm.mid = '$mid')")
      ->executeQuery(true);
    return $groups;
  }

  public function addGroupToMaterial($gid, $mid) {
    $db            = $this->getInstance('kb');
    $mid           = QB::esc($mid);
    $gid           = QB::esc($gid);
    $qb            = QB::instance($db);
    $insert['mid'] = $mid;
    $insert['gid'] = $gid;
    $qb->table('grups_has_materials gm')
      ->insert($insert, true)
      ->execute();
    return $qb->getAffectedRows();
  }

  public function removeGroupFromMaterial($gid, $mid) {
    $db  = $this->getInstance('kb');
    $mid = QB::esc($mid);
    $gid = QB::esc($gid);
    $qb  = QB::instance($db);
    $qb->table('grups_has_materials gm')
      ->delete()
      ->where('mid', $mid)
      ->where('gid', $gid)
      ->execute();
    return $qb->getAffectedRows();
  }

}