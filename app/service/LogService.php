<?php

class LogService extends CoreService
{

  public function log($uid, $seq, $action, $time_client, $data, $phpsessid, $mid, $gmid, $rid = null, $cmapState = null, $kid = null, $feedbackState = null)
  {
    $db = $this->getInstance('kb');
    $insert['uid'] = $uid;
    $insert['seq'] = $seq;
    $insert['action'] = $action;
    $insert['time_client'] = QB::raw("FROM_UNIXTIME($time_client)");
    $insert['data'] = QB::esc(json_encode($data));
    $insert['phpsessid'] = $phpsessid;

    $gmap['mid'] = $mid;
    $gmap['rid'] = $rid;
    $gmapi['mid'] = $mid;

    $kbuild['gmid'] = $gmid;
    $kbuild['rid'] = $rid;
    $kbuildi['gmid'] = $gmid;

    $feedback['gmid'] = $gmid;
    $feedback['kid'] = $kid;
    $feedback['uid'] = $uid;

    $qb = QB::instance($db);
    try {
      $qb->begin()
        ->table('logs_cmapping')
        ->insert($insert)
        ->execute();
      $lid = $qb->insertId();
      if ($gmid !== null && $rid !== null) {
        $kbuild['lid'] = $lid;
        $qb->clear();
        $qb->table('logs_kitbuilding_collab')
          ->insert($kbuild)
          ->execute();
      }
      if ($gmid !== null && $rid == null) {
        $kbuildi['lid'] = $lid;
        $qb->clear();
        $qb->table('logs_kitbuilding')
          ->insert($kbuildi)
          ->execute();
      }
      if ($mid !== null && $rid !== null) {
        $gmap['lid'] = $lid;
        $qb->clear();
        $qb->table('logs_goalmapping_collab')
          ->insert($gmap)
          ->execute();
      }
      if ($mid !== null && $rid == null) {
        $gmapi['lid'] = $lid;
        $qb->clear();
        $qb->table('logs_goalmapping')
          ->insert($gmapi)
          ->execute();
      }
      if ($feedbackState !== null && $kid !== null) {

        $feedback['lid'] = $lid;
        $feedback['lmid'] = $feedbackState->lmid;
        $feedback['cmpr_data'] = QB::esc(json_encode($feedbackState->compare));
        $qb->clear();
        $qb->table('logs_feedback')
          ->insert($feedback)
          ->execute();
      }
      
      if($cmapState) {
        $cms = [];
        $cms['lid'] = $lid;
        $cms['concepts'] = QB::esc(json_encode($cmapState->concepts));
        $cms['links'] = QB::esc(json_encode($cmapState->links));
        $cms['edges'] = QB::esc(json_encode($cmapState->edges));
        $cms['propositions'] = QB::esc(json_encode($cmapState->propositions));
        $cms['partial_propositions'] = QB::esc(json_encode($cmapState->partPropositions));
        $cms['no_propositions'] = QB::esc(json_encode($cmapState->noPropositions));
        $cms['nc'] = $cmapState->nc;
        $cms['nl'] = $cmapState->nl;
        $cms['ne'] = $cmapState->ne;
        $cms['np'] = $cmapState->np;
        $cms['npp'] = $cmapState->npp;
        $cms['nnp'] = $cmapState->nnp;
        $qb->clear();
        $qb->table('logs_cmap_state')
          ->insert($cms)
          ->execute();
      }
      $qb->commit();
      return $lid;
    } catch (Exception $ex) {
      $qb->rollback();
      throw $ex;
    }
  }

  public function logGoalmapping($uid, $seq, $action, $time_client, $data, $phpsessid, $mid, $rid)
  {
  }

  public function logKitbuilding($uid, $seq, $action, $time_client, $data, $phpsessid, $gmid, $rid)
  {
  }

  // public function getUserIds()
  // {
  //   $db = $this->getInstance('workshop');
  //   $userIds = QB::instance($db)
  //     ->distinct()
  //     ->table('log l')
  //     ->select(array('u.uid', 'username'))
  //     ->leftJoin('users u', 'l.uid', 'u.uid')
  //     ->orderBy('u.username')
  //     ->executeQuery(true);
  //   return $userIds;
  // }

  // public function getUserIdsWithGmid($gmid)
  // {
  //   $db = $this->getInstance('workshop');
  //   $userIds = QB::instance($db)
  //     ->distinct()
  //     ->table('log l')
  //     ->select(array('u.uid', 'username'))
  //     ->leftJoin('users u', 'l.uid', 'u.uid')
  //     ->where('gmid', $gmid)
  //     ->orderBy('u.username')
  //     ->executeQuery(true);
  //   return $userIds;
  // }

  // public function getGoalmapIds()
  // {
  //   $db = $this->getInstance('workshop');
  //   $goalmapIds = QB::instance($db)
  //     ->distinct()
  //     ->table('log l')
  //     ->select(array('gm.gmid', 'gm.name'))
  //     ->leftJoin('goalmaps gm', 'l.gmid', 'gm.gmid')
  //     ->orderBy('gm.name')
  //     ->executeQuery(true);
  //   return $goalmapIds;
  // }

  // public function getActivityLogs($gmid, $uid)
  // {
  //   $db = $this->getInstance('workshop');
  //   $logs = QB::instance($db)
  //     ->table('log l')
  //     ->select(array('lid', 'date', 'seq', 'action', 'data'))
  //     ->where('gmid', $gmid)
  //     ->where('uid', $uid)
  //     ->executeQuery(true);
  //   return $logs;
  // }
}
