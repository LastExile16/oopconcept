<?php

class KitbuildApiController extends CoreController {

  public function registerUser() {
    $name     = isset($_POST['name']) ? $_POST['name'] : null;
    $username = isset($_POST['username']) ? $_POST['username'] : null;
    $password = isset($_POST['password']) ? $_POST['password'] : null;
    $rid      = isset($_POST['rid']) ? $_POST['rid'] : null;
    $gid      = isset($_POST['gid']) ? $_POST['gid'] : null;
    try {
      $userService  = new UserService();
      $uid          = $userService->insertUser($username, $password, $name);
      $result       = $userService->assignRoleToUser($uid, $rid);
      $groupService = new GroupService();
      $result       = $groupService->addUserToGroup($uid, $gid);
      CoreResult::instance(true)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function signIn() {
    $username = trim($_POST['username']);
    $password = trim($_POST['password']);
    try {
      $userService = new UserService();
      $user        = $userService->signIn($username, $password);
      if ($user) {
        CoreResult::instance($user)->show();
      } else {
        CoreError::instance('Invalid username and/or password.<br>ユーザー名やパスワードが無効です。')->show();
      }

    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function signInRole() {
    $username = trim($_POST['username']);
    $password = trim($_POST['password']);
    $role_id  = trim($_POST['role_id']);
    try {
      $userService = new UserService();
      $user        = $userService->signInRole($username, $password, $role_id);
      if ($user) {
        $sessionService = new SessionService();
        $session        = $sessionService->getSessionData($user->uid);
        if ($session) {
          $user->session = $session;
          $sessionData   = json_decode($session->data);
          if ($sessionData !== null) {
            foreach ($sessionData as $key => $value) {
              $_SESSION[$key] = $value;
            }
          } else {
            $sessionService->setSessionData($user->uid, $_SESSION);
          }

        } else {
          $sessionService->setSessionData($user->uid, $_SESSION);
        }
        CoreResult::instance($user)->show();
      } else {
        CoreError::instance('Invalid username and/or password.<br>ユーザー名やパスワードが無効です。')->show();
      }
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function signOut($uid) {
    try {
      $uid            = $uid ? $uid : $_POST['uid'];
      $sessionService = new SessionService();
      $sessionService->clearSessionData($uid);
      CoreResult::instance(true)->show();
    } catch (Exception $ex) {
      CoreError::instance(false)->show();
    }
  }

  public function loadUser($uid) {
    try {
      $userService = new UserService();
      $user        = $userService->loadUser($uid);
      CoreResult::instance($user)->show();
    } catch (Exception $ex) {
      CoreError::instance(false)->show();
    }
  }

  public function getMaterials($enabled = true) {
    try {
      $materialService = new MaterialService();
      $materials       = $materialService->getMaterials($enabled);
      CoreResult::instance($materials)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getMaterialsWithGids() {
    $gids = isset($_POST['gids']) ? $_POST['gids'] : [];
    try {
      $materialService = new MaterialService();
      $materials       = $materialService->getMaterialsWithGids($gids);
      CoreResult::instance($materials)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getMaterialByMid($mid) {
    try {
      $materialService = new MaterialService();
      $material        = $materialService->getMaterialByMid($mid); // because content is in table material_collab
      if ($material) {
        CoreResult::instance($material)->show();
      } else {
        CoreError::instance(null)->show();
      }
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getCMMMaterialByMid($mid) {
    try {
      $materialService = new CMMMaterialService();
      $material        = $materialService->getMaterialByMid($mid);
      if ($material) {
        CoreResult::instance($material)->show();
      } else {
        CoreError::instance(null)->show();
      }
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getMaterialGroups($mid) {
    try {
      $materialService = new MaterialService();
      $groups        = $materialService->getGroupMaterial($mid);
      $gs = (isset($groups->in) && count($groups->in)) ? $groups->in : [];
      if ($gs) {
        CoreResult::instance($gs)->show();
      } else {
        CoreError::instance(null)->show();
      }
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function saveGoalmap() {
    // var_dump($_POST);
    // exit;
    try {
      $gmid = isset($_POST['gmid']) ? $_POST['gmid'] : null;
      if (!$gmid) {
        $name           = isset($_POST['name']) ? $_POST['name'] : null;
        $type           = isset($_POST['type']) ? $_POST['type'] : null;
        $mid            = isset($_POST['mid']) ? $_POST['mid'] : null;
        $creator_id     = isset($_POST['creator_id']) ? $_POST['creator_id'] : null;
        $updater_id     = isset($_POST['updater_id']) ? $_POST['updater_id'] : null;
        $concepts       = isset($_POST['concepts']) ? $_POST['concepts'] : [];
        $links          = isset($_POST['links']) ? $_POST['links'] : [];
        $goalmapService = new GoalmapService();
        $result         = $goalmapService->insertGoalmap($name, $type, $mid, $creator_id, $updater_id, $concepts, $links);
        CoreResult::instance($result)->show();
      } else { // overwrite
        $updater_id     = isset($_POST['updater_id']) ? $_POST['updater_id'] : null;
        $concepts       = isset($_POST['concepts']) ? $_POST['concepts'] : [];
        $links          = isset($_POST['links']) ? $_POST['links'] : [];
        $type           = isset($_POST['type']) ? $_POST['type'] : null;
        $goalmapService = new GoalmapService();
        $result         = $goalmapService->overwriteGoalmap($gmid, $updater_id, $concepts, $links, $type);
        CoreResult::instance($result)->show();
      }
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getLastDraftGoalmap($mid, $rid) {
    try {
      $goalmapService = new GoalmapService();
      $result         = $goalmapService->getLastDraftGoalmap($mid, $rid);
      if (count($result)) {
        CoreResult::instance($result[0])->show();
      } else {
        CoreResult::instance(null)->show();
      }
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function loadLastDraftGoalmap($mid, $uid) {
    try {
      $goalmapService = new GoalmapService();
      $result         = $goalmapService->getLastDraftGoalmap($mid, $uid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getGoalmaps($mid, $enabled = null) {
    try {
      $enabled = filter_var($enabled, FILTER_VALIDATE_BOOLEAN);
      $goalmapService = new GoalmapService();
      $result         = $goalmapService->getGoalmaps($mid, $enabled);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getUserGoalmaps($mid, $uid) {
    try {
      $goalmapService = new GoalmapService();
      $result         = $goalmapService->getUserGoalmaps($mid, $uid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function openGoalmap($gmid, $withExtras = true) {
    try {
      $goalmapService = new GoalmapService();
      $result         = $goalmapService->getGoalmap($gmid, $withExtras);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function openKit($gmid) {
    try {
      $goalmapService = new GoalmapService();
      $result         = $goalmapService->getGoalmap($gmid, true);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function saveKitmap() {
    // var_dump($_POST);
    $name     = isset($_POST['name']) ? $_POST['name'] : null;
    $gmid     = isset($_POST['gmid']) ? $_POST['gmid'] : null;
    $author   = isset($_POST['author']) ? $_POST['author'] : null;
    $layout   = isset($_POST['layout']) ? $_POST['layout'] : null;
    $directed = isset($_POST['directed']) ? $_POST['directed'] : null;
    $enabled  = isset($_POST['enabled']) ? $_POST['enabled'] : null;
    $concepts = isset($_POST['concepts']) ? $_POST['concepts'] : [];
    $links    = isset($_POST['links']) ? $_POST['links'] : [];
    $kid      = isset($_POST['kid']) ? $_POST['kid'] : null;
    try {
      $kitmapService = new KitmapService();
      $kid           = $kitmapService->insertKitmapOverwrite($name, $gmid, $author, $layout, $directed, $enabled, $concepts, $links, $kid);
      CoreResult::instance($kid)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getKitmaps($gmid, $enabled = true) {
    try {
      $enabled = filter_var($enabled, FILTER_VALIDATE_BOOLEAN);
      $kitmapService = new KitmapService();
      $result            = $kitmapService->getKitmaps($gmid, $enabled);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getKitmap($kid) {
    try {
      $kitmapService = new KitmapService();
      $result            = $kitmapService->getKitmap($kid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getMaterialKits($mid, $enabled = false) {
    $enabled = filter_var($enabled, FILTER_VALIDATE_BOOLEAN);
    try {
      $kitmapService = new KitmapService();
      $result            = $kitmapService->getKitmapsByMid($mid, $enabled);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getGoalmapKits($gmid, $enabled = true) {
    try {
      $enabled = filter_var($enabled, FILTER_VALIDATE_BOOLEAN);
      $kitmapService = new KitmapService();
      $result            = $kitmapService->getKitmaps($gmid, $enabled);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function saveLearnermap() {
    $gmid     = isset($_POST['gmid']) ? $_POST['gmid'] : null;
    $type     = isset($_POST['type']) ? $_POST['type'] : null;
    $uid      = isset($_POST['uid']) ? $_POST['uid'] : null;
    $concepts = isset($_POST['concepts']) ? $_POST['concepts'] : [];
    $links    = isset($_POST['links']) ? $_POST['links'] : [];
    try {
      $learnermapService = new LearnermapService();
      $result            = $learnermapService->insertLearnermap($gmid, $uid, $concepts, $links, $type);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function saveLearnermapOverwrite() {
    // var_dump($_POST);
    // exit;
    $gmid     = isset($_POST['gmid']) ? $_POST['gmid'] : null;
    $kid      = isset($_POST['kid']) ? $_POST['kid'] : null;
    $type     = isset($_POST['type']) ? $_POST['type'] : null;
    $uid      = isset($_POST['uid']) ? $_POST['uid'] : null;
    $concepts = isset($_POST['concepts']) ? $_POST['concepts'] : [];
    $links    = isset($_POST['links']) ? $_POST['links'] : [];
    $lmid     = isset($_POST['lmid']) && !empty($_POST['lmid']) ? $_POST['lmid'] : null;
    // var_dump($lmid);
    // exit;
    try {
      $learnermapService = new LearnermapService();
      $lmid              = $learnermapService->insertLearnermapOverwrite($gmid, $kid, $uid, $concepts, $links, $type, $lmid);
      $learnermap        = $learnermapService->getLearnermap($lmid);
      CoreResult::instance($learnermap)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function loadLastDraftLearnermap($gmid, $uid) {
    try {
      $learnermapService = new LearnermapService();
      $result            = $learnermapService->getLastDraftLearnermap($gmid, $uid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getLearnermaps($gmid) {
    try {
      $learnermapService = new LearnermapService();
      $result            = $learnermapService->getLearnermaps($gmid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getLearnermapsWithGroups($gmid) {
    try {
      $learnermapService = new LearnermapService();
      $result            = $learnermapService->getLearnermapsWithGroups($gmid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getLearnermap($lmid) {
    try {
      $learnermapService = new LearnermapService();
      $result            = $learnermapService->getLearnermap($lmid);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

  public function getLastLearnermap($gmid, $kid, $uid, $type = 'draft') {
    try {
      $learnermapService = new LearnermapService();
      $result            = $learnermapService->getLastLearnermap($gmid, $kid, $uid, $type);
      CoreResult::instance($result)->show();
    } catch (Exception $e) {
      CoreError::instance($e->getMessage())->show();
    }
  }

}
