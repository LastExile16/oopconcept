<?php

class LogApiController extends CoreController {

  function log() {

    if(!isset($_POST['uid'])) {
      CoreError::instance('invalid log')->show();
      return;
    }

    $uid = $_POST['uid'];
    $seq = $_POST['seq'];
    $action = $_POST['action'];
    $time_client = $_POST['time_client'];
    $data = $_POST['data'];
    $phpsessid = $_POST['phpsessid'];

    $mid = isset($_POST['mid']) ? $_POST['mid'] : null;
    $gmid = isset($_POST['gmid']) ? $_POST['gmid'] : null;
    $rid = isset($_POST['rid']) ? (!empty($_POST['rid']) ? $_POST['rid'] : null) : null;
    $kid = isset($_POST['kid']) ? (!empty($_POST['kid']) ? $_POST['kid'] : null) : null;

    $data = json_decode($data);

    $cmapState = isset($data->cmapState) ? $data->cmapState : null;
    if($cmapState) unset($data->cmapState);
    
    $feedbackState = null;
    if(isset($data->feedbackState)) {
      $feedbackState = $data->feedbackState;
      unset($data->feedbackState);
      if(isset($data->compare)) {
        $feedbackState->compare = $data->compare;
        unset($data->compare);
      }
    }
    // var_dump($kid, $feedbackState);
    try {
      $logService = new LogService();
      $lid = $logService->log($uid, $seq, $action, $time_client, $data, $phpsessid, $mid, $gmid, $rid, $cmapState, $kid, $feedbackState);
      $_SESSION['seq'] = $seq;
      CoreResult::instance($lid)->show();
    } catch(Exception $ex) {
      CoreError::instance($ex->getMessage())->show();
    }

  }

}