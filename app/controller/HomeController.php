<?php

class HomeController extends CoreController {

  function __construct()
  {
    parent::__construct('home');
  }

  function index() { // exit;

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    // $this->ui->addPlugin('colorjoe');
    $this->ui->addPlugin('huebee');
    $this->ui->addPlugin('cytoscape');

    $this->ui->addScript('js/general/language.js'); // Language Lib

    $this->ui->addScript('js/kbui/lang/en.js', '?t=' . time());
    $this->ui->addScript('js/sho/lang/en.js', '?t=' . time());
    if(isset($_SESSION['lang']) && $_SESSION['lang'] == 'jp') {
      $this->ui->addScript('js/kbui/lang/jp.js', '?t=' . time());
      $this->ui->addScript('js/sho/lang/jp.js', '?t=' . time());
    }

    $this->ui->addPlugin('kbui', '?t=' . time());

    $this->ui->addStyle('css/general/three-dots.css', '?t=' . time());
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/analyzer/analyzer.lib.js', '?t=' . time());

    $this->ui->addStyle('css/cmm/support.css', '?t=' . time());
    $this->ui->addScript('js/cmm/nlp.js', '?t=' . time());
    $this->ui->addScript('js/cmm/support.js', '?t=' . time());

    $this->ui->addScript('js/sho/cmap.js', '?t=' . time());
    $this->ui->addStyle('css/general/base.css', '?t=' . time());
    $this->ui->addStyle('css/sho/cmap.css', '?t=' . time());
    $this->ui->view('sho/cmap.php');
  }

  function admin() {
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('tinymce');
    $this->ui->addScript('vendors/md5.min.js');
    $this->ui->addScript('vendors/socket.io.slim.js', '?t='.time());
    $this->ui->addScript('js/general/language.js'); // Language Lib

    $this->ui->addScript('js/kbui/lang/en.js', '?t=' . time());
    $this->ui->addScript('js/sho/lang/en.js', '?t=' . time());
    if(isset($_SESSION['lang']) && $_SESSION['lang'] == 'jp') {
      $this->ui->addScript('js/kbui/lang/jp.js', '?t=' . time());
      $this->ui->addScript('js/sho/lang/jp.js', '?t=' . time());
    }

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/admin/admin.js', '?t='.time());
    $this->ui->addStyle('css/admin.css', '?t='.time());
    $this->ui->view('sho/admin.php');
  }

  public function analyzer() {
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate'); // required for bs-notify
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('rangeslider');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('huebee');
    $this->ui->addPlugin('jqui');

    $this->ui->addScript('js/general/language.js'); // Language Lib

    $this->ui->addScript('js/kbui/lang/en.js', '?t=' . time());
    $this->ui->addScript('js/sho/lang/en.js', '?t=' . time());
    if(isset($_SESSION['lang']) && $_SESSION['lang'] == 'jp') {
      $this->ui->addScript('js/kbui/lang/jp.js', '?t=' . time());
      $this->ui->addScript('js/sho/lang/jp.js', '?t=' . time());
    }

    $this->ui->addPlugin('kbui', '?t=' . time());
    
    $this->ui->addStyle('css/general/base.css', '?t=' . time());
    $this->ui->addStyle('css/sho/analyzer.css');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/general/session.js');

    $this->ui->addScript('js/analyzer/analyzer.lib.js');
    $this->ui->addScript('js/analyzer/analyzer.js');
    $this->ui->view('sho/analyzer.php');
  }

}